import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa, missing_value_policies
from eureqa.variable_options import VariableOptions
from eureqa.variable_options_dict import VariableOptionsDict
from eureqa.passwords import _CredentialGetter

import unittest
import mock
import getpass

from etestutils import capture, EureqaTestBase

class TestEureqa(unittest.TestCase, EureqaTestBase):
    @classmethod
    def setUpClass(cls):
        # Clean up after previous test runs
        for a in cls._eureqa.get_analyses():
            if a.name == 'Test session key 1':
                a.delete()

    # clears the saved password file on setup and breakdown, since the tests of interactive login rely on it not existing
    @classmethod
    def setUp(cls):
        _CredentialGetter._clear_passwords()

    @classmethod
    def breakDown(cls):
        _CredentialGetter._clear_passwords()

    def test_session_key_login(self):
        self._eureqa.create_analysis('Test session key 1', 'Test description 1')
        self.assertEqual(len([x for x in self._eureqa.get_analyses() if x.name == 'Test session key 1']), 1)

        # now make a eureqa object from an existing session key
        eureqa_from_session_key = Eureqa(url=self._eureqa_url, user_name='root', organization='root', session_key=self._eureqa._get_session_key())

        self.assertEqual(len([x for x in eureqa_from_session_key.get_analyses() if x.name == 'Test session key 1']), 1)

    def test_login_non_interactive_correct_initial_password(self):
        Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1', organization='root', interactive_mode=False)

    def test_login_interactive_correct_initial_password(self):
        Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1', organization='root', interactive_mode=True)

    def test_login_verbose(self):
        Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1', organization='root', verbose=True)

    def test_login_non_interactive_wrong_password(self):
        #enter the wrong password and the login should fail because in non interactive mode we can't prompt the user for the correct password
        with self.assertRaises(Exception):
            Eureqa(url=self._eureqa_url, user_name='root', password='wrong_password', organization='root', interactive_mode=False)

    def test_login_interactive_wrong_password(self):
        with self.assertRaises(Exception):
            Eureqa(url=self._eureqa_url, user_name='root', password='WRONG', organization='root', interactive_mode=False)

        #enter the wrong password followed by the correct one and the login should succeed
        with mock.patch('__builtin__.raw_input', side_effect=['root']):
            with mock.patch('getpass.getpass', return_value='Changeme1'):
                Eureqa(url=self._eureqa_url, user_name='root', password='wrong_password', organization='root', interactive_mode=True)

    def test_login_interactive_no_default_credentials(self):
        with mock.patch('__builtin__.raw_input', side_effect=['root']):
            with mock.patch('getpass.getpass', return_value='Changeme1'):
                Eureqa(url=self._eureqa_url, interactive_mode=True)

    def test_login_non_interactive_save_credentials(self):
        # clear and recreate a password file
        _CredentialGetter._clear_passwords()

        with mock.patch('__builtin__.raw_input', side_effect=['root']):
            with mock.patch('getpass.getpass', return_value='Changeme1'):
                Eureqa(url=self._eureqa_url, interactive_mode=False, save_credentials=True)

        # subsequent login will succeed
        Eureqa(url=self._eureqa_url, interactive_mode=False, save_credentials=True)

    def test_login_interactive_save_credentials(self):
        # clear and recreate a password file
        _CredentialGetter._clear_passwords()

        with mock.patch('__builtin__.raw_input', side_effect=['root']):
            with mock.patch('getpass.getpass', return_value='Changeme1'):
                Eureqa(url=self._eureqa_url, interactive_mode=True, save_credentials=True)

        # subsequent login will succeed
        Eureqa(url=self._eureqa_url, interactive_mode=True, save_credentials=True)

    def test_login_non_interactive_two_factor(self):
        eureqa = Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1', organization='root')
        # have to create a new organization that requires two factor auth
        response = eureqa._session.execute('/api/v2/organizations', 'POST', args={'name': 'org1'})
        response['require_two_factor_auth'] = True
        eureqa._session.execute('/api/v2/organizations/' + response['id'], 'POST', args=response)
        # make a signup key and a new user
        key = eureqa._session.execute('/api/v2/' + response['id'] + '/auth/signup_key', 'POST', args={'organization':response['id'],'total_uses':1,'roles':['admin']})
        # because of the way the two factor login works the first post will fail then you post again with the one time password
        with self.assertRaises(Exception):
            eureqa._session.execute('/api/v2/auth/signup', 'POST', args={'firstname':'pink','lastname':'mike','username':'pinkmike1','useremail':'pinkmike1@nutonian.com','phone_number':'1234','password':'Changeme1','signup_key':key['signup_key']})

        eureqa._session.execute('/api/v2/auth/signup', 'POST', args={'firstname':'pink','lastname':'mike','username':'pinkmike1','useremail':'pinkmike1@nutonian.com','phone_number':'1234','password':'Changeme1','signup_key':key['signup_key'], 'one_time_password':'one-time_password'})
        self._eureqa._grant_user_api_access('pinkmike1', 'org1')

        #login should fail because you can't enter the one-time password in non interactive mode
        with self.assertRaises(Exception):
            Eureqa(url=self._eureqa_url, user_name='pinkmike1', password='Changeme1', organization=response['id'])

    def test_login_interactive_two_factor(self):
        eureqa = Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1', organization='root')
        # have to create a new organization that requires two factor auth
        response = eureqa._session.execute('/api/v2/organizations', 'POST', args={'name': 'org2'})
        response['require_two_factor_auth'] = True
        eureqa._session.execute('/api/v2/organizations/' + response['id'], 'POST', args=response)
        # make a signup key and a new user
        key = eureqa._session.execute('/api/v2/' + response['id'] + '/auth/signup_key', 'POST', args={'organization':response['id'],'total_uses':1,'roles':['admin']})
        # because of the way the two factor login works the first post will fail then you post again with the one time password
        with self.assertRaises(Exception):
            eureqa._session.execute('/api/v2/auth/signup', 'POST', args={'firstname':'pink','lastname':'mike','username':'pinkmike2','useremail':'pinkmike2@nutonian.com','phone_number':'1234','password':'Changeme1','signup_key':key['signup_key']})
        eureqa._session.execute('/api/v2/auth/signup', 'POST', args={'firstname':'pink','lastname':'mike','username':'pinkmike2','useremail':'pinkmike2@nutonian.com','phone_number':'1234','password':'Changeme1','signup_key':key['signup_key'], 'one_time_password':'one-time_password'})
        self._eureqa._grant_user_api_access('pinkmike2', 'org2')

        #enter the one-time password and the login should succeed
        with mock.patch('__builtin__.raw_input', side_effect=['one-time_password']):
            Eureqa(url=self._eureqa_url, user_name='pinkmike2', password='Changeme1', organization=response['id'], interactive_mode=True)

    def test_login_interactive_two_factor_wrong_password(self):
        eureqa = Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1', organization='root')
        # have to create a new organization that requires two factor auth
        response = eureqa._session.execute('/api/v2/organizations', 'POST', args={'name': 'org3'})
        response['require_two_factor_auth'] = True
        eureqa._session.execute('/api/v2/organizations/' + response['id'], 'POST', args=response)
        # make a signup key and a new user
        key = eureqa._session.execute('/api/v2/' + response['id'] + '/auth/signup_key', 'POST', args={'organization':response['id'],'total_uses':1,'roles':['admin']})
        # because of the way the two factor login works the first post will fail then you post again with the one time password
        with self.assertRaises(Exception):
            eureqa._session.execute('/api/v2/auth/signup', 'POST', args={'firstname':'pink','lastname':'mike','username':'pinkmike3','useremail':'pinkmike3@nutonian.com','phone_number':'1234','password':'Changeme1','signup_key':key['signup_key']})
        eureqa._session.execute('/api/v2/auth/signup', 'POST', args={'firstname':'pink','lastname':'mike','username':'pinkmike3','useremail':'pinkmike3@nutonian.com','phone_number':'1234','password':'Changeme1','signup_key':key['signup_key'], 'one_time_password':'one-time_password'})
        self._eureqa._grant_user_api_access('pinkmike3', 'org3')

        #enter the wrong one-time password followed by the correct one and the login should succeed
        with mock.patch('__builtin__.raw_input', side_effect=['wrong_password', 'one-time_password']):
            Eureqa(url=self._eureqa_url, user_name='pinkmike3', password='Changeme1', organization=response['id'], interactive_mode=True)

    def test_evaluate_expression(self):
        ## We already test most of this functionality elsewhere.
        ## The one thing we don't test is the 'variable_options' argument.
        list_vals = self._eureqa.evaluate_expression(self._data_source, "10",
                                                variable_options=[
                                                VariableOptions(self._variables[0],
                                                                missing_value_policy=missing_value_policies.ignore_row)
                                             ])
        v = VariableOptionsDict()
        v.add(VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.ignore_row))
        dict_vals = self._eureqa.evaluate_expression(self._data_source, "10", variable_options=v)
        self.assertEqual(list_vals, dict_vals)

    def test_expression_values(self):
        x_val, y_val = self._eureqa._expression_values(self._data_source, 'x', 'inc_x')
        self.assertEqual(x_val, [1,2,3,4,5])
        self.assertEqual(y_val, [2,3,4,5,6])

        ts_datasource = self._eureqa.create_data_source('SimpleTimeseriesDataSource', 'simple_time_series_data.csv')
        x_val, y_val = self._eureqa._expression_values(ts_datasource, 'ts', 'y')
        self.assertEqual(x_val, ['2016-01-01 00:00:00', '2016-01-02 00:00:00', '2016-01-03 00:00:00', '2016-01-04 00:00:00'])
        self.assertEqual(y_val, [1,2,3,4])

    def test_splits_no_search(self):
        with self.assertRaises(Exception):
            self._eureqa.evaluate_expression(self._data_source, "10", _data_split="training")

        with self.assertRaises(Exception):
            self._eureqa.evaluate_expression(self._data_source, "10", _data_split="validation")

        # Should not throw
        self._eureqa.evaluate_expression(self._data_source, "10", self._search, _data_split="training")
        self._eureqa.evaluate_expression(self._data_source, "10", self._search, _data_split="validation")

        with self.assertRaises(Exception):
            self._eureqa.compute_error_metrics(self._data_source, self._variables[0], "10", _data_split="training")

        with self.assertRaises(Exception):
            self._eureqa.compute_error_metrics(self._data_source, self._variables[0], "10", _data_split="validation")

        # Should not throw
        self._eureqa.compute_error_metrics(self._data_source, self._variables[0], "10", self._search, _data_split="validation")
        self._eureqa.compute_error_metrics(self._data_source, self._variables[0], "10", self._search, _data_split="validation")

    def test_error_api_version_missmatch(self):
        ACTUAL_API_VERSION = Eureqa.API_VERSION
        Eureqa.API_VERSION = 0
        try:
            with self.assertRaises(Exception):
                Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1', organization='root')
        finally:
            Eureqa.API_VERSION = ACTUAL_API_VERSION

    def test_error_server_version_missmatch(self):
        ACTUAL_SERVER_VERSION = Eureqa.SERVER_VERSION
        Eureqa.SERVER_VERSION = '0'
        try:
            with capture() as out:
                Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1', organization='root')
            self.assertTrue('Warning using version' in out[0])
        finally:
            Eureqa.SERVER_VERSION = ACTUAL_SERVER_VERSION

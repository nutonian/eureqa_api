import contextlib
import os
import sys
import unittest
import warnings

from cStringIO import StringIO

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa.utils import utils

# http://stackoverflow.com/questions/5136611/capture-stdout-from-a-script-in-python
# Capture stdout from a function call within a Python application
# Used as a context manager, ie, "with capture() as ...: function_to_capture()"
# Very not-thread-safe.
@contextlib.contextmanager
def capture():
    oldout,olderr = sys.stdout, sys.stderr
    try:
        out = [StringIO(), StringIO()]
        sys.stdout,sys.stderr = out
        yield out
    finally:
        sys.stdout,sys.stderr = oldout, olderr
    out[0] = out[0].getvalue()
    out[1] = out[1].getvalue()


class TestDeprecation(unittest.TestCase):
    def test_deprecation_warning_from_eureqa(self):
        with capture() as out:
            utils._deprecated("DeprecationWarning Test")

        stdout, stderr = out
        stdout = str(stdout)
        stderr = str(stderr)
        self.assertEqual(stdout, "")
        # Can't do an exact match because the path in the error message will change
        self.assertIn("""\
DeprecationWarning: DeprecationWarning Test
  warnings.warn(msg, DeprecationWarning)
""", stderr)

    def test_deprecation_warning_from_not_eureqa(self):
        with capture() as out:
            warnings.warn("Test Deprecation", DeprecationWarning)

        stdout, stderr = out
        stdout = str(stdout)
        stderr = str(stderr)
        self.assertEqual(stdout, "")
        self.assertEqual(stderr, "")

import sys
import os
sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa.analysis_templates import Parameters, TextParameter, DataSourceParameter, VariableParameter, TopLevelModelParameter, DataFileParameter, ComboBoxParameter, NumericParameter

import json
import unittest


class TestAnalysisTemplateParameters(unittest.TestCase):

    def test_from_json(self):
        body = json.loads('{"parameters": '
                                '[{"id": "abc", "label": "This is the text input label.", "type":"text"}, '
                                '{"id": "def", "label": "This is the editable model input label.", "type": "top_level_model"}, '
                                '{"id": "ghi", "label": "This is the not editable model input label.", "type": "top_level_model", "custom_disabled": true}, '
                                '{"id": "123", "label": "label 123", "type":"datasource", "parameters": '
                                    '[{"id": "345", "label": "label 345", "type":"variable"}]},'
                                '{"id": "jkl", "description": "This is the data file description.", "filetypes": ["csv"], "label": "This is the data file input label.", "type":"data_file"}, '
                                '{"id": "mno", "label": "This is the combo box input label.", "type":"combo_box", "items": '
                                    '["a","b","c"]}, '
                                '{"id": "pqr", "label": "This is the numeric input label.", "max": 10, "min": 0, "require_int": true, "type":"numeric"}]'
                          '}')
        parameters = Parameters._from_json(body)

        self.assertEqual(len(parameters.parameters), 7)
        text_input = parameters.parameters.values()[0]
        self.assertEqual(text_input.id, 'abc')
        self.assertEqual(text_input.label, 'This is the text input label.')
        self.assertEqual(text_input._type, 'text')
        editable_model_input = parameters.parameters.values()[1]
        self.assertEqual(editable_model_input.id, 'def')
        self.assertEqual(editable_model_input.label, 'This is the editable model input label.')
        self.assertEqual(editable_model_input._type, 'top_level_model')
        self.assertEqual(editable_model_input.custom_disabled, None)
        not_editable_model_input = parameters.parameters.values()[2]
        self.assertEqual(not_editable_model_input.id, 'ghi')
        self.assertEqual(not_editable_model_input.label, 'This is the not editable model input label.')
        self.assertEqual(not_editable_model_input._type, 'top_level_model')
        self.assertEqual(not_editable_model_input.custom_disabled, True)
        data_source = parameters.parameters.values()[3]
        self.assertEqual(data_source.id, '123')
        self.assertEqual(data_source.label, 'label 123')
        self.assertEqual(data_source._type, 'datasource')
        self.assertEqual(len(data_source.variables), 1)
        variable = data_source.variables[0]
        self.assertEqual(variable.id, '345')
        self.assertEqual(variable.label, 'label 345')
        self.assertEqual(variable._type, 'variable')
        data_file = parameters.parameters.values()[4]
        self.assertEqual(data_file.id, 'jkl')
        self.assertEqual(data_file.label, 'This is the data file input label.')
        self.assertEqual(data_file.description, 'This is the data file description.')
        self.assertEqual(data_file.filetypes, ['csv'])
        self.assertEqual(data_file._type, 'data_file')
        combo_box = parameters.parameters.values()[5]
        self.assertEqual(combo_box.id, 'mno')
        self.assertEqual(combo_box.label, 'This is the combo box input label.')
        self.assertEqual(combo_box.items, ['a', 'b', 'c'])
        self.assertEqual(combo_box._type, 'combo_box')
        numeric_input = parameters.parameters.values()[6]
        self.assertEqual(numeric_input.id, 'pqr')
        self.assertEqual(numeric_input.label, 'This is the numeric input label.')
        self.assertEqual(numeric_input.min, 0)
        self.assertEqual(numeric_input.max, 10)
        self.assertEqual(numeric_input.require_int, True)
        self.assertEqual(numeric_input._type, 'numeric')

    def test_from_json_text_multiline(self):
        body = json.loads('{"parameters": '
                                '[{"id": "abc", "label": "This is the multiline text input label.", "type":"text_multiline"}]'
                          '}')
        parameters = Parameters._from_json(body)

        self.assertEqual(len(parameters.parameters), 1)
        text_input = parameters.parameters.values()[0]
        self.assertEqual(text_input.id, 'abc')
        self.assertEqual(text_input.label, 'This is the multiline text input label.')
        self.assertEqual(text_input._type, 'text_multiline')

    def test_from_json_bad_type(self):
        body = json.loads('{"parameters": '
                                '[{"id": "abc", "label": "This is the text input label.", "type":"BAD_TYPE"}]'
                          '}')
        try:
            parameters = Parameters._from_json(body)
            self.fail("Parameter parsing should have thrown an exception.")
        except Exception as e:
            self.assertEqual(str(e), "Invalid analysis template parameter type 'BAD_TYPE' for parameter 'abc' with label 'This is the text input label.'");

    def test_to_json(self):
        parameters = Parameters(
            [TextParameter('abc','This is the text input label.'),
             TopLevelModelParameter('def','This is the editable model input label.', None),
             TopLevelModelParameter('ghi','This is the not editable model input label.', True),
             DataSourceParameter('123', 'label 123',
                                 {VariableParameter('345', 'label 345')}),
             DataFileParameter('jkl', 'This is the data file input label.', 'This is the data file description.', ['csv']),
             ComboBoxParameter('mno', 'This is the combo box input label.', ['a','b','c']),
             NumericParameter('pqr','This is the numeric input label.', min=0, max=10, require_int=True)]
        )
        body = parameters._to_json()
        text = json.dumps(body, sort_keys=True)
        self.assertEqual(text, '{"parameters": '
                                    '[{"id": "abc", "label": "This is the text input label.", "type": "text"}, '
                                    '{"id": "def", "label": "This is the editable model input label.", "type": "top_level_model"}, '
                                    '{"custom_disabled": true, "id": "ghi", "label": "This is the not editable model input label.", "type": "top_level_model"}, '
                                    '{"id": "123", "label": "label 123", "parameters": '
                                        '[{"id": "345", "label": "label 345", "type": "variable"}], '
                                    '"type": "datasource"}, '
                                    '{"description": "This is the data file description.", "filetypes": ["csv"], "id": "jkl", "label": "This is the data file input label.", "type": "data_file"}, '
                                    '{"id": "mno", "items": '
                                        '["a", "b", "c"], '
                                    '"label": "This is the combo box input label.", "type": "combo_box"}, '
                                    '{"id": "pqr", "label": "This is the numeric input label.", "max": 10, "min": 0, "require_int": true, "type": "numeric"}]'
                                '}')

    def test_to_json_text_multiline(self):
        parameters = Parameters([TextParameter('abc','This is the multiline text input label.', text_multiline=True)])
        body = parameters._to_json()
        text = json.dumps(body, sort_keys=True)
        self.assertEqual(text, '{"parameters": '
                                    '[{"id": "abc", "label": "This is the multiline text input label.", "type": "text_multiline"}]'
                                '}')

    def test_to_string(self):
        parameters = Parameters(
            [TextParameter('abc','This is the text input label.'),
             TopLevelModelParameter('def','This is the editable model input label.', None),
             TopLevelModelParameter('ghi','This is the not editable model input label.', True),
             DataSourceParameter('123', 'label 123',
                                 {VariableParameter('345', 'label 345')}),
             DataFileParameter('jkl', 'This is the data file label.', 'This is the data file description.', ['csv']),
             ComboBoxParameter('mno', 'This is the combo box label.', ['a','b','c']),
             NumericParameter('pqr','This is the numeric input label.', min=0, max=10, require_int=True)]
        )
        parameters.__str__()

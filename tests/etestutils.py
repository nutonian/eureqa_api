import os
import contextlib
import unittest
import sys

from StringIO import StringIO

import eureqa

from etestutils_nosearch import EureqaTestBaseNoSearch

""" 'extended_test' test decorator

Use as:
@extendedTest
def test_something(self):
    # ...
    pass
 """
extendedTest = unittest.skipIf('EXTENDED_TEST' not in os.environ,
                               "Test skipped due to having a long execution-time.  Set the 'EXTENDED_TEST' environment variable to enable.")

def _load_data_source(_eureqa, name, fxp_file):
    data_sources = _eureqa.get_all_data_sources()
    for ds in data_sources:
        if name == ds.name:
            return ds
    return _eureqa.create_data_source(name, fxp_file)

def _create_search(data_source, settings):
    print "Creating example search..."
    search = data_source.create_search(settings)
    search.submit(60)
    while len(search.get_solutions()) < 2 and search.is_running:
        pass
    search.stop()
    if len(search.get_solutions()) < 2:
        raise RuntimeError("The sample search was not able to find at least two solutions.")
    print "Done (", search._data_source_id, ",", search._id, ")"
    return search

class EureqaTestBase(EureqaTestBaseNoSearch):
    """
    EureqaTestBase

    Contains a bunch of useful global fields and methods
    for use by tests that need to interact with eureqa.

    Expensive objects are constructed exactly once, so that
    each test doesn't have to make a new one.
    """

    """ Persistent connection to the eureqa server """
    _eureqa = eureqa.Eureqa(url=EureqaTestBaseNoSearch._eureqa_url, user_name='root', password='Changeme1', organization='root')

    """
    Simple data source.  5 rows with the following columns:
       x             -- values {1..5}
       inc_x         -- x + 1
       dbl_x         -- x * 2
       x_plus_three  -- x + 3
    """
    _data_source = EureqaTestBaseNoSearch._data_source

    """ Variables from _data_source """
    _variables = _data_source.get_variables()

    """ Search settings used to construct _search (see below) """
    _settings = _eureqa.search_templates.numeric('Analysis Search', _variables[0], _variables[1:])

    """ Search.  Has been executed; will have at least two solutions. """
    _search = _create_search(_data_source, EureqaTestBaseNoSearch._settings)

    """ The best solution to _search """
    _solution = _search.get_best_solution()

    """ A solution to _search that's not the best solution """
    _solution2 = [x for x in _search.get_solutions() if x._id != _solution._id][0]

# http://stackoverflow.com/questions/5136611/capture-stdout-from-a-script-in-python
# Capture stdout from a function call within a Python application
# Used as a context manager, ie, "with capture() as ...: function_to_capture()"
# Very not-thread-safe.
@contextlib.contextmanager
def capture():
    oldout,olderr = sys.stdout, sys.stderr
    try:
        out = [StringIO(), StringIO()]
        sys.stdout,sys.stderr = out
        yield out
    finally:
        sys.stdout,sys.stderr = oldout, olderr
        out[0] = out[0].getvalue()
        out[1] = out[1].getvalue()


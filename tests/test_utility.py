import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa, search_settings

from eureqa.utils.utils import Throttle

import unittest
import time

## Tests utility functions
class CallCountingClass:
    """ Tracks the number of calls made to a given function """
    def __init__(self):
        self._normal_calls     = 0
        self._throttled1_calls = 0
        self._throttled2_calls = 0

    def normal_function(self):
        self._normal_calls = self._normal_calls + 1
        pass

    @Throttle()
    def throttled1_function(self):
        self._throttled1_calls = self._throttled1_calls + 1
        pass

    @Throttle()
    def throttled2_function(self):
        self._throttled2_calls = self._throttled2_calls + 1
        pass

class TestUtililty(unittest.TestCase):
    def start_timing(self):
        self._start_time = time.time()

    def elapsed(self):
        """ Returns number of seconds (float) since time s """
        return time.time() - self._start_time

    def test_normal(self):
        c = CallCountingClass()
        self.start_timing()
        self.assertEqual(c._normal_calls, 0)

        c.normal_function()
        self.assertEqual(c._normal_calls, 1)
        self.assertTrue(self.elapsed() < 0.5)

        c.normal_function()
        self.assertTrue(self.elapsed() < 0.5)
        self.assertEqual(c._normal_calls, 2)
        pass

    def test_throttled(self):
        c = CallCountingClass()
        self.start_timing()
        self.assertEqual(c._throttled1_calls, 0)
        c.throttled1_function()
        self.assertTrue(self.elapsed() < 0.5)
        self.assertEqual(c._throttled1_calls, 1)

        c.throttled1_function() # should be throttled
        self.assertTrue(self.elapsed() >= 1.0)
        self.assertTrue(self.elapsed() < 1.5)
        self.assertEqual(c._throttled1_calls, 2)

        c.throttled1_function() # should be non-throttled
        self.assertTrue(self.elapsed() >= 1.0)
        self.assertTrue(self.elapsed() < 1.5)
        self.assertEqual(c._throttled1_calls, 3)

        c.throttled1_function() # should be throttled again
        self.assertTrue(self.elapsed() >= 2.0)
        self.assertTrue(self.elapsed() < 2.5)
        self.assertEqual(c._throttled1_calls, 4)
        pass

    def test_throttled_different_method(self):
        c = CallCountingClass()
        self.start_timing()
        self.assertEqual(c._throttled1_calls, 0)
        self.assertEqual(c._throttled2_calls, 0)
        c.throttled1_function()
        self.assertTrue(self.elapsed() < 0.5)
        self.assertEqual(c._throttled1_calls, 1)
        self.assertEqual(c._throttled2_calls, 0)

        c.throttled2_function() # should NOT be throttled (different function)
        self.assertTrue(self.elapsed() < 0.5)
        self.assertEqual(c._throttled1_calls, 1)
        self.assertEqual(c._throttled2_calls, 1)


    # test two instances don't share state (aren't cothrottled)
    def test_throttled_different_classes(self):
        c1 = CallCountingClass()
        c2 = CallCountingClass()

        self.start_timing()
        self.assertEqual(c1._throttled1_calls, 0)
        self.assertEqual(c2._throttled1_calls, 0)

        # same function but different instances shouldn't be throttled
        c1.throttled1_function()
        c2.throttled1_function()
        self.assertTrue(self.elapsed() < 0.5)
        self.assertEqual(c1._throttled1_calls, 1)
        self.assertEqual(c2._throttled1_calls, 1)

        # should throttle second call to once instance
        c1.throttled1_function()
        self.assertTrue(self.elapsed() >= 1.0)
        self.assertTrue(self.elapsed() < 1.5)
        self.assertEqual(c1._throttled1_calls, 2)
        self.assertEqual(c2._throttled1_calls, 1)

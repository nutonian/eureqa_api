import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.

import eureqa
import unittest
from eureqa.analysis.components import *

from etestutils_twosearch import EureqaTestBaseTwoSearch

class TestSearchLinkComponent(unittest.TestCase, EureqaTestBaseTwoSearch):

    def test_component(self):
        analysis = self._analysis
        sl = SearchLink(search=self._search, link_text="This is the link text")
        card = analysis.create_html_card("Target search is: {0}".format(analysis.html_ref(sl)))

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(self._search._id, sl._search_id)
        self.assertEqual(self._search._data_source_id,   sl._datasource_id)
        self.assertEqual("This is the link text", sl.link_text)

        # Make sure we can fetch all fields from scratch
        sl = analysis._get_component_by_id(sl._component_id)
        self.assertEqual(self._search._id, sl._search_id)
        self.assertEqual(self._search._data_source_id,   sl._datasource_id)
        self.assertEqual("This is the link text", sl.link_text)

        # ensure we made the html that we expected
        content = card.component.content # is a html content
        self.assertEqual(content.html, u'Target search is: <nutonian-component id="{0}" />'.format(sl._component_id))

        # test updating fields via setters and ensure they match
        sl.search = self._search_2
        self.assertEqual(self._search_2._id, sl._search_id)
        self.assertEqual(self._search_2._data_source_id, sl._datasource_id)
        self.assertEqual("This is the link text", sl.link_text)

        sl.link_text = "This is the next link text"
        self.assertEqual("This is the next link text", sl.link_text)

        # refetch
        sl = analysis._get_component_by_id(sl._component_id)
        self.assertEqual(self._search_2._id, sl._search_id)
        self.assertEqual(self._search_2._data_source_id, sl._datasource_id)
        self.assertEqual("This is the next link text", sl.link_text)


    def test_component_no_link_text(self):
        analysis = self._analysis
        sl = SearchLink(search=self._search)
        # ensure the link picks up the search name as link_text
        self.assertEqual("Analysis Search",   sl.link_text)
        card = analysis.create_html_card("Target search is: {0}".format(analysis.html_ref(sl)))

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(self._search._id, sl._search_id)
        self.assertEqual(self._search._data_source_id,   sl._datasource_id)
        self.assertEqual("Analysis Search",   sl.link_text)

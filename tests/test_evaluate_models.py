import sys
import os


sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa

import unittest

from etestutils import EureqaTestBase

class TestEvaluateModels(unittest.TestCase, EureqaTestBase):

    def test_pre_existing_datasource(self):
        result = self._eureqa.evaluate_models(self._data_source,
                                              solutions=self._search.get_solutions(),
                                              expressions=['inc_x - 10'],
                                              include_data=True,
                                              calculate_error_metrics=True)

        # Make sure that we did not blow up the target datasource.
        result._evaluation_search.get_data_source()

        self.assertEqual(result.num_future_rows, 0)

        data = result.data
        self.assertListEqual(data[u'x_plus_three'], [4.0, 5.0, 6.0, 7.0, 8.0])
        self.assertListEqual(data[u'dbl_x'], [2.0, 4.0, 6.0, 8.0, 10.0])
        self.assertListEqual(data[u'inc_x'], [2.0, 3.0, 4.0, 5.0, 6.0])
        self.assertListEqual(data[u'x'], [1.0, 2.0, 3.0, 4.0, 5.0])
        self.assertListEqual(data[u'inc_x - 10'], [-8.0, -7.0, -6.0, -5.0, -4.0])
        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(data), 4 + 1 + 2)

        frame = result.frame
        self.assertListEqual(frame[u'x_plus_three'].tolist(), [4, 5, 6, 7, 8])
        self.assertListEqual(frame[u'dbl_x'].tolist(), [2, 4, 6, 8, 10])
        self.assertListEqual(frame[u'inc_x'].tolist(), [2, 3, 4, 5, 6])
        self.assertListEqual(frame[u'x'].tolist(), [1, 2, 3, 4, 5])
        self.assertListEqual(frame[u'inc_x - 10'].tolist(), [-8, -7, -6, -5, -4])
        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(frame.columns), 4 + 1 + 2)

        error_metrics = result.error_metrics
        # There are 4 data columns which don't have error metrics.
        self.assertEqual(len(data) - 4, len(error_metrics))
        self.assertEqual(9, error_metrics[u'inc_x - 10'].mean_absolute_error)

    def test_raw_data(self):
        result = self._eureqa.evaluate_models('simple_data.csv',
                                              solutions=self._search.get_solutions(),
                                              expressions=['inc_x - 10'],
                                              include_data=True,
                                              calculate_error_metrics=True)

        # Datasource has to be removed after the evaluation is done with it.
        with self.assertRaises(Exception):
            result._evaluation_search.get_data_source()

        data = result.data
        self.assertListEqual(data[u'x_plus_three'], [4.0, 5.0, 6.0, 7.0, 8.0])
        self.assertListEqual(data[u'dbl_x'], [2.0, 4.0, 6.0, 8.0, 10.0])
        self.assertListEqual(data[u'inc_x'], [2.0, 3.0, 4.0, 5.0, 6.0])
        self.assertListEqual(data[u'x'], [1.0, 2.0, 3.0, 4.0, 5.0])
        self.assertListEqual(data[u'inc_x - 10'], [-8.0, -7.0, -6.0, -5.0, -4.0])
        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(data), 4 + 1 + 2)

        frame = result.frame
        self.assertListEqual(frame[u'x_plus_three'].tolist(), [4, 5, 6, 7, 8])
        self.assertListEqual(frame[u'dbl_x'].tolist(), [2, 4, 6, 8, 10])
        self.assertListEqual(frame[u'inc_x'].tolist(), [2, 3, 4, 5, 6])
        self.assertListEqual(frame[u'x'].tolist(), [1, 2, 3, 4, 5])
        self.assertListEqual(frame[u'inc_x - 10'].tolist(), [-8, -7, -6, -5, -4])
        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(frame.columns), 4 + 1 + 2)

        error_metrics = result.error_metrics
        # There are 4 data columns which don't have error metrics.
        self.assertEqual(len(data) - 4, len(error_metrics))
        self.assertEqual(9, error_metrics[u'inc_x - 10'].mean_absolute_error)

    def test_without_data(self):
        result = self._eureqa.evaluate_models(self._data_source,
                                              solutions=self._search.get_solutions(),
                                              expressions=['inc_x - 10'],
                                              include_data=False,
                                              calculate_error_metrics=True)

        data = result.data
        self.assertListEqual(data[u'inc_x - 10'], [-8.0, -7.0, -6.0, -5.0, -4.0])
        # There are 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(data), 1 + 2)

        frame = result.frame
        self.assertListEqual(frame[u'inc_x - 10'].tolist(), [-8, -7, -6, -5, -4])
        # There are 1 string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(frame.columns), 1 + 2)

        error_metrics = result.error_metrics
        self.assertEqual(len(data), len(error_metrics))
        self.assertEqual(9, error_metrics[u'inc_x - 10'].mean_absolute_error)


    def test_without_error_metrics(self):
        result = self._eureqa.evaluate_models('simple_data.csv',
                                              solutions=self._search.get_solutions(),
                                              expressions=['inc_x - 10'],
                                              include_data=True,
                                              calculate_error_metrics=False)

        data = result.data
        self.assertListEqual(data[u'x_plus_three'], [4.0, 5.0, 6.0, 7.0, 8.0])
        self.assertListEqual(data[u'dbl_x'], [2.0, 4.0, 6.0, 8.0, 10.0])
        self.assertListEqual(data[u'inc_x'], [2.0, 3.0, 4.0, 5.0, 6.0])
        self.assertListEqual(data[u'x'], [1.0, 2.0, 3.0, 4.0, 5.0])
        self.assertListEqual(data[u'inc_x - 10'], [-8.0, -7.0, -6.0, -5.0, -4.0])
        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(data), 4 + 1 + 2)

        frame = result.frame
        self.assertListEqual(frame[u'x_plus_three'].tolist(), [4, 5, 6, 7, 8])
        self.assertListEqual(frame[u'dbl_x'].tolist(), [2, 4, 6, 8, 10])
        self.assertListEqual(frame[u'inc_x'].tolist(), [2, 3, 4, 5, 6])
        self.assertListEqual(frame[u'x'].tolist(), [1, 2, 3, 4, 5])
        self.assertListEqual(frame[u'inc_x - 10'].tolist(), [-8, -7, -6, -5, -4])
        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(frame.columns), 4 + 1 + 2)

        self.assertEqual(0, len(result.error_metrics))

    def test_with_datetime_column(self):
        from datetime import datetime
        from pandas import Timestamp

        target_datasource = self._eureqa.create_data_source('SimpleDataSourceWithDate', 'simple_data_with_datetime.csv',
            series_order_column_name='date')
        result = self._eureqa.evaluate_models(target_datasource,
                                              solutions=self._search.get_solutions(),
                                              expressions=['inc_x - 10'],
                                              include_data=True,
                                              calculate_error_metrics=False)
        data = result.data
        self.assertListEqual(data[u'x_plus_three'], [4.0, 5.0, 6.0, 7.0, 8.0])
        self.assertListEqual(data[u'dbl_x'], [2.0, 4.0, 6.0, 8.0, 10.0])
        self.assertListEqual(data[u'inc_x'], [2.0, 3.0, 4.0, 5.0, 6.0])
        self.assertListEqual(data[u'x'], [1.0, 2.0, 3.0, 4.0, 5.0])
        self.assertListEqual(data[u'inc_x - 10'], [-8.0, -7.0, -6.0, -5.0, -4.0])
        self.assertListEqual(data[u'date'], [datetime(2017, 1, 23, 12, 34, 56),
                                             datetime(2017, 1, 24, 12, 34, 56),
                                             datetime(2017, 1, 25, 12, 34, 56),
                                             datetime(2017, 1, 26, 12, 34, 56),
                                             datetime(2017, 1, 27, 12, 34, 56)])

        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(data), 4 + 1 + 2)

        frame = result.frame
        self.assertListEqual(frame[u'x_plus_three'].tolist(), [4, 5, 6, 7, 8])
        self.assertListEqual(frame[u'dbl_x'].tolist(), [2, 4, 6, 8, 10])
        self.assertListEqual(frame[u'inc_x'].tolist(), [2, 3, 4, 5, 6])
        self.assertListEqual(frame[u'x'].tolist(), [1, 2, 3, 4, 5])
        self.assertListEqual(frame[u'inc_x - 10'].tolist(), [-8, -7, -6, -5, -4])
        self.assertListEqual(frame[u'date'].tolist(), [Timestamp('2017-1-23 12:34:56'),
                                                       Timestamp('2017-1-24 12:34:56'),
                                                       Timestamp('2017-1-25 12:34:56'),
                                                       Timestamp('2017-1-26 12:34:56'),
                                                       Timestamp('2017-1-27 12:34:56')])
        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(frame.columns), 4 + 1 + 2)

        self.assertEqual(0, len(result.error_metrics))

    def test_with_date_column(self):
        from datetime import datetime
        from pandas import Timestamp

        target_datasource = self._eureqa.create_data_source('SimpleDataSourceWithDate', 'simple_data_with_date.csv',
            series_order_column_name='date')
        result = self._eureqa.evaluate_models(target_datasource,
                                              solutions=self._search.get_solutions(),
                                              expressions=['inc_x - 10'],
                                              include_data=True,
                                              calculate_error_metrics=False)
        data = result.data
        self.assertListEqual(data[u'x_plus_three'], [4.0, 5.0, 6.0, 7.0, 8.0])
        self.assertListEqual(data[u'dbl_x'], [2.0, 4.0, 6.0, 8.0, 10.0])
        self.assertListEqual(data[u'inc_x'], [2.0, 3.0, 4.0, 5.0, 6.0])
        self.assertListEqual(data[u'x'], [1.0, 2.0, 3.0, 4.0, 5.0])
        self.assertListEqual(data[u'inc_x - 10'], [-8.0, -7.0, -6.0, -5.0, -4.0])
        self.assertListEqual(data[u'date'], [datetime(2017, 1, 23),
                                             datetime(2017, 1, 24),
                                             datetime(2017, 1, 25),
                                             datetime(2017, 1, 26),
                                             datetime(2017, 1, 27)])

        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(data), 4 + 1 + 2)

        frame = result.frame
        self.assertListEqual(frame[u'x_plus_three'].tolist(), [4, 5, 6, 7, 8])
        self.assertListEqual(frame[u'dbl_x'].tolist(), [2, 4, 6, 8, 10])
        self.assertListEqual(frame[u'inc_x'].tolist(), [2, 3, 4, 5, 6])
        self.assertListEqual(frame[u'x'].tolist(), [1, 2, 3, 4, 5])
        self.assertListEqual(frame[u'inc_x - 10'].tolist(), [-8, -7, -6, -5, -4])
        self.assertListEqual(frame[u'date'].tolist(), [Timestamp('2017-1-23 00:00:00'),
                                                       Timestamp('2017-1-24 00:00:00'),
                                                       Timestamp('2017-1-25 00:00:00'),
                                                       Timestamp('2017-1-26 00:00:00'),
                                                       Timestamp('2017-1-27 00:00:00')])
        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(frame.columns), 4 + 1 + 2)

        self.assertEqual(0, len(result.error_metrics))

    def test_with_series_id_column(self):
        from datetime import datetime
        from pandas import Timestamp

        target_datasource = self._eureqa.create_data_source('SimpleDataSourceWithSeriesID', 'simple_data_with_series_id.csv',
            series_id_column_name='series_id')
        solutions=self._search.get_solutions()
        result = self._eureqa.evaluate_models(target_datasource,
                                              solutions,
                                              expressions=['inc_x - 10'],
                                              include_data=False,
                                              calculate_error_metrics=False)
        data = result.data
        self.assertEqual(len(data), 2 + len(solutions))
        self.assertListEqual(data[u'inc_x - 10'], [-8.0, -7.0, -6.0, -5.0, -4.0])
        self.assertListEqual(data[u'series_id'], ['3', '3', 'B', 'B', 'C'])

        frame = result.frame
        self.assertEqual(len(frame.columns), 2 + len(solutions))
        self.assertListEqual(frame[u'inc_x - 10'].tolist(), [-8, -7, -6, -5, -4])
        self.assertListEqual(frame[u'series_id'].tolist(), ['3', '3', 'B', 'B', 'C'])

    def test_with_existing_derived_variable(self):
        from datetime import datetime
        from pandas import Timestamp

        original_datasource = self._eureqa.create_data_source('SimpleDataSource', 'simple_data.csv')
        original_datasource.create_variable('inc_x + 20', 'inc_x_plus_20')
        variables = original_datasource.get_variables()
        settings = self._eureqa.search_templates.numeric('Test search', variables[0], variables[1:])
        search = original_datasource.create_search(settings)
        solution = search.create_solution('2*inc_x_plus_20')

        target_datasource = self._eureqa.create_data_source('SimpleDataSource', 'simple_data.csv')
        target_datasource.create_variable('inc_x + 20', 'inc_x_plus_20')

        result = self._eureqa.evaluate_models(target_datasource,
                                              solutions = [solution])
        # There is no need to create another datasource because the target datasource
        # already has all required derived variables.
        self.assertEqual(result._evaluation_search._data_source_id, target_datasource._datasource_id)
        # Make sure that we did not blow up the target datasource.
        result._evaluation_search.get_data_source()

        data = result.data
        self.assertEqual(len(data), 1)
        self.assertListEqual(data[u'2*inc_x_plus_20'], [44.0, 46.0, 48.0, 50.0, 52.0])

        frame = result.frame
        self.assertEqual(len(frame.columns), 1)
        self.assertListEqual(frame[u'2*inc_x_plus_20'].tolist(), [44.0, 46.0, 48.0, 50.0, 52.0])

    def test_with_non_existing_derived_variable(self):
        from datetime import datetime
        from pandas import Timestamp

        original_datasource = self._eureqa.create_data_source('SimpleDataSource', 'simple_data.csv')
        original_datasource.create_variable('inc_x + 20', 'inc_x_plus_20')
        variables = original_datasource.get_variables()
        settings = self._eureqa.search_templates.numeric('Test search', variables[0], variables[1:])
        search = original_datasource.create_search(settings)
        solution = search.create_solution('2*inc_x_plus_20')

        target_datasource = self._eureqa.create_data_source('SimpleDataSource', 'simple_data.csv')

        result = self._eureqa.evaluate_models(target_datasource,
                                              solutions = [solution])
        # Need to create a hidden datasource to be able to add derived variables to it
        # without modifying the target datasource.
        self.assertNotEqual(result._evaluation_search._data_source_id, target_datasource._datasource_id)
        # Datasource has to be removed after the evaluation is done with it.
        with self.assertRaises(Exception):
            result._evaluation_search.get_data_source()

        data = result.data
        self.assertEqual(len(data), 1)
        self.assertListEqual(data[u'2*inc_x_plus_20'], [44.0, 46.0, 48.0, 50.0, 52.0])

        frame = result.frame
        self.assertEqual(len(frame.columns), 1)
        self.assertListEqual(frame[u'2*inc_x_plus_20'].tolist(), [44.0, 46.0, 48.0, 50.0, 52.0])

    def test_delayed(self):
        result = self._eureqa.evaluate_models(self._data_source,
                                              solutions=self._search.get_solutions(),
                                              expressions=['delay(inc_x, 2)'],
                                              include_data=True,
                                              calculate_error_metrics=True)

        self.assertEqual(result.num_future_rows, 2)

        data = result.data
        import math
        nan = float('nan')
        def assertListEqual (l1, l2):
            self.assertEqual(len(l1), len(l2))
            for element in zip(l1, l2):
                self.assertTrue((math.isnan(element[0]) and math.isnan(element[1]))
                                or element[0] == element[1])
        assertListEqual(data[u'x_plus_three'], [4.0, 5.0, 6.0, 7.0, 8.0, nan, nan])
        assertListEqual(data[u'dbl_x'], [2.0, 4.0, 6.0, 8.0, 10.0, nan, nan])
        assertListEqual(data[u'inc_x'], [2.0, 3.0, 4.0, 5.0, 6.0, nan, nan])
        assertListEqual(data[u'x'], [1.0, 2.0, 3.0, 4.0, 5.0, nan, nan])
        assertListEqual(data[u'delay(inc_x, 2)'], [nan, nan, 2.0, 3.0, 4.0, 5.0, 6.0])
        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(data), 4 + 1 + 2)

        frame = result.frame
        assertListEqual(frame[u'x_plus_three'].tolist(), [4, 5, 6, 7, 8, nan, nan])
        assertListEqual(frame[u'dbl_x'].tolist(), [2, 4, 6, 8, 10, nan, nan])
        assertListEqual(frame[u'inc_x'].tolist(), [2, 3, 4, 5, 6, nan, nan])
        assertListEqual(frame[u'x'].tolist(), [1, 2, 3, 4, 5, nan, nan])
        assertListEqual(frame[u'delay(inc_x, 2)'].tolist(), [nan, nan, 2, 3, 4, 5, 6])
        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(frame.columns), 4 + 1 + 2)

        error_metrics = result.error_metrics
        # There are 4 data columns which don't have error metrics.
        self.assertEqual(len(data) - 4, len(error_metrics))
        self.assertEqual(1, error_metrics[u'delay(inc_x, 2)'].mean_absolute_error)

    def test_override_num_future_rows(self):
        import math
        nan = float('nan')
        def assertListEqual (l1, l2):
            self.assertEqual(len(l1), len(l2))
            for element in zip(l1, l2):
                self.assertTrue((math.isnan(element[0]) and math.isnan(element[1]))
                                or element[0] == element[1])

        result = self._eureqa.evaluate_models(self._data_source,
                                              solutions=self._search.get_solutions(),
                                              expressions=['delay(inc_x, 2)'],
                                              include_data=False,
                                              calculate_error_metrics=False,
                                              num_future_rows=0)
        self.assertEqual(result.num_future_rows, 0)
        data = result.data
        assertListEqual(data[u'delay(inc_x, 2)'], [nan, nan, 2.0, 3.0, 4.0])

        result = self._eureqa.evaluate_models(self._data_source,
                                              solutions=self._search.get_solutions(),
                                              expressions=['delay(inc_x, 2)'],
                                              include_data=False,
                                              calculate_error_metrics=False,
                                              num_future_rows=1)
        self.assertEqual(result.num_future_rows, 1)
        data = result.data
        assertListEqual(data[u'delay(inc_x, 2)'], [nan, nan, 2.0, 3.0, 4.0, 5.0])

        result = self._eureqa.evaluate_models(self._data_source,
                                              solutions=self._search.get_solutions(),
                                              expressions=['delay(inc_x, 2)'],
                                              include_data=False,
                                              calculate_error_metrics=False,
                                              num_future_rows=2)
        self.assertEqual(result.num_future_rows, 2)
        data = result.data
        assertListEqual(data[u'delay(inc_x, 2)'], [nan, nan, 2.0, 3.0, 4.0, 5.0, 6.0])

        result = self._eureqa.evaluate_models(self._data_source,
                                              solutions=self._search.get_solutions(),
                                              expressions=['delay(inc_x, 2)'],
                                              include_data=False,
                                              calculate_error_metrics=False,
                                              num_future_rows=3)
        self.assertEqual(result.num_future_rows, 3)
        data = result.data
        assertListEqual(data[u'delay(inc_x, 2)'], [nan, nan, 2.0, 3.0, 4.0, 5.0, 6.0, nan])

    def test_delayed_with_confidence_interval(self):
        result = self._eureqa.evaluate_models(self._data_source,
                                              solutions=self._search.get_solutions(),
                                              expressions=['delay(inc_x, 2)'],
                                              include_data=True,
                                              calculate_error_metrics=True,
                                              calculate_confidence_intervals=True)

        self.assertEqual(result.num_future_rows, 2)

        data = result.data
        import math
        nan = float('nan')
        def assertListEqual (l1, l2):
            self.assertEqual(len(l1), len(l2))
            for element in zip(l1, l2):
                self.assertTrue((math.isnan(element[0]) and math.isnan(element[1]))
                                or element[0] == element[1],
                                msg="Values %s and %s are not equal.\nl1=%s\nl2=%s" %
                                    (str(element[0]), str(element[1]),
                                     repr(l1), repr(l2)))

        for solution in self._search.get_solutions():
            model = solution.model
            self.assertTrue(model in data)
            self.assertTrue(u'Lower CI -- ' + model in data)
            self.assertTrue(u'Upper CI -- ' + model in data)

        assertListEqual(data[u'x_plus_three'], [4.0, 5.0, 6.0, 7.0, 8.0, nan, nan])
        assertListEqual(data[u'dbl_x'], [2.0, 4.0, 6.0, 8.0, 10.0, nan, nan])
        assertListEqual(data[u'inc_x'], [2.0, 3.0, 4.0, 5.0, 6.0, nan, nan])
        assertListEqual(data[u'x'], [1.0, 2.0, 3.0, 4.0, 5.0, nan, nan])
        assertListEqual(data[u'delay(inc_x, 2)'], [nan, nan, 2.0, 3.0, 4.0, 5.0, 6.0])
        assertListEqual(data[u'Lower CI -- delay(inc_x, 2)'], [nan, nan, nan, nan, nan, 4.0, 5.0])
        assertListEqual(data[u'Upper CI -- delay(inc_x, 2)'], [nan, nan, nan, nan, nan, 6.0, 7.0])
        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(data), 4 + 1 + 2)

        frame = result.frame
        assertListEqual(frame[u'x_plus_three'].tolist(), [4, 5, 6, 7, 8, nan, nan])
        assertListEqual(frame[u'dbl_x'].tolist(), [2, 4, 6, 8, 10, nan, nan])
        assertListEqual(frame[u'inc_x'].tolist(), [2, 3, 4, 5, 6, nan, nan])
        assertListEqual(frame[u'x'].tolist(), [1, 2, 3, 4, 5, nan, nan])
        assertListEqual(frame[u'delay(inc_x, 2)'].tolist(), [nan, nan, 2, 3, 4, 5, 6])
        assertListEqual(frame[u'Lower CI -- delay(inc_x, 2)'], [nan, nan, nan, nan, nan, 4.0, 5.0])
        assertListEqual(frame[u'Upper CI -- delay(inc_x, 2)'], [nan, nan, nan, nan, nan, 6.0, 7.0])
        # There are 4 columns for variables, 1 for string model, and at least two solutions which are
        # generated every test run by the text fixture.  Plus the two confidence-interval columns.
        self.assertGreaterEqual(len(frame.columns), 4 + 1 + 2 + 2)

        error_metrics = result.error_metrics
        # There are 4 data columns and 6 confidence columns which don't have error metrics.
        self.assertEqual(len(data) - 4 - 6, len(error_metrics))
        self.assertEqual(1, error_metrics[u'delay(inc_x, 2)'].mean_absolute_error)

    def test_delayed_only_one_solution(self):
        variables = self._data_source.get_variables()
        settings = self._eureqa.search_templates.time_series(
                'Test search', variables[0], variables[1:], max_delays_per_variable=2)
        search = self._data_source.create_search(settings)
        solution = search.create_solution('delay(inc_x, 2)')
        result = self._eureqa.evaluate_models(self._data_source,
                                              solutions={solution},
                                              expressions=[],
                                              include_data=False,
                                              calculate_error_metrics=False)

        self.assertEqual(result.num_future_rows, 2)

        data = result.data
        import math
        nan = float('nan')
        def assertListEqual (l1, l2):
            self.assertEqual(len(l1), len(l2))
            for element in zip(l1, l2):
                self.assertTrue((math.isnan(element[0]) and math.isnan(element[1]))
                                or element[0] == element[1])
        assertListEqual(data[u'delay(inc_x, 2)'], [nan, nan, 2.0, 3.0, 4.0, 5.0, 6.0])
        self.assertGreaterEqual(len(data), 1)

        frame = result.frame
        assertListEqual(frame[u'delay(inc_x, 2)'].tolist(), [nan, nan, 2, 3, 4, 5, 6])
        self.assertGreaterEqual(len(frame.columns), 1)

    def test_expression_reordering(self):
        solutions = self._search.get_solutions()
        result = self._eureqa.evaluate_models(self._data_source,
                                              solutions=solutions,
                                              # Both of these expressions will be parsed as 'inc_x - 10'
                                              # on the back-end. This test makes sure that they will return
                                              # unchanged.
                                              expressions=['inc_x-10', '-10+inc_x'],
                                              include_data=True,
                                              calculate_error_metrics=True)

        data = result.data
        self.assertListEqual(data[u'x_plus_three'], [4.0, 5.0, 6.0, 7.0, 8.0])
        self.assertListEqual(data[u'dbl_x'], [2.0, 4.0, 6.0, 8.0, 10.0])
        self.assertListEqual(data[u'inc_x'], [2.0, 3.0, 4.0, 5.0, 6.0])
        self.assertListEqual(data[u'x'], [1.0, 2.0, 3.0, 4.0, 5.0])
        self.assertListEqual(data[u'inc_x-10'], [-8.0, -7.0, -6.0, -5.0, -4.0])
        self.assertListEqual(data[u'-10+inc_x'], [-8.0, -7.0, -6.0, -5.0, -4.0])
        # There are 4 columns for variables, 2 for string models, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(data), 4 + 2 + 2)

        frame = result.frame
        self.assertListEqual(frame[u'x_plus_three'].tolist(), [4, 5, 6, 7, 8])
        self.assertListEqual(frame[u'dbl_x'].tolist(), [2, 4, 6, 8, 10])
        self.assertListEqual(frame[u'inc_x'].tolist(), [2, 3, 4, 5, 6])
        self.assertListEqual(frame[u'x'].tolist(), [1, 2, 3, 4, 5])
        self.assertListEqual(frame[u'inc_x-10'].tolist(), [-8, -7, -6, -5, -4])
        self.assertListEqual(frame[u'-10+inc_x'].tolist(), [-8, -7, -6, -5, -4])
        # There are 4 columns for variables, 2 for string models, and at least two solutions which are
        # generated every test run by the text fixture.
        self.assertGreaterEqual(len(frame.columns), 4 + 2 + 2)

        error_metrics = result.error_metrics
        self.assertEqual(2 + len(solutions), len(error_metrics))
        self.assertEqual(9, error_metrics[u'inc_x-10'].mean_absolute_error)
        self.assertEqual(9, error_metrics[u'-10+inc_x'].mean_absolute_error)

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa, search_settings

import unittest

from etestutils_twosearch import EureqaTestBaseTwoSearch, _create_search
from eureqa.analysis.components import MostFrequentVariablesPlot, _Component

class TestMostFrequentVariablesPlotComponent(unittest.TestCase, EureqaTestBaseTwoSearch):
    def test_component(self):
        comp = MostFrequentVariablesPlot(self._search)

        # Make sure constructor set all fields
        self.assertEqual(comp._datasource_id, self._search._data_source_id)
        self.assertEqual(comp._search_id, self._search._id)

        # Make sure associated search can be retreived
        self.assertEqual(comp.search._id, self._search._id)

        self._analysis.create_card(comp)

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(comp._datasource_id, self._search._data_source_id)
        self.assertEqual(comp._search_id, self._search._id)

        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # Make sure we can fetch all fields from scratch
        self.assertEqual(comp._datasource_id, self._search._data_source_id)
        self.assertEqual(comp._search_id, self._search._id)

        # Make sure all fields were updated
        comp.search = self._search_2
        self.assertEqual(comp._datasource_id, self._search_2._data_source_id)
        self.assertEqual(comp._search_id, self._search_2._id)

        # Make sure correct associated search is retreived
        self.assertEqual(comp.search._id, self._search_2._id)

        # And that they survive a round trip
        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        self.assertEqual(comp._datasource_id, self._search_2._data_source_id)
        self.assertEqual(comp._search_id, self._search_2._id)
        self.assertEqual(comp.search._id, self._search_2._id)

        # Test with analysis passed to constructor
        comp = MostFrequentVariablesPlot(self._search, _analysis=self._analysis)
        self.assertEqual(comp._datasource_id, self._search._data_source_id)
        self.assertEqual(comp._search_id, self._search._id)
 
        # Make sure constructor set all fields
        self.assertTrue(hasattr(comp, "_component_id"))

import sys
import os
sys.path.insert(1, os.path.abspath('..')) #Need this to be able to reference eureqa package.
from eureqa import error_metric, Eureqa
from eureqa.error_metric import ErrorMetrics
from etestutils import EureqaTestBase
from eureqa.variable_options import VariableOptions
from eureqa.variable_options_dict import VariableOptionsDict
from eureqa import missing_value_policies

import unittest

class TestErrorMetric(unittest.TestCase, EureqaTestBase):

    def test_error_metric_init(self):
        """Test that ErrorMetrics()'s __init__ behaves propery"""
        self.assertEqual(42, ErrorMetrics(mean_absolute_error=42).mean_absolute_error)
        self.assertEqual(42, ErrorMetrics(r2_goodness_of_fit=42).r2_goodness_of_fit)
        self.assertEqual(42, ErrorMetrics(correlation_coefficient=42).correlation_coefficient)
        self.assertEqual(42, ErrorMetrics(maximum_absolute_error=42).maximum_absolute_error)
        self.assertEqual(42, ErrorMetrics(signed_difference_between_lhs_and_rhs=42).signed_difference_between_lhs_and_rhs)
        self.assertEqual(42, ErrorMetrics(area_under_roc_error=42).area_under_roc_error)
        self.assertEqual(42, ErrorMetrics(log_loss_error=42).log_loss_error)
        self.assertEqual(42, ErrorMetrics(rank_correlation_1_minus_r=42).rank_correlation_1_minus_r)
        self.assertEqual(42, ErrorMetrics(mean_square_error=42).mean_square_error)
        self.assertEqual(42, ErrorMetrics(mean_squared_error_auc_hybrid=42).mean_squared_error_auc_hybrid)
        self.assertEqual(42, ErrorMetrics(mean_absolute_percentage_error=42).mean_absolute_percentage_error)

    def test_error_metric_const(self):
        """Sanity check that error metric constants are visible from the namespace."""

        self.assertEqual(error_metric.mean_square_error(), 'Mean Squared Error')

    def test_matches_server_list(self):
        """Verifies that the list of error metrics exposed by the back-end matches the list in API"""

        api_metrics = {eval('error_metric.%s()' % x) for x in dir(error_metric) if not x.startswith('__') and x != "ErrorMetrics"}
        session = self._eureqa._session
        settings_info = session.execute('/api/v2/fxp/settings_info', 'GET')
        server_metrics = settings_info['error_metrics']
        #compare sets because they ignore order
        self.assertSetEqual(api_metrics, set([x['error_metric'] for x in server_metrics]))

    def test_metric_map(self):
        """ Verifies that _metric_map is the inverse of the functions on error_metric"""
        for longname, slug in error_metric.ErrorMetrics._metric_map.iteritems():
            self.assertEqual(longname, getattr(error_metric, slug)())

    def test_compute_error_metric_variable_options_list(self):
        metrics = self._eureqa.compute_error_metrics(
            self._data_source,
            "%s - 2" % self._variables[0],
            "%s + 5" % self._variables[1],
            row_weight="%s + 9000" % self._variables[2],
            variable_options=[
                VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean),
                VariableOptions(self._variables[1], remove_outliers_enabled=True)
            ]
        )

        self.assertGreaterEqual(metrics.mean_absolute_error, 0)
        self.assertGreaterEqual(metrics.r2_goodness_of_fit, -50)
        self.assertGreaterEqual(metrics.correlation_coefficient, 0)
        self.assertGreaterEqual(metrics.maximum_absolute_error, 0)
        self.assertGreaterEqual(metrics.signed_difference_between_lhs_and_rhs, -50)
        self.assertGreaterEqual(metrics.area_under_roc_error, 0)
        self.assertGreaterEqual(metrics.rank_correlation_1_minus_r, 0)
        self.assertGreaterEqual(metrics.mean_square_error, 0)
        self.assertGreaterEqual(metrics.mean_squared_error_auc_hybrid, 0)

        # Not currently computed.
        self.assertEqual(metrics.log_loss_error, None)
    
    def test_compute_error_metric_variable_options_dict(self):
        v = VariableOptionsDict()
        v.add(VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean))
        v.add(VariableOptions(self._variables[1], remove_outliers_enabled=True))

        metrics = self._eureqa.compute_error_metrics(
            self._data_source,
            "%s - 2" % self._variables[0],
            "%s + 5" % self._variables[1],
            row_weight="%s + 9000" % self._variables[2],
            variable_options=v
        )

        self.assertGreaterEqual(metrics.mean_absolute_error, 0)
        self.assertGreaterEqual(metrics.r2_goodness_of_fit, -50)
        self.assertGreaterEqual(metrics.correlation_coefficient, 0)
        self.assertGreaterEqual(metrics.maximum_absolute_error, 0)
        self.assertGreaterEqual(metrics.signed_difference_between_lhs_and_rhs, -50)
        self.assertGreaterEqual(metrics.area_under_roc_error, 0)
        self.assertGreaterEqual(metrics.rank_correlation_1_minus_r, 0)
        self.assertGreaterEqual(metrics.mean_square_error, 0)
        self.assertGreaterEqual(metrics.mean_squared_error_auc_hybrid, 0)

        # Not currently computed.
        self.assertEqual(metrics.log_loss_error, None)

    def test_bogus_error_metric(self):
        with self.assertRaises(Exception):
            print self._eureqa.compute_error_metrics(
                self._data_source,
                "This is a bogus variable.",
                "%s + 5" % self._variables[1],
                "%s + 9000" % self._variables[2],
                None,
                [
                    VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean),
                    VariableOptions(self._variables[1], remove_outliers_enabled=True)
                ]
            )._to_json()

        with self.assertRaises(Exception):
            print self._eureqa.compute_error_metrics(
                self._data_source,
                "%s - 2" % self._variables[0],
                "This is a bogus variable.",
                row_weight="%s + 9000" % self._variables[2],
                variable_options=[
                    VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean),
                    VariableOptions(self._variables[1], remove_outliers_enabled=True)
                ]
            )._to_json()

        with self.assertRaises(Exception):
            print self._eureqa.compute_error_metrics(
                self._data_source,
                "%s - 2" % self._variables[0],
                "%s + 5" % self._variables[1],
                row_weight="This is a bogus variable.",
                variable_options=[
                    VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean),
                    VariableOptions(self._variables[1], remove_outliers_enabled=True)
                ]
            )._to_json()
            
    def test_compute_error_metric_data_types(self):
        all_metrics = self._eureqa.compute_error_metrics(
            self._data_source,
            "%s - 2" % self._variables[0],
            "%s + 5" % self._variables[1],
            self._search,
            row_weight="%s + 9000" % self._variables[2],
            _data_split="all",
            variable_options=[
                VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean),
                VariableOptions(self._variables[1], remove_outliers_enabled=True)
            ]
        )

        validation_metrics = self._eureqa.compute_error_metrics(
            self._data_source,
            "%s - 2" % self._variables[0],
            "%s + 5" % self._variables[1],
            self._search,
            row_weight="%s + 9000" % self._variables[2],
            _data_split="validation",
            variable_options=[
                VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean),
                VariableOptions(self._variables[1], remove_outliers_enabled=True)
            ]
        )
        
        training_metrics = self._eureqa.compute_error_metrics(
            self._data_source,
            "%s - 2" % self._variables[0],
            "%s + 5" % self._variables[1],
            self._search,
            row_weight="%s + 9000" % self._variables[2],
            _data_split="training",
            variable_options=[
                VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean),
                VariableOptions(self._variables[1], remove_outliers_enabled=True)
            ]
        )
        
        self.assertNotEqual(all_metrics, validation_metrics)
        self.assertNotEqual(all_metrics, training_metrics)
        self.assertNotEqual(validation_metrics, training_metrics)
        
    def test_compute_error_metric_row_weight_types(self):
        uniform_metrics = self._eureqa.compute_error_metrics(
            self._data_source,
            "%s - 2" % self._variables[0],
            "%s + 5" % self._variables[1],
            self._search,
            row_weight="%s + 9000" % self._variables[2],
            row_weight_type="uniform",
            _data_split="all",
            variable_options=[
                VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean),
                VariableOptions(self._variables[1], remove_outliers_enabled=True)
            ]
        )

        target_frequency_metrics = self._eureqa.compute_error_metrics(
            self._data_source,
            "%s - 2" % self._variables[0],
            "%s + 5" % self._variables[1],
            self._search,
            row_weight="%s + 9000" % self._variables[2],
            row_weight_type="target_frequency",
            _data_split="all",
            variable_options=[
                VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean),
                VariableOptions(self._variables[1], remove_outliers_enabled=True)
            ]
        )
        
        variable_metrics = self._eureqa.compute_error_metrics(
            self._data_source,
            "%s - 2" % self._variables[0],
            "%s + 5" % self._variables[1],
            self._search,
            row_weight=self._variables[2],
            row_weight_type="variable",
            _data_split="all",
            variable_options=[
                VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean),
                VariableOptions(self._variables[1], remove_outliers_enabled=True)
            ]
        )
        
        custom_expr_metrics = self._eureqa.compute_error_metrics(
            self._data_source,
            "%s - 2" % self._variables[0],
            "%s + 5" % self._variables[1],
            self._search,
            row_weight="%s + 9000" % self._variables[2],
            row_weight_type="custom_expr",
            _data_split="all",
            variable_options=[
                VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean),
                VariableOptions(self._variables[1], remove_outliers_enabled=True)
            ]
        )
        
        self.assertNotEqual(uniform_metrics, target_frequency_metrics)
        self.assertNotEqual(variable_metrics, uniform_metrics)
        self.assertNotEqual(target_frequency_metrics, variable_metrics)
        self.assertNotEqual(uniform_metrics, custom_expr_metrics)
        self.assertNotEqual(custom_expr_metrics, target_frequency_metrics)
        self.assertNotEqual(variable_metrics, custom_expr_metrics)


class TestErrorMetricWithSearch(unittest.TestCase, EureqaTestBase):
    def test_per_search_metric(self):
            metrics_with_varopts = self._eureqa.compute_error_metrics(
                self._data_source,
                "1/(x-1)",  # Get some NaN values
                "%s + 5" % self._variables[1],
                variable_options = [
                    VariableOptions(self._variables[0], missing_value_policy=missing_value_policies.column_mean),
                    VariableOptions(self._variables[1], missing_value_policy=missing_value_policies.column_mean)
                ]
            )

            # self._search uses a missing-value policy of 'column_mean' as well
            metrics_search_template = self._eureqa.compute_error_metrics(
                self._data_source,
                "1/(x-1)",
                "%s + 5" % self._variables[1],
                template_search=self._search
            )

            self.assertEqual(metrics_with_varopts._to_json(), metrics_search_template._to_json())

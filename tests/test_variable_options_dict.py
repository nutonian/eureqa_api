import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import variable_options_dict, VariableOptions

import copy
import json
import unittest


class TestVariableOptionsDict(unittest.TestCase):
    def test_add_set(self):
        v = variable_options_dict.VariableOptionsDict()
        v.add(VariableOptions(name="asdf1"))

        v['asdf2'].remove_outliers_enabled = False

        ## Negative tests:
        with self.assertRaises(AssertionError):
            v.add("asdf3")
        with self.assertRaises(AssertionError):
            v['asdf4'] = "VariableOptions"
        with self.assertRaises(AssertionError):
            v['asdf5'] = VariableOptions(name='asdf6')
        with self.assertRaises(AssertionError):
            v['asdf1'] = VariableOptions(name="asdf1")
        with self.assertRaises(AssertionError):
            v.add(VariableOptions(name="asdf1"))

    def test_json_serde(self):
        v = variable_options_dict.VariableOptionsDict()
        v.add(VariableOptions(name="asdf1"))
        v.add(VariableOptions(name="asdf2"))

        self.assertEqual(v._to_json(), [
            {
             'missing_value_policy': 'column_iqm',
             'remove_outliers_enabled': False,
             'normalize_enabled': False,
             'variable_name': 'asdf2',
             'filter_enabled': False
            }, {
             'missing_value_policy': 'column_iqm',
             'remove_outliers_enabled': False,
             'normalize_enabled': False,
             'variable_name': 'asdf1',
             'filter_enabled': False
            }])

        v2 = variable_options_dict.VariableOptionsDict.from_json(v._to_json())
        self.assertEqual(v, v2)

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa

import unittest
import time

from etestutils import EureqaTestBase

class TestModelEvaluatorCard(unittest.TestCase, EureqaTestBase):
    def test_create(self):
        card = self._analysis.create_model_evaluator_card(self._solution,
                                                          "Test Card",
                                                          False)
        card.add_solution_info(self._solution2)
        self.assertEqual(card.title, "Test Card")
        self.assertEqual(card.collapse, False)
        self.assertEqual(len(card.solution_infos), 2)

        solution_info = card.solution_infos[0]
        self.assertEqual(solution_info.solution._datasource_id,
                self._data_source._data_source_id)
        self.assertEqual(solution_info.solution._search_id, self._search._id)
        self.assertEqual(solution_info.solution._id, self._solution._id)
        self.assertEqual(solution_info.has_target_variable, True)

        solution_info = card.solution_infos[1]
        self.assertEqual(solution_info.solution._datasource_id,
                self._data_source._data_source_id)
        self.assertEqual(solution_info.solution._search_id, self._search._id)
        self.assertEqual(solution_info.solution._id, self._solution2._id)
        self.assertEqual(solution_info.has_target_variable, True)

    def test_update_get(self):
        card = self._analysis.create_model_evaluator_card(self._solution,
                                                          "Test Update Card",
                                                          False)
        card.add_solution_info(self._solution2)
        card.collapse = True
        card.title = "Test Update Card updated"
        card.solution_infos[0].has_target_variable = True
        card.solution_infos[1].has_target_variable = False

        self.assertEqual(card.title, "Test Update Card updated")
        self.assertEqual(card.collapse, True)
        self.assertEqual(len(card.solution_infos), 2)

        self.assertEqual(card.solution_infos[0].has_target_variable, True)
        self.assertEqual(card.solution_infos[1].has_target_variable, True)  # Should be re-computed

        fetched_cards = [x for x in self._analysis.get_cards() if getattr(x, 'title', None) == 'Test Update Card updated']
        self.assertEqual(len(fetched_cards), 1)
        fetched_card = fetched_cards[0]

        self.assertEqual(len(fetched_card.solution_infos), 2)

        self.assertEqual(card.solution_infos[0].solution._datasource_id,
                         fetched_card.solution_infos[0].solution._datasource_id)
        self.assertEqual(card.solution_infos[0].solution._search_id,
                         fetched_card.solution_infos[0].solution._search_id)
        self.assertEqual(card.solution_infos[0].solution._id,
                         fetched_card.solution_infos[0].solution._id)
        self.assertEqual(card.solution_infos[0].has_target_variable,
                         fetched_card.solution_infos[0].has_target_variable)

        self.assertEqual(card.solution_infos[1].solution._datasource_id,
                         fetched_card.solution_infos[1].solution._datasource_id)
        self.assertEqual(card.solution_infos[1].solution._search_id,
                         fetched_card.solution_infos[1].solution._search_id)
        self.assertEqual(card.solution_infos[1].solution._id,
                         fetched_card.solution_infos[1].solution._id)
        self.assertEqual(card.solution_infos[1].has_target_variable,
                         fetched_card.solution_infos[1].has_target_variable)

    def test_delete(self):
        card = self._analysis.create_model_evaluator_card(self._solution,
                                                          "Test Delete Card")
        card.delete()

        cards = self._analysis.get_cards()
        cards = [x for x in cards if getattr(x, 'title', None) == 'Test Delete Card']
        self.assertEqual(len(cards), 0)


    def test_create_no_target(self):
        ds = self._eureqa.create_data_source('SimpleDataSource', 'simple_data.csv')
        ds.create_variable('nan', 'Nan_var')
        settings = self._eureqa.search_templates.numeric('Nan test', target_variable='Nan_var',
                                                                 input_variables=['x'])
        search = ds.create_search(settings)
        custom_solution = search.create_solution('3 + x')
        card = self._analysis.create_model_evaluator_card(custom_solution,
                                                          "Test Card",
                                                          False)

        self.assertEqual(card.title, "Test Card")
        self.assertEqual(card.collapse, False)
        self.assertEqual(len(card.solution_infos), 1)

        self.assertEqual(card.solution_infos[0].solution._datasource_id,
                ds._data_source_id)
        self.assertEqual(card.solution_infos[0].solution._search_id, search._id)
        self.assertEqual(card.solution_infos[0].solution._id, custom_solution._id)
        self.assertEqual(card.solution_infos[0].has_target_variable, False)

    def test_legacy_create(self):
        card = self._analysis.create_model_evaluator_card(self._data_source,
                                                          self._solution,
                                                          "Test Card",
                                                          False)
        card.add_solution_info(self._solution2)
        self.assertEqual(card.title, "Test Card")
        self.assertEqual(card.description, '')
        self.assertEqual(card.collapse, False)
        self.assertEqual(len(card.solution_infos), 2)

        solution_info = card.solution_infos[0]
        self.assertEqual(solution_info.solution._datasource_id,
                self._data_source._data_source_id)
        self.assertEqual(solution_info.solution._search_id, self._search._id)
        self.assertEqual(solution_info.solution._id, self._solution._id)
        self.assertEqual(solution_info.has_target_variable, True)

        solution_info = card.solution_infos[1]
        self.assertEqual(solution_info.solution._datasource_id,
                self._data_source._data_source_id)
        self.assertEqual(solution_info.solution._search_id, self._search._id)
        self.assertEqual(solution_info.solution._id, self._solution2._id)
        self.assertEqual(solution_info.has_target_variable, True)

        card = self._analysis.create_model_evaluator_card(datasource = self._data_source,
                                                          solution = self._solution,
                                                          title="Test Card",
                                                          collapse=False)
        card.add_solution_info(self._solution2)
        self.assertEqual(card.title, "Test Card")
        self.assertEqual(card.description, '')
        self.assertEqual(card.collapse, False)
        self.assertEqual(len(card.solution_infos), 2)

        card = self._analysis.create_model_evaluator_card(self._data_source,
                                                          self._solution,
                                                          "Test Card")
        self.assertEqual(card.title, "Test Card")
        self.assertEqual(card.description, '')
        self.assertEqual(card.collapse, False)

        card = self._analysis.create_model_evaluator_card(self._data_source,
                                                          self._solution,
                                                          "Test Card",
                                                          True)

        self.assertEqual(card.title, "Test Card")
        self.assertEqual(card.description, '')
        self.assertEqual(card.collapse, True)

        card = self._analysis.create_model_evaluator_card(self._data_source,
                                                          self._solution,
                                                          "Test Card",
                                                          "Test Description")
        self.assertEqual(card.title, "Test Card")
        self.assertEqual(card.description, 'Test Description')
        self.assertEqual(card.collapse, False)

        card = self._analysis.create_model_evaluator_card(self._data_source,
                                                          self._solution,
                                                          "Test Card",
                                                          "Test Description",
                                                          True)
        self.assertEqual(card.title, "Test Card")
        self.assertEqual(card.description, 'Test Description')
        self.assertEqual(card.collapse, True)

        card = self._analysis.create_model_evaluator_card(datasource = self._data_source,
                                                          solution = self._solution,
                                                          title = "Test Card",
                                                          description = "Test Description")
        self.assertEqual(card.title, "Test Card")
        self.assertEqual(card.description, 'Test Description')
        self.assertEqual(card.collapse, False)

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import *
from eureqa.analysis_templates import AnalysisTemplate

import unittest
import tempfile
import shutil
import zipfile

class TestAnalysisTemplateModulePacking(unittest.TestCase):
    """ Tests for packaging code in analysis template"""

    def test_basic(self):
        tempdir = tempfile.mkdtemp()
        try:
            testmodule_path = os.path.join(tempdir, "testmodule")
            os.mkdir(testmodule_path)

            ## Create an __init__.py to make it a module
            with open(os.path.join(testmodule_path, "__init__.py"), "wb") as f:
                f.write("def create_analysis(eureqa, template_execution):\n    print('DEF')")

            ## Create the zipfile
            with tempfile.TemporaryFile() as f:
                AnalysisTemplate._create_analysis_template_zip(f=f,
                                                               main_module_name='testmodule',
                                                               module_fs_path=testmodule_path,
                                                               ignore_files=None)

                ## Get the zip file back
                with zipfile.ZipFile(f, 'r') as zf:
                    self.assertEqual(set(zf.namelist()), set([
                        "manifest/eureqa_main_module_name.txt",
                        "testmodule/__init__.py"
                    ]))
                    # make sure the contents are cool
                    self.assertEqual("def create_analysis(eureqa, template_execution):\n    print('DEF')", zf.read('testmodule/__init__.py'))

                # ensure the manifest got written and can be interpreted correctly
                self.assertEqual('testmodule', AnalysisTemplate._get_main_module_name_from_analysis_template_zip(f))

        finally:
            shutil.rmtree(tempdir)

    def test_multiple_directories(self):
        """ Test that multiple directories are successfully included """
        tempdir = tempfile.mkdtemp()
        tempdir2 = tempfile.mkdtemp()
        try:
            testmodule_path = os.path.join(tempdir, "testmodule")
            os.mkdir(testmodule_path)

            subdir_path = os.path.join(testmodule_path, "subdir")
            os.mkdir(subdir_path)

            other_module_path = os.path.join(tempdir2, "other_module")
            os.mkdir(other_module_path)

            empty_subdir_path = os.path.join(testmodule_path, "empty_subdir")
            os.mkdir(empty_subdir_path)

            with open(os.path.join(testmodule_path, "__init__.py"), "wb") as f:
                f.write("def create_analysis(eureqa, template_execution):\n    print('ABC')")

            with open(os.path.join(testmodule_path, "foo.py"), "wb") as f:
                f.write("def andrew():\n    print('DEF')")

            with open(os.path.join(subdir_path, "bar.py"), "wb") as f:
                f.write("def lamb():\n    print('GHI')")

            with open(os.path.join(subdir_path, "ignore_me.py"), "wb") as f:
                f.write("def zzz():\n    print('JKL')")

            with open(os.path.join(other_module_path, "baz.py"), "wb") as f:
                f.write("def xxx():\n    print('MNO')")

            ## Create the zipfile
            with tempfile.TemporaryFile() as f:
                AnalysisTemplate._create_analysis_template_zip(f=f,
                                                               main_module_name='testmodule',
                                                               module_fs_path=testmodule_path,
                                                               ignore_files=['ignore_me.py', 'blah'],
                                                               additional_modules_paths=[other_module_path])

                ## Get the zip file back
                with zipfile.ZipFile(f, 'r') as zf:
                    self.assertEqual(set(zf.namelist()), set([
                        "manifest/eureqa_main_module_name.txt",
                        "testmodule/__init__.py",
                        "testmodule/foo.py",
                        "other_module/baz.py",
                        "testmodule/subdir/bar.py"
                    ]))
                self.assertEqual('testmodule', AnalysisTemplate._get_main_module_name_from_analysis_template_zip(f))

        finally:
            shutil.rmtree(tempdir)

    # TODO test for error where there is no __init__ file
    # (aka there isn't a create_analysis endpoint)?

    # test for when the create_analysis specifies a directory that does not exist
    def test_non_existent_dir(self):
        with self.assertRaisesRegexp(Exception, "non-existent-module-path is not an existing file or directory"):
            with tempfile.TemporaryFile() as f:
                AnalysisTemplate._create_analysis_template_zip(f=f,
                                                               main_module_name='testmodule',
                                                               module_fs_path='non-existent-module-path',
                                                               ignore_files=None)

        # test for when the create_analysis specifies an extra directory that does not exist
        tempdir = tempfile.mkdtemp()
        try:
            testmodule_path = os.path.join(tempdir, "testmodule")
            os.mkdir(testmodule_path)
            ## Create an __init__.py to make it a module
            with open(os.path.join(testmodule_path, "__init__.py"), "wb") as f:
                f.write("def create_analysis(eureqa, template_execution):\n    print('DEF')")

            with self.assertRaisesRegexp(Exception, "non-existent-module-path2 is not an existing file or directory"):
                with tempfile.TemporaryFile() as f:
                    AnalysisTemplate._create_analysis_template_zip(f=f,
                                                                   main_module_name='testmodule',
                                                                   module_fs_path=testmodule_path,
                                                                   ignore_files=None,
                                                                   additional_modules_paths=['non-existent-module-path2'])
        finally:
            shutil.rmtree(tempdir)

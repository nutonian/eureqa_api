from etestutils import EureqaTestBase, _create_search

class EureqaTestBaseTwoSearch(EureqaTestBase):

    # Create a second data source for testing
    _data_source_2 = EureqaTestBase._eureqa.create_data_source('SimpleDataSource', 'simple_data.csv')

    # And a second search
    _search_2 = _create_search(_data_source_2, EureqaTestBase._settings)

    # And capture the second search's Solution
    # Note that this masks EureqaTestBase._solution_2 -- different use case
    _solution_2 = _search_2.get_best_solution()
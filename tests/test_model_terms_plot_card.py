import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa, search_settings

import unittest

from etestutils import EureqaTestBase

class TestModelTermsPlotCard(unittest.TestCase, EureqaTestBase):
    def test_create(self):
        card = self._analysis.create_model_terms_plot_card(self._solution, "Test Title", "Test Description",  False)
        self.assertIsNotNone(card._item_id)
        self.assertEqual(card.title, "Test Title")
        self.assertEqual(card.description, "Test Description")
        self.assertEqual(card.collapse, False)

    def test_update(self):
        card = self._analysis.create_model_terms_plot_card(self._solution, "Test Title",
                                           "Test Description", False)
        card.title = "Test Title 2"
        card.description = "Test Description 2"
        card.collapse = True
        self.assertEqual(card.title, "Test Title 2")
        self.assertEqual(card.description, "Test Description 2")
        self.assertEqual(card.collapse, True)

    def test_get(self):
        card = self._analysis.create_model_terms_plot_card(self._solution)
        cards = self._analysis.get_cards()
        cards = [x for x in cards if x._item_id == card._item_id]
        self.assertEqual(len(cards), 1)
        self.assertEqual(cards[0]._item_id, card._item_id)

    def test_delete(self):
        card = self._analysis.create_model_terms_plot_card(self._solution)
        card.delete()
        cards = self._analysis.get_cards()
        cards = [x for x in cards if x._item_id == card._item_id]
        self.assertEqual(len(cards), 0)

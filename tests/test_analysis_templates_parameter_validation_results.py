import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.

import unittest
from eureqa.analysis_templates import ParameterValidationResult

class TestAnalysisTempalteParameterValidationResults(unittest.TestCase):
    def test_to_json(self):
        res = ParameterValidationResult('INFO')
        res.message = 'the message'
        self.assertEquals(res._to_json(), {'message': 'the message', 'type': 'INFO'})
        # double
        self.assertEquals(res._to_json(), {'message': 'the message', 'type': 'INFO'})
        res.details = 'the details'
        self.assertEquals(res._to_json(),
                          {'message': 'the message',
                           'type': 'INFO',
                           'details': 'the details'})

        res.parameter_id = '27'
        self.assertEquals(res._to_json(),
                          {'message': 'the message',
                           'type': 'INFO',
                           'details': 'the details',
                           'parameter_id' : '27'})

        res.unrecoginized_field = 'f2'
        self.assertEquals(res._to_json(),
                          {'message': 'the message',
                           'type': 'INFO',
                           'details': 'the details',
                           'parameter_id' : '27'})

    def test_to_json_types(self):
        res = ParameterValidationResult('INFO')
        res.message = 'the message'
        self.assertEquals(res._to_json(), {'message': 'the message', 'type': 'INFO'})
        res = ParameterValidationResult('WARNING')
        res.message = 'the message2'
        self.assertEquals(res._to_json(), {'message': 'the message2', 'type': 'WARNING'})
        res = ParameterValidationResult('ERROR')
        res.message = 'the message3'
        self.assertEquals(res._to_json(), {'message': 'the message3', 'type': 'ERROR'})

    def test_to_json_errors(self):
        res = ParameterValidationResult('INVALID')
        with self.assertRaisesRegexp(ValueError, 'type must be INFO, WARNING, ERROR, not INVALID'):
            res._to_json()
        res.type = 'INFO'

        with self.assertRaisesRegexp(ValueError, 'message must be specified in parameter validation result'):
            res._to_json()

        res.message = 'the message'

        delattr(res, 'type')
        with self.assertRaisesRegexp(ValueError, 'Must specify a type of parameter validation result'):
            res._to_json()

    def test_from_json(self):

        body =  {'message' : 'the message',
                 'type'    : 'INFO',
                 'details' : 'the details',
                 'parameter_id' : '27'}

        res = ParameterValidationResult('INFO')
        res._from_json(body)
        self.assertEquals(res._to_json(), body)
        self.assertEquals(res._to_json(),
                          {'message' : 'the message',
                           'type'    : 'INFO',
                           'details' : 'the details',
                           'parameter_id' : '27'})

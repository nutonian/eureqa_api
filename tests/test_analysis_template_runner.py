import sys
import os
import unittest
import tempfile

# Import analysis runner able to reference eureqa and analysis running package.
sys.path.insert(1, os.path.abspath('..'))

from eureqa.analysis_templates.runner import analysis_template_runner
from eureqa import Eureqa

from etestutils_nosearch import EureqaTestBaseNoSearch

class TestAnalysisTemplateRunner(unittest.TestCase, EureqaTestBaseNoSearch):

    @classmethod
    def setUpClass(cls):
        cls.analysis_template_runner = analysis_template_runner();
        cls.temp_dir = tempfile.mkdtemp(prefix='test_run_analysis_template')
        # add to path
        sys.path.append(cls.temp_dir)

    @classmethod
    def tearDownClass(cls):
        import shutil
        sys.path.remove(cls.temp_dir)
        shutil.rmtree(cls.temp_dir)


    def write_string_to_temp_file(self, contents):
        """Writes contents to a temp file and returns the appropriate module_name
        Note that the fixture handles cleanup of files
        """
        tup = tempfile.mkstemp(prefix='test_run_analysis_template', suffix='.py', dir=self.temp_dir)
        os.close(tup[0]) # close descriptor
        filepath = tup[1]
        crap_file = open(filepath, 'w')
        crap_file.write(contents)
        crap_file.close()
        filename  = os.path.basename(filepath) # remove .py suffix
        module_name = os.path.splitext(filename)[0]
        return module_name

    def get_eureqa_credentials(self):
        return { 'user_name'    : 'root',
                 'password'     : 'Changeme1',
                 'organization' : 'root',
                 'url'          : self._eureqa_url }

    def get_eureqa_credentials_as_array(self):
        """ returns ['--user_name', 'root', ...] """
        args = []
        creds = self.get_eureqa_credentials()
        for key in creds:
            args.append('--'+key)
            args.append(creds[key])
        return args

    def get_eureqa(self):
        """ Get the eureqa credentials to connect """
        eureqa = Eureqa(**self.get_eureqa_credentials())
        return eureqa

    def run_analysis_template(self, filename):
        """ Invokes an analysis using the test credentials stored in the specified file """
        self.run_analysis_template_args(['--module_name', filename])

    def run_analysis_template_args(self, input_args):
        """ Invokes an analysis using the test credentials and additional args """
        args = self.get_eureqa_credentials_as_array()
        args.extend(input_args)
        self.analysis_template_runner.run_args(args)


    def test_no_args(self):
        with self.assertRaisesRegexp(Exception, "Must supply either --module_name or --analysis_template_id"):
            self.run_analysis_template_args([])


    def test_non_existent_file(self):
        with self.assertRaisesRegexp(Exception, "No module named this_does_not_exist"):
            self.run_analysis_template_args(['--module_name','this_does_not_exist'])


    def test_with_a_bad_python_file(self):
        module_name = self.write_string_to_temp_file('This is not valid python')
        with self.assertRaisesRegexp(Exception, "invalid syntax"):
            self.run_analysis_template_args(['--module_name', module_name])


    def test_with_no_entry_point(self):
        module_name = self.write_string_to_temp_file("def my_func():\n"
                                                  "    print 'Valid python, but not create_analysis'")
        with self.assertRaisesRegexp(Exception, "Can not find create_analysis in " + module_name):
            self.run_analysis_template_args(['--module_name', module_name])


    def test_with_non_function_entry_point(self):
        module_name = self.write_string_to_temp_file("create_analysis = 5")
        with self.assertRaisesRegexp(Exception, "create_analysis is not a function"):
            self.run_analysis_template_args(['--module_name', module_name])


    def test_with_wrong_signature(self):
        module_name = self.write_string_to_temp_file("def create_analysis():\n"
                                                  "    pass")
        with self.assertRaisesRegexp(Exception, "create_analysis\(\) takes no arguments \(2 given\)"):
            self.run_analysis_template_args(['--module_name', module_name])

    def test_with_unexpected_non_eureqa_arg_name(self):
        #unexpected names
        module_name = self.write_string_to_temp_file("def create_analysis(non_eureqa=None):\n"
                                                  "    pass")
        with self.assertRaisesRegexp(Exception, "create_analysis\(\) got an unexpected keyword argument 'eureqa'"):
            self.run_analysis_template_args(['--module_name', module_name])

    def test_with_unexpected_second_arg_name(self):
        # unexpected names
        module_name = self.write_string_to_temp_file("def create_analysis(eureqa, not_analysis=None):\n"
                                                  "    pass")
        with self.assertRaisesRegexp(Exception, "create_analysis\(\) got an unexpected keyword argument 'template_execution'"):
            self.run_analysis_template_args(['--module_name', module_name])

    def test_with_too_many_arg_names(self):
        # too many
        module_name = self.write_string_to_temp_file("def create_analysis(eureqa, template_execution, what_is_this):\n"
                                                  "    pass")
        with self.assertRaisesRegexp(Exception, "create_analysis\(\) takes exactly 3 arguments \(2 given\)"):
            self.run_analysis_template_args(['--module_name', module_name])

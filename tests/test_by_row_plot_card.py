import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa

import unittest

from etestutils import EureqaTestBase

class TestTwoVariablePlot(unittest.TestCase, EureqaTestBase):
    @classmethod
    def setUpClass(cls):
        assert len(cls._variables) >= 4
        cls._x_var = cls._variables[0]
        cls._y_vars = cls._variables[1:4]

    def test_create(self):
        card = self._analysis.create_by_row_plot_card(self._data_source,
                                                      self._x_var, self._y_vars,
                                                      "Test Card",
                                                      "Test Card Description",
                                                      self._y_vars[0],
                                                      False, True, False)

        self.assertEqual(card.title, "Test Card")
        self.assertEqual(card.description, "Test Card Description")
        self.assertEqual(card.focus_variable, self._y_vars[0]),
        self.assertEqual(card.x_var, self._x_var)
        for elt in card.plotted_variables:
            self.assertIn(elt, self._y_vars)
        self.assertEqual(card.should_center, False)
        self.assertEqual(card.should_scale, True)
        self.assertEqual(card.collapse, False)

    def test_update_get(self):
        card = self._analysis.create_by_row_plot_card(self._data_source,
                                                      self._x_var, self._y_vars,
                                                      "Test Card",
                                                      "Test Card Description",
                                                      None,
                                                      False, True, False)

        card.title = "Test Updated TwoVariablePlot Card"
        card.description = "Test Updated Description"
        card.focus_variable = self._y_vars[1]
        card.x_var = self._x_var
        card.plotted_variables = [ self._x_var ] + self._y_vars[1:]
        card.should_center = True
        card.should_scale = False
        card.collapse = True

        self.assertEqual(card.title, "Test Updated TwoVariablePlot Card")
        self.assertEqual(card.description, "Test Updated Description")
        self.assertEqual(card.focus_variable, self._y_vars[1])
        self.assertEqual(card.x_var, self._x_var)
        self.assertEqual(card.plotted_variables[0], self._x_var)
        self.assertEqual(card.should_center, True)
        self.assertEqual(card.should_scale, False)
        self.assertEqual(card.collapse, True)

        fetched_cards = [x for x in self._analysis.get_cards()
                         if getattr(x, "title", None) == 'Test Updated TwoVariablePlot Card']
        self.assertEqual(len(fetched_cards), 1)
        fetched_card = fetched_cards[0]

        self.assertEqual(card.title, fetched_card.title)
        self.assertEqual(card.description, fetched_card.description)
        self.assertEqual(card.focus_variable, fetched_card.focus_variable)
        self.assertEqual(card.x_var, fetched_card.x_var)
        self.assertEqual(card.plotted_variables, fetched_card.plotted_variables)
        self.assertEqual(card.should_center, fetched_card.should_center)
        self.assertEqual(card.should_scale, fetched_card.should_scale)
        self.assertEqual(card.collapse, fetched_card.collapse)

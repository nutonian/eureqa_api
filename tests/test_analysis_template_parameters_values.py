import sys
import os
sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa.analysis_templates import ParametersValues, TextParameterValue, TopLevelModelParameterValue, DataSourceParameterValue, VariableParameterValue, DataFileParameterValue, ComboBoxParameterValue, NumericParameterValue, Parameters

from etestutils import EureqaTestBase

import json
import unittest


class TestAnalysisTemplateParametersValues(unittest.TestCase, EureqaTestBase):

    def test_from_json(self):
        body = json.loads('{"parameters": '
                                '[{"id": "abc", "type":"text", "value": "This is the text input value."}, '
                                '{"id": "def", "type":"top_level_model", "value": "a + 2"}, '
                                '{"id": "ghi", "type":"top_level_model", "value": "a + b", "datasource_id": "SimpleDataSource", "search_id": "0", "solution_id": "0"}, '
                                '{"id": "123", "type":"datasource", "value": "SimpleDataSource", "parameters": '
                                    '[{"id": "345", "type":"variable", "value": "value 345"}]}, '
                                    '{"id": "jkl", "type":"data_file", "value": "file-0x700009c2c000-1422169630860595", "file_path" : "path/to/file.csv"}, '
                                '{"id": "mno", "type":"combo_box", "value": "a"}, '
                                '{"id": "pqr", "type":"numeric", "value": 7}]'
                          '}')
        parameters = ParametersValues._from_json(self._eureqa, body)

        self.assertEqual(len(parameters.parameters), 7)
        text_input = parameters.parameters.values()[0]
        self.assertEqual(text_input.id, 'abc')
        self.assertEqual(text_input.value, 'This is the text input value.')
        self.assertEqual(text_input._type, 'text')
        model_input_1 = parameters.parameters.values()[1]
        self.assertEqual(model_input_1.id, 'def')
        self.assertEqual(model_input_1.value, 'a + 2')
        self.assertEqual(model_input_1._type, 'top_level_model')
        model_input_2 = parameters.parameters.values()[2]
        self.assertEqual(model_input_2.id, 'ghi')
        self.assertEqual(model_input_2.value, 'a + b')
        self.assertEqual(model_input_2._type, 'top_level_model')
        self.assertEqual(model_input_2.datasource_id, 'SimpleDataSource')
        self.assertEqual(model_input_2.search_id, '0')
        self.assertEqual(model_input_2.solution_id, '0')
        self.assertEqual(model_input_2.get_solution()._id, '0')
        self.assertEqual(model_input_2.get_solution().search._id, '0')
        self.assertEqual(model_input_2.get_solution().search._data_source_id, 'SimpleDataSource')
        data_source = parameters.parameters.values()[3]
        self.assertEqual(data_source.id, '123')
        self.assertEqual(data_source.value, 'SimpleDataSource')
        self.assertEqual(data_source._type, 'datasource')
        self.assertEqual(data_source.get_data_source().name, 'SimpleDataSource')
        self.assertEqual(len(data_source.variables), 1)
        variable = data_source.variables[0]
        self.assertEqual(variable.id, '345')
        self.assertEqual(variable.value, 'value 345')
        self.assertEqual(variable._type, 'variable')
        data_file = parameters.parameters.values()[4]
        self.assertEqual(data_file.id, 'jkl')
        self.assertEqual(data_file.value, 'file-0x700009c2c000-1422169630860595')
        self.assertEqual(data_file.file_path, 'path/to/file.csv')
        self.assertEqual(data_file._type, 'data_file')
        combo_box = parameters.parameters.values()[5]
        self.assertEqual(combo_box.id, 'mno')
        self.assertEqual(combo_box.value, 'a')
        self.assertEqual(combo_box._type, 'combo_box')
        numeric_input = parameters.parameters.values()[6]
        self.assertEqual(numeric_input.id, 'pqr')
        self.assertEqual(numeric_input.value, 7)
        self.assertEqual(numeric_input._type, 'numeric')

    def test_from_json_text_multiline(self):
        body = json.loads('{"parameters": '
                                '[{"id": "abc", "type": "text_multiline", "value": "This\\nis\\nthe\\ntext\\ninput\\nvalue."}]'
                          '}')
        parameters = ParametersValues._from_json(self._eureqa, body)

        self.assertEqual(len(parameters.parameters.values()), 1)
        text_input = parameters.parameters.values()[0]
        self.assertEqual(text_input.id, 'abc')
        self.assertEqual(text_input.value, 'This\nis\nthe\ntext\ninput\nvalue.')
        self.assertEqual(text_input._type, 'text_multiline')

    def test_from_json_bad_type(self):
        body = json.loads('{"parameters": '
                                '[{"id": "abc", "value": "This is the text input label.", "type":"BAD_TYPE"}]'
                          '}')
        try:
            parameters = ParametersValues._from_json(self._eureqa, body)
            self.fail("Parameter parsing should have thrown an exception.")
        except Exception as e:
            self.assertEqual(str(e), "Invalid analysis template parameter value type 'BAD_TYPE' for parameter 'abc' with value 'This is the text input label.'");

    def test_to_json(self):
        parameters = ParametersValues(
            [TextParameterValue(self._eureqa, 'abc','This is the text input value.'),
             TopLevelModelParameterValue(self._eureqa, "def", "a + 2", None, None, None),
             TopLevelModelParameterValue(self._eureqa, "ghi", "a + b", "SimpleDataSource", "0", "0"),
             DataSourceParameterValue(self._eureqa, '123', 'SimpleDataSource',
                {VariableParameterValue(self._eureqa, '345', 'value 345')}),
             DataFileParameterValue(self._eureqa, 'jkl', '/path/to/file'),
             ComboBoxParameterValue(self._eureqa, 'mno','a'),
             NumericParameterValue(self._eureqa, 'pqr',7)]
        )
        body = parameters._to_json()
        text = json.dumps(body, sort_keys=True)
        self.assertEqual(text, '{"parameters": '
                                    '[{"id": "abc", "type": "text", "value": "This is the text input value."}, '
                                    '{"id": "def", "type": "top_level_model", "value": "a + 2"}, '
                                    '{"datasource_id": "SimpleDataSource", "id": "ghi", "search_id": "0", "solution_id": "0", "type": "top_level_model", "value": "a + b"}, '
                                    '{"id": "123", "parameters": '
                                        '[{"id": "345", "type": "variable", "value": "value 345"}], '
                                    '"type": "datasource", "value": "SimpleDataSource"}, '
                                    '{"file_path": "/path/to/file", "id": "jkl", "type": "data_file", "value": null}, '
                                    '{"id": "mno", "type": "combo_box", "value": "a"}, '
                                    '{"id": "pqr", "type": "numeric", "value": 7}]'
                               '}')

    def test_to_json_text_multiline(self):
        parameters = ParametersValues([TextParameterValue(self._eureqa, 'abc','This\nis\nthe\nmultiline\ntext\ninput\nvalue.', text_multiline=True)])
        body = parameters._to_json()
        text = json.dumps(body, sort_keys=True)
        self.assertEqual(text, '{"parameters": '
                                    '[{"id": "abc", "type": "text_multiline", "value": "This\\nis\\nthe\\nmultiline\\ntext\\ninput\\nvalue."}]'
                               '}')

    def test_to_string(self):
        parameters = ParametersValues(
            [TextParameterValue(self._eureqa, 'abc','This is the text input value.'),
             TopLevelModelParameterValue(self._eureqa, "def", "a + 2", None, None, None),
             TopLevelModelParameterValue(self._eureqa, "ghi", "a + b", "SimpleDataSource", "0", "0"),
             DataSourceParameterValue(self._eureqa, '123', 'SimpleDataSource',
                {VariableParameterValue(self._eureqa, '345', 'value 345')}),
             DataFileParameterValue(self._eureqa, 'jkl','path'),
             ComboBoxParameterValue(self._eureqa, 'mno','a'),
             NumericParameterValue(self._eureqa, 'pqr',7)]
        )
        parameters.__str__()

    def test_add_data_file_to_server(self):
        template = self._eureqa.create_analysis_template(
            "Test_data_file_template",
            "Test description",
            Parameters([]),
            None)
        dfpv = DataFileParameterValue(self._eureqa, 'jkl','./Nutrition.csv')
        self.assertIsNone(dfpv.value)
        dfpv._add_file_to_template(template)
        self.assertIsNotNone(dfpv.value)

        with self.assertRaises(Exception):
            dfpv._add_file_to_template("not_a_template_id")
        

        
        

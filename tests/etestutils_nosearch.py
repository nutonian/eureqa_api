import os
import sys
import atexit
import urllib2
import threading
import subprocess
import eureqa

# Eureqa URL.  Get this from an environment variable if set; otherwise assume the local machine.
_eureqa_url = os.environ.get("EUREQAURL", "http://localhost:10002")

def _create_eeb_server():
    try:
        x = urllib2.urlopen(_eureqa_url)
        print "Using existing eeb instance"
        return None  ## Server must already be open
    except:
        assert "EUREQAURL" not in os.environ, "No running eeb detected at the specified URL [%s].  Please specify a different URL or start an app server at that URL." % _eureqa_url
        print "No running eeb detected.  Starting a new one."

    build_dir = os.path.abspath(os.path.join('..', '..', 'Debug', 'eureqa_application_server', 'Debug'))
    if not os.path.exists(build_dir):
        # Try with less nesting. (Often true on non-Windows.)
        build_dir = os.path.abspath(os.path.join('..', '..', 'Debug', 'eureqa_application_server'))
    cmd = [os.path.join(build_dir, 'eureqa_application_server'),
           "--two_factor_auth_test_password", "one-time_password"]
    print "$ %s" % ' '.join(cmd)

    with open("eeb.log", "ab") as stderr:
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=stderr, cwd=build_dir)

    while proc.stdout:
        line = proc.stdout.readline()
        sys.stdout.write("EEB => " + line)
        if "Hit CTRL-C / SIGINT to shutdown" in line:
            # Server has reached the "started-up" stage
            # Register some handlers to clean up after it,
            # then exit.

            def _eeb_logger_thread():
                with open("eeb.log","ab") as f:
                    while proc.stdout:
                        line = proc.stdout.readline()
                        f.write(line)

            def _kill_eeb_at_exit():
                print "Killing eeb process"
                proc.terminate()

            t = threading.Thread(target = _eeb_logger_thread)
            t.daemon = True
            t.start()
            atexit.register(_kill_eeb_at_exit)

            return proc

    assert False, "eeb server process died early"


class EureqaTestBaseNoSearch(object):
    """
    EureqaTestBase without the search object or solutions

    Expensive objects are constructed exactly once, so that
    each test doesn't have to make a new one.
    """

    """ `subprocess.Proc` instance pointing to the running eeb instance, if we started it.
    This field mostly shouldn't be used directly; it's there because its initializer starts an eeb."""
    _eeb = _create_eeb_server()

    """ Persistent connection to the eureqa server """
    _eureqa = eureqa.Eureqa(url=_eureqa_url, user_name='root', password='Changeme1', organization='root')

    """
    Simple data source.  5 rows with the following columns:
       x             -- values {1..5}
       inc_x         -- x + 1
       dbl_x         -- x * 2
       x_plus_three  -- x + 3
    """
    _data_source = _eureqa.create_data_source('SimpleDataSource', 'simple_data.csv')

    """ Variables from _data_source """
    _variables = _data_source.get_variables()

    """ Search settings used to construct _search (see below) """
    _settings = _eureqa.search_templates.numeric('Analysis Search', _variables[0], _variables[1:])

    """ Empty example analysis """
    _analysis = _eureqa.create_analysis('Test By Row Plot Analysis')

    """ URL to log into the Eureqa app server """
    _eureqa_url = _eureqa_url

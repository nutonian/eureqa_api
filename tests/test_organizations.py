import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa
import eureqa.session as session
from eureqa.organization import _Organization

from etestutils_nosearch import EureqaTestBaseNoSearch

import tempfile
import unittest

class TestOrganizations(unittest.TestCase, EureqaTestBaseNoSearch):
    @classmethod
    def setUpClass(cls):
        #Removes the Test Organization if it is present before the test.
        conn = Eureqa(url=cls._eureqa_url, user_name='root', password='Changeme1')

        # Clean up from previous runs if necessary; ignore if not.
        try:
            conn._delete_organization("_Test_Organization")
        except session.Http404Exception as e:
            pass

        try:
            conn._delete_organization("org_test_sandboxed")
        except session.Http404Exception as e:
            pass

        try:
            conn._delete_organization("org_test_export")
        except session.Http404Exception as e:
            pass

    def test_organization_interactions(self):
        e = Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1')

        e._create_organization("_Test_Organization")

        # Ensure the new organization shows up
        org_ids = e._get_organization_ids()
        self.assertTrue('root' in org_ids)
        self.assertTrue("_Test_Organization" in org_ids)

        #tests that you can get the new organization
        org = e._get_organization("_Test_Organization")

        #tests that you cannot create a duplicate organization
        with self.assertRaises(Exception):
            e._create_organization("_Test_Organization")

        e._delete_organization("_Test_Organization")

        #tests you can no longer get the deleted organization
        with self.assertRaises(Exception):
            org = e._get_organization("_Test_Organization")

        #tests you cannot delete an organization that no longer exists
        with self.assertRaises(Exception):
            e._delete_organization("_Test_Organization")

        org_ids = e._get_organization_ids()
        self.assertTrue('root' in org_ids)
        self.assertFalse("_Test_Organization" in org_ids)

        # add organization from eqx file
        e._create_organization('_Test_Organization_from_eqx', 'test.eqx')

        # Ensure the new organization shows up
        org_ids = e._get_organization_ids()
        self.assertTrue('_Test_Organization_from_eqx' in org_ids)

        # Check getting users when there are none returns an empty iterable object
        org = e._get_organization('_Test_Organization_from_eqx')
        org_users = org._get_all_users()
        self.assertEqual(0, len(org_users))

        # Ensure the new organization has three datasources,
        # not just 2 like in the default
        temp_e = Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1', organization='_Test_Organization_from_eqx')
        self.assertTrue(len(temp_e.get_all_data_sources())==3)

        # drop organization created from eqx
        e._delete_organization('_Test_Organization_from_eqx')

        org_ids = e._get_organization_ids()
        self.assertTrue('root' in org_ids)
        self.assertFalse('_Test_Organization_from_eqx' in org_ids)

        # Check getting users works properly
        org = e._get_organization('root')
        org_users = org._get_all_users()
        self.assertGreater(len(org_users), 0)
        self.assertTrue('root' in org_users)

    def test_load_eqx(self):
        e = Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1')
        org = _Organization(e, "")

        org._load_eqx_file("sandboxed_ai_app.eqx", "org_test_sandboxed")

        # Log into new org
        e = Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1', organization="org_test_sandboxed")

        # Check whether we successfully loaded the org
        # (Org should contain one sandboxed AI App, plus the built-in App)
        apps = e.get_all_analysis_templates()
        self.assertEqual(len(apps), 2)

        # While we're at it, make sure 'sandboxed' setting is set properly.
        # Org was saved on a cloud cluster, but we're running locally
        # without Docker configured.
        # So, should not be sandboxed.
        for app in apps:
            self.assertFalse(app._body.get("sandboxed"))

    def test_export_eqx(self):
        e = Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1')
        org = _Organization(e, "")


        org._load_eqx_file("sandboxed_ai_app.eqx", "org_test_export")

        filename = tempfile.mktemp()
        e._get_organization("org_test_export")._export_eqx_file(filename)
        # We don't have code to meaningfully diff .eqx files yet.
        # For now, just confirm that something was downloaded.
        self.assertGreater(os.stat(filename).st_size, 0)
        os.remove(filename)

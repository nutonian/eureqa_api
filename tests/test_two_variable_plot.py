import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa

import unittest
import time

from eureqa.analysis.components import *
from etestutils import EureqaTestBase

class TestTwoVariablePlot(unittest.TestCase, EureqaTestBase):
    @classmethod
    def setUpClass(cls):
        assert len(cls._variables) >= 4
        cls._x_var = cls._variables[0]
        cls._y_var = cls._variables[1]
        cls._x_var_2 = cls._variables[2]
        cls._y_var_2 = cls._variables[3]

    def test_create_tvp(self):
        card = self._analysis._create_two_variable_plot_card(BoxPlot,
                                                             self._data_source,
                                                             self._x_var,
                                                             self._y_var,
                                                             "Test Plot",
                                                             "Test Plot Description",
                                                             False,
                                                             {'x': 'X Axis',
                                                              'y': 'Y Axis'},
                                                             {'x': '.2f',
                                                              'y': '.4f'},
                                                              False)

        self.assertEqual(card.title, "Test Plot")
        self.assertEqual(card.description, "Test Plot Description")
        self.assertEqual(card.x_var, self._x_var)
        self.assertEqual(card.y_var, self._y_var)
        self.assertEqual(card.needs_guides, False)
        self.assertEqual(card.axis_labels.x, 'X Axis')
        self.assertEqual(card.axis_labels.y, 'Y Axis')
        self.assertEqual(card.label_format.x, '.2f')
        self.assertEqual(card.label_format.y, '.4f')
        self.assertEqual(card.collapse, False)
        
    def test_create_all_types(self):
        card_ctors = [
            self._analysis.create_box_plot_card,
            self._analysis.create_double_histogram_plot_card,
            self._analysis.create_scatter_plot_card,
            self._analysis.create_binned_mean_plot_card
            ]

        cards = [x(self._data_source,
                   self._x_var, self._y_var,
                   "Test Plot for %s" % repr(x.im_func.func_name),
                   "Test Plot Description",
                   False,
                   {'x': 'X Axis', 'y': 'Y Axis'},
                   {'x': '.2f', 'y': '.4f'}, False)
                   for x in card_ctors]

        for card, fn in zip(cards, card_ctors):
            self.assertEqual(card.title, "Test Plot for %s" % repr(fn.im_func.func_name))
            self.assertEqual(card.description, "Test Plot Description")
            self.assertEqual(card.x_var, self._x_var)
            self.assertEqual(card.y_var, self._y_var)
            self.assertEqual(card.needs_guides, False)
            self.assertEqual(card.axis_labels.x, 'X Axis')
            self.assertEqual(card.axis_labels.y, 'Y Axis')
            self.assertEqual(card.label_format.x, '.2f')
            self.assertEqual(card.label_format.y, '.4f')
            self.assertEqual(card.collapse, False)

    def test_create_all_types_default_labels(self):

        current_defaults = {'create_box_plot_card': { 'default_label' : { 'x': self._x_var, 'y': self._y_var },
                                                      'default_format': { 'x': 'g',   'y': '.2s' }},

                            'create_double_histogram_plot_card': {'default_label' : { 'x': [self._x_var, self._y_var], 'y': 'number of values' },
                                                                  'default_format': { 'x': 'g', 'y': '.3s' }},

                            'create_scatter_plot_card': {'default_label' : { 'x': self._x_var, 'y': self._y_var },
                                                         'default_format': { 'x': '.3s', 'y': '.3s' }},

                            'create_binned_mean_plot_card': {'default_label' : { 'x': self._x_var, 'y': self._y_var },
                                                             'default_format': { 'x': '.3s', 'y': '.2f' }}}

        card_ctors = [
            self._analysis.create_box_plot_card,
            self._analysis.create_double_histogram_plot_card,
            self._analysis.create_scatter_plot_card,
            self._analysis.create_binned_mean_plot_card
            ]

        cards = [x(self._data_source,
                   self._x_var, self._y_var,
                   "Test Plot for %s" % repr(x.im_func.func_name),
                   "Test Plot Description",
                   False, collapse=False)
                   for x in card_ctors]

        for card, fn in zip(cards, card_ctors):
            self.assertEqual(card.title, "Test Plot for %s" % repr(fn.im_func.func_name))
            self.assertEqual(card.description, "Test Plot Description")
            self.assertEqual(card.x_var, self._x_var)
            self.assertEqual(card.y_var, self._y_var)
            self.assertEqual(card.needs_guides, False)
            self.assertEqual(card.axis_labels.x, current_defaults[fn.im_func.func_name]['default_label']['x'])
            self.assertEqual(card.axis_labels.y, current_defaults[fn.im_func.func_name]['default_label']['y'])
            self.assertEqual(card.label_format.x, current_defaults[fn.im_func.func_name]['default_format']['x'])
            self.assertEqual(card.label_format.y, current_defaults[fn.im_func.func_name]['default_format']['y'])
            self.assertEqual(card.collapse, False)

    def test_construct_all_types(self):

        constructors = [
            BoxPlot,
            DoubleHistogramPlot,
            ScatterPlot,
            BinnedMeanPlot
            ]

        plots = [x(datasource = self._data_source,
                   x_var = self._x_var, y_var = self._y_var,
                   axis_labels = {'x': 'X Axis', 'y': 'Y Axis'},
                   label_format = {'x': '.2f', 'y': '.4f'})
                   for x in constructors]

        for plot in plots:
            self.assertEqual(plot.x_var, self._x_var)
            self.assertEqual(plot.y_var, self._y_var)
            self.assertEqual(plot.axis_labels.x, 'X Axis')
            self.assertEqual(plot.axis_labels.y, 'Y Axis')
            self.assertEqual(plot.label_format.x, '.2f')
            self.assertEqual(plot.label_format.y, '.4f')

    def test_construct_all_types_default_args(self):

        current_defaults = {'BoxPlot':           {'default_label' : { 'x': self._x_var, 'y': self._y_var },
                                                  'default_format': { 'x': 'g',   'y': '.2s' }},

                            'DoubleHistogramPlot':{'default_label' : { 'x': [self._x_var, self._y_var], 'y': 'number of values' },
                                                   'default_format': { 'x': 'g', 'y': '.3s' }},

                            'ScatterPlot':        {'default_label' : { 'x': self._x_var, 'y': self._y_var },
                                                   'default_format': { 'x': '.3s', 'y': '.3s' }},

                            'BinnedMeanPlot':     {'default_label' : { 'x': self._x_var, 'y': self._y_var },
                                                   'default_format': { 'x': '.3s', 'y': '.2f' }}}

        constructors = [
            BoxPlot,
            DoubleHistogramPlot,
            ScatterPlot,
            BinnedMeanPlot
            ]

        plots = [x(datasource = self._data_source,
                   x_var = self._x_var, y_var = self._y_var)
                   for x in constructors]

        for plot, con in zip(plots, constructors):
            self.assertEqual(plot.x_var, self._x_var)
            self.assertEqual(plot.y_var, self._y_var)
            self.assertEqual(plot.axis_labels.x, current_defaults[con.__name__]['default_label']['x'])
            self.assertEqual(plot.axis_labels.y, current_defaults[con.__name__]['default_label']['y'])
            self.assertEqual(plot.label_format.x, current_defaults[con.__name__]['default_format']['x'])
            self.assertEqual(plot.label_format.y, current_defaults[con.__name__]['default_format']['y'])


    def test_update_get(self):
        card = self._analysis._create_two_variable_plot_card(BoxPlot,
                                                             self._data_source,
                                                             self._x_var,
                                                             self._y_var,
                                                             "Test Plot",
                                                             "Test Plot Description",
                                                             False,
                                                             {'x': 'X Axis',
                                                              'y': 'Y Axis'},
                                                             {'x': '.2f',
                                                              'y': '.4f'},
                                                              False)

        card.title = "-Test Updated Plot-"
        card.description = "Test Updated Plot Description"
        card.x_var = self._x_var_2
        card.y_var = self._y_var_2
        card.needs_guides = True
        card.axis_labels.x = 'Another X Axis'
        card.axis_labels.y = 'Another Y Axis'
        card.label_format.x = '.1f'
        card.label_format.y = '.5f'
        card.collapse = True

        self.assertEqual(card.title, "-Test Updated Plot-")
        self.assertEqual(card.description, "Test Updated Plot Description")
        self.assertEqual(card.x_var, self._x_var_2)
        self.assertEqual(card.y_var, self._y_var_2)
        self.assertEqual(card.needs_guides, True)
        self.assertEqual(card.axis_labels.x, 'Another X Axis')
        self.assertEqual(card.axis_labels.y, 'Another Y Axis')
        self.assertEqual(card.label_format.x, '.1f')
        self.assertEqual(card.label_format.y, '.5f')
        self.assertEqual(card.collapse, True)

        fetched_cards = [x for x in self._analysis.get_cards() if getattr(x, 'title', None) == '-Test Updated Plot-']
        self.assertEqual(len(fetched_cards), 1)
        fetched_card = fetched_cards[0]

        self.assertEqual(card.title, fetched_card.title)
        self.assertEqual(card.description, fetched_card.description)
        self.assertEqual(card.x_var, fetched_card.x_var)
        self.assertEqual(card.y_var, fetched_card.y_var)
        self.assertEqual(card.needs_guides, fetched_card.needs_guides)
        self.assertEqual(card.axis_labels.x, fetched_card.axis_labels.x)
        self.assertEqual(card.axis_labels.y, fetched_card.axis_labels.y)
        self.assertEqual(card.label_format.x, fetched_card.label_format.x)
        self.assertEqual(card.label_format.y, fetched_card.label_format.y)
        self.assertEqual(card.collapse, fetched_card.collapse)

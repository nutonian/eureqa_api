import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa, DataSource

import unittest

from datetime import datetime
from etestutils_nosearch import EureqaTestBaseNoSearch

class TestVariableDetails(unittest.TestCase, EureqaTestBaseNoSearch):
    def test_value_formatting(self):
        data_source = self._eureqa.create_data_source('Test', 'Nutrition.csv')
        numeric_var = data_source.get_variable_details('Calories')
        datetime_var = data_source.get_variable_details('Date')

        self.assertEqual(numeric_var.min_value, 893)
        self.assertEqual(numeric_var.max_value, 4264)
        self.assertAlmostEqual(numeric_var.mean_value, 2116.3333333333335)
        self.assertEqual(numeric_var._min_value, 893)
        self.assertEqual(numeric_var._max_value, 4264)
        self.assertAlmostEqual(numeric_var._mean_value, 2116.3333333333335)

        self.assertEqual(datetime_var._min_value, 1404345600000)
        self.assertEqual(datetime_var._max_value, 1415664000000)
        self.assertEqual(datetime_var._mean_value, 1410048000000)
        self.assertEqual(datetime_var.min_value, datetime(2014, 7, 3, 0, 0))
        self.assertEqual(datetime_var.max_value, datetime(2014, 11, 11, 0, 0))
        self.assertEqual(datetime_var.mean_value, datetime(2014, 9, 7, 0, 0))

        #test that printing VariableDetails doesn't throw an exception
        numeric_var.__str__()

        self.assertIn(str(datetime_var.min_value),datetime_var.__str__())
        self.assertIn(str(datetime_var.max_value),datetime_var.__str__())
        self.assertIn(str(datetime_var.mean_value),datetime_var.__str__())

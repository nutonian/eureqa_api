import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa.analysis.components import *

from etestutils_nosearch import EureqaTestBaseNoSearch

import unittest

class TestAnalysisComponent(unittest.TestCase, EureqaTestBaseNoSearch):

    def test_empty_text(self):
        # Pick a type arbitrarily
        p = TextBlock(_analysis=self._analysis)
        self.assertEqual(p._analysis, self._analysis)
        self.assertIsNotNone(p._component_id)
        self.assertEqual(p.text, '')

    def test_empty_text_and_populated_description(self):
        # Pick a type arbitrarily
        p = TextBlock(_analysis=self._analysis, description = 'Some')
        self.assertEqual(p._analysis, self._analysis)
        self.assertIsNotNone(p._component_id)
        self.assertEqual(p.text, 'Some')

    def test_populated_text_and_populated_description(self):
        # Pick a type arbitrarily
        p = TextBlock(_analysis=self._analysis, text = 'Some', description = 'Other')
        self.assertEqual(p._analysis, self._analysis)
        self.assertIsNotNone(p._component_id)
        self.assertEqual(p.text, 'Some')

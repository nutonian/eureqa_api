import sys
import os
import time

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa
from eureqa.analysis_cards.plot import Plot
from eureqa.analysis_cards.custom_plot_card import CustomPlotCard

import unittest

from etestutils import EureqaTestBase

class TestCustomPlotCard(unittest.TestCase, EureqaTestBase):
    def test_create(self):
        plot = Plot(x_axis_label="X axis label", y_axis_label="Y axis label")
        plot.plot([1,2,3],[4,5,6],legend_label='Test Plot Legend Label')
        card = self._analysis.create_custom_plot_card(plot, title='Test Plot Card Title', description='Test Plot Card Description')
        self.assertEqual(card.title, 'Test Plot Card Title')
        self.assertEqual(card.description, 'Test Plot Card Description')
        self.assertEqual(card.collapse, False)

        # verify the data was uploaded behind the scenes
        self.assertTrue(plot._datasource)
        self.assertEqual(plot._datasource.get_variables(), ['(Series 1 X)', '(Series 1 Y)'])
        self.assertEqual(plot._datasource.evaluate_expression('(Series 1 X)')['(Series 1 X)'], [1,2,3])
        self.assertEqual(plot._datasource.evaluate_expression('(Series 1 Y)')['(Series 1 Y)'], [4,5,6])

    def test_get(self):
        original_plot = Plot(x_axis_label="X axis label", y_axis_label="Y axis label")
        original_plot.plot([1,2,3],[4,5,6],legend_label='Test Plot Legend Label')
        original_card = self._analysis.create_custom_plot_card(original_plot,
                                                        title='Test Plot Card Title -- Get',
                                                        description='Test Plot Card Description')

        cards = self._analysis.get_cards()
        cards = [c for c in cards if getattr(c, 'title', None) == 'Test Plot Card Title -- Get']
        self.assertEqual(len(cards), 1)
        card = cards[0]
        self.assertEqual(card.title, 'Test Plot Card Title -- Get')
        self.assertEqual(card.description, 'Test Plot Card Description')
        self.assertEqual(card.collapse, False)

        self.assertEqual(card.plot._options['plot']['x_axis_label'], 'X axis label')
        self.assertEqual(card.plot._options['plot']['y_axis_label'], 'Y axis label')

    def test_delete(self):
        plot = Plot(x_axis_label="X axis label", y_axis_label="Y axis label")
        plot.plot([1,2,3],[4,5,6],legend_label='Test Plot Legend Label')
        card = self._analysis.create_custom_plot_card(plot, title='Test Plot Card Title', description='Test Plot Card Description')
        datasource_id = plot._datasource._data_source_id
        self.assertTrue(self._eureqa.get_data_source_by_id(datasource_id) is not None)

        card.delete()
        with self.assertRaisesRegexp(Exception, "Unknown datasource_id: '%s'" % datasource_id):
            self._eureqa.get_data_source_by_id(datasource_id)

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.

import eureqa
import unittest
from eureqa.analysis.components import *

from etestutils_nosearch import EureqaTestBaseNoSearch

class TestVariableLinkComponent(unittest.TestCase, EureqaTestBaseNoSearch):

    @classmethod
    def setUpClass(cls):
        cls._data_source_2 = cls._eureqa.create_data_source('SimpleDataSource', 'simple_data.csv')

    def test_component(self):
        analysis = self._analysis
        vl = VariableLink(datasource=self._data_source, variable_name=self._variables[0])
        card = analysis.create_html_card("Target variable is: {0}".format(analysis.html_ref(vl)))

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(self._variables[0], vl.variable_name)
        self.assertEqual(self._data_source._data_source_id, vl.datasource._data_source_id)

        # Make sure we can fetch all fields from scratch
        vl = analysis._get_component_by_id(vl._component_id)
        self.assertEqual(self._variables[0], vl.variable_name)
        self.assertEqual(self._data_source._data_source_id, vl.datasource._data_source_id)

        # ensure we made the html that we expected
        content = card.component.content # is a html content
        self.assertEqual(content.html, u'Target variable is: <nutonian-component id="{0}" />'.format(vl._component_id))

        # test updating fields via setters and ensure they match
        vl.datasource = self._data_source_2
        vl.variable_name = self._variables[1]
        self.assertEqual(self._variables[1], vl.variable_name)
        self.assertEqual(self._data_source_2._data_source_id, vl.datasource._data_source_id)

        # refetch
        vl = analysis._get_component_by_id(vl._component_id)
        self.assertEqual(self._variables[1], vl.variable_name)
        self.assertEqual(self._data_source_2._data_source_id, vl.datasource._data_source_id)

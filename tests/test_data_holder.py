import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa.utils.data_holder import DataHolder

import unittest

class TestDataHolder(unittest.TestCase):
    def test_basic(self):
        dataHolder = DataHolder()
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual(csvBuf.getvalue(), "\r\n")
        self.assertTrue(len(csvBuf.name) > 4)
        self.assertEqual(csvBuf.name[-4:], ".csv")

        dataHolder.add_column("(Series 1 X)", [0, 1, 2])
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual("(Series 1 X)\r\n0\r\n1\r\n2\r\n", csvBuf.getvalue())
        self.assertTrue(len(csvBuf.name) > 4)
        self.assertEqual(csvBuf.name[-4:], ".csv")

        dataHolder.add_column("(Series 1 Y)",[3, 4, 5])
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual("(Series 1 X),(Series 1 Y)\r\n0,3\r\n1,4\r\n2,5\r\n", csvBuf.getvalue())
        self.assertTrue(len(csvBuf.name) > 4)
        self.assertEqual(csvBuf.name[-4:], ".csv")

    def test_invalid(self):
        dataHolder = DataHolder()
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual(csvBuf.getvalue(), "\r\n")
        self.assertTrue(len(csvBuf.name) > 4)
        self.assertEqual(csvBuf.name[-4:], ".csv")

        dataHolder.add_column("(Series 1 X)", [])
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual('(Series 1 X)\r\n', csvBuf.getvalue())

        with self.assertRaisesRegexp(Exception, "Error in DataHolder.add_column: all input values must be lists, not "):
            dataHolder.add_column('dummy name', {'a':'b'})
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual('(Series 1 X)\r\n', csvBuf.getvalue())

    def test_duplicate(self):
        dataHolder = DataHolder()
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual(csvBuf.getvalue(), "\r\n")
        self.assertTrue(len(csvBuf.name) > 4)
        self.assertEqual(csvBuf.name[-4:], ".csv")

        dataHolder.add_column("(Series 1 X)", [1, 2, 3])
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual('(Series 1 X)\r\n1\r\n2\r\n3\r\n', csvBuf.getvalue())

        dataHolder.add_column("(Series 1 X)", [4, 5])
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual('(Series 1 X)\r\n4\r\n5\r\n', csvBuf.getvalue())

    def test_add_smaller(self):
        dataHolder = DataHolder()
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual(csvBuf.getvalue(), "\r\n")
        self.assertTrue(len(csvBuf.name) > 4)
        self.assertEqual(csvBuf.name[-4:], ".csv")

        # first start with a large series
        dataHolder.add_column('A', [0, 1, 2, 3, 4, 5])
        # add a small series -- should get padded with nans
        dataHolder.add_column('B', [0, 1, 2])
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual("A,B\r\n0,0\r\n1,1\r\n2,2\r\n3,\r\n4,\r\n5,\r\n", csvBuf.getvalue())

    def test_add_larger(self):
        dataHolder = DataHolder()
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual(csvBuf.getvalue(), "\r\n")
        self.assertTrue(len(csvBuf.name) > 4)
        self.assertEqual(csvBuf.name[-4:], ".csv")

        # first start with a small series
        dataHolder.add_column('A', [0, 1, 2])
        # add a large series -- original series should get padded with nans
        dataHolder.add_column('B', [0, 1, 2, 3, 4, 5])
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual("A,B\r\n0,0\r\n1,1\r\n2,2\r\n,3\r\n,4\r\n,5\r\n", csvBuf.getvalue())

        # add another small series
        dataHolder.add_column('C', [0, 1, 2, 3])
        csvBuf = dataHolder.get_csv_file()
        self.assertEqual("A,B,C\r\n0,0,0\r\n1,1,1\r\n2,2,2\r\n,3,3\r\n,4,\r\n,5,\r\n", csvBuf.getvalue())

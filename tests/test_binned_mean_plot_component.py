# Copyright (c) 2017, Nutonian Inc
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the Nutonian Inc nor the
#     names of its contributors may be used to endorse or promote products
#     derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL NUTONIAN INC BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa

import unittest

from etestutils_nosearch import EureqaTestBaseNoSearch
from eureqa.analysis.components import BinnedMeanPlot, _Component

class TestBinnedMeanPlotComponent(unittest.TestCase, EureqaTestBaseNoSearch):
    @classmethod
    def setUpClass(cls):
        assert len(cls._variables) >= 4
        cls._x_var = cls._variables[0]
        cls._y_var = cls._variables[1]
        cls._x_var_2 = cls._variables[2]
        cls._y_var_2 = cls._variables[3]

        cls._data_source_2 = cls._eureqa.create_data_source('SimpleDataSource', 'simple_data.csv')

    def test_component(self):
        comp = BinnedMeanPlot(self._data_source,
                              {'x': 'X Axis',
                               'y': 'Y Axis'},
                              {'x': '.2f',
                               'y': '.4f'},
                              False,
                              self._x_var,
                              self._y_var)

        # Make sure constructor set all fields
        self.assertEqual(comp.datasource._data_source_id, self._data_source._data_source_id)
        self.assertEqual(comp.x_var, self._x_var)
        self.assertEqual(comp.y_var, self._y_var)
        self.assertEqual(comp.needs_guides, False)
        self.assertEqual(comp.axis_labels.x, 'X Axis')
        self.assertEqual(comp.axis_labels.y, 'Y Axis')
        self.assertEqual(comp.label_format.x, '.2f')
        self.assertEqual(comp.label_format.y, '.4f')

        self._analysis.create_card(comp)

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(comp.datasource._data_source_id, self._data_source._data_source_id)
        self.assertEqual(comp.x_var, self._x_var)
        self.assertEqual(comp.y_var, self._y_var)
        self.assertEqual(comp.needs_guides, False)
        self.assertEqual(comp.axis_labels.x, 'X Axis')
        self.assertEqual(comp.axis_labels.y, 'Y Axis')
        self.assertEqual(comp.label_format.x, '.2f')
        self.assertEqual(comp.label_format.y, '.4f')

        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # Make sure we can fetch all fields from scratch
        self.assertEqual(comp.datasource._data_source_id, self._data_source._data_source_id)
        self.assertEqual(comp.x_var, self._x_var)
        self.assertEqual(comp.y_var, self._y_var)
        self.assertEqual(comp.needs_guides, False)
        self.assertEqual(comp.axis_labels.x, 'X Axis')
        self.assertEqual(comp.axis_labels.y, 'Y Axis')
        self.assertEqual(comp.label_format.x, '.2f')
        self.assertEqual(comp.label_format.y, '.4f')

        comp.datasource = self._data_source_2
        comp.x_var = self._x_var_2
        comp.y_var = self._y_var_2
        comp.needs_guides = True
        comp.axis_labels.x = "X Axis 2"
        comp.axis_labels.y = "Y Axis 2"
        comp.label_format.x = ".3f"
        comp.label_format.y = ".5f"

        # Make sure all fields were updated
        self.assertEqual(comp.datasource._data_source_id, self._data_source_2._data_source_id)
        self.assertEqual(comp.x_var, self._x_var_2)
        self.assertEqual(comp.y_var, self._y_var_2)
        self.assertEqual(comp.needs_guides, True)
        self.assertEqual(comp.axis_labels.x, 'X Axis 2')
        self.assertEqual(comp.axis_labels.y, 'Y Axis 2')
        self.assertEqual(comp.label_format.x, '.3f')
        self.assertEqual(comp.label_format.y, '.5f')

        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # And that they survive a round trip
        self.assertEqual(comp.datasource._data_source_id, self._data_source_2._data_source_id)
        self.assertEqual(comp.x_var, self._x_var_2)
        self.assertEqual(comp.y_var, self._y_var_2)
        self.assertEqual(comp.needs_guides, True)
        self.assertEqual(comp.axis_labels.x, 'X Axis 2')
        self.assertEqual(comp.axis_labels.y, 'Y Axis 2')
        self.assertEqual(comp.label_format.x, '.3f')
        self.assertEqual(comp.label_format.y, '.5f')

        # Test with analysis passed to constructor
        comp = BinnedMeanPlot(self._data_source,
                              {'x': 'X Axis',
                               'y': 'Y Axis'},
                              {'x': '.2f',
                               'y': '.4f'},
                              False,
                              self._x_var,
                              self._y_var,
                              _analysis=self._analysis)

        # Make sure constructor set all fields
        self.assertEqual(comp.datasource._data_source_id, self._data_source._data_source_id)
        self.assertEqual(comp.x_var, self._x_var)
        self.assertEqual(comp.y_var, self._y_var)
        self.assertEqual(comp.needs_guides, False)
        self.assertEqual(comp.axis_labels.x, 'X Axis')
        self.assertEqual(comp.axis_labels.y, 'Y Axis')
        self.assertEqual(comp.label_format.x, '.2f')
        self.assertEqual(comp.label_format.y, '.4f')
        self.assertTrue(hasattr(comp, "_component_id"))

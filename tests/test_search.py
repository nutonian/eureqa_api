import sys
import os

from math import floor

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa, ComplexityWeights, DataSplitting, error_metric, SearchSettings, math_block, \
    math_block_set, VariableOptions
from eureqa.session import Http400Exception

import unittest

from etestutils import EureqaTestBase, extendedTest

from cStringIO import StringIO

class TestSearchSettings(unittest.TestCase, EureqaTestBase):
    @classmethod
    def setUpClass(cls):
        cls._eureqa = Eureqa(url=cls._eureqa_url, user_name='root', password='Changeme1', organization='root')
        cls._data_source = cls._eureqa.create_data_source('search_data_source', 'Nutrition.csv')
        cls._math_blocks = math_block_set.MathBlockSet()

    def test_no_prior_solutions(self):
        search_settings = SearchSettings(eureqa=self._eureqa,
                                  name='Test_1',
                                  search_template_id='generic',
                                  target_variable='(Calories by Lunch (%))',
                                  input_variables=['(Calories by Dinner (%))'],
                                  data_splitting=DataSplitting(shuffle=True, training_data_percentage=50,
                                                               validation_data_percentage=50,
                                                               training_selection_expression="(Calories by Lunch (%))",
                                                               training_selection_expression_type="VARIABLE",
                                                               validation_selection_expression="(Calories by Lunch (%)) < (Calories by Dinner (%)) + 1",
                                                               validation_selection_expression_type="EXPRESSION"),
                                  error_metric=error_metric._absolute_error_aic(),
                                  max_num_variables_per_term=10,
                                  maximum_history_percentage=20,
                                  prior_solutions=[],
                                  row_weight='1.1',
                                  row_weight_type="custom_expr",
                                  target_expression='(Calories by Lunch (%)) = f((Calories by Dinner (%)))')


        # Manually create a search without a prior_solutions
        # (This is not currently possible via the public Python API, but it is possible from the UI.)
        endpoint = "/fxp/datasources/%s/searches" % self._data_source._data_source_id
        body = search_settings._to_json()
        body['hidden'] = False
        del body['prior_solutions']
        result = self._eureqa._session.execute(endpoint, 'POST', body)
        search_id = result['search_id']
        return self._eureqa._get_search_by_search_id(self._data_source._data_source_id, search_id)

        # This should not crash.  (It used to crash.)
        self._eureqa.get_searches()


    def test_create_search_from_scratch(self):
        settings = SearchSettings(eureqa=self._eureqa,
                                  name='Test_1',
                                  search_template_id='generic',
                                  target_variable='(Calories by Lunch (%))',
                                  input_variables=['(Calories by Dinner (%))'],
                                  data_splitting=DataSplitting(shuffle=True, training_data_percentage=50,
                                                               validation_data_percentage=50,
                                                               training_selection_expression="(Calories by Lunch (%))",
                                                               training_selection_expression_type="VARIABLE",
                                                               validation_selection_expression="(Calories by Lunch (%)) < (Calories by Dinner (%)) + 1",
                                                               validation_selection_expression_type="EXPRESSION"),
                                  error_metric=error_metric._absolute_error_aic(),
                                  max_num_variables_per_term=10,
                                  maximum_history_percentage=20,
                                  prior_solutions=['(Calories by Dinner (%))',
                                                   '(Calories by Dinner (%)) + 1'],
                                  row_weight='1.1',
                                  row_weight_type="custom_expr",
                                  target_expression='(Calories by Lunch (%)) = f((Calories by Dinner (%)))')

        settings.variable_options.add(VariableOptions(name='(Calories by Lunch (%))',
                                                      remove_outliers_enabled=True,
                                                      normalize_enabled=True,
                                                      min_delay=5))
        settings.math_blocks.abs.enable()
        settings.math_blocks.add.enable()

        search = self._data_source.create_search(settings)

        self.assertEqual(search.name, 'Test_1')
        self.assertSetEqual(set(search.math_blocks), {self._math_blocks.abs, self._math_blocks.add})
        self.assertEqual(search._complexity_weights, ComplexityWeights())
        self.assertEqual(search.data_splitting, DataSplitting(shuffle=True, training_data_percentage=50,
                                                              validation_data_percentage=50,
                                                              training_selection_expression="(Calories by Lunch (%))",
                                                              training_selection_expression_type="VARIABLE",
                                                              validation_selection_expression="(Calories by Lunch (%)) = (Calories by Dinner (%)) + 1",
                                                              validation_selection_expression_type="EXPRESSION"))
        self.assertEqual(search.get_data_source().name, self._data_source.name)
        self.assertEqual(search.error_metric, error_metric._absolute_error_aic())
        self.assertEqual(search._max_num_variables_per_term, 10)
        self.assertSetEqual(set(search.prior_solutions), {'(Calories by Dinner (%))',
                                                          '(Calories by Dinner (%)) + 1'})
        self.assertEqual(search.row_weight, '1.1')
        self.assertEqual(search.row_weight_type, 'custom_expr')
        self.assertEqual(search.target_expression, '(Calories by Lunch (%)) = f((Calories by Dinner (%)))')
        actual_variable_option = search.variable_options['(Calories by Lunch (%))']
        self.assertEqual(actual_variable_option,
                         VariableOptions(name='(Calories by Lunch (%))',
                                         remove_outliers_enabled=True,
                                         normalize_enabled=True,
                                         min_delay=5))

        settings.math_blocks.abs.disable()
        settings.math_blocks.add.disable()

    def test_modify_settings_target_expr(self):
        ## Templates should start off with non-edited target expressions.
        ## It should be possible to edit the expression, and to undo the edit.
        settings = self._eureqa.search_templates.numeric('Test_2', target_variable='Calories',
                                                         input_variables=['Steps', 'Weight', '(Protein (g))',
                                                                          '(Fat (g))'])
        self.assertFalse(settings._body.get('target_expression_edited'))

        settings.target_expression = "Steps = f(Weight)"
        self.assertEqual(settings._body.get('target_expression_edited', None), True)

        del settings.target_expression
        self.assertEqual(settings._body.get('target_expression_edited', None), None)

        ## New settings start off with edited target expressions, and should stay that way.
        settings = SearchSettings(eureqa=self._eureqa,
                                  name='Test_1',
                                  search_template_id='generic',
                                  target_variable='(Calories by Lunch (%))',
                                  input_variables=['(Calories by Dinner (%))'],
                                  data_splitting=DataSplitting(shuffle=True, training_data_percentage=50,
                                                               validation_data_percentage=50),
                                  error_metric=error_metric._absolute_error_aic(),
                                  max_num_variables_per_term=10,
                                  maximum_history_percentage=20,
                                  prior_solutions=['(Calories by Dinner (%))',
                                                   '(Calories by Dinner (%)) + 1'],
                                  row_weight='1.1',
                                  target_expression='(Calories by Lunch (%)) = f((Calories by Dinner (%)))')
        self.assertEqual(settings._body.get('target_expression_edited', None), True)

        def test_fn():
            del settings.target_expression
        self.assertRaises(AssertionError, test_fn)


    def test_create_numeric_search(self):
        settings = self._eureqa.search_templates.numeric('Test_2', target_variable='Calories',
                                                                   input_variables=['Steps', 'Weight', '(Protein (g))',
                                                                                    '(Fat (g))'])
        search = self._data_source.create_search(settings)

        self.assertEqual(search.name, 'Test_2')
        expected_math_blocks = [self._math_blocks.const,
                                self._math_blocks.var,
                                self._math_blocks.add,
                                self._math_blocks.sub,
                                self._math_blocks.mult,
                                self._math_blocks.div,
                                self._math_blocks.log,
                                self._math_blocks.if_op,
                                self._math_blocks.logistic,
                                self._math_blocks.step,
                                self._math_blocks.min,
                                self._math_blocks.max,
                                self._math_blocks.less,
                                self._math_blocks.sqrt]
        self.assertSetEqual(set(search.math_blocks), set(expected_math_blocks))
        self.assertEqual(search._complexity_weights, ComplexityWeights())
        self.assertEqual(search.data_splitting, DataSplitting(shuffle=True, training_data_percentage=50,
                                                              validation_data_percentage=50))
        self.assertEqual(search.get_data_source().name, self._data_source.name)
        self.assertEqual(search.error_metric, error_metric.mean_absolute_error())
        self.assertEqual(search._max_num_variables_per_term, 0)
        self.assertEqual(len(search.prior_solutions), 0)
        self.assertEqual(search.row_weight, '1.0')
        self.assertEqual(search.target_expression, 'Calories = f(Steps, Weight, (Protein (g)), (Fat (g)))')

    def test_create_classification_search(self):
        settings = self._eureqa.search_templates.classification('Test_3', target_variable='Calories',
                                                                input_variables=['Steps', 'Weight', '(Protein (g))',
                                                                                 '(Fat (g))'])
        search = self._data_source.create_search(settings)

        self.assertEqual(search.name, 'Test_3')
        expected_math_blocks = [self._math_blocks.const,
                                self._math_blocks.var,
                                self._math_blocks.add,
                                self._math_blocks.sub,
                                self._math_blocks.mult,
                                self._math_blocks.div,
                                self._math_blocks.log,
                                self._math_blocks.if_op,
                                self._math_blocks.logistic,
                                self._math_blocks.step,
                                self._math_blocks.min,
                                self._math_blocks.max,
                                self._math_blocks.less,
                                self._math_blocks.sqrt]
        self.assertSetEqual(set(search.math_blocks), set(expected_math_blocks))
        self.assertEqual(search._complexity_weights, ComplexityWeights())
        self.assertEqual(search.data_splitting, DataSplitting(shuffle=True, training_data_percentage=50,
                                                              validation_data_percentage=50))
        self.assertEqual(search.get_data_source().name, self._data_source.name)
        self.assertEqual(search.error_metric, error_metric.mean_squared_error_auc_hybrid())
        self.assertEqual(search._max_num_variables_per_term, 0)
        self.assertEqual(len(search.prior_solutions), 0)
        self.assertEqual(search.row_weight, '1.0')
        self.assertEqual(search.target_expression,
                         'Calories = logistic(f0() + f2()*f1(Steps, Weight, (Protein (g)), (Fat (g))))')

    def test_create_time_series_search(self):
        # Base case should not error out
        settings = self._eureqa.search_templates.time_series(
            'Test_4', target_variable='Calories',
            input_variables=['Date', 'Steps', 'Weight'])

        search = self._data_source.create_search(settings)

        # Test complicated case
        settings = self._eureqa.search_templates.time_series(
            'Test_4', target_variable='Calories',
            input_variables=['Date', 'Steps', 'Weight'],
            min_delay=4, data_custom_history_fraction=0.2,
            max_delays_per_variable=4)

        search = self._data_source.create_search(settings)

        self.assertEqual(search.default_min_delay, 4)

        self.assertEqual(search.name, 'Test_4')

        ## Special case in the server; match in the SDK
        ## Prioritize time-series-ish functions for time-series searches
        self._math_blocks.delay.complexity = 2
        self._math_blocks.simple_moving_average.complexity = 2
        self._math_blocks.weighted_moving_average.complexity = 2

        expected_math_blocks = [self._math_blocks.const,
                                self._math_blocks.var,
                                self._math_blocks.add,
                                self._math_blocks.sub,
                                self._math_blocks.mult,
                                self._math_blocks.div,
                                self._math_blocks.log,
                                self._math_blocks.if_op,
                                self._math_blocks.logistic,
                                self._math_blocks.step,
                                self._math_blocks.min,
                                self._math_blocks.max,
                                self._math_blocks.less,
                                self._math_blocks.sqrt,
                                self._math_blocks.delay,
                                self._math_blocks.simple_moving_average,
                                self._math_blocks.weighted_moving_average]

        self.assertSetEqual(set(search.math_blocks), set(expected_math_blocks))
        self.assertEqual(search._complexity_weights, ComplexityWeights())
        self.assertEqual(search.data_splitting, DataSplitting(shuffle=False, training_data_percentage=70,
                                                              validation_data_percentage=30))
        self.assertEqual(search.get_data_source().name, self._data_source.name)
        self.assertEqual(search.error_metric, error_metric.mean_absolute_error())
        self.assertEqual(search._max_num_variables_per_term, 0)
        self.assertEqual(len(search.prior_solutions), 0)
        self.assertEqual(search.row_weight, '1.0')
        self.assertEqual(search.target_expression,
                         'Calories = f(delay(Date, 4), delay(Steps, 4), delay(Weight, 4))')

        search_variable_keys = {v for v in search.variable_options}
        expected_variable_keys = {'Date', 'Weight', 'Steps', 'Calories'}
        self.assertTrue(expected_variable_keys.issubset(search_variable_keys))

        self._math_blocks.delay.complexity = 4
        self._math_blocks.simple_moving_average.complexity = 4
        self._math_blocks.weighted_moving_average.complexity = 4

    def test_create_time_series_classification_search(self):
        settings = self._eureqa.search_templates.time_series_classification(
            'Test_4', target_variable='Calories',
            input_variables=['Date', 'Steps', 'Weight'],
            min_delay=4, data_custom_history_fraction=0.2,
            max_delays_per_variable=4)

        search = self._data_source.create_search(settings)

        self.assertEqual(search.name, 'Test_4')

        ## Special case in the server; match in the SDK
        ## Prioritize time-series-ish functions for time-series searches
        self._math_blocks.delay.complexity = 2
        self._math_blocks.simple_moving_average.complexity = 2
        self._math_blocks.weighted_moving_average.complexity = 2

        expected_math_blocks = [self._math_blocks.const,
                                self._math_blocks.var,
                                self._math_blocks.add,
                                self._math_blocks.sub,
                                self._math_blocks.mult,
                                self._math_blocks.div,
                                self._math_blocks.log,
                                self._math_blocks.if_op,
                                self._math_blocks.logistic,
                                self._math_blocks.step,
                                self._math_blocks.min,
                                self._math_blocks.max,
                                self._math_blocks.less,
                                self._math_blocks.sqrt,
                                self._math_blocks.delay,
                                self._math_blocks.simple_moving_average,
                                self._math_blocks.weighted_moving_average]

        self.assertSetEqual(set(search.math_blocks), set(expected_math_blocks))
        self.assertEqual(search._complexity_weights, ComplexityWeights())
        self.assertEqual(search.data_splitting, DataSplitting(shuffle=False, training_data_percentage=70,
                                                              validation_data_percentage=30))
        self.assertEqual(search.get_data_source().name, self._data_source.name)
        self.assertEqual(search.error_metric, error_metric.mean_squared_error_auc_hybrid())
        self.assertEqual(search._max_num_variables_per_term, 0)
        self.assertEqual(len(search.prior_solutions), 0)
        self.assertEqual(search.row_weight, '1.0')
        self.assertEqual(search.target_expression,
                         'Calories = logistic(f0() + f2()*f1(delay(Date, 4), delay(Steps, 4), delay(Weight, 4)))')

        search_variable_keys = {v for v in search.variable_options}
        expected_variable_keys = {'Date', 'Weight', 'Steps', 'Calories'}
        self.assertTrue(expected_variable_keys.issubset(search_variable_keys))

        self._math_blocks.delay.complexity = 4
        self._math_blocks.simple_moving_average.complexity = 4
        self._math_blocks.weighted_moving_average.complexity = 4

    def test_create_time_series_search_seasonality(self):
        data_source = self._eureqa.create_data_source('data_source_12', 'Nutrition.csv',
                                                      series_order_column_name='Date')
        original_variables = data_source.get_variables()

        # test create_seasonality_variable
        seasonal_var_name = data_source.create_seasonality_variable(seasonal_target_variable = 'Weight', seasonal_period = 'weekly')
        # check the results of get_variables
        self.assertItemsEqual(data_source.get_variables('original'), original_variables)
        self.assertItemsEqual(data_source.get_variables('seasonal'), [seasonal_var_name])
        self.assertItemsEqual(data_source.get_variables('all'), original_variables + [seasonal_var_name])
        self.assertItemsEqual(data_source.get_variables('all'), data_source.get_variables())

        # note we provide datasource here
        settings = self._eureqa.search_templates.time_series(
            'Test_4', target_variable='Calories',
            input_variables=['Date', 'Steps', 'Weight', seasonal_var_name],
            datasource = data_source)
        self.assertEqual(settings.target_expression,
                         'Calories = f(delay(Date, 1), delay(Steps, 1), delay(Weight, 1), ' + seasonal_var_name + ')')

        search = data_source.create_search(settings)
        self.assertEqual(search.target_expression,
                         'Calories = f(delay(Date, 1), delay(Steps, 1), delay(Weight, 1), ' + seasonal_var_name + ')')

    def test_create_time_series_search_seasonality_no_datasource(self):
        data_source = self._eureqa.create_data_source('data_source_12', 'Nutrition.csv',
                                                      series_order_column_name='Date')
        original_variables = data_source.get_variables()

        # test create_seasonality_variable
        data_source.series_order_column_name = 'Date'
        seasonal_var_name = data_source.create_seasonality_variable(seasonal_target_variable = 'Weight', seasonal_period = 'weekly')
        # check the results of get_variables
        self.assertItemsEqual(data_source.get_variables('original'), original_variables)
        self.assertItemsEqual(data_source.get_variables('seasonal'), [seasonal_var_name])
        self.assertItemsEqual(data_source.get_variables('all'), original_variables + [seasonal_var_name])
        self.assertItemsEqual(data_source.get_variables('all'), data_source.get_variables())

        # note we do not provide datasource here
        settings = self._eureqa.search_templates.time_series(
            'Test_4', target_variable='Calories',
            input_variables=['Date', 'Steps', 'Weight', seasonal_var_name])

        # note the seasonality var is erroneously delayed in the target expr here
        self.assertEqual(settings.target_expression,
                         'Calories = f(delay(Date, 1), delay(Steps, 1), delay(Weight, 1), delay(' + seasonal_var_name + ', 1))')

        try:
            search = data_source.create_search(settings)
            self.fail()
        except Exception as e:
            expected_error_msg = "Cannot delay seasonality variables in target expression. Please specify a datasource when using a search template."
            self.assertEqual(str(e), expected_error_msg)

    @extendedTest
    def test_running_wait_until_done(self):
        variables = self._data_source.get_variables()
        settings = self._eureqa.search_templates.numeric('Test_5', target_variable=variables[0],
                                                                 input_variables=variables[1:])
        s = self._data_source.create_search(settings)
        s.submit(1)

        self.assertTrue(s.is_running)

        # Should not throw
        s._get_progress_summary()

        s.wait_until_done()
        self.assertFalse(s.is_running)

    def test_solutions(self):
        self.assertTrue(len(self._search.get_solutions()) >= 2)

    def test_best_solution(self):
        self.assertTrue(self._search.get_best_solution().is_best)

    def test_best_solution_none(self):
        variables = self._data_source.get_variables()
        settings = self._eureqa.search_templates.numeric('Test_7', target_variable=variables[0],
                                                                 input_variables=variables[1:])
        s = self._data_source.create_search(settings)
        self.assertIsNone(s.get_best_solution())

    def test_most_accurate_solution(self):
        most_accurate_solution = self._search.get_most_accurate_solution()
        best_optimized_error_metric_value = min([soln.optimized_error_metric_value for soln in self._search.get_solutions()])
        self.assertEqual(most_accurate_solution.optimized_error_metric_value, best_optimized_error_metric_value)

    def test_most_accurate_solution_none(self):
        variables = self._data_source.get_variables()
        settings = self._eureqa.search_templates.numeric('Test_7', target_variable=variables[0],
                                                                 input_variables=variables[1:])
        s = self._data_source.create_search(settings)
        self.assertIsNone(s.get_most_accurate_solution())

    def test_delete_search(self):
        variables = self._data_source.get_variables()
        settings = self._eureqa.search_templates.numeric('Test_9', target_variable=variables[0],
                                                                 input_variables=variables[1:])
        s = self._data_source.create_search(settings)
        s.delete()
        self.assertEqual(len([x for x in self._data_source.get_searches() if x.name == 'Test_8']), 0)
        with self.assertRaises(Exception):
            s.delete()

    def test_evaluate_expression(self):
        # evaluate all variables on two searches with different settings
        variables = self._data_source.get_variables()
        settings1 = self._eureqa.search_templates.numeric('Test_10_1',
                                                          target_variable=variables[0],
                                                          input_variables=variables[1:])
        settings2 = self._eureqa.search_templates.numeric('Test_10_2',
                                                          target_variable=variables[0],
                                                          input_variables=variables[1:])

        settings1.variable_options['Weight'].missing_value_policy='set_zero'
        settings2.variable_options['Weight'].missing_value_policy='interpolate'

        search1 = self._data_source.create_search(settings1)
        search2 = self._data_source.create_search(settings2)
        values1 = search1.evaluate_expression(variables)
        values2 = search2.evaluate_expression(variables)
        num_lines = open('Nutrition.csv').read().count('\n')

        # should be right size and expressions returned
        self.assertEqual(values1.keys(), values2.keys())
        self.assertEqual(sorted(values1.keys()), sorted(variables))
        self.assertEqual(sorted(values1.keys()), sorted(variables))
        for var in variables:
            self.assertEqual(len(values1[var]), num_lines - 1)
            self.assertEqual(len(values2[var]), num_lines - 1)

        # should be different for different settings
        self.assertEqual(values1['Calories'], values2['Calories'])
        self.assertEqual(values1['Weight'], values1['Weight'])  # make sure no nans
        self.assertEqual(values2['Weight'], values2['Weight'])  # make sure no nans
        self.assertNotEqual(values1['Weight'], values2['Weight'])
        self.assertNotEqual(settings1, settings2)
        self.assertNotEqual(values1, values2)

    def test_create_solution(self):
        variables = self._data_source.get_variables()
        settings = self._eureqa.search_templates.numeric('abc',
                                                         target_variable=variables[0],
                                                         input_variables=variables[1:])
        solution_string = '(Calories by Dinner (%)) + 1'
        s = self._data_source.create_search(settings)
        custom_solution = s.create_solution(solution_string, True)
        self.assertIsNotNone(custom_solution)
        self.assertEquals(custom_solution.model, '1 + (Calories by Dinner (%))')
        with self.assertRaises(Exception):
            s.create_solution('(Calories by Dinnerrrr) + 1', True)

    def test_to_string(self):
        settings = SearchSettings(eureqa=self._eureqa,
                                  name='Test_1',
                                  search_template_id='generic',
                                  target_variable='(Calories by Lunch (%))',
                                  input_variables=['(Calories by Dinner (%))'],
                                  data_splitting=DataSplitting(shuffle=True, training_data_percentage=50,
                                                               validation_data_percentage=50),
                                  error_metric=error_metric._absolute_error_aic(),
                                  max_num_variables_per_term=10,
                                  maximum_history_percentage=20,
                                  prior_solutions=['(Calories by Dinner (%))',
                                                   '(Calories by Dinner (%)) + 1'],
                                  row_weight='1.1',
                                  target_expression='(Calories by Lunch (%)) = f((Calories by Dinner (%)))')
        settings.variable_options.add(VariableOptions(name='(Calories by Lunch (%))',
                                                      remove_outliers_enabled=True,
                                                      normalize_enabled=True))

        settings.math_blocks.abs.enable()
        settings.math_blocks.add.enable()

        search = self._data_source.create_search(settings)
        search.__str__()

        settings.math_blocks.abs.disable()
        settings.math_blocks.add.disable()

    def test_rename(self):
        variables = self._data_source.get_variables()
        settings = self._eureqa.search_templates.numeric('Test_13',
                                                         target_variable=variables[0],
                                                         input_variables=variables[1:])
        s = self._data_source.create_search(settings)
        s.rename('New Fancy Name')
        self.assertEqual(s.name, 'New Fancy Name')
        s = s._get_updated()
        self.assertEqual(s.name, 'New Fancy Name')

    def test_search_settings_math_blocks_setter(self):
        variables = self._data_source.get_variables()
        settings = self._eureqa.search_templates.numeric('Test_14',
                                                          target_variable=variables[0],
                                                          input_variables=variables[1:])


        new_math_blocks = math_block_set.MathBlockSet()
        self.assertFalse(settings.math_blocks is new_math_blocks)

        settings.math_blocks = new_math_blocks
        self.assertTrue(settings.math_blocks is new_math_blocks)

    def test_search_settings_row_weight_setters(self):
        search_settings = self._eureqa.search_templates.time_series('search',
                                                                    target_variable='(Calories by Lunch (%))',
                                                                    input_variables=['(Calories by Dinner (%))'])
        search_settings.row_weight = '(Calories by Lunch (%))'
        search_settings.row_weight_type = 'variable'
        self.assertEqual(search_settings.row_weight, '(Calories by Lunch (%))')
        self.assertEqual(search_settings.row_weight_type, 'variable')

        search_settings.row_weight = '(Calories by Lunch (%)) + 1'
        search_settings.row_weight_type = 'custom_expr'
        self.assertEqual(search_settings.row_weight, '(Calories by Lunch (%)) + 1')
        self.assertEqual(search_settings.row_weight_type, 'custom_expr')

        # make the actual search and make sure it comes back
        s = self._data_source.create_search(search_settings)
        self.assertEqual(s.row_weight, '(Calories by Lunch (%)) + 1')
        self.assertEqual(s.row_weight_type, 'custom_expr')

    def test_search_settings_autofix_custom_row_weight_type(self):
        search_settings = SearchSettings(eureqa=self._eureqa,
                                         name='Test_1',
                                         search_template_id='generic',
                                         target_variable='(Calories by Lunch (%))',
                                         input_variables=['(Calories by Dinner (%))'],
                                         data_splitting=DataSplitting(shuffle=True, training_data_percentage=50,
                                                                      validation_data_percentage=50,
                                                                      training_selection_expression="(Calories by Lunch (%))",
                                                                      training_selection_expression_type="VARIABLE",
                                                                      validation_selection_expression="(Calories by Lunch (%)) < (Calories by Dinner (%)) + 1",
                                                                      validation_selection_expression_type="EXPRESSION"),
                                         error_metric=error_metric._absolute_error_aic(),
                                         max_num_variables_per_term=10,
                                         maximum_history_percentage=20,
                                         prior_solutions=[],
                                         row_weight='1.1',
                                         row_weight_type="custom",
                                         target_expression='(Calories by Lunch (%)) = f((Calories by Dinner (%)))')
        self.assertEqual(search_settings.row_weight_type, "custom_expr")

        s = self._data_source.create_search(search_settings)

        self.assertEqual(s.row_weight_type, "custom_expr")



    def test_search_templates_target_expression_change_by_var_options(self):
        templates = [self._eureqa.search_templates.numeric,
                     self._eureqa.search_templates.classification,
                     self._eureqa.search_templates.time_series,
                     self._eureqa.search_templates.time_series_classification]

        for template in templates:
            settings = template(name="Test",
                                target_variable='(Calories by Lunch (%))',
                                input_variables=['(Calories by Dinner (%))'])

            old_target_expression = settings.target_expression

            changed_settings = template(name="Test",
                                target_variable='(Calories by Lunch (%))',
                                input_variables=['(Calories by Dinner (%))'],
                                variable_options=[VariableOptions(name='(Calories by Dinner (%))', min_delay=5)])

            self.assertEqual(changed_settings.target_expression, old_target_expression.replace('delay((Calories by Dinner (%)), 1)',
                                                                                               'delay((Calories by Dinner (%)), 5)'))

            self.assertEqual(changed_settings.variable_options['(Calories by Dinner (%))'].min_delay, 5)

    def test_search_settings_immutable_fields(self):
        settings = SearchSettings(eureqa=self._eureqa,
                                  name='Test_1',
                                  search_template_id='generic',
                                  target_variable='(Calories by Lunch (%))',
                                  input_variables=['(Calories by Dinner (%))'],
                                  data_splitting=DataSplitting(shuffle=True, training_data_percentage=50,
                                                               validation_data_percentage=50,
                                                               training_selection_expression="(Calories by Lunch (%))",
                                                               training_selection_expression_type="VARIABLE",
                                                               validation_selection_expression="(Calories by Lunch (%)) = (Calories by Dinner (%)) + 1",
                                                               validation_selection_expression_type="EXPRESSION"),
                                  error_metric=error_metric._absolute_error_aic(),
                                  max_num_variables_per_term=10,
                                  maximum_history_percentage=20,
                                  prior_solutions=[],
                                  row_weight='1.1',
                                  row_weight_type="custom",
                                  target_expression='(Calories by Lunch (%)) = f((Calories by Dinner (%)))')

        with self.assertRaises(Exception):
            settings.variable_options= VariableOptionsDict([VariableOptions(name='(Calories by Lunch (%))', min_delay=5)])

    def test_search_settings_min_delay_changes_non_custom_target_expr(self):
        settings = self._eureqa.search_templates.time_series(
                        name = "test",
                        target_variable='(Calories by Lunch (%))',
                        input_variables=['(Calories by Dinner (%))', '(Protein (g))'])


        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')
        settings.variable_options['(Calories by Dinner (%))'].min_delay = 5
        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 5), delay((Protein (g)), 1))')

    def test_search_settings_default_min_delay_changes_non_custom_target_expr(self):
        settings = self._eureqa.search_templates.time_series(
                        name = "test",
                        target_variable='(Calories by Lunch (%))',
                        input_variables=['(Calories by Dinner (%))', '(Protein (g))'])


        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')
        settings.default_min_delay = 3
        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 3), delay((Protein (g)), 3))')

    def test_search_settings_default_min_delay_doesnt_overwrite_var_options(self):
        settings = self._eureqa.search_templates.time_series(
                        name = "test",
                        target_variable='(Calories by Lunch (%))',
                        input_variables=['(Calories by Dinner (%))', '(Protein (g))'])
        settings2 = self._eureqa.search_templates.time_series(
                        name = "test",
                        target_variable='(Calories by Lunch (%))',
                        input_variables=['(Calories by Dinner (%))', '(Protein (g))'])

        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')
        settings.variable_options['(Calories by Dinner (%))'].min_delay = 5
        settings.default_min_delay = 3
        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 5), delay((Protein (g)), 3))')

        self.assertEquals(settings2.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')
        settings2.default_min_delay = 3
        settings2.variable_options['(Calories by Dinner (%))'].min_delay = 5
        self.assertEquals(settings2.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 5), delay((Protein (g)), 3))')

    def test_search_settings_min_delay_update_with_custom_expression(self):
        settings = self._eureqa.search_templates.time_series(
                        name = "test",
                        target_variable='(Calories by Lunch (%))',
                        input_variables=['(Calories by Dinner (%))', '(Protein (g))'])
        settings2 = self._eureqa.search_templates.time_series(
                        name = "test",
                        target_variable='(Calories by Lunch (%))',
                        input_variables=['(Calories by Dinner (%))', '(Protein (g))'])

        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')
        self.assertEquals(settings._original_target_expression, '(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')
        self.assertEquals(settings2.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')
        self.assertEquals(settings2._original_target_expression, '(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')

        settings.target_expression = '(Calories by Lunch (%)) = f((Calories by Dinner (%)))'
        settings.default_min_delay = 3
        with self.assertRaises(Http400Exception):
            settings.variable_options['(Calories by Dinner (%))'].min_delay = 5
        self.assertEquals(settings.target_expression, '(Calories by Lunch (%)) = f((Calories by Dinner (%)))')
        self.assertEquals(settings._original_target_expression, '(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 3), delay((Protein (g)), 3))')

        settings2.default_min_delay = 3
        settings2.variable_options['(Calories by Dinner (%))'].min_delay = 5
        settings2.target_expression = '(Calories by Lunch (%)) = f((Calories by Dinner (%)))'
        self.assertEquals(settings2.target_expression, '(Calories by Lunch (%)) = f((Calories by Dinner (%)))')
        self.assertEquals(settings2._original_target_expression, '(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 5), delay((Protein (g)), 3))')

        del settings.target_expression
        del settings2.target_expression

        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 3), delay((Protein (g)), 3))')
        self.assertEquals(settings2.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 5), delay((Protein (g)), 3))')

    def test_search_templates_invalid_args(self):
        templates = [self._eureqa.search_templates.numeric,
                     self._eureqa.search_templates.classification,
                     self._eureqa.search_templates.time_series,
                     self._eureqa.search_templates.time_series_classification]

        for template in templates:

            with self.assertRaises(Exception):
                settings = template(name="Test",
                                    target_variable='(Calories by Lunch (%))',
                                    input_variables='(Calories by Dinner (%))')

            with self.assertRaises(Exception):
                settings = template(name="Test",
                                    target_variable='(Calories by Lunch (%))',
                                    input_variables=5)

    def test_search_settings_target_variable_changes_non_custom_target_expr(self):
        settings = self._eureqa.search_templates.time_series(
                        name = "test",
                        target_variable='(Calories by Lunch (%))',
                        input_variables=['(Calories by Dinner (%))', '(Protein (g))'])


        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')
        settings.target_variable = "Something Else"
        self.assertEquals(settings.target_expression,'Something Else = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')

    def test_search_settings_input_variables_changes_non_custom_target_expr(self):
        settings = self._eureqa.search_templates.time_series(
                        name = "test",
                        target_variable='(Calories by Lunch (%))',
                        input_variables=['(Calories by Dinner (%))', '(Protein (g))'])


        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')
        settings.input_variables.append("Something Else")
        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1), delay(Something Else, 1))')
        settings.input_variables[1] = "Another Var"
        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay(Another Var, 1), delay(Something Else, 1))')
        del settings.input_variables[0]
        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay(Another Var, 1), delay(Something Else, 1))')
        settings.input_variables = ["A", "B", "C"]
        self.assertEquals(settings.target_expression,'(Calories by Lunch (%)) = f(delay(A, 1), delay(B, 1), delay(C, 1))')

    def test_search_settings_variable_changes_with_custom_target_expr(self):
        settings = self._eureqa.search_templates.time_series(
                        name = "test",
                        target_variable='(Calories by Lunch (%))',
                        input_variables=['(Calories by Dinner (%))', '(Protein (g))'])


        settings.target_expression = '(Calories by Lunch (%)) = f((Calories by Dinner (%)))'
        self.assertEquals(settings.target_expression, '(Calories by Lunch (%)) = f((Calories by Dinner (%)))')
        self.assertEquals(settings._original_target_expression,'(Calories by Lunch (%)) = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')

        settings.target_variable = "Something Else"
        self.assertEquals(settings.target_expression, '(Calories by Lunch (%)) = f((Calories by Dinner (%)))')
        self.assertEquals(settings._original_target_expression,'Something Else = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')

        with self.assertRaises(Http400Exception):
            settings.input_variables.append("Something Else")

        with self.assertRaises(Http400Exception):
            del settings.input_variables[0]

        with self.assertRaises(Http400Exception):
            settings.input_variables = ["A", "B", "C"]

        settings.input_variables[1] = "Another Var"
        self.assertEquals(settings.target_expression, '(Calories by Lunch (%)) = f((Calories by Dinner (%)))')
        self.assertEquals(settings._original_target_expression,'Something Else = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')

        del settings.target_expression
        self.assertEquals(settings.target_expression, 'Something Else = f(delay((Calories by Dinner (%)), 1), delay((Protein (g)), 1))')

    def test_outliers(self):
        data = """\
        a,b
        1, 1
        2, 2
        3, 3
        4, 4
        5, 5
        6, 6
        7, 9000
        8, 8
        9, 9
        """

        ds = self._eureqa.create_data_source("test_outliers", StringIO(data))
        variable_option = VariableOptions(name="b", remove_outliers_enabled=True)

        settings = self._eureqa.search_templates.numeric(name="test_outliers",
                                                         target_variable="b",
                                                         input_variables=["a"],
                                                         variable_options=[variable_option],
                                                         datasource=ds)

        search = ds.create_search(settings)
        search.submit(100)
        while len(search.get_solutions()) == 0:
            pass
        search.stop()

        # If we dropped the outlier, there is a trivial perfect solution.
        # Eureqa's linear fitter should find it right away.
        self.assertEqual("a", search.get_best_solution().model)

    def test_search_settings_defaults_propagated(self):
        data = """\
        a,b
        1, 1
        2, 2
        3, 3
        4, 4
        5, 5
        """

        ds = self._eureqa.create_data_source(
            "test_search_settings_defaults_propagated", StringIO(data))

        settings = self._eureqa.search_templates.numeric(
            name="test_search_settings_defaults_propagated",
            target_variable="b",
            input_variables=["a"],
            datasource=ds)

        search = ds.create_search(settings)
        search.submit(100)
        while len(search.get_solutions()) == 0:
            pass
        search.stop()

        # Canonicalize everything to 'str' byte-strings.
        # Doesn't affect the actual comparison, but having different types
        # makes unittest's output diff huge.
        settings_body = _simplify_pod_struct(settings._body)
        search_body = _simplify_pod_struct(search._body)

        # Delete fields that are expected to be present on the search,
        # but not automatically on the settings.

        # Informational state (available at search-template time but not added)
        del search_body['datasource_name']
        del search_body['error_metric_abbreviation']
        del search_body['error_metric_id']
        del search_body['error_metric_info_id']
        del search_body['search_template_name']

        # State related to search storage or execution
        del search_body['display_state']
        del search_body['estimated_time_remaining_seconds']
        del search_body['id']
        del search_body['is_searching']
        del search_body['is_submitted']
        del search_body['last_modified']
        del search_body['last_modified_by']
        del search_body['max_accuracy']
        del search_body['max_runtime_sec']
        del search_body['message']
        del search_body['search_id']
        del search_body['search_queue_state']
        del search_body['search_stats']
        del search_body['solution_count']

        self.maxDiff = 100000
        self.assertEqual(settings_body, search_body)

def _simplify_pod_struct(d):
    """Simplify a structure 'd' composed exclusively of Python primitive ('Plain Old Data' aka POD,
    in C++ parlance) types
    """
    if isinstance(d, dict):
        return {
            _simplify_pod_struct(k): _simplify_pod_struct(v)
            for k, v in d.iteritems()
        }
    elif isinstance(d, list):
        return [_simplify_pod_struct(x) for x in d]
    elif isinstance(d, basestring):
        return str(d)  # No Unicode strings
    else:
        return d

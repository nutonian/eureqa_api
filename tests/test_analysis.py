import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa, search_settings

import unittest

from etestutils_nosearch import EureqaTestBaseNoSearch

from eureqa.analysis.analysis_file import AnalysisFile

class TestAnalysis(unittest.TestCase, EureqaTestBaseNoSearch):
    @classmethod
    def setUpClass(cls):
        for analysis in cls._eureqa.get_analyses():
            if analysis.name in {'Test name 1', 'Test name 2', 'Test name 3', 'Test name 4'}:
                analysis.delete() 

    def test_create(self):
        analysis = self._eureqa.create_analysis('Test name 1', 'Test description 1')
        self.assertEqual(analysis.name, 'Test name 1')
        self.assertEqual(analysis.description, 'Test description 1')
        self.assertEqual(len(analysis.get_cards()), 0)
        self.assertEqual(analysis.url(), self._eureqa_url + '/root/analyses/%s' % analysis._id)

    def test_edit(self):
        analysis = self._eureqa.create_analysis('Test name 2', 'Test description 2')
        analysis.name = 'Test name 2.5'
        analysis.description = 'Test description 2.5'
        self.assertEqual(analysis.name, 'Test name 2.5')
        self.assertEqual(analysis.description, 'Test description 2.5')

    def test_get(self):
        analysis3 = self._eureqa.create_analysis('Test name 3', 'Test description 3')
        analyses = self._eureqa.get_analyses()
        analyses = [x for x in analyses if x.name == 'Test name 3']
        self.assertEqual(len(analyses), 1)
        self.assertEqual(analyses[0].name, 'Test name 3')
        self.assertEqual(analyses[0].description, 'Test description 3')
        analysis3_again = self._eureqa.get_analysis(analysis3.analysis_id)
        self.assertEqual(analysis3.name, analysis3_again.name);

    def test_delete(self):
        analysis = self._eureqa.create_analysis('Test name 4', 'Test description 4')
        analysis.delete()
        analyses = self._eureqa.get_analyses()
        analyses = [x for x in analyses if x.name == 'Test name 4']
        self.assertEqual(len(analyses), 0)

    def test_file(self):
        analysis = self._eureqa.create_analysis('Test name 5', 'Test description 5')
        analysis._create_file("filename", "file data")
        self.assertEqual(analysis._get_file("filename").get().read(), "file data")

    def test_file_dupes(self):
        analysis = self._eureqa.create_analysis('Test name 5', 'Test description 5')
        analysis._create_file("filename", "file data")
        f = analysis._create_file("filename", "file data 2")

        # Should get the new file data (overwrite semantics)
        self.assertEqual(analysis._get_file("filename").get().read(), "file data 2")

        # Old file data should not be recoverable
        f.delete()
        with self.assertRaises(Exception):
            analysis._get_file("filename")

        # Forcibly create a file with a duplicate name
        AnalysisFile.create(analysis, "file data 3", filename="filename")

        # We don't specify in this case which data we get back.
        # But we should get something back.
        self.assertIn(analysis._get_file("filename").get().read(),
                      {"file data 2", "file data 3"})
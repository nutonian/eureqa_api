import sys
import os
sys.path.insert(1, os.path.abspath('..')) #Need this to be able to reference eureqa package.
from eureqa import Solution

import unittest
import json

from etestutils import EureqaTestBase

class TestSolution(unittest.TestCase, EureqaTestBase):

    def setUp(self):
        self.maxDiff = 4096

    def test_constructor_from_json(self):
        body = json.loads('''
            {
                "error_metric_values":
                [
                    {"metric_name": "Metric 1", "metric_value": 2},
                    {"metric_name": "Metric 2", "metric_value": 3}
                ],
                "model_complexity": 4,
                "model_optimized_error_metric": "Metric 3",
                "model_optimized_error_metric_value": 5,
                "model_fitness": 0.1234,
                "solution_id": 5,
                "search_id": 6,
                "datasource_id": 7,
                "target_variable": "var_1",
                "model_string": "a + b",
                "solution_type":
                {
                    "type": "pareto",
                    "best": false,
                    "most_accurate": false
                },
                "terms":
                [
                    {"term_string": "a"},
                    {"term_string": "b"}
                ]
            }
            ''')
        solution = Solution(body, None)

        self.assertEqual(solution.get_error_metric_value("Metric 1"), 2)
        self.assertEqual(solution.get_error_metric_value("Metric 2"), 3)
        self.assertEqual(solution.complexity, 4)
        self.assertEqual(solution.optimized_error_metric, "Metric 3")
        self.assertEqual(solution.optimized_error_metric_value, 5)
        self.assertEqual(solution._model_fitness, 0.1234)
        self.assertEqual(solution._id, 5)
        self.assertEqual(solution._search_id, 6)
        self.assertEqual(solution._datasource_id, 7)
        self.assertEqual(solution.target, "var_1")
        self.assertEqual(solution.model, "a + b")
        self.assertFalse(solution.is_best)
        self.assertFalse(solution.is_most_accurate)
        self.assertEqual(solution.terms, ['a', 'b'])

    def test_constructor_best_most_accurate(self):
        body = json.loads('''
            {
                "error_metric_values":
                [
                    {"metric_name": "Metric 1", "metric_value": 2},
                    {"metric_name": "Metric 2", "metric_value": 3}
                ],
                "model_complexity": 4,
                "model_optimized_error_metric": "Metric 3",
                "model_optimized_error_metric_value": 5,
                "model_fitness": 0.1234,
                "solution_id": 5,
                "search_id": 6,
                "datasource_id": 7,
                "target_variable": "var_1",
                "model_string": "a + b",
                "solution_type":
                {
                    "type": "pareto",
                    "best": true,
                    "most_accurate": true
                },
                "terms":
                [
                    {"term_string": "a"},
                    {"term_string": "b"}
                ]
            }
            ''')
        solution = Solution(body, None)

        self.assertEqual(solution.get_error_metric_value("Metric 1"), 2)
        self.assertEqual(solution.get_error_metric_value("Metric 2"), 3)
        self.assertEqual(solution.complexity, 4)
        self.assertEqual(solution.optimized_error_metric, "Metric 3")
        self.assertEqual(solution.optimized_error_metric_value, 5)
        self.assertEqual(solution._model_fitness, 0.1234)
        self.assertEqual(solution._id, 5)
        self.assertEqual(solution._search_id, 6)
        self.assertEqual(solution._datasource_id, 7)
        self.assertEqual(solution.target, "var_1")
        self.assertEqual(solution.model, "a + b")
        self.assertTrue(solution.is_best)
        self.assertTrue(solution.is_most_accurate)
        self.assertEqual(solution.terms, ['a', 'b'])

    def test_to_string(self):
        body = json.loads('''
            {
                "error_metric_values":
                [
                    {"metric_name": "Metric 1", "metric_value": 2},
                    {"metric_name": "Metric 2", "metric_value": 3}
                ],
                "model_complexity": 4,
                "model_optimized_error_metric": "Metric 3",
                "model_optimized_error_metric_value": 5,
                "model_fitness": 0.1234,
                "solution_id": 5,
                "search_id": 6,
                "datasource_id": 7,
                "target_variable": "var_1",
                "model_string": "a + b",
                "solution_type":
                {
                    "type": "pareto",
                    "best": true,
                    "most_accurate": false
                },
                "terms":
                [
                    {"term_string": "a"},
                    {"term_string": "b"}
                ]
            }
            ''')
        solution = Solution(body, None)
        solution.__str__()

    def test_metrics_multi_series(self):
        single_series_data_source = self._eureqa.create_data_source('single_series_data_source', 'simple_data.csv')
        multi_series_data_source = self._eureqa.create_data_source('multi_series_data_source', 'simple_multi_series_data.csv')
        multi_series_data_source_with_metadata = self._eureqa.create_data_source('single_series_data_source_with_metadata', 'simple_data.csv', series_id_column_name='x')

        variables = multi_series_data_source.get_variables()
        metadata_variables = multi_series_data_source_with_metadata.get_variables()

        single_series_settings = \
            self._eureqa.search_templates.numeric('single_series_data_source',
                                                  target_variable=variables[0],
                                                  input_variables=variables[1:])

        solution_string = 'dbl_x / x_plus_three'
        single_series_search = single_series_data_source.create_search(single_series_settings)
        single_series_solution = single_series_search.create_solution(solution_string, True)

        multi_series_settings = self._eureqa.search_templates.numeric('multi_series_data_source',
                                                                      target_variable=variables[0],
                                                                      input_variables=variables[1:])
        multi_series_settings_with_metadata = \
            self._eureqa.search_templates.numeric('multi_series_data_source_with_metadata',
                                                  target_variable=metadata_variables[0],
                                                  input_variables=metadata_variables[1:])

        multi_series_search = multi_series_data_source.create_search(multi_series_settings)
        multi_series_search_with_metadata = multi_series_data_source_with_metadata.create_search(multi_series_settings_with_metadata)

        multi_series_solution = multi_series_search.create_solution(solution_string, True)
        multi_series_solution_with_metadata = multi_series_search_with_metadata.create_solution(solution_string, True)

        single_series_metrics = single_series_solution.get_single_series_error_metrics(0)
        first_multi_series_metrics = multi_series_solution.get_single_series_error_metrics(0)
        second_multi_series_metrics = multi_series_solution.get_single_series_error_metrics(1)
        multi_series_metrics = multi_series_solution.get_all_series_error_metrics()

        multi_series_metrics_with_metadata = multi_series_solution_with_metadata.get_all_series_error_metrics()
        first_multi_series_metrics_with_metadata = multi_series_solution_with_metadata.get_single_series_error_metrics(0)
        second_multi_series_metrics_with_metadata = multi_series_solution_with_metadata.get_single_series_error_metrics(1)
        first_multi_series_metrics_by_value = multi_series_solution.get_single_series_error_metrics('Series 1')
        second_multi_series_metrics_by_value = multi_series_solution.get_single_series_error_metrics('Series 2')
        first_multi_series_metrics_by_value_with_metadata = multi_series_solution_with_metadata.get_single_series_error_metrics('1')
        second_multi_series_metrics_by_value_with_metadata = multi_series_solution_with_metadata.get_single_series_error_metrics('2')

        with self.assertRaises(Exception):
            multi_series_solution.get_single_series_error_metrics_by_series_id_value('1')
        with self.assertRaises(Exception):
            multi_series_solution_with_metadata.get_single_series_error_metrics('not a series')

        self.assertEqual(single_series_metrics._to_json(), first_multi_series_metrics._to_json())
        self.assertNotEqual(first_multi_series_metrics._to_json(), second_multi_series_metrics._to_json())
        self.assertEqual(first_multi_series_metrics._to_json(), multi_series_metrics[0]._to_json())
        self.assertEqual(second_multi_series_metrics._to_json(), multi_series_metrics[1]._to_json())

        self.assertEqual(single_series_metrics.series_id_value, 'Series 1')
        self.assertEqual(first_multi_series_metrics.series_id_value, 'Series 1')
        self.assertEqual(second_multi_series_metrics.series_id_value, 'Series 2')
        self.assertEqual(first_multi_series_metrics_with_metadata.series_id_value, '1')
        self.assertEqual(second_multi_series_metrics_with_metadata.series_id_value, '2')
        self.assertEqual(first_multi_series_metrics_by_value.series_id_value, 'Series 1')
        self.assertEqual(second_multi_series_metrics_by_value.series_id_value, 'Series 2')
        self.assertEqual(first_multi_series_metrics_by_value_with_metadata.series_id_value, '1')
        self.assertEqual(second_multi_series_metrics_by_value_with_metadata.series_id_value, '2')


        self.assertAlmostEqual(single_series_metrics.mean_absolute_error, 2.06142857)
        self.assertAlmostEqual(first_multi_series_metrics.mean_absolute_error, 2.06142857)
        self.assertAlmostEqual(second_multi_series_metrics.mean_absolute_error, 5.6040404)
        self.assertAlmostEqual(multi_series_metrics[0].mean_absolute_error, 2.06142857)
        self.assertAlmostEqual(multi_series_metrics[1].mean_absolute_error, 5.6040404)
        self.assertAlmostEqual(first_multi_series_metrics_with_metadata.mean_absolute_error, 1.5)
        self.assertAlmostEqual(second_multi_series_metrics_with_metadata.mean_absolute_error, 2.2)
        self.assertAlmostEqual(multi_series_metrics_with_metadata[0].mean_absolute_error, 1.5)
        self.assertAlmostEqual(multi_series_metrics_with_metadata[1].mean_absolute_error, 2.2)
        self.assertAlmostEqual(first_multi_series_metrics_by_value.mean_absolute_error, 2.06142857)
        self.assertAlmostEqual(second_multi_series_metrics_by_value.mean_absolute_error, 5.6040404)
        self.assertAlmostEqual(first_multi_series_metrics_by_value_with_metadata.mean_absolute_error, 1.5)
        self.assertAlmostEqual(second_multi_series_metrics_by_value_with_metadata.mean_absolute_error, 2.2)

        self.assertAlmostEqual(single_series_metrics.r2_goodness_of_fit, -1.7915765)
        self.assertAlmostEqual(first_multi_series_metrics.r2_goodness_of_fit, -1.7915765)
        self.assertAlmostEqual(second_multi_series_metrics.r2_goodness_of_fit, -46.99037649)
        self.assertAlmostEqual(multi_series_metrics[0].r2_goodness_of_fit, -1.7915765)
        self.assertAlmostEqual(multi_series_metrics[1].r2_goodness_of_fit, -46.99037649)


    def test_metrics_multi_series_data_types(self):
        data_source = self._eureqa.create_data_source('data_source', 'Nutrition.csv')
        variables = data_source.get_variables()
        settings = self._eureqa.search_templates.numeric('search',
                                                         target_variable=variables[0],
                                                         input_variables=variables[1:])

        solution_string = '(Calories by Dinner (%)) + 1'
        s = data_source.create_search(settings)
        custom_solution = s.create_solution(solution_string, True)

        all_metrics = custom_solution.get_single_series_error_metrics(0, data_split='all')
        training_metrics = custom_solution.get_single_series_error_metrics(0, data_split='training')
        validation_metrics = custom_solution.get_single_series_error_metrics(0, data_split='validation')

        all_metrics_get_by_name = custom_solution.get_single_series_error_metrics('Series 1', data_split='all')
        training_metrics_get_by_name = custom_solution.get_single_series_error_metrics('Series 1', data_split='training')
        validation_metrics_get_by_name = custom_solution.get_single_series_error_metrics('Series 1', data_split='validation')

        with self.assertRaises(Exception):
            custom_solution.get_single_series_error_metrics(0, data_split='not_valid_split')
        with self.assertRaises(Exception):
            custom_solution.get_single_series_error_metrics('Series 1', data_split='not_valid_split')

        self.assertAlmostEqual(validation_metrics.mean_absolute_error, 40.55151515)
        self.assertAlmostEqual(training_metrics.mean_absolute_error, 45.3803030)
        self.assertAlmostEqual(all_metrics.mean_absolute_error, 42.96590909)
        self.assertAlmostEqual(validation_metrics_get_by_name.mean_absolute_error, 40.55151515)
        self.assertAlmostEqual(training_metrics_get_by_name.mean_absolute_error, 45.3803030)
        self.assertAlmostEqual(all_metrics_get_by_name.mean_absolute_error, 42.96590909)

        self.assertAlmostEqual(validation_metrics.r2_goodness_of_fit, -0.94245395)
        self.assertAlmostEqual(training_metrics.r2_goodness_of_fit, -0.92359136)
        self.assertAlmostEqual(all_metrics.r2_goodness_of_fit, -0.93212519)
        self.assertAlmostEqual(validation_metrics_get_by_name.r2_goodness_of_fit, -0.94245395)
        self.assertAlmostEqual(training_metrics_get_by_name.r2_goodness_of_fit, -0.92359136)
        self.assertAlmostEqual(all_metrics_get_by_name.r2_goodness_of_fit, -0.93212519)

    def test_metrics_multi_series_backwards_compatibility(self):
        data_source = self._eureqa.create_data_source('data_source', 'Nutrition.csv')
        variables = data_source.get_variables()
        settings = self._eureqa.search_templates.numeric('search',
                                                         target_variable=variables[0],
                                                         input_variables=variables[1:])

        solution_string = '(Calories by Dinner (%)) + 1'
        s = data_source.create_search(settings)
        custom_solution = s.create_solution(solution_string, True)

        old_method_no_kw = custom_solution.get_single_series_error_metrics(0)
        old_method_no_kw_with_split = custom_solution.get_single_series_error_metrics(0, 'all')
        old_method_with_kw = custom_solution.get_single_series_error_metrics(series_index=0)
        old_method_with_all_kws = custom_solution.get_single_series_error_metrics(series_index=0, data_split='all')
        new_method_no_kw = custom_solution.get_single_series_error_metrics('Series 1')
        new_method_no_kw_with_split = custom_solution.get_single_series_error_metrics('Series 1', 'all')
        new_method_with_numeric_kw = custom_solution.get_single_series_error_metrics(series=0)
        new_method_with_numeric_kw_and_split = custom_solution.get_single_series_error_metrics(series=0, data_split='all')
        new_method_with_string_kw = custom_solution.get_single_series_error_metrics(series='Series 1')
        new_method_with_string_kw_and_split = custom_solution.get_single_series_error_metrics(series='Series 1', data_split='all')

        with self.assertRaises(Exception):
            custom_solution.get_single_series_error_metrics()

        with self.assertRaises(Exception):
            custom_solution.get_single_series_error_metrics(series="Series 1", series_index=0)

        with self.assertRaises(Exception):
            custom_solution.get_single_series_error_metrics(series=0, series_index=0)

        with self.assertRaises(Exception):
            custom_solution.get_single_series_error_metrics(1, 'All', 1)

        self.assertAlmostEqual(old_method_no_kw.mean_absolute_error, 42.96590909)
        self.assertAlmostEqual(old_method_no_kw_with_split.mean_absolute_error, 42.96590909)
        self.assertAlmostEqual(old_method_with_kw.mean_absolute_error, 42.96590909)
        self.assertAlmostEqual(old_method_with_all_kws.mean_absolute_error, 42.96590909)
        self.assertAlmostEqual(new_method_no_kw.mean_absolute_error, 42.96590909)
        self.assertAlmostEqual(new_method_no_kw_with_split.mean_absolute_error, 42.96590909)
        self.assertAlmostEqual(new_method_with_numeric_kw.mean_absolute_error, 42.96590909)
        self.assertAlmostEqual(new_method_with_numeric_kw_and_split.mean_absolute_error, 42.96590909)
        self.assertAlmostEqual(new_method_with_string_kw.mean_absolute_error, 42.96590909)
        self.assertAlmostEqual(new_method_with_string_kw_and_split.mean_absolute_error, 42.96590909)

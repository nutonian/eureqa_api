import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa, search_settings

import unittest
from etestutils_nosearch import EureqaTestBaseNoSearch


class TestAnalysisCard(unittest.TestCase, EureqaTestBaseNoSearch):

    def test_repr(self):
        analysis = self._eureqa.create_analysis('Test name 1')
        card = analysis.create_text_card("Test text 1")
        self.assertEqual(repr(card),
                         "Card(component=TitledLayout(analysis_id=u'%(analysis_id)s', component_id=u'1', component_type=u'TITLED_LAYOUT', content_component_id=u'0', description=u'', title=u'Text'), _order_index=0, _collapse=False, _item_id=u'0', _analysis_id=u'%(analysis_id)s')"
                         % {"analysis_id": analysis._id})

    def test_clone(self):
        analysis = self._eureqa.create_analysis('Test name 1')
        card1 = analysis.create_text_card("Test text 1", 'Title 1')
        self.assertEqual(repr(card1),
                         "Card(component=TitledLayout(analysis_id=u'%(analysis_id)s', component_id=u'1', component_type=u'TITLED_LAYOUT', content_component_id=u'0', description=u'', title=u'Title 1'), _order_index=0, _collapse=False, _item_id=u'0', _analysis_id=u'%(analysis_id)s')"
                         % {"analysis_id": analysis._id})

        card2 = card1.clone()
        self.assertEqual(repr(card2),
                         "Card(component=TitledLayout(component_type=u'TITLED_LAYOUT', description=u'', title=u'Title 1'), _order_index=0, _collapse=False, _item_id=None, _analysis_id=None)")

        card3 = card2.clone()
        self.assertEqual(repr(card2),
                         "Card(component=TitledLayout(component_type=u'TITLED_LAYOUT', description=u'', title=u'Title 1'), _order_index=0, _collapse=False, _item_id=None, _analysis_id=None)")

        card3.component.title = u"New title"
        self.assertEqual(repr(card1),
                         "Card(component=TitledLayout(analysis_id=u'%(analysis_id)s', component_id=u'1', component_type=u'TITLED_LAYOUT', content_component_id=u'0', description=u'', title=u'Title 1'), _order_index=0, _collapse=False, _item_id=u'0', _analysis_id=u'%(analysis_id)s')"
                         % {"analysis_id": analysis._id})
        self.assertEqual(repr(card2),
                         "Card(component=TitledLayout(component_type=u'TITLED_LAYOUT', description=u'', title=u'Title 1'), _order_index=0, _collapse=False, _item_id=None, _analysis_id=None)")
        self.assertEqual(repr(card3),
                         "Card(component=TitledLayout(component_type=u'TITLED_LAYOUT', description=u'', title=u'New title'), _order_index=0, _collapse=False, _item_id=None, _analysis_id=None)")

        analysis2 = self._eureqa.create_analysis('Test name 2')
        analysis2.add_card(card3)
        self.assertEqual(repr(card1),
                         "Card(component=TitledLayout(analysis_id=u'%(analysis_id)s', component_id=u'1', component_type=u'TITLED_LAYOUT', content_component_id=u'0', description=u'', title=u'Title 1'), _order_index=0, _collapse=False, _item_id=u'0', _analysis_id=u'%(analysis_id)s')"
                         % {"analysis_id": analysis._id})
        self.assertEqual(repr(card2),
                         "Card(component=TitledLayout(component_type=u'TITLED_LAYOUT', description=u'', title=u'Title 1'), _order_index=0, _collapse=False, _item_id=None, _analysis_id=None)")
        self.assertEqual(repr(card3),
                         "Card(component=TitledLayout(analysis_id=u'%(analysis_id)s', component_id=u'0', component_type=u'TITLED_LAYOUT', content_component_id=u'1', description=u'', title=u'New title'), _order_index=0, _collapse=False, _item_id=u'0', _analysis_id=u'%(analysis_id)s')"
                         % {"analysis_id": analysis2._id})


    def test_move_up(self):
        analysis = self._eureqa.create_analysis('Test name 1')
        top_card = analysis.create_text_card('Test text 1')
        bottom_card = analysis.create_text_card('Test text 2')
        cards = analysis.get_cards();
        self.assertEqual(cards[0].text, 'Test text 1')
        self.assertEqual(cards[1].text, 'Test text 2')
        bottom_card.move_below(bottom_card)
        cards = analysis.get_cards();
        self.assertEqual(cards[0].text, 'Test text 1')
        self.assertEqual(cards[1].text, 'Test text 2')
        bottom_card.move_below(top_card)
        cards = analysis.get_cards();
        self.assertEqual(cards[0].text, 'Test text 1')
        self.assertEqual(cards[1].text, 'Test text 2')
        top_card.move_below(bottom_card)
        cards = analysis.get_cards();
        self.assertEqual(cards[0].text, 'Test text 2')
        self.assertEqual(cards[1].text, 'Test text 1')

    def test_move_down(self):
        analysis = self._eureqa.create_analysis('Test name 2')
        top_card = analysis.create_text_card('Test text 1')
        bottom_card = analysis.create_text_card('Test text 2')
        cards = analysis.get_cards();
        self.assertEqual(cards[0].text, 'Test text 1')
        self.assertEqual(cards[1].text, 'Test text 2')
        top_card.move_above(top_card)
        cards = analysis.get_cards();
        self.assertEqual(cards[0].text, 'Test text 1')
        self.assertEqual(cards[1].text, 'Test text 2')
        top_card.move_above(bottom_card)
        cards = analysis.get_cards();
        self.assertEqual(cards[0].text, 'Test text 1')
        self.assertEqual(cards[1].text, 'Test text 2')
        bottom_card.move_above(top_card)
        cards = analysis.get_cards();
        self.assertEqual(cards[0].text, 'Test text 2')
        self.assertEqual(cards[1].text, 'Test text 1')

    def test_move_up_with_int_order(self):
        analysis = self._eureqa.create_analysis('Test name 3')
        top_card = analysis.create_text_card("Test text 1")
        bottom_card = analysis.create_text_card("Test text 2")
        self.assertEqual(bottom_card._order_index, 1)
        bottom_card.move_below(1)
        self.assertEqual(bottom_card._order_index, 1)
        bottom_card.move_below(0)
        self.assertEqual(bottom_card._order_index, 1)
        top_card.move_below(1)
        self.assertEqual(top_card._order_index, 1)

    def test_move_down_with_int_order(self):
        analysis = self._eureqa.create_analysis('Test name 4')
        top_card = analysis.create_text_card("Test text 1")
        bottom_card = analysis.create_text_card("Test text 2")
        self.assertEqual(top_card._order_index, 0)
        top_card.move_above(0)
        self.assertEqual(top_card._order_index, 0)
        top_card.move_above(1)
        self.assertEqual(top_card._order_index, 0)
        bottom_card.move_above(0)
        self.assertEqual(bottom_card._order_index, 0)

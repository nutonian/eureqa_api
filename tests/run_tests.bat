ECHO OFF

REM If python or pip are in the path, use those instead
REM http://stackoverflow.com/questions/4781772/how-to-test-if-an-executable-exists-in-the-path-from-a-windows-batch-file

where /q python.exe && SET PYTHON_EXE="python" || SET PYTHON_EXE="c:\Python27\python.exe"
where /q pip.exe    && SET PIP_EXE="pip"       || SET PIP_EXE="c:\Python27\Scripts\pip.exe"

echo Using python executable:  %PYTHON_EXE%
echo Using pip executable:      %PIP_EXE%

echo Installing dependencies
%PIP_EXE% install requests
%PIP_EXE% install mock
%PIP_EXE% install numpy
%PIP_EXE% install pandas

SET old_dir=%cd%
SET bat_file_path=%~dp0
cd "%bat_file_path%..\..\skynet\"
node clear_docstore.js

cd "%bat_file_path%"
REM -v parameter makes it to include test names into the output
REM -b parameter makes it to buffer standard output from all tests unless they fail.
%PYTHON_EXE% -m unittest discover -v -b
@if ERRORLEVEL 1 ( exit /b 1 )
cd "%old_dir%

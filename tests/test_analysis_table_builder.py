import sys
import os
import copy

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa.analysis.components import *
from eureqa.analysis.components.table import _Table

from etestutils_nosearch import EureqaTestBaseNoSearch

from mock import patch
import unittest

import numpy as np
import pandas as pd
import requests

def mock_uuid4():
    mock_uuid4.curr_posfix += 1
    return "mock-uuid-" + str(mock_uuid4.curr_posfix);

def reset_mock_uuid4():
    mock_uuid4.curr_posfix = 0

class TestAnalysisTableBuilder(unittest.TestCase, EureqaTestBaseNoSearch):

    default_table_data = [
            {'cluster_id': 'a', 'priority': 'mid',  'row_components': None},
            {'cluster_id': 'b', 'priority': 'high', 'row_components': None},
            {'cluster_id': 'c', 'priority': 'low',  'row_components': None}]

    default_table_component = {
            'columns': [
                {'displayKey': 'cluster_id', 'name': 'cluster_id', 'sortKey': 'cluster_id', 'width': 1},
                {'displayKey': 'priority',   'name': 'priority',   'sortKey': 'priority',   'width': 1}],
            'component_type': 'TABLE',
            'defaultSort': {'key': 'cluster_id', 'order': 'ASC'},
            'filters': [],
            'pageSizes': [20, 10, 50, 100],
            'searchBoxAttributes': ['cluster_id', 'priority'],
            'searchBoxPlaceholder': 'Search',
            'striped': True,
            'table_id': 'dummy_table_id',
            'title': 'test table'}

    def test_create_using_pandas_dataframe(self):
        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})

        table_builder = TableBuilder(df, title = 'test table')
        self.assertEqual(self.default_table_data, table_builder._get_table_data(analysis=None))
        self.assertEqual(self.default_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

        # test failure case
        with self.assertRaises(RuntimeError) as context:
            table_builder = TableBuilder(df, title = 'test table', column_names = ['dummy name'])
            self.assertTrue('The length of column_names must be the same as number of columns' in context.exception)

        # test specifying column_names
        table_builder = TableBuilder(df, title = 'test table', column_names = ['col0', 'col1'])
        expected_table_data = [
                {'col0': 'a', 'col1': 'mid',  'row_components': None},
                {'col0': 'b', 'col1': 'high', 'row_components': None},
                {'col0': 'c', 'col1': 'low',  'row_components': None}]

        expected_table_component = {
                'columns': [
                    {'displayKey': 'col0', 'name': 'col0', 'sortKey': 'col0', 'width': 1},
                    {'displayKey': 'col1', 'name': 'col1', 'sortKey': 'col1', 'width': 1}],
                'component_type': 'TABLE',
                'defaultSort': {'key': 'col0', 'order': 'ASC'},
                'filters': [],
                'pageSizes': [20, 10, 50, 100],
                'searchBoxAttributes': ['col0', 'col1'],
                'searchBoxPlaceholder': 'Search',
                'striped': True,
                'table_id': 'dummy_table_id',
                'title': 'test table'}
        self.assertEqual(expected_table_data, table_builder._get_table_data(analysis=None))
        self.assertEqual(expected_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

    def test_create_using_numpy_array(self):
        ar = np.array([["a", "b", "c"],["mid", "high", "low"]]).T

        table_builder = TableBuilder(ar, title = 'test table', column_names = ['cluster_id', 'priority'])
        self.assertEqual(self.default_table_data, table_builder._get_table_data(analysis=None))
        self.assertEqual(self.default_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

        # test failure case
        with self.assertRaises(RuntimeError) as context:
            table_builder = TableBuilder(ar, title = 'test table')
            self.assertTrue('Must provide column_names and its length must be the same as number of columns' in context.exception)

    def test_create_using_pandas_dataframe_with_special_floags(self):
        df = pd.DataFrame({"a":[1, 2, np.nan, np.inf, -np.inf, 6]})

        table_builder = TableBuilder(df, title = 'test table')
        expected_table_data = [{'a': 1.0, 'row_components': None},
                               {'a': 2.0, 'row_components': None},
                               {'a': None, 'row_components': None},
                               {'a': None, 'row_components':None},
                               {'a': None, 'row_components': None},
                               {'a': 6.0, 'row_components': None}]
        self.assertEqual(expected_table_data, table_builder._get_table_data(analysis=None))

        analysis = self._eureqa.create_analysis('Test Use Table In Columns Analysis')
        analysis.create_card(table_builder)

    def test_create_using_numpy_array(self):
        ar = np.array([[1, 2, np.nan, np.inf, -np.inf, 6]]).T

        table_builder = TableBuilder(ar, title = 'test table')
        expected_table_data = [{'0': 1.0, 'row_components': None},
                               {'0': 2.0, 'row_components': None},
                               {'0': None, 'row_components': None},
                               {'0': None, 'row_components':None},
                               {'0': None, 'row_components': None},
                               {'0': 6.0, 'row_components': None}]
        self.assertEqual(expected_table_data, table_builder._get_table_data(analysis=None))

        analysis = self._eureqa.create_analysis('Test Use Table In Columns Analysis')
        analysis.create_card(table_builder)

    def test_create_using_list_of_columns(self):
        lt = [["a", "b", "c"],["mid", "high", "low"]]

        table_builder = TableBuilder(lt, title = 'test table', column_names = ['cluster_id', 'priority'])
        self.assertEqual(self.default_table_data, table_builder._get_table_data(analysis=None))
        self.assertEqual(self.default_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

        # test failure case
        with self.assertRaises(RuntimeError) as context:
            table_builder = TableBuilder(lt, title = 'test table')
            self.assertTrue('Must provide column_names and its length must be the same as number of columns' in context.exception)

    @patch('uuid.uuid4', mock_uuid4)
    def test_using_pandas_series_in_sort_and_rendered_values(self):
        reset_mock_uuid4()

        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})

        table_builder = TableBuilder(df, title = 'test table')
        table_builder['priority'].sort_values = pd.Series([1,2,3])
        table_builder['priority'].rendered_values = df['priority']
        expected_table_component = [
                {'cluster_id': 'a', 'mock-uuid-1': 1, 'mock-uuid-2': 'mid', 'priority': 'mid', 'row_components': []},
                {'cluster_id': 'b', 'mock-uuid-1': 2, 'mock-uuid-2': 'high', 'priority': 'high', 'row_components': []},
                {'cluster_id': 'c', 'mock-uuid-1': 3, 'mock-uuid-2': 'low', 'priority': 'low', 'row_components': []}]
        self.assertEqual(expected_table_component, table_builder._get_table_data(analysis=None))  # should pass and not throw exception

    @patch('uuid.uuid4', mock_uuid4)
    def test_sort_values_types(self):
        reset_mock_uuid4()

        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})

        table_builder = TableBuilder(df, title = 'test table')
        table_builder['priority'].sort_values = [3,1,2]
        table_builder['priority'].rendered_values = df['priority']
        expected_table_component = [
                {'cluster_id': 'a', 'mock-uuid-1': 3, 'mock-uuid-2': 'mid', 'priority': 'mid', 'row_components': []},
                {'cluster_id': 'b', 'mock-uuid-1': 1, 'mock-uuid-2': 'high', 'priority': 'high', 'row_components': []},
                {'cluster_id': 'c', 'mock-uuid-1': 2, 'mock-uuid-2': 'low', 'priority': 'low', 'row_components': []}]
        self.assertEqual(expected_table_component, table_builder._get_table_data(analysis=None))  # should pass and not throw exception

        table_builder = TableBuilder(df, title = 'test table')
        table_builder['priority'].sort_values = ['3','1','2']
        table_builder['priority'].rendered_values = df['priority']
        expected_table_component = [
                {'cluster_id': 'a', 'mock-uuid-3': '3', 'mock-uuid-4': 'mid', 'priority': 'mid', 'row_components': []},
                {'cluster_id': 'b', 'mock-uuid-3': '1', 'mock-uuid-4': 'high', 'priority': 'high', 'row_components': []},
                {'cluster_id': 'c', 'mock-uuid-3': '2', 'mock-uuid-4': 'low', 'priority': 'low', 'row_components': []}]
        self.maxDiff = None
        self.assertEqual(expected_table_component, table_builder._get_table_data(analysis=None))  # should pass and not throw exception

        table_builder = TableBuilder(df, title = 'test table')
        table_builder['priority'].sort_values = pd.Series([np.nan, -np.inf, np.inf])
        table_builder['priority'].rendered_values = df['priority']
        expected_table_component = [
                {'cluster_id': 'a', 'mock-uuid-5': -(2**52), 'mock-uuid-6': 'mid', 'priority': 'mid', 'row_components': []},
                {'cluster_id': 'b', 'mock-uuid-5': -(2**52), 'mock-uuid-6': 'high', 'priority': 'high', 'row_components': []},
                {'cluster_id': 'c', 'mock-uuid-5': 2**52, 'mock-uuid-6': 'low', 'priority': 'low', 'row_components': []}]
        self.assertEqual(expected_table_component, table_builder._get_table_data(analysis=None))  # should pass and not throw exception

        table_builder = TableBuilder(df, title = 'test table')
        table_builder['priority'].sort_values = np.array([None, -np.inf, np.inf])
        table_builder['priority'].rendered_values = df['priority']
        expected_table_component = [
                {'cluster_id': 'a', 'mock-uuid-7': None, 'mock-uuid-8': 'mid', 'priority': 'mid', 'row_components': []},
                {'cluster_id': 'b', 'mock-uuid-7': -(2**52), 'mock-uuid-8': 'high', 'priority': 'high', 'row_components': []},
                {'cluster_id': 'c', 'mock-uuid-7': 2**52, 'mock-uuid-8': 'low', 'priority': 'low', 'row_components': []}]
        self.assertEqual(expected_table_component, table_builder._get_table_data(analysis=None))  # should pass and not throw exception

    @patch('uuid.uuid4', mock_uuid4)
    def test_specify_sort_order_for_columns(self):
        reset_mock_uuid4()

        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})
        table_builder = TableBuilder(df, title = 'test table')
        table_builder['cluster_id'].sort_values = [ord(n) - ord('a') for n in df["cluster_id"]]

        # expect to add a synthesized column named with uuid, representing the sort order
        expected_table_data = [{'cluster_id': 'a', 'mock-uuid-1': 0, 'priority': 'mid',  'row_components': None},
                               {'cluster_id': 'b', 'mock-uuid-1': 1, 'priority': 'high', 'row_components': None},
                               {'cluster_id': 'c', 'mock-uuid-1': 2, 'priority': 'low',  'row_components': None}]
        self.assertEqual(expected_table_data, table_builder._get_table_data(analysis=None))

        # expect the sortKey of the column 'priority' is the synthesized column; the defaultSortKey should also reference the synthesized column
        expected_table_component = {
            'columns': [{'displayKey': 'cluster_id', 'name': 'cluster_id', 'sortKey': 'mock-uuid-1', 'width': 1},
                        {'displayKey': 'priority',   'name': 'priority',   'sortKey': 'priority',    'width': 1}],
            'component_type': 'TABLE',
            'defaultSort': {'key': 'mock-uuid-1', 'order': 'ASC'},
            'filters': [],
            'pageSizes': [20, 10, 50, 100],
            'searchBoxAttributes': ['cluster_id', 'priority'],
            'searchBoxPlaceholder': 'Search',
            'striped': True,
            'table_id': 'dummy_table_id',
            'title': 'test table' }
        self.assertEqual(expected_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

    @patch('uuid.uuid4', mock_uuid4)
    def test_use_component_in_columns(self):
        reset_mock_uuid4()

        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})
        table_builder = TableBuilder(df, title = 'test table')
        table_builder['priority'].rendered_values = [_Component() for i in df['priority']]

        # expect to add a synthesized column named with uuid, representing the rendered value
        # for the 'priority' column and the 'row_components' column contains the component definition
        expected_table_data = [{'cluster_id': 'a',
                                'mock-uuid-1': '<nutonian-component id="mock-uuid-2" />',
                                'priority': 'mid',
                                'row_components': [{'component_id': 'mock-uuid-2',
                                                    'component_type': 'GENERIC_COMPONENT'}]},
                               {'cluster_id': 'b',
                                'mock-uuid-1': '<nutonian-component id="mock-uuid-3" />',
                                'priority': 'high',
                                'row_components': [{'component_id': 'mock-uuid-3',
                                                    'component_type': 'GENERIC_COMPONENT'}]},
                               {'cluster_id': 'c',
                                'mock-uuid-1': '<nutonian-component id="mock-uuid-4" />',
                                'priority': 'low',
                                'row_components': [{'component_id': 'mock-uuid-4',
                                                    'component_type': 'GENERIC_COMPONENT'}]}]
        self.assertEqual(expected_table_data, table_builder._get_table_data(analysis=None))

        # expect the displayKey of the priority column to reference the synthesized column
        expected_table_component = {
            'columns': [{'displayKey': 'cluster_id',  'name': 'cluster_id', 'sortKey': 'cluster_id', 'width': 1},
                        {'displayKey': 'mock-uuid-1', 'name': 'priority',   'sortKey': 'priority',   'width': 1}],
            'component_type': 'TABLE',
            'defaultSort': {'key': 'cluster_id', 'order': 'ASC'},
            'filters': [],
            'pageSizes': [20, 10, 50, 100],
            'searchBoxAttributes': ['cluster_id', 'priority'],
            'searchBoxPlaceholder': 'Search',
            'striped': True,
            'table_id': 'dummy_table_id',
            'title': 'test table'}
        self.assertEqual(expected_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

    @patch('uuid.uuid4', mock_uuid4)
    def test_use_table_in_columns(self):
        reset_mock_uuid4()

        analysis = self._eureqa.create_analysis('Test Use Table In Columns Analysis')

        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})
        inner_table_builder = TableBuilder(df, title = 'inner test table')

        # outer table contains modal, which contains inner table
        modal = Modal(component=inner_table_builder, title='My modal')
        modal_link = modal.link(link_text='Open a modal with a table in it')
        outer_table_builder = TableBuilder(df, title = 'outer test table')
        outer_table_builder['priority'].rendered_values = [(modal.link(link_text='Open a modal with a table in it') if i=='high' else '')
                                                           for i in df['priority']]

        # expect to add a synthesized column named with uuid, representing the rendered value
        # for the 'priority' column and the 'row_components' column contains the component definition
        expected_table_data = [{'priority': 'mid',  'row_components': None, 'cluster_id': 'a'},
                               {'priority': 'high', 'row_components': None, 'cluster_id': 'b'},
                               {'priority': 'low',  'row_components': None, 'cluster_id': 'c'}]
        self.assertEqual(expected_table_data, inner_table_builder._get_table_data(analysis=analysis))
        expected_table_data = [{'priority': 'mid', 'row_components': [], 'mock-uuid-1': '', 'cluster_id': 'a'},
                               {'cluster_id': 'b',
                                'mock-uuid-1': '<nutonian-component id="mock-uuid-3" />',
                                'priority': 'high',
                                'row_components': [{'component_id': 'mock-uuid-3',
                                                    'component_type': 'MODAL_LINK',
                                                    'link_text': 'Open a modal with a table in it',
                                                    'modal_component_id': 'mock-uuid-2'},
                                                   {'component_id': 'mock-uuid-2',
                                                    'component_type': 'MODAL',
                                                    'content_component_id': '0',
                                                    'size': 'small',
                                                    'title': 'My modal'},
                                                   {'analysis_id': analysis._id,
                                                    'columns': [{'displayKey': 'cluster_id',
                                                                 'name': 'cluster_id',
                                                                 'sortKey': 'cluster_id',
                                                                 'width': 1},
                                                                {'displayKey': 'priority',
                                                                 'name': 'priority',
                                                                 'sortKey': 'priority',
                                                                 'width': 1}],
                                                    'component_id': '0',
                                                    'component_type': 'TABLE',
                                                    'defaultSort': {'key': 'cluster_id', 'order': 'ASC'},
                                                    'filters': [],
                                                    'pageSizes': [20, 10, 50, 100],
                                                    'searchBoxAttributes': ['cluster_id', 'priority'],
                                                    'searchBoxPlaceholder': 'Search',
                                                    'striped': True,
                                                    'table_id': '0',
                                                    'title': 'inner test table'}]},
                               {'priority': 'low', 'row_components': [], 'mock-uuid-1': '', 'cluster_id': 'c'}]
        self.assertEqual(expected_table_data, outer_table_builder._get_table_data(analysis=analysis))

        # make sure we can add the tables to an analysis (covers TableBuilder._associate_with_table)
        analysis.create_card(outer_table_builder)

    @patch('uuid.uuid4', mock_uuid4)
    def test_use_component_in_columns_bug_repro(self):
        reset_mock_uuid4()

        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})
        table_builder = TableBuilder(df, title = 'test table')

        # the bug was that if the last column doesn't have any components, the 'row_components' was set to empty
        # so we're setting the first column 'cluster_id' to use some components and the last column 'priority' to not use any component
        table_builder['cluster_id'].rendered_values = [_Component() for i in df['cluster_id']]

        # expect to add a synthesized column named with uuid, representing the rendered value
        # for the 'cluster_id' column and the 'row_components' column contains the component definition
        expected_table_data = [{'cluster_id': 'a',
                                'mock-uuid-1': '<nutonian-component id="mock-uuid-2" />',
                                'priority': 'mid',
                                'row_components': [{'component_id': 'mock-uuid-2',
                                                    'component_type': 'GENERIC_COMPONENT'}]},
                               {'cluster_id': 'b',
                                'mock-uuid-1': '<nutonian-component id="mock-uuid-3" />',
                                'priority': 'high',
                                'row_components': [{'component_id': 'mock-uuid-3',
                                                    'component_type': 'GENERIC_COMPONENT'}]},
                               {'cluster_id': 'c',
                                'mock-uuid-1': '<nutonian-component id="mock-uuid-4" />',
                                'priority': 'low',
                                'row_components': [{'component_id': 'mock-uuid-4',
                                                    'component_type': 'GENERIC_COMPONENT'}]}]
        self.assertEqual(expected_table_data, table_builder._get_table_data(analysis=None))

        # expect the displayKey of the 'cluster_id' column to reference the synthesized column
        expected_table_component = {
            'columns': [{'displayKey': 'mock-uuid-1', 'name': 'cluster_id', 'sortKey': 'cluster_id', 'width': 1},
                        {'displayKey': 'priority',    'name': 'priority',   'sortKey': 'priority',   'width': 1}],
            'component_type': 'TABLE',
            'defaultSort': {'key': 'cluster_id', 'order': 'ASC'},
            'filters': [],
            'pageSizes': [20, 10, 50, 100],
            'searchBoxAttributes': ['cluster_id', 'priority'],
            'searchBoxPlaceholder': 'Search',
            'striped': True,
            'table_id': 'dummy_table_id',
            'title': 'test table'}
        self.assertEqual(expected_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

    @patch('uuid.uuid4', mock_uuid4)
    def test_use_component_in_title_and_column_header(self):
        reset_mock_uuid4()

        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})
        table_builder = TableBuilder(df, title = Tooltip(html='table title', tooltip='this is the tooltip'))
        table_builder['cluster_id'].column_header = FormattedText("here is tooltip comp {0}", Tooltip(html='cluster_id', tooltip='this is the tooltip'))  # test using FormattedText
        table_builder['priority'].column_header = Tooltip(html='priority', tooltip='this is the tooltip')                                                 # test using Component directly

        analysis = self._eureqa.create_analysis('Test By Row Plot Analysis')
        table_builder._associate(analysis)

        table_data_endpoint = '/analysis/%s/tables/%s' % (analysis._id, table_builder._table_id)
        table_data_resp = analysis._eureqa._session.execute(table_data_endpoint, method='GET')
        self.assertEqual(self.default_table_data, table_data_resp)

        table_component_endpoint = '/analysis/%s/components/%s' % (analysis._id, table_builder._component_id)
        table_component_resp = analysis._eureqa._session.execute(table_component_endpoint, method='GET')

        expected_table_component = self.default_table_component
        expected_table_component['analysis_id']  = analysis._id
        expected_table_component['table_id']     = table_builder._table_id
        expected_table_component['component_id'] = table_builder._component_id
        expected_table_component['title'] = '<nutonian-component id="0" />'  # expect title to reference a component
        expected_table_component['columns'] = [  # expect 'name' of each column reference a component
                {'displayKey': 'cluster_id', 'name': 'here is tooltip comp <nutonian-component id="1" />', 'sortKey': 'cluster_id', 'width': 1},
                {'displayKey': 'priority',   'name': '<nutonian-component id="2" />',                      'sortKey': 'priority',   'width': 1}]
        self.maxDiff = None
        self.assertEqual(expected_table_component, table_component_resp)

    @patch('uuid.uuid4', mock_uuid4)
    def test_use_formatted_text_in_columns(self):
        reset_mock_uuid4()

        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})
        table_builder = TableBuilder(df, title = 'test table')
        table_builder['priority'].rendered_values = [FormattedText("here is a {comp}", comp=_Component()) for i in df['priority']]

        # expect to add a synthesized column named with uuid, representing the rendered value
        # for the 'priority' column and the 'row_components' column contains the component definition
        expected_table_data = [{'cluster_id': 'a',
                                'mock-uuid-1': 'here is a <nutonian-component id="mock-uuid-2" />',
                                'priority': 'mid',
                                'row_components': [{'component_id': 'mock-uuid-2',
                                                    'component_type': 'GENERIC_COMPONENT'}]},
                               {'cluster_id': 'b',
                                'mock-uuid-1': 'here is a <nutonian-component id="mock-uuid-3" />',
                                'priority': 'high',
                                'row_components': [{'component_id': 'mock-uuid-3',
                                                    'component_type': 'GENERIC_COMPONENT'}]},
                               {'cluster_id': 'c',
                                'mock-uuid-1': 'here is a <nutonian-component id="mock-uuid-4" />',
                                'priority': 'low',
                                'row_components': [{'component_id': 'mock-uuid-4',
                                                    'component_type': 'GENERIC_COMPONENT'}]}]
        self.assertEqual(expected_table_data, table_builder._get_table_data(analysis=None))

        # expect the displayKey of the priority column to reference the synthesized column
        expected_table_component = {
            'columns': [{'displayKey': 'cluster_id',  'name': 'cluster_id', 'sortKey': 'cluster_id', 'width': 1},
                        {'displayKey': 'mock-uuid-1', 'name': 'priority',   'sortKey': 'priority',   'width': 1}],
            'component_type': 'TABLE',
            'defaultSort': {'key': 'cluster_id', 'order': 'ASC'},
            'filters': [],
            'pageSizes': [20, 10, 50, 100],
            'searchBoxAttributes': ['cluster_id', 'priority'],
            'searchBoxPlaceholder': 'Search',
            'striped': True,
            'table_id': 'dummy_table_id',
            'title': 'test table'}
        self.assertEqual(expected_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

    def test_filter_only_column(self):
        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})
        table_builder = TableBuilder(df, title = 'test table')
        table_builder['priority'].filter_only = True

        # expect the filter_only column still show up in the table data
        expected_table_data = [
                {'cluster_id': 'a', 'priority': 'mid',  'row_components': None},
                {'cluster_id': 'b', 'priority': 'high', 'row_components': None},
                {'cluster_id': 'c', 'priority': 'low',  'row_components': None}]
        self.assertEqual(self.default_table_data, table_builder._get_table_data(analysis=None))

        expected_table_component = {
            'columns': [{'displayKey': 'cluster_id', 'name': 'cluster_id', 'sortKey': 'cluster_id', 'width': 1}],
            'component_type': 'TABLE',
            'defaultSort': {'key': 'cluster_id', 'order': 'ASC'},
            'filters': [{'key': 'priority', 'label': 'priority'}],
            'pageSizes': [20, 10, 50, 100],
            'searchBoxAttributes': ['cluster_id'],
            'searchBoxPlaceholder': 'Search',
            'striped': True,
            'table_id': 'dummy_table_id',
            'title': 'test table'}
        self.assertEqual(expected_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

    def test_set_table_properties(self):
        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})

        # test throwing exception if default_rows_per_page is not one of the allowed values
        with self.assertRaises(RuntimeError) as context:
            table_builder = TableBuilder(df, title = 'test table', default_rows_per_page = 11)
            self.assertTrue('Only one of [10, 20, 50, 100] is allowed for default_rows_per_page' in context.exception)

        table_builder = TableBuilder(df, title = 'test table', default_rows_per_page = 50, striped = False, search_box_place_holder = 'Please type search keywords')

        # test changing column name, from now on, have to use the new name to access the column
        table_builder['cluster_id'].column_name = 'cluster'
        table_builder['cluster'].column_header = 'cluster_name'
        table_builder['cluster'].searchable = False

        # test throwing exception if column width is less than or equal to zero
        with self.assertRaises(RuntimeError) as context:
            table_builder['cluster'].width = 0
            self.assertTrue('Column width must be larger than zero' in context.exception)
        with self.assertRaises(RuntimeError) as context:
            table_builder['cluster'].width = -0.1
            self.assertTrue('Column width must be larger than zero' in context.exception)

        table_builder['cluster'].width = 0.5
        table_builder['cluster'].filterable = True
        table_builder['cluster'].filter_name = "Filter by cluster name"

        # expect the column name is 'cluster' instead of 'cluster_id'
        expected_table_data = [
                {'cluster': 'a', 'priority': 'mid',  'row_components': None},
                {'cluster': 'b', 'priority': 'high', 'row_components': None},
                {'cluster': 'c', 'priority': 'low',  'row_components': None}]
        self.assertEqual(expected_table_data, table_builder._get_table_data(analysis=None))

        # expect column references to 'cluster' instead of 'cluster_id', and all modified properties are reflected in the generated component
        expected_table_component = {
                'columns': [{'displayKey': 'cluster', 'name': 'cluster_name', 'sortKey': 'cluster', 'width': 0.5},
                            {'displayKey': 'priority',   'name': 'priority',  'sortKey': 'priority',   'width': 1}],
                'component_type': 'TABLE',
                'defaultSort': {'key': 'cluster', 'order': 'ASC'},
                'filters': [{'key': 'cluster', 'label': 'Filter by cluster name'}],
                'pageSizes': [50, 10, 20, 100],
                'searchBoxAttributes': ['priority'],
                'searchBoxPlaceholder': 'Please type search keywords',
                'striped': False,
                'table_id': 'dummy_table_id',
                'title': 'test table'}

        self.assertEqual(expected_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

    def _send_receive_round_trip(self, send_to_server_func):
        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})

        table_builder = TableBuilder(df, title = 'test table')
        self.assertEqual(self.default_table_data, table_builder._get_table_data(analysis=None))
        self.assertEqual(self.default_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

        analysis = self._eureqa.create_analysis('Test By Row Plot Analysis')

        send_to_server_func(table_builder, analysis)

        table_data_endpoint = '/analysis/%s/tables/%s' % (analysis._id, table_builder._table_id)
        table_data_resp = analysis._eureqa._session.execute(table_data_endpoint, method='GET')
        self.assertEqual(self.default_table_data, table_data_resp)

        table_component_endpoint = '/analysis/%s/components/%s' % (analysis._id, table_builder._component_id)
        table_component_resp = analysis._eureqa._session.execute(table_component_endpoint, method='GET')

        expected_table_component = copy.copy(self.default_table_component)
        expected_table_component['analysis_id']  = analysis._id
        expected_table_component['table_id']     = table_builder._table_id
        expected_table_component['component_id'] = table_builder._component_id
        self.assertEqual(expected_table_component, table_component_resp)

    def test_send_receive_round_trip_with_associate(self):
        def send_to_server_func(table_builder, analysis):
            self.assertFalse(hasattr(table_builder, "_component_id"))
            table_builder._associate(analysis)
            self.assertTrue(hasattr(table_builder, "_component_id"))
        self._send_receive_round_trip(send_to_server_func)

    def test_send_receive_round_trip_with_create_item(self):
        def send_to_server_func(table_builder, analysis):
            self.assertFalse(hasattr(table_builder, "_component_id"))
            analysis.create_card(component = table_builder)
            self.assertTrue(hasattr(table_builder, "_component_id"))
        self._send_receive_round_trip(send_to_server_func)

    def test_using_download_file_component_in_table(self):
        analysis = self._eureqa.create_analysis('Test By Row Plot Analysis')

        df = pd.DataFrame({"file":["a", "b", "c"], "user":["jlpicard", "root", "spock"]})

        table_builder = TableBuilder(df, title = 'test table')
        download_file_components = []
        download_file_components.append(DownloadFile(file_content="content a", link_text='link to file_a', filename='a.txt'))
        download_file_components.append(DownloadFile(file_content="content b", link_text='link to file_b', filename='b.txt'))
        download_file_components.append(FormattedText("here is link to file {comp}", comp = DownloadFile(file_content="content c", link_text='file_c', filename='c.txt')))
        table_builder["file"].rendered_values = download_file_components

        # RE #6540: the DOWNLOAD_FILE component 'row_components' column should have file_id and file_url fields
        table_data = table_builder._get_table_data(analysis=analysis)
        self.assertEqual(3, len(table_data))
        for i, row in enumerate(table_data):
            row_components = row['row_components']
            self.assertEqual(1, len(row_components))
            component = row_components[0]

            self.assertEqual('DOWNLOAD_FILE', component['component_type'])
            self.assertIsNotNone(component['file_id'])
            self.assertIsNotNone(component['file_url'])

            expected_file_url = '/api/%s/analysis/%s/files/%s' % (analysis._organization, analysis._id, component['file_id'])
            self.assertEqual(expected_file_url, component['file_url'])

            # test the download filename is set correctly
            download_resp = requests.get(
                    analysis._eureqa._session.url + expected_file_url,
                    cookies=analysis._eureqa._session._session.cookies)
            expected_file_name = 'abc'[i] + '.txt'
            self.assertEqual('attachment; filename="'+expected_file_name+'"', download_resp.headers['content-disposition'])

    def test_add_to_layout(self):
        dropdown = DropdownLayout()
        df = pd.DataFrame({col: np.random.random(10) for col in range(5)})

        table = TableBuilder(df, 'table title')
        dropdown.add_component(title='title', content=table)
        self._analysis.create_card(dropdown)

        self.assertEqual(table._table_component._component_id, dropdown._components[0]["component_id"])
        self.assertTrue(isinstance(table._table_component._component_id, basestring))

        # Make sure table was actually saved properly
        t = _Table._get(_component_id=table._table_component._component_id, _analysis=self._analysis)
        self.assertEqual(t._component_id, dropdown._components[0]["component_id"])
        self.assertEqual(t.title, 'table title')

    def test_create_using_pandas_dataframe_with_numeric_column_names(self):
        numeric_columns_table_data = [
                {'0': 'a', '1': 'mid',  'row_components': None},
                {'0': 'b', '1': 'high', 'row_components': None},
                {'0': 'c', '1': 'low',  'row_components': None}]

        numeric_columns_table_component = {
                'columns': [
                    {'displayKey': '0', 'name': '0', 'sortKey': '0', 'width': 1},
                    {'displayKey': '1',   'name': '1',   'sortKey': '1',   'width': 1}],
                'component_type': 'TABLE',
                'defaultSort': {'key': '0', 'order': 'ASC'},
                'filters': [],
                'pageSizes': [20, 10, 50, 100],
                'searchBoxAttributes': ['0', '1'],
                'searchBoxPlaceholder': 'Search',
                'striped': True,
                'table_id': 'dummy_table_id',
                'title': 'test table'}

        df = pd.DataFrame({0:["a", "b", "c"], 1:["mid", "high", "low"]})

        table_builder = TableBuilder(df, title = 'test table')
        self.assertEqual(numeric_columns_table_data, table_builder._get_table_data(analysis=None))
        self.assertEqual(numeric_columns_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

    def test_specify_default_sort_column(self):
        self.maxDiff = None

        expected_table_component = copy.copy(self.default_table_component)

        df = pd.DataFrame({"cluster_id":["a", "b", "c"], "priority":["mid", "high", "low"]})
        table_builder = TableBuilder(df, title = 'test table', default_sort_order = 'priority')
        expected_table_component['defaultSort'] = {'key': 'priority', 'order': 'ASC'}
        self.assertEqual(expected_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

        table_builder = TableBuilder(df, title = 'test table', default_sort_order = ('priority', 'DESC'))
        expected_table_component['defaultSort'] = {'key': 'priority', 'order': 'DESC'}
        self.assertEqual(expected_table_component, table_builder._get_table_component("dummy_table_id")._to_json())

        table_builder = TableBuilder(df, title = 'test table', default_sort_order = 123)
        with self.assertRaises(Exception) as e:
            table_builder._get_table_component("dummy_table_id")._to_json()
            self.assertTrue('default_sort_order can only contain strings and tuples' in e.exception)

        table_builder = TableBuilder(df, title = 'test table', default_sort_order = ('cluster_id', 'ASC', 'DESC'))
        with self.assertRaises(Exception) as e:
            table_builder._get_table_component("dummy_table_id")._to_json()
            self.assertTrue('default_sort_order can only contain tuples of length 2' in e.exception)

        table_builder = TableBuilder(df, title = 'test table', default_sort_order = ('cluster_id'))
        with self.assertRaises(Exception) as e:
            table_builder._get_table_component("dummy_table_id")._to_json()
            self.assertTrue('default_sort_order can only contain tuples of length 2' in e.exception)



import sys
import os
from types import MethodType

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa.analysis.analysis_file import AnalysisFile

from etestutils_nosearch import EureqaTestBaseNoSearch

import unittest
from cStringIO import StringIO

class TestAnalysisFile(unittest.TestCase, EureqaTestBaseNoSearch):
    @classmethod
    def setUpClass(cls):
        all_bytes = "".join(chr(i) for i in xrange(256))
        cls.FILE_DATA = all_bytes + " test " + all_bytes

    def test_create_get_file(self):
        # As string
        f = AnalysisFile.create(self._analysis, self.FILE_DATA)
        self.assertEqual(f.get().read(), self.FILE_DATA)

        # As file-like object
        f = AnalysisFile.create(self._analysis, StringIO(self.FILE_DATA))
        self.assertEqual(f.get().read(), self.FILE_DATA)

    def test_update_file(self):
        f = AnalysisFile.create(self._analysis, self.FILE_DATA)

        new_file_data = self.FILE_DATA + " 2"
        # As string
        f.update(new_file_data)
        self.assertEqual(f.get().read(), new_file_data)

        new_file_data = self.FILE_DATA + " 3"
        # As file-like object
        f.update(StringIO(new_file_data))
        self.assertEqual(f.get().read(), new_file_data)

    def test_delete_file(self):
        f = AnalysisFile.create(self._analysis, self.FILE_DATA)
        f.delete()

        with self.assertRaises(Exception):
            # File should no longer be accessible
            f.get()

    def test_types(self):
        def test_type_of_file(filename, expected_content_type):
            f = AnalysisFile.create(self._analysis, open(filename, 'rb'), filename=filename)
            file_data = f.get()
            headers = self._eureqa._session._last_response.headers
            self.assertEqual(headers['content-type'], expected_content_type)
            self.assertEqual(file_data.read(), open(filename, 'rb').read())

        test_type_of_file('Test image 1.gif',  'image/gif')
        test_type_of_file('Test image 2.png',  'image/png')
        test_type_of_file('Test image 3.jpg',  'image/jpeg')
        test_type_of_file('Test image 4.jpeg', 'image/jpeg')
        test_type_of_file('Test image 5.svg',  'image/svg+xml')
        test_type_of_file('simple_data.csv',   'application/octet-stream')

# Copyright (c) 2017, Nutonian Inc
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the Nutonian Inc nor the
#     names of its contributors may be used to endorse or promote products
#     derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL NUTONIAN INC BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa

import unittest

from etestutils_nosearch import EureqaTestBaseNoSearch
from eureqa.analysis.components import TabbedLayout, TextBlock, HtmlBlock, _Component
from copy import deepcopy

class TestTabbedLayoutComponent(unittest.TestCase, EureqaTestBaseNoSearch):

    def tabs_as_json(self, tabs):
        tabs_copy = deepcopy(tabs)
        for tab in tabs_copy:
            tab._associate(self._analysis)
        return [x._to_json() for x in tabs_copy]

    def test_component(self):
        text_tab = TextBlock("Test")
        html_tab = HtmlBlock("Test")

        with open("Test image 2.png","rb") as f:
            img_data = f.read()

        tabs = [TabbedLayout._Tab("Text Tab", text_tab, img_data),
                TabbedLayout._Tab("HTML Tab", html_tab)]

        comp = TabbedLayout()
        comp.add_component("Text Tab", text_tab, img_data)
        comp.add_component("HTML Tab", html_tab)

        self.assertEqual(comp._tab_type, "default")

        # Make sure constructor set all fields
        self.assertEqual(len(comp._queued_tabs), 2)
        self.assertEqual(comp._queued_tabs[0]._title, "Text Tab")
        self.assertEqual(comp._queued_tabs[0]._component, text_tab)
        self.assertEqual(comp._queued_tabs[0]._icon, img_data)
        self.assertEqual(comp._queued_tabs[1]._title, "HTML Tab")
        self.assertEqual(comp._queued_tabs[1]._component, html_tab)
        self.assertEqual(comp._queued_tabs[1]._icon, None)

        self._analysis.create_card(comp)

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(len(comp._tabs), 2)
        self.assertEqual(len(comp._queued_tabs), 0)
        self.assertEqual(comp._tabs[0]["label"], "Text Tab")
        self.assertTrue(comp._tabs[0].get("component_id"))
        self.assertTrue(comp._tabs[0].get("icon_file_id"))
        self.assertEqual(comp._tabs[1]["label"], "HTML Tab")
        self.assertTrue(comp._tabs[1].get("component_id"))
        self.assertTrue("icon_file_id" in comp._tabs[1])

        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # Make sure we can fetch all fields from scratch
        self.assertEqual(len(comp._tabs), 2)
        self.assertEqual(len(comp._queued_tabs), 0)
        self.assertEqual(comp._tabs[0]["label"], "Text Tab")
        self.assertTrue(comp._tabs[0].get("component_id"))
        self.assertTrue(comp._tabs[0].get("icon_file_id"))
        self.assertEqual(comp._tabs[1]["label"], "HTML Tab")
        self.assertTrue(comp._tabs[1].get("component_id"))
        self.assertTrue("icon_file_id" in comp._tabs[1])

        # Test with analysis passed to constructor
        comp = TabbedLayout(tabs, _analysis=self._analysis)

        # Make sure constructor set all fields
        self.assertEqual(len(comp._tabs), 2)
        self.assertEqual(len(comp._queued_tabs), 0)
        self.assertEqual(comp._tabs[0]["label"], "Text Tab")
        self.assertTrue(comp._tabs[0].get("component_id"))
        self.assertTrue(comp._tabs[0].get("icon_file_id"))
        self.assertEqual(comp._tabs[1]["label"], "HTML Tab")
        self.assertTrue(comp._tabs[1].get("component_id"))
        self.assertTrue("icon_file_id" in comp._tabs[1])

    def test_tab_types(self):
        comp = TabbedLayout()
        self.assertEqual(comp._tab_type, "default")

        comp = TabbedLayout(tab_type="mode_switcher")
        self.assertEqual(comp._tab_type, "mode_switcher")

        with self.assertRaises(Exception) as content:
            comp = TabbedLayout(tab_type="invalid!")
            self.assertTrue("'invalid!' is not a valid tab_type. Valid tab_types are: ['default', 'mode_switcher']" in context.exception)

    def test_tab_type_to_json(self):
        comp = TabbedLayout()
        self.assertEqual(comp._to_json(), {
            "component_type": "TABBED_LAYOUT",
            "tab_type": "default"
        })

        comp = TabbedLayout(tab_type="default")
        self.assertEqual(comp._to_json(), {
            "component_type": "TABBED_LAYOUT",
            "tab_type": "default"
        })

        comp = TabbedLayout(tab_type="mode_switcher")
        self.assertEqual(comp._to_json(), {
            "component_type": "TABBED_LAYOUT",
            "tab_type": "mode_switcher"
        })

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import *
from eureqa.analysis_templates import *

import unittest
import tempfile
import shutil
import zipfile

from etestutils import EureqaTestBase

class TestAnalysisTemplate(unittest.TestCase, EureqaTestBase):
    def test_create(self):
        template = self._eureqa.create_analysis_template(
            "Test template 1",
            "Test description 1",
            Parameters([TextParameter('text_1', 'Text input 1 label.'),
                        DataSourceParameter('datasource_1', 'Data source 1',
                                            [VariableParameter('variable_1', 'Variable 1')]),
                        TextParameter('text_2', 'Text input 2 label.')]),
            None)
        self.assertEqual(template.name, "Test template 1")
        self.assertEqual(template.description, "Test description 1")
        self.assertEqual(template._icon_url, None)
        self.assertEqual(template._icon_url_fill_custom, False)
        self.assertEqual(len(template.parameters.parameters), 3)
        text_input_1 = template.parameters.parameters.values()[0]
        self.assertEqual(text_input_1.id, 'text_1')
        self.assertEqual(text_input_1.label, 'Text input 1 label.')
        self.assertEqual(text_input_1._type, 'text')
        data_source = template.parameters.parameters.values()[1]
        self.assertEqual(data_source.id, 'datasource_1')
        self.assertEqual(data_source.label, 'Data source 1')
        self.assertEqual(data_source._type, "datasource")
        self.assertEqual(len(data_source.variables), 1)
        variable = data_source.variables[0]
        self.assertEqual(variable.id, 'variable_1')
        self.assertEqual(variable.label, 'Variable 1')
        self.assertEqual(variable._type, 'variable')
        text_input_2 = template.parameters.parameters.values()[2]
        self.assertEqual(text_input_2.id, 'text_2')
        self.assertEqual(text_input_2.label, 'Text input 2 label.')
        self.assertEqual(text_input_2._type, 'text')

    def test_create_with_icon(self):
        template = self._eureqa.create_analysis_template(
            "Test template 1",
            "Test description 1",
            Parameters([TextParameter('text_1', 'Text input 1 label.'),
                        DataSourceParameter('datasource_1', 'Data source 1',
                                            [VariableParameter('variable_1', 'Variable 1')]),
                        TextParameter('text_2', 'Text input 2 label.')]),
            'bogus icon URL which will be ignored',
            'Test image 2.png')
        self.assertEqual(template.name, "Test template 1")
        self.assertEqual(template.description, "Test description 1")
        self.assertEqual(template._icon_url, "/api/root/analysis_templates/%s/icon" % template._id)
        self.assertEqual(template._icon_url_fill_custom, True)

        image_data = self._eureqa._session.execute(template._icon_url, method='GET', raw_return=True)
        self.assertEqual(image_data, open('Test image 2.png', 'rb').read())

    def test_create_with_icon_url(self):
        template = self._eureqa.create_analysis_template(
            "Test template 1",
            "Test description 1",
            Parameters([TextParameter('text_1', 'Text input 1 label.'),
                        DataSourceParameter('datasource_1', 'Data source 1',
                                            [VariableParameter('variable_1', 'Variable 1')]),
                        TextParameter('text_2', 'Text input 2 label.')]),
            None, "Test image 5.svg")
        self.assertEqual(template.name, "Test template 1")
        self.assertEqual(template.description, "Test description 1")
        self.assertEqual(template._icon_url_fill_custom, True)

        image_data = self._eureqa._session.execute(template._icon_url, method='GET', raw_return=True)
        self.assertEqual(image_data, open('Test image 5.svg', 'rb').read())

    def test_create_non_root(self):
        non_root_org = self._eureqa._create_organization("analysis_templates_test_org")
        try:
            non_root_org.create_user("non.root.user@nutonian.com", "Changeme1", "admin", "Mike", "Pink")
        except:
            pass  ## User probably already exists, which is fine.  If not, we'll blow up later.

        # Changing to the new org as root to create analysis templates.
        eureqa_non_root = Eureqa(url=self._eureqa_url, user_name='non.root.user@nutonian.com', password='Changeme1',
                                 organization='analysis_templates_test_org')

        try:
            template = eureqa_non_root.create_analysis_template(
                "Test template 1",
                "Test description 1",
                Parameters([DataSourceParameter('datasource_1', 'Data source 1',
                                                [VariableParameter('variable_1', 'Variable 1')]),
                           TextParameter('text_1', 'Text input 1 label.'),
                            TextParameter('text_2', 'Text input 2 label.')]),
                None)
            self.fail("Analysis templates shouldn't be creatable as non-root user")
        except Exception as e:
            self.assertEqual(str(e), "Analysis templates can only be modified by a root user. You are 'non.root.user@nutonian.com'")

    def test_get(self):
        template = self._eureqa.create_analysis_template(
            "Test template 2",
            "Test description 2",
            Parameters([TextParameter('text_1', 'Text input 1 label.'),
                        DataSourceParameter('datasource_1', 'Data source 1',
                                            [VariableParameter('variable_1', 'Variable 1')]),
                        TextParameter('text_2', 'Text input 2 label.')]),
            None)
        template = self._eureqa._get_analysis_template(template._id)
        self.assertEqual(template.name, "Test template 2")
        self.assertEqual(template.description, "Test description 2")
        self.assertEqual(len(template.parameters.parameters), 3)
        text_input_1 = template.parameters.parameters.values()[0]
        self.assertEqual(text_input_1.id, 'text_1')
        self.assertEqual(text_input_1.label, 'Text input 1 label.')
        self.assertEqual(text_input_1._type, 'text')
        data_source = template.parameters.parameters.values()[1]
        self.assertEqual(data_source.id, 'datasource_1')
        self.assertEqual(data_source.label, 'Data source 1')
        self.assertEqual(data_source._type, "datasource")
        self.assertEqual(len(data_source.variables), 1)
        variable = data_source.variables[0]
        self.assertEqual(variable.id, 'variable_1')
        self.assertEqual(variable.label, 'Variable 1')
        self.assertEqual(variable._type, 'variable')
        text_input_2 = template.parameters.parameters.values()[2]
        self.assertEqual(text_input_2.id, 'text_2')
        self.assertEqual(text_input_2.label, 'Text input 2 label.')
        self.assertEqual(text_input_2._type, 'text')

    def test_delete(self):
        template = self._eureqa.create_analysis_template(
            "Test template 3",
            "Test description 3",
            Parameters([DataSourceParameter('datasource_1', 'Data source 1',
                                            [VariableParameter('variable_1', 'Variable 1')])]),
            None)
        template.delete()
        templates = self._eureqa.get_all_analysis_templates()
        templates = [x for x in templates if x.name == 'Test template 3']
        self.assertEqual(len(templates), 0)

    def test_to_string(self):
        template = self._eureqa.create_analysis_template(
            "Test template 4",
            "Test description 4",
            Parameters([TextParameter('text_1', 'Text input 1 label.'),
                        DataSourceParameter('datasource_1', 'Data source 1',
                                            [VariableParameter('variable_1', 'Variable 1')]),
                        TextParameter('text_2', 'Text input 2 label.')]),
            None)
        template.__str__()

    def test_update_module(self):
        template = self._eureqa.create_analysis_template(
            "Test template 1",
            "Test description 1",
            Parameters([DataSourceParameter('datasource_1', 'Data source 1',
                                            [VariableParameter('variable_1', 'Variable 1')])]),
            None)

        tempdir = tempfile.mkdtemp()

        try:
            testmodule_path = os.path.join(tempdir, "testmodule")
            os.mkdir(testmodule_path)

            ## Create an __init__.py to make it a module
            with open(os.path.join(testmodule_path, "__init__.py"), "wb") as f:
                pass  ## File can be empty.  Opening it creates it.

            with open(os.path.join(testmodule_path, "analytic_template_test.py"), "wb") as f:
                f.write("def analysis_template_function_body(eureqa, template_execution):\n    print('DEF')")

            ## Upload the zip file
            template.set_module("testmodule.analytic_template_test", testmodule_path)

            ## Get it back get_module(), while we're here
            zip_filename = os.path.join(tempdir, "analysis_module.zip")

            template.get_module(zip_filename)
            with zipfile.ZipFile(zip_filename, 'r') as zf:
                self.assertEqual(set(zf.namelist()), set([
                    "manifest/eureqa_main_module_name.txt",
                    "testmodule/__init__.py",
                    "testmodule/analytic_template_test.py",
                ]))
            self.assertEqual('testmodule.analytic_template_test', AnalysisTemplate._get_main_module_name_from_analysis_template_zip(zip_filename))

        finally:
            shutil.rmtree(tempdir)

    def test_update_name(self):
        template = self._eureqa.create_analysis_template(
            "Test template 5",
            "Test description 5",
            Parameters([TextParameter('text_1', 'Text input 1 label.'),
                        DataSourceParameter('datasource_1', 'Data source 1',
                                            [VariableParameter('variable_1', 'Variable 1')]),
                        TextParameter('text_2', 'Text input 2 label.')]),
            None)

        template.name = "Test template 5 but Renamed"
        self.assertEqual(template.name, "Test template 5 but Renamed")

        template = self._eureqa._get_analysis_template(template._id)
        self.assertEqual(template.name, "Test template 5 but Renamed")

    def test_update_description(self):
        template = self._eureqa.create_analysis_template(
            "Test template 5",
            "Test description 5",
            Parameters([TextParameter('text_1', 'Text input 1 label.'),
                        DataSourceParameter('datasource_1', 'Data source 1',
                                            [VariableParameter('variable_1', 'Variable 1')]),
                        TextParameter('text_2', 'Text input 2 label.')]),
            None)

        template.description = "Test description 5 but Renamed"
        self.assertEqual(template.description, "Test description 5 but Renamed")

        template = self._eureqa._get_analysis_template(template._id)
        self.assertEqual(template.description, "Test description 5 but Renamed")

    def test_update_parameters(self):
        params = Parameters([TextParameter('text_1', 'Text input 1 label.'),
                        DataSourceParameter('datasource_1', 'Data source 1',
                                            [VariableParameter('variable_1', 'Variable 1')]),
                        TextParameter('text_2', 'Text input 2 label.')])
        template = self._eureqa.create_analysis_template(
            "Test template 5",
            "Test description 5",
            params,
            None)

        new_params = Parameters([TextParameter('text_2', 'Text input 2 label.'),
                                 DataSourceParameter('datasource_2', 'Data source 2',
                                                     [VariableParameter('variable_2', 'Variable 2')]),
                                 TextParameter('text_3', 'Text input 3 label.')])
        template.parameters = new_params
        self.assertEqual(template.parameters._to_json(), new_params._to_json())

        template = self._eureqa._get_analysis_template(template._id)
        self.assertEqual(template.parameters._to_json(), new_params._to_json())

        template.parameters = new_params._to_json()
        self.assertEqual(template.parameters._to_json(), new_params._to_json())

        template = self._eureqa._get_analysis_template(template._id)
        self.assertEqual(template.parameters._to_json(), new_params._to_json())

    def test_execute_auto_add_and_retreive_files(self):
        template = self._eureqa.create_analysis_template(
            "Test template 6",
            "Test description",
            Parameters([]),
            None)
        dfpv1 = DataFileParameterValue(self._eureqa, 'id1', 'Nutrition.csv')
        dfpv2 = DataFileParameterValue(self._eureqa, 'id2', 'Nutrition.csv')

        self.assertIsNone(dfpv1.value)
        self.assertIsNone(dfpv2.value)

        vals = ParametersValues([dfpv1])
        execution = template._execute(vals)

        self.assertIsNotNone(dfpv1.value)
        self.assertIsNone(dfpv2.value)

        f = open('Nutrition.csv', 'rb')
        expected_contents = f.read()
        f.close()

        #gets files from local fs
        contents1 = dfpv1.file
        self.assertEqual(expected_contents, contents1)
        contents2 = dfpv2.file
        self.assertEqual(expected_contents, contents2)

        #gets file from server
        dfpv1s = execution.parameters['id1']
        contents1s = dfpv1s.file
        self.assertEqual(expected_contents, contents1)

        #ensures edge cases don't throw exceptions
        template._execute(ParametersValues())
        template._execute(ParametersValues([]))

        dfpv4 = DataFileParameterValue(self._eureqa, 'id4', 'does_not_exist.csv')
        with self.assertRaises(Exception):
            dfpv4.file
        with self.assertRaises(Exception):
            template.execute(ParametersValues([dfpv4]))

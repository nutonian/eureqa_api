import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import math_block, math_block_set, MathBlock, Eureqa

from etestutils_nosearch import EureqaTestBaseNoSearch

import unittest
import inspect
import json


# Modified from
# https://stackoverflow.com/questions/3755136/pythonic-way-to-check-if-a-list-is-sorted-or-not/17224104#17224104
from itertools import imap, tee
import operator
def is_sorted(iterable, key=lambda x: x, compare=operator.le):
  a, b = tee(iterable)
  next(b, None)
  return all(imap(lambda x, y: compare(key(x), key(y)),
                  a, b))


class TestMathBlockSet(unittest.TestCase, EureqaTestBaseNoSearch):
    @classmethod
    def setUpClass(cls):
        cls._math_blocks = math_block_set.MathBlockSet()

    def test_op_const_pattern(self):
        """Sanity check that operation constants are visible and behave as expected."""

        op = self._math_blocks.var
        self.assertEqual(op.name, 'Input Variable')
        self.assertEqual(op.complexity, 1)
        self.assertEqual(op._notation, 'x_')

        op.complexity = 3
        
        op = self._math_blocks.var
        self.assertEqual(op.name, 'Input Variable')
        self.assertEqual(op.complexity, 3)
        self.assertEqual(op._notation, 'x_')

        op.complexity = 1

        self.assertEqual(self._math_blocks.var.name, 'Input Variable')
        self.assertEqual(self._math_blocks.var.complexity, 1)
        self.assertEqual(self._math_blocks.var._notation, 'x_')
        
    def test_to_json_with_notation(self):
        body = self._math_blocks.const._to_json()
        text = json.dumps(body, sort_keys=True)
        self.assertEqual(text, '{"complexity": 0, "enabled": false, "op_id": 0, "op_name": "Constant", "op_notation": "c_"}')

    def test_to_json_sorted(self):
        # Our output should be sorted
        simple_numeric_search = self._eureqa.search_templates.numeric("test", "x", ["y"])
        math_blocks_json = simple_numeric_search.math_blocks._to_json()
        self.assertTrue(is_sorted(math_blocks_json,
                                  key=lambda b: b["op_id"]),
                        msg=repr(math_blocks_json))

        # The server's default output should also be sorted,
        # so that when the UI compares them, they compare as equal.
        tmpls = self._eureqa._session.execute("/fxp/search_templates/generic", method='GET')
        tmpl_blocks_json = tmpls["building_blocks"]

        # Python is only responsible for providing these fields.
        # The backend adds additional fields.
        tmpl_blocks_json = [{"complexity": x["complexity"],
                             "enabled": x["enabled"],
                             "op_id": x["op_id"],
                             "op_name": x["op_name"],
                             "op_notation": x["op_notation"]}
                            for x in tmpl_blocks_json]

        self.assertTrue(is_sorted(tmpl_blocks_json,
                                  key=lambda b: b["op_id"]),
                        msg=repr(tmpl_blocks_json))
        self.assertEqual(tmpl_blocks_json, math_blocks_json)

    def test_matches_server_list(self):
        """Verifies that the list of math blocks exposed by the back-end matches the list in the default set"""

        api_blocks = self._math_blocks._blocks.itervalues()
        api_blocks = {x.name: x for x in api_blocks}

        session = Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1',
                         organization='root')._session
        settings_info = session.execute('/api/v2/fxp/settings_info', 'GET')
        server_blocks = settings_info['building_blocks']
        server_blocks = {x['op_name']: x for x in server_blocks}

        self.assertEqual(set(api_blocks.iterkeys()), set(server_blocks.iterkeys()))
        
        for name in api_blocks.iterkeys():
            self.assertEqual(api_blocks[name].complexity, server_blocks[name]['complexity'],
                             msg="Block \"%s\" has complexity %d locally but %d on the server"
                                 % (name, api_blocks[name].complexity, server_blocks[name]['complexity']))
            self.assertEqual(api_blocks[name]._notation, server_blocks[name]['op_notation'],
                             msg="Block \"%s\" has notation '%s' locally but '%s' on the server"
                                 % (name, api_blocks[name]._notation, server_blocks[name]['op_notation']))

    def test_all_blocks(self):
        s = self._math_blocks

        all_blocks_by_prop = [block for block in
                                (getattr(self._math_blocks, name)
                                 ## Get all properties on MathBlockSet
                                 for (name, value) in inspect.getmembers(math_block_set.MathBlockSet,
                                                                         lambda v: isinstance(v, property)))
                              ## Filter out and return properties that evaluate to MathBlocks
                              if isinstance(block, math_block.MathBlock)]

        all_blocks_by_prop_dict = {x.name: x for x in all_blocks_by_prop}

        ## Compare as sets first.
        ## Python's unittest has an overload for sets that produces
        ## a diff in case of common failures -- more readable.
        self.assertEqual(set(self._math_blocks._blocks.iterkeys()),
                         set(all_blocks_by_prop_dict.iterkeys()))
        self.assertEqual(self._math_blocks._blocks, all_blocks_by_prop_dict)
        self.assertEqual(self._math_blocks._blocks, self._math_blocks._all_blocks)

    def test_json_serde(self):
        self._math_blocks.pow.enable()
        self._math_blocks.var.enable()

        # Test repr() while we're at it
        self.assertEqual(repr(self._math_blocks),
                         "MathBlockSet([MathBlock(11, u'Power', 4, u'^'), MathBlock(2, u'Input Variable', 1, u'x_')])")
        
        serialized_blocks = str(self._math_blocks)
        
        self.assertEqual(serialized_blocks,
                         '[\n    {\n        "complexity": 0, \n        "enabled": false, \n        "op_id": 0, \n        "op_name": "Constant", \n        "op_notation": "c_"\n    }, \n    {\n        "complexity": 1, \n        "enabled": false, \n        "op_id": 1, \n        "op_name": "Integer Constant", \n        "op_notation": "n_"\n    }, \n    {\n        "complexity": 1, \n        "enabled": true, \n        "op_id": 2, \n        "op_name": "Input Variable", \n        "op_notation": "x_"\n    }, \n    {\n        "complexity": 0, \n        "enabled": false, \n        "op_id": 3, \n        "op_name": "Addition", \n        "op_notation": " + "\n    }, \n    {\n        "complexity": 0, \n        "enabled": false, \n        "op_id": 4, \n        "op_name": "Subtraction", \n        "op_notation": " - "\n    }, \n    {\n        "complexity": 0, \n        "enabled": false, \n        "op_id": 5, \n        "op_name": "Multiplication", \n        "op_notation": "*"\n    }, \n    {\n        "complexity": 2, \n        "enabled": false, \n        "op_id": 6, \n        "op_name": "Division", \n        "op_notation": "/"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 7, \n        "op_name": "Sine", \n        "op_notation": "sin"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 8, \n        "op_name": "Cosine", \n        "op_notation": "cos"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 9, \n        "op_name": "Negation", \n        "op_notation": "-"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 10, \n        "op_name": "Factorial", \n        "op_notation": "factorial"\n    }, \n    {\n        "complexity": 4, \n        "enabled": true, \n        "op_id": 11, \n        "op_name": "Power", \n        "op_notation": "^"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 12, \n        "op_name": "Exponential", \n        "op_notation": "exp"\n    }, \n    {\n        "complexity": 2, \n        "enabled": false, \n        "op_id": 13, \n        "op_name": "Natural Logarithm", \n        "op_notation": "log"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 14, \n        "op_name": "Absolute Value", \n        "op_notation": "abs"\n    }, \n    {\n        "complexity": 1, \n        "enabled": false, \n        "op_id": 15, \n        "op_name": "If-Then-Else", \n        "op_notation": "if"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 23, \n        "op_name": "Logistic Function", \n        "op_notation": "logistic"\n    }, \n    {\n        "complexity": 2, \n        "enabled": false, \n        "op_id": 24, \n        "op_name": "Step Function", \n        "op_notation": "step"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 25, \n        "op_name": "Sign Function", \n        "op_notation": "sgn"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 26, \n        "op_name": "Gaussian Function", \n        "op_notation": "gauss"\n    }, \n    {\n        "complexity": 1, \n        "enabled": false, \n        "op_id": 27, \n        "op_name": "Minimum", \n        "op_notation": "min"\n    }, \n    {\n        "complexity": 1, \n        "enabled": false, \n        "op_id": 28, \n        "op_name": "Maximum", \n        "op_notation": "max"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 29, \n        "op_name": "Modulo", \n        "op_notation": "mod"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 30, \n        "op_name": "Floor", \n        "op_notation": "floor"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 31, \n        "op_name": "Ceiling", \n        "op_notation": "ceil"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 32, \n        "op_name": "Round", \n        "op_notation": "round"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 33, \n        "op_name": "Tangent", \n        "op_notation": "tan"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 34, \n        "op_name": "Equal-To", \n        "op_notation": "equal"\n    }, \n    {\n        "complexity": 1, \n        "enabled": false, \n        "op_id": 35, \n        "op_name": "Less-Than", \n        "op_notation": "less"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 36, \n        "op_name": "Less-Than-Or-Equal", \n        "op_notation": "less_or_equal"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 37, \n        "op_name": "Greater-Than", \n        "op_notation": "greater"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 38, \n        "op_name": "Greater-Than-Or-Equal", \n        "op_notation": "greater_or_equal"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 39, \n        "op_name": "Logical And", \n        "op_notation": "and"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 40, \n        "op_name": "Logical Or", \n        "op_notation": "or"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 41, \n        "op_name": "Logical Xor", \n        "op_notation": "xor"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 42, \n        "op_name": "Logical Not", \n        "op_notation": "not"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 43, \n        "op_name": "Hyperbolic Tangent", \n        "op_notation": "tanh"\n    }, \n    {\n        "complexity": 1, \n        "enabled": false, \n        "op_id": 44, \n        "op_name": "Square Root", \n        "op_notation": "sqrt"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 45, \n        "op_name": "Delayed Variable", \n        "op_notation": "delay"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 46, \n        "op_name": "Simple Moving Average", \n        "op_notation": "sma"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 49, \n        "op_name": "Arcsine", \n        "op_notation": "asin"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 50, \n        "op_name": "Arccosine", \n        "op_notation": "acos"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 51, \n        "op_name": "Arctangent", \n        "op_notation": "atan"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 52, \n        "op_name": "Two-Argument Arctangent", \n        "op_notation": "atan2"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 70, \n        "op_name": "Error Function", \n        "op_notation": "erf"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 71, \n        "op_name": "Complementary Error Function", \n        "op_notation": "erfc"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 72, \n        "op_name": "Weighted Moving Average", \n        "op_notation": "wma"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 73, \n        "op_name": "Modified Moving Average", \n        "op_notation": "mma"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 74, \n        "op_name": "Simple Moving Median", \n        "op_notation": "smm"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 78, \n        "op_name": "Hyperbolic Sine", \n        "op_notation": "sinh"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 79, \n        "op_name": "Hyperbolic Cosine", \n        "op_notation": "cosh"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 80, \n        "op_name": "Inverse Hyperbolic Sine", \n        "op_notation": "asinh"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 81, \n        "op_name": "Inverse Hyperbolic Cosine", \n        "op_notation": "acosh"\n    }, \n    {\n        "complexity": 4, \n        "enabled": false, \n        "op_id": 82, \n        "op_name": "Inverse Hyperbolic Tangent", \n        "op_notation": "atanh"\n    }\n]')

        deserialized_blocks = math_block_set.MathBlockSet.from_json(json.loads(serialized_blocks))

        ## Compare as sets first.
        ## Python's unittest has an overload for sets that produces
        ## a diff in case of common failures -- more readable.
        self.assertEqual(set(self._math_blocks._blocks.iterkeys()),
                         set(deserialized_blocks._blocks.iterkeys()))
        
        self.assertEqual(deserialized_blocks, self._math_blocks)
        self.assertEqual(deserialized_blocks._blocks, self._math_blocks._blocks)

        self._math_blocks.pow.disable()
        self._math_blocks.var.disable()

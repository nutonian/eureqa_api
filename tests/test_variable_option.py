import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import VariableOptions

import copy
import json
import unittest


class TestVariableOption(unittest.TestCase):
    def test_default_properties(self):
        # Validates that the default values match the values sent by UI
        var_op = VariableOptions(normalize_enabled=True, remove_outliers_enabled=True,
                                 name='(Calories by Breakfast (%))')

        self.assertFalse(var_op._filter_enabled)
        self.assertIsNone(var_op._filter_expression)
        self.assertEqual(var_op.missing_value_policy, 'column_iqm')
        self.assertTrue(var_op.normalize_enabled)
        self.assertEqual(var_op.normalization_offset, '<none>')
        self.assertEqual(var_op.normalization_scale, '<none>')
        self.assertEqual(var_op.outlier_threshold, None)
        self.assertTrue(var_op.remove_outliers_enabled)
        self.assertEqual(var_op.min_delay, None)
        self.assertEqual(var_op.name, '(Calories by Breakfast (%))')

    def test_from_json_with_optional_properties(self):
        body = json.loads('{"filter_enabled": true, '
                          '"filter_expression": "expression1", '
                          '"missing_value_policy": "column_mean", '
                          '"normalize_enabled": true, '
                          '"normalize_offset": "expression2", '
                          '"normalize_scale": "expression3", '
                          '"outlier_threshold": 2.0, '
                          '"remove_outliers_enabled": true, '
                          '"min_delay": 5,'
                          '"variable_name": "(Calories by Breakfast (%))"}')
        var_op = VariableOptions._from_json(body)

        self.assertTrue(var_op._filter_enabled)
        self.assertEqual(var_op._filter_expression, 'expression1')
        self.assertEqual(var_op.missing_value_policy, 'column_mean')
        self.assertTrue(var_op.normalize_enabled)
        self.assertEqual(var_op.normalization_offset, 'expression2')
        self.assertEqual(var_op.normalization_scale, 'expression3')
        self.assertEqual(var_op.outlier_threshold, 2)
        self.assertTrue(var_op.remove_outliers_enabled)
        self.assertEqual(var_op.min_delay, 5)
        self.assertEqual(var_op.name, '(Calories by Breakfast (%))')

    def test_from_json_without_optional_properties(self):
        body = json.loads('{"filter_enabled": false, '
                          '"normalize_enabled": false, '
                          '"remove_outliers_enabled": false, '
                          '"variable_name": "(Calories by Breakfast (%))"}')
        var_op = VariableOptions._from_json(body)

        self.assertFalse(var_op._filter_enabled)
        self.assertIsNone(var_op._filter_expression)
        self.assertIsNone(var_op.missing_value_policy)
        self.assertFalse(var_op.normalize_enabled)
        self.assertIsNone(var_op.normalization_offset)
        self.assertIsNone(var_op.normalization_scale)
        self.assertIsNone(var_op.outlier_threshold)
        self.assertFalse(var_op.remove_outliers_enabled)
        self.assertEqual(var_op.min_delay, None)
        self.assertEqual(var_op.name, '(Calories by Breakfast (%))')

    def test_to_json_with_optional_properties(self):
        var_op = VariableOptions(missing_value_policy='ignore_nans',
                                 normalize_enabled=True, normalization_offset='expression2',
                                 normalization_scale='expression3', outlier_threshold=2.0,
                                 remove_outliers_enabled=True, min_delay=5, name='(Calories by Breakfast (%))')
        var_op._set_filter(filter_enabled=True, filter_expression='expression1')
        text = json.dumps(var_op._to_json(), sort_keys=True)
        expected = ('{"filter_enabled": true, '
                    '"filter_expression": "expression1", '
                    '"min_delay": 5, '
                    '"missing_value_policy": "ignore_nans", '
                    '"normalize_enabled": true, '
                    '"normalize_offset": "expression2", '
                    '"normalize_scale": "expression3", '
                    '"outlier_threshold": 2.0, '
                    '"remove_outliers_enabled": true, '
                    '"variable_name": "(Calories by Breakfast (%))"}')
        self.assertEqual(text, expected)

    def test_to_json_without_optional_properties(self):
        var_op = VariableOptions(normalize_enabled=False, remove_outliers_enabled=False,
                                 name='(Calories by Breakfast (%))')
        text = json.dumps(var_op._to_json(), sort_keys=True)
        expected = ('{"filter_enabled": false, '
                    '"missing_value_policy": "column_iqm", '
                    '"normalize_enabled": false, '
                    '"remove_outliers_enabled": false, '
                    '"variable_name": "(Calories by Breakfast (%))"}')
        self.assertEqual(text, expected)

    def test_equal(self):
        one = VariableOptions(normalize_enabled=False, remove_outliers_enabled=False, min_delay=5,
                              name='(Calories by Breakfast (%))')
        two = VariableOptions(normalize_enabled=False, remove_outliers_enabled=False, min_delay=5,
                              name='(Calories by Breakfast (%))')
        self.assertEqual(one, two)

    def test_not_equal(self):
        template = VariableOptions(missing_value_policy='column_mean',
                                   normalize_enabled=True, normalization_offset='expression2',
                                   normalization_scale='expression3', outlier_threshold=2.0,
                                   remove_outliers_enabled=True,
                                   name='(Calories by Breakfast (%))')

        changed = copy.deepcopy(template)
        changed.missing_value_policy = 'junk'
        self.assertNotEqual(changed, template)

        changed = copy.deepcopy(template)
        changed.normalize_enabled = False
        self.assertNotEqual(changed, template)

        changed = copy.deepcopy(template)
        changed.normalization_offset = 'junk'
        self.assertNotEqual(changed, template)

        changed = copy.deepcopy(template)
        changed.normalization_scale = 'junk'
        self.assertNotEqual(changed, template)

        changed = copy.deepcopy(template)
        changed.outlier_threshold = 3.0
        self.assertNotEqual(changed, template)

        changed = copy.deepcopy(template)
        changed.remove_outliers_enabled = False
        self.assertNotEqual(changed, template)
 
        changed = copy.deepcopy(template)
        changed.name = 'junk'
        self.assertNotEqual(changed, template)

        changed = copy.deepcopy(template)
        changed.min_delay = 5
        self.assertNotEqual(changed, template)

    def test_equal_hash(self):
        one = VariableOptions(normalize_enabled=False, remove_outliers_enabled=False,
                              name='(Calories by Breakfast (%))')
        two = VariableOptions(normalize_enabled=False, remove_outliers_enabled=False,
                              name='(Calories by Breakfast (%))')
        self.assertEqual(hash(one), hash(two))

    def test_not_equal_hash(self):
        template = VariableOptions(missing_value_policy='column_mean',
                                   normalize_enabled=True, normalization_offset='expression2',
                                   normalization_scale='expression3', outlier_threshold=2.0,
                                   remove_outliers_enabled=True,
                                   name='(Calories by Breakfast (%))')

        changed = copy.deepcopy(template)
        changed.missing_value_policy = 'junk'
        self.assertNotEqual(hash(changed), hash(template))

        changed = copy.deepcopy(template)
        changed.normalize_enabled = False
        self.assertNotEqual(hash(changed), hash(template))

        changed = copy.deepcopy(template)
        changed.normalization_offset = 'junk'
        self.assertNotEqual(hash(changed), hash(template))

        changed = copy.deepcopy(template)
        changed.normalization_scale = 'junk'
        self.assertNotEqual(hash(changed), hash(template))

        changed = copy.deepcopy(template)
        changed.outlier_threshold = 3.0
        self.assertNotEqual(hash(changed), hash(template))

        changed = copy.deepcopy(template)
        changed.remove_outliers_enabled = False
        self.assertNotEqual(hash(changed), hash(template))

        changed = copy.deepcopy(template)
        changed.name = 'junk'
        self.assertNotEqual(hash(changed), hash(template))

        changed = copy.deepcopy(template)
        changed.min_delay = 5
        self.assertNotEqual(hash(changed), hash(template))

    def test_to_string(self):
        # Validates that the default values match the values sent by UI
        var_op = VariableOptions(normalize_enabled=True, remove_outliers_enabled=True,
                                 name='(Calories by Breakfast (%))')
        var_op.__str__()

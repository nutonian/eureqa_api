import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa
from eureqa.analysis_cards.plot import Plot

import unittest

from etestutils_nosearch import EureqaTestBaseNoSearch

class TestPlot(unittest.TestCase, EureqaTestBaseNoSearch):
    @classmethod
    def setUpClass(cls):
        cls.maxDiff=None

    def assertAlmostEqual(self, a, b):
        """
        A custom override of unittest.assertAlmostEqual which supports lists
        """
        if isinstance(a, list) and isinstance(b, list):
            if len(a) is not len(b): return False
            for va, vb in zip(a,b):
                if not unittest.TestCase.assertAlmostEqual(self, va, vb): return False
            return True
        return unittest.TestCase.assertAlmostEqual(self, a, b)

    def test_to_json(self):
        plot = Plot(x_axis_label="X axis label",
                    y_axis_label="Y axis label")
        json = plot._to_json()["options"]
        self.assertEqual(json, {
            'datasource_id': None,
            'plot':{
                'dimensions': {
                    'width': None,
                    'height':'400px'
                },
                'x_axis_label': 'X axis label',
                'y_axis_label': 'Y axis label',
                'legend': True,
                'zero_x_line': False,
                'zero_y_line': False,
                'x_tick_format': None,
                'y_tick_format': None,
                'num_x_ticks': None,
                'num_y_ticks': None,
                'number_of_rows': 200,
                'guides': {
                    'type': 'XY'
                }
            }, 'components':[]})

        plot = Plot(width='500px',
                    height='500px',
                    x_axis_label="X axis label",
                    y_axis_label="Y axis label",
                    show_legend=False,
                    zero_x_line=True,
                    zero_y_line=True,
                    x_tick_format='.3s',
                    y_tick_format='.4s',
                    guides_type='YY')
        json = plot._to_json()["options"]
        self.assertEqual(json, {
            'datasource_id': None,
            'plot':{
                'dimensions': {
                    'width': '500px',
                    'height': '500px'
                },
                'x_axis_label': 'X axis label',
                'y_axis_label': 'Y axis label',
                'legend': False,
                'zero_x_line': True,
                'zero_y_line': True,
                'x_tick_format': '.3s',
                'y_tick_format': '.4s',
                'num_x_ticks': None,
                'num_y_ticks': None,
                'number_of_rows': 200,
                'guides': {
                    'type': 'YY'
                }
            }, 'components':[]})

        plot = Plot(x_axis_label="X axis label",
                    y_axis_label="Y axis label")
        plot.plot([1,2,3],[4,5,6],
            legend_label='Test Plot Legend Label',
            tooltip_template='X: {{x_value}} Y: {{y_value}} Tooltip: {{tooltip}}',
            tooltips=['A', 'B', 'C'])
        json = plot._to_json()["options"]
        self.assertEqual(json, {
            'datasource_id': None,
            'plot': {
                'dimensions': {
                    'width': None,
                    'height':'400px'
                },
                'x_axis_label': 'X axis label',
                'y_axis_label': 'Y axis label',
                'legend': True,
                'zero_x_line': False,
                'zero_y_line': False,
                'x_tick_format': None,
                'y_tick_format': None,
                'num_x_ticks': None,
                'num_y_ticks': None,
                'number_of_rows': 200,
                'guides': {
                    'type': 'XY'
                }
            },
            'components': [
                {
                    'type': 'line',
                    'color': 'black',
                    'width': 1,
                    'radius': 1,
                    'x_values': '(Series 1 X)',
                    'y_values': '(Series 1 Y)',
                    'use_in_plot_range': True,
                    'error_bars_upper_values': None,
                    'error_bars_lower_values': None,
                    'inside_legend': True,
                    'legend_label': 'Test Plot Legend Label',
                    'tooltip_template': 'X: {{x_value}} Y: {{y_value}} Tooltip: {{tooltip}}',
                    'tooltips': ['A', 'B', 'C']
                }
            ]})

        datasource = self._eureqa.create_data_source('data_source_1', 'Nutrition.csv')
        plot.plot('<row>', 'Weight', datasource=datasource, legend_label='Test Plot Legend Label Weight', color='blue')
        plot.plot('<row>', 'Calories', datasource=datasource,
                  style='o',
                  color='red',
                  line_width=5,
                  circle_radius=7,
                  use_in_plot_range=False,
                  error_bars_upper_values='1.3*Calories',
                  error_bars_lower_values='0.7*Calories',
                  legend_label=None,
                  tooltip_template='Existing datasource point: {{tooltip}}',
                  tooltips=['foo', 'bar', 'baz'])
        json = plot._to_json()["options"]
        self.assertEqual(json, {
            'datasource_id': None,
            'plot':{
                'dimensions': {
                    'width': None,
                    'height':'400px'
                },
                'x_axis_label': 'X axis label',
                'y_axis_label': 'Y axis label',
                'legend': True,
                'zero_x_line': False,
                'zero_y_line': False,
                'x_tick_format': None,
                'y_tick_format': None,
                'num_x_ticks': None,
                'num_y_ticks': None,
                'number_of_rows': 200,
                'guides': {
                    'type': 'XY'
                }
            },
            'components': [
                {
                    'type': 'line',
                    'color': 'black',
                    'width': 1,
                    'radius': 1,
                    'x_values': '(Series 1 X)',
                    'y_values': '(Series 1 Y)',
                    'use_in_plot_range': True,
                    'error_bars_upper_values': None,
                    'error_bars_lower_values': None,
                    'inside_legend': True,
                    'legend_label': 'Test Plot Legend Label',
                    'tooltip_template': 'X: {{x_value}} Y: {{y_value}} Tooltip: {{tooltip}}',
                    'tooltips': ['A', 'B', 'C']
                },
                {
                    'type': 'line',
                    'color': 'blue',
                    'width': 1,
                    'radius': 1,
                    'x_values': '(Datasource data_source_1: <row>)',
                    'y_values': '(Datasource data_source_1: Weight)',
                    'use_in_plot_range': True,
                    'error_bars_upper_values': None,
                    'error_bars_lower_values': None,
                    'inside_legend': True,
                    'legend_label': 'Test Plot Legend Label Weight',
                    'tooltip_template': None,
                    'tooltips': None
                },
                {
                    'type': 'circle',
                    'color': 'red',
                    'width': 5,
                    'radius': 7,
                    'x_values': '(Datasource data_source_1: <row>)',
                    'y_values': '(Datasource data_source_1: Calories)',
                    'use_in_plot_range': False,
                    'error_bars_upper_values': '(Datasource data_source_1: 1.3*Calories)',
                    'error_bars_lower_values': '(Datasource data_source_1: 0.7*Calories)',
                    'inside_legend': False,
                    'legend_label': None,
                    'tooltip_template': 'Existing datasource point: {{tooltip}}',
                    'tooltips': ['foo', 'bar', 'baz']
                }
            ]})

    def test_plot_invalid(self):
        datasource = self._eureqa.create_data_source('data_source_1', 'Nutrition.csv')
        plot = Plot(x_axis_label="X axis label", y_axis_label="Y axis label")
        original_json = plot._to_json()["options"]
        with self.assertRaisesRegexp(Exception, 'Plot.plot error: x and y inputs must be either lists of numbers or strings referencing a variable or expression'):
            plot.plot('a', [1])
        with self.assertRaisesRegexp(Exception, 'Plot.plot error: an existing DataSource object must be provided if x and y are provided as strings referencing variables or expressions'):
            plot.plot('a', 'b', 'data_source_1')
        with self.assertRaisesRegexp(Exception, 'Plot.plot error: since x and y were inputted as expression strings, error bars must be provided as expression strings'):
            plot.plot('a', 'b', datasource, error_bars_upper_values=[3,4], error_bars_lower_values=[1,2])
        with self.assertRaisesRegexp(Exception, 'Plot.plot error: since x and y were inputted as lists, error bars must be provided as lists'):
            plot.plot([1,2,3], [4,5,6], error_bars_upper_values='a', error_bars_lower_values='b')
        end_json = plot._to_json()["options"]
        self.assertEqual(original_json, end_json)

    def test_plot_and_upload_data(self):
        plot = Plot(x_axis_label="X axis label", y_axis_label="Y axis label")
        plot.plot([1,2,3],[4,5,6],legend_label='Test Plot Legend Label')
        json = plot._to_json()["options"]
        self.assertEqual(json['datasource_id'], None)

        plot.upload_data(self._eureqa)
        json = plot._to_json()["options"]
        datasource_id = json['datasource_id']
        self.assertTrue(len(datasource_id) > 0)

        data_holder_datasource = self._eureqa.get_data_source_by_id(datasource_id)
        self.assertTrue(data_holder_datasource.name, 'Data Holder for Plot')
        self.assertEqual(['(Series 1 X)', '(Series 1 Y)'], data_holder_datasource.get_variables())

        datasource = self._eureqa.create_data_source('data_source_1', 'Nutrition.csv')
        plot.plot('<row>', 'Weight', datasource=datasource, legend_label='Test Plot Legend Label')
        plot.plot('<row>', '(Fat (g))', datasource=datasource, legend_label='Test Plot Legend Label')
        plot.plot('<row>', 'Calories', datasource=datasource, legend_label='Test Plot Legend Label',
                  error_bars_upper_values='1.3*Calories',
                  error_bars_lower_values='0.7*Calories')
        err_bars_upper = datasource.evaluate_expression('1.3*Calories')['1.3*Calories']
        err_bars_lower = datasource.evaluate_expression('0.7*Calories')['0.7*Calories']

        plot.upload_data(self._eureqa)
        json = plot._to_json()["options"]
        datasource_id = json['datasource_id']
        self.assertTrue(len(datasource_id) > 0)

        data_holder_datasource = self._eureqa.get_data_source_by_id(datasource_id)
        self.assertTrue(data_holder_datasource.name, 'Data Holder for Plot')
        expected_vars = {
            '(Series 1 X)',
            '(Series 1 Y)',
            '(Datasource data_source_1: <row>)',
            '(Datasource data_source_1: Weight)',
            '(Datasource data_source_1: (Fat (g)))',
            '(Datasource data_source_1: Calories)',
            '(Datasource data_source_1: 1.3*Calories)',
            '(Datasource data_source_1: 0.7*Calories)'
        }
        self.assertSetEqual(expected_vars, set(data_holder_datasource.get_variables()))

        # check the data in the datasource
        num_rows_expected = datasource.number_rows
        self.assertAlmostEqual(data_holder_datasource.evaluate_expression('(Series 1 X)')['(Series 1 X)'],
                         [1,2,3] + [None]*(num_rows_expected-3))
        self.assertAlmostEqual(data_holder_datasource.evaluate_expression('(Series 1 Y)')['(Series 1 Y)'],
                         [4,5,6] + [None]*(num_rows_expected-3))
        self.assertAlmostEqual(data_holder_datasource.evaluate_expression('(Datasource data_source_1: <row>)')['(Datasource data_source_1: <row>)'],
                         range(1, num_rows_expected + 1))
        self.assertAlmostEqual(data_holder_datasource.evaluate_expression('(Datasource data_source_1: Weight)')['(Datasource data_source_1: Weight)'],
                         datasource.evaluate_expression('Weight')['Weight'])
        self.assertAlmostEqual(data_holder_datasource.evaluate_expression('(Datasource data_source_1: <row>)')['(Datasource data_source_1: <row>)'],
                         range(1, num_rows_expected + 1))
        self.assertAlmostEqual(data_holder_datasource.evaluate_expression('(Datasource data_source_1: (Fat (g)))')['(Datasource data_source_1: (Fat (g)))'],
                         datasource.evaluate_expression('(Fat (g))')['(Fat (g))'])
        self.assertAlmostEqual(data_holder_datasource.evaluate_expression('(Datasource data_source_1: Calories)')['(Datasource data_source_1: Calories)'],
                         datasource.evaluate_expression('Calories')['Calories'])
        self.assertAlmostEqual(data_holder_datasource.evaluate_expression('(Datasource data_source_1: 1.3*Calories)')['(Datasource data_source_1: 1.3*Calories)'],
                         err_bars_upper)
        self.assertAlmostEqual(data_holder_datasource.evaluate_expression('(Datasource data_source_1: 0.7*Calories)')['(Datasource data_source_1: 0.7*Calories)'],
                         err_bars_lower)

    def _wrap_json(self, options):
        return { "component_type": "CUSTOM_GRAPH", "options": options }

    def test_from_json(self):
        json = {"component_type": "CUSTOM_GRAPH"}
        plot = Plot._construct_from_json(json, _analysis=self._analysis)

        input_json = {
            'plot': {
                'x_axis_label': 'X axis label',
                'y_axis_label': 'Y axis label',
                'number_of_rows': 300,
                'legend': False,
        }, 'components': []
        }
        plot = Plot._construct_from_json(self._wrap_json(input_json))
        output_json = plot._to_json()["options"]
        self.assertEqual(output_json.get('datasource_id'), None)
        self.assertEqual(output_json['plot']['x_axis_label'], 'X axis label')
        self.assertEqual(output_json['plot']['y_axis_label'], 'Y axis label')
        self.assertEqual(output_json['plot']['number_of_rows'], 300)
        self.assertEqual(output_json['plot']['legend'], False)
        self.assertEqual(output_json['components'], [])

        input_json['components'] = [
            {
                'type': 'line',
                'x_values': 'x1',
                'y_values': 'y1',
                'legend_label': 'label1'
            },
            {
                'type': 'circle',
                'x_values': 'x2',
                'y_values': 'y2',
                'legend_label': 'label2'
            }
        ]
        plot = Plot._construct_from_json(self._wrap_json(input_json))
        output_json = plot._to_json()["options"]
        self.assertEqual(output_json.get('datasource_id'), None)
        self.assertEqual(output_json['plot']['x_axis_label'], 'X axis label')
        self.assertEqual(output_json['plot']['y_axis_label'], 'Y axis label')
        self.assertEqual(output_json['plot']['number_of_rows'], 300)
        self.assertEqual(output_json['plot']['legend'], False)
        self.assertEqual(len(output_json['components']), 2)
        self.assertEqual(output_json['components'][0]['type'], 'line')
        self.assertEqual(output_json['components'][0]['x_values'], 'x1')
        self.assertEqual(output_json['components'][0]['y_values'], 'y1')
        self.assertEqual(output_json['components'][0]['legend_label'], 'label1')
        self.assertEqual(output_json['components'][1]['type'], 'circle')
        self.assertEqual(output_json['components'][1]['x_values'], 'x2')
        self.assertEqual(output_json['components'][1]['y_values'], 'y2')
        self.assertEqual(output_json['components'][1]['legend_label'], 'label2')

        datasource = self._eureqa.create_data_source('data_source_1', 'Nutrition.csv')
        input_json['datasource_id'] = datasource._data_source_id
        plot = Plot._construct_from_json(self._wrap_json(input_json), _analysis=self._analysis)
        self.assertEqual(plot._options['datasource_id'], datasource._data_source_id)

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa
from eureqa.session import Http404Exception
from eureqa.analysis_templates import Parameters, ParametersValues, \
    TextParameter, TextParameterValue, \
    DataSourceParameter, DataSourceParameterValue, \
    TopLevelModelParameter, TopLevelModelParameterValue, \
    VariableParameter, VariableParameterValue, AnalysisTemplate, \
    DataFileParameter, DataFileParameterValue

import unittest

import shutil
import tempfile
import time
import textwrap

from cStringIO import StringIO

from etestutils_nosearch import EureqaTestBaseNoSearch

#########################################
############ Example analysis template
#########################################

def create_analysis_normal(eureqa, template_execution):
    template_execution.update_progress('Starting running analysis template.')
    analysis = template_execution.get_analysis()
    analysis.name = 'Test analysis template execution 1'
    analysis.description = 'Test analysis'

    assert(len(template_execution.parameters) == 3)
    text_parameter = template_execution.parameters.values()[0]
    assert(text_parameter.id == 'text1')
    assert(text_parameter._type == 'text')
    assert(text_parameter.value == 'This is an input value for the example text input.')

    data_source_parameter = template_execution.parameters.values()[1]
    assert(data_source_parameter._type == 'datasource')

    text_parameter_multiline = template_execution.parameters.values()[2]
    assert(text_parameter_multiline.id == 'text2')
    assert(text_parameter_multiline._type == 'text_multiline')
    assert(text_parameter_multiline.value == 'This\nis\nan\ninput\nvalue\nfor\nthe\nmultiline\nexample\ntext\ninput.')

    data_source = eureqa.get_data_source(data_source_parameter.value)
    target_variable = data_source_parameter.variables[0].value
    input_variables = set(data_source.get_variables()) - {target_variable}

    settings = eureqa.search_templates.numeric('Test analysis templates 1', target_variable, input_variables)
    template_execution.update_progress('Creating search.')
    search = data_source.create_search(settings)
    ## Don't run search; expensive and not necessary in this test
    ## (CLI test validates it)
    template_execution.update_progress('Done!')


#########################################
############ quits abnormally
#########################################
def create_quitting_analysis(eureqa, template_execution):
    # this runs on the server python, not client, thus make sure to import the right libs here
    import os
    template_execution.update_progress('Starting quitting analysis.')
    # abnormal termination
    os._exit(1)
    template_execution.update_progress('Completed quitting analysis.')


#########################################
############ quits with success code
#########################################
def create_success_quitting_analysis(eureqa, template_execution):
    # this runs on the server python, not client, thus make sure to import the right libs here
    import os
    template_execution.update_progress('Starting success quitting analysis.')
    # normal termination
    os._exit(0)
    template_execution.update_progress('Completed success quitting analysis.')


#########################################
############ gets into an infinite loop
#########################################
def create_infinite_loop_analysis(eureqa, template_execution):
    # this runs on the server python, not client, thus make sure to import the right libs here
    import os
    import time
    template_execution.update_progress('Starting infinite loop analysis.')
    while True:
        time.sleep(1)
    template_execution.update_progress('Completed infinite loop analysis.')


#########################################
############ Example validation procedure (no errors)
#########################################
def normal_validate_parameters(eureqa, template_execution):
    # this runs on the server python, not client, thus make sure to import the right libs here
    template_execution.update_progress('Beginning validation')
    template_execution.report_validation_result(type='INFO', message='This is informational')
    template_execution.report_validation_result(type='WARNING', message='This is a warning')
    template_execution.update_progress('Done with validation')


#########################################
############ Example validation procedure (report an error)
#########################################
def error_validate_parameters(eureqa, template_execution):
    template_execution.update_progress('Beginning validation 2')
    template_execution.report_validation_result(type='INFO', message='This is informational')
    template_execution.report_validation_result(type='ERROR', message='This is an error')
    template_execution.update_progress('Done with validation 2')


#########################################
############ Example validation which quits
#########################################
def quitting_validate_parameters(eureqa, template_execution):
    # this runs on the server python, not client, thus make sure to import the right libs here
    import os
    template_execution.update_progress('Beginning quitting validation')
    template_execution.report_validation_result(type='INFO', message='This is informational')
    os._exit(1)
    template_execution.report_validation_result(type='INFO', message='This too')
    template_execution.update_progress('Done with quitting validation')


#########################################
############ Example validation which quits with success code
#########################################
def quitting_success_validate_parameters(eureqa, template_execution):
    # this runs on the server python, not client, thus make sure to import the right libs here
    import os
    template_execution.update_progress('Beginning quitting success validation')
    template_execution.report_validation_result(type='INFO', message='This is success information')
    os._exit(0) # NB success code
    template_execution.report_validation_result(type='INFO', message='This too')
    template_execution.update_progress('Done with quitting success validation')


#########################################
############ Example validation procedure which never stops
#########################################
def infinite_loop_validate_parameters(eureqa, template_execution):
    # this runs on the server python, not client, thus make sure to import the right libs here
    import os
    import time
    template_execution.update_progress('Beginning infinite loop validation')
    template_execution.report_validation_result(type='INFO', message='We are about to do bad things...')
    while True:
        time.sleep(1)
    template_execution.report_validation_result(type='INFO', message='This too')
    template_execution.update_progress('Done with quitting validation')


def _get_function_string(function, new_function_name):
    """Given a function object returns a string representation of that function renamed to new_function_name.
    """

    # Clear linecache otherwise the inspect call below returns the
    # code from the *first* time this method is invoked even if
    # the method is subsequently redefined.

    # Workaround for python bug: https://bugs.python.org/issue1218234
    import linecache
    linecache.clearcache()
    import inspect
    body = textwrap.dedent(inspect.getsource(function))
    function_name = function.__name__
    body = body.replace(function_name, new_function_name)

    blank_lines = ''

    import types
    if isinstance(function, types.FunctionType):
        line_offset = function.func_code.co_firstlineno
        blank_lines = "\n" * line_offset

    return blank_lines + body



class TestAnalysisTemplatesExecution(unittest.TestCase, EureqaTestBaseNoSearch):
    # Helpful debugging messages
    #_DEBUG = True
    _DEBUG = True

    def _log(self, message):
        """ Print out debug / progress messages if DEBUG is set to true """
        if TestAnalysisTemplatesExecution._DEBUG:
            print message

    def _wait_for_not_running(self, execution):
        count = 0
        while execution._is_running():
            self._log( 'execution is still validating... ')
            for progress_update in execution.progress_updates:
                self._log('  ' + progress_update.message)
                time.sleep(1)
                self.assertTrue(count < 200, 'Did not stop executing after %i seconds' % count)
                count += 1


    def _make_module(self, template, main_module_name, create_analysis_function, validate_parameters_function=None):
        """Create a python module in a temp directory with the contents of
        function in __init__.py and install it as the code for the
        specified analysis template. """
        module_fs_path = os.path.join(self._tempdir, main_module_name)
        os.mkdir(module_fs_path)
        init_py_path = os.path.join(module_fs_path, '__init__.py')
        with open(init_py_path, 'w') as f:
            if (create_analysis_function):
                f.write(_get_function_string(create_analysis_function, 'create_analysis'))
            if (validate_parameters_function):
                f.write(_get_function_string(validate_parameters_function, 'validate_parameters'))

        template.set_module(main_module_name, module_fs_path)

    def _get_progress_update_messages(self, execution):
        """ Returns the progress update messages (strings) as an array """
        messages = []
        for progress_update in execution.progress_updates:
            messages.append(progress_update.message)
        return messages

    def _get_validation_results_as_strings(self, execution):
        """ Returns an array of string representation of the validation results """
        messages = []
        for validation_result in execution.validation_results:
            messages.append(validation_result.type + ':' + validation_result.message)
        return messages

    def setUp(self):
        self._tempdir = tempfile.mkdtemp()

    def tearDown(self):
        if (self._tempdir):
            shutil.rmtree(self._tempdir)

    @classmethod
    def setUpClass(cls):
        counter = 1
        found = True
        org_name = "analysis_templates_org"
        while (found is True):
            try:
                cls._eureqa._get_organization(org_name)
                org_name = "analysis_templates_org_%d" % (counter)
                counter += 1
            except Http404Exception:
                found = False
        non_root_org = cls._eureqa._create_organization(org_name)

        print 'using org %s' % (non_root_org.name)

        try:
            non_root_org._delete_user('mike.pink@nutonian.com')
        except Exception as e:
            print 'error deleting user %s' % (e)
            pass  ## User probably already exists, which is fine.

        try:
            non_root_org.create_user("mike.pink@nutonian.com", "Changeme23", "admin", "Mike", "Pink")
        except Exception as e:
            print 'error creating user %s' % (e)
            pass  ## User probably already exists, which is fine.

        # Changing to the new org as root to create analysis templates.
        cls.root_eureqa = Eureqa(url=cls._eureqa_url,
                                 user_name='root',
                                 password='Changeme1',
                                 organization=org_name)

        cls.root_eureqa._grant_user_api_access("mike.pink@nutonian.com", org_name)
        cls.user_eureqa = Eureqa(url=cls._eureqa_url,
                                 user_name='mike.pink@nutonian.com',
                                 password='Changeme23',
                                 organization=org_name)

        ## Clear out old state
        for a in cls.root_eureqa.get_analyses():
            a.delete()
        for d in cls.root_eureqa.get_all_data_sources():
            d.delete()

        cls.root_eureqa.create_data_source('T1', 'Nutrition.csv')
        cls.root_eureqa.create_data_source('T2', 'classification.csv')
        cls.parameters = Parameters([TextParameter('text1', 'This is an example text input.'),
                                     DataSourceParameter('datasource', 'Data Source',
                                                        [VariableParameter('target_var', 'Variable')]),
                                     TextParameter('text2', 'This is an example multiline text input.', text_multiline=True)])

        cls.values = ParametersValues([TextParameterValue(cls.user_eureqa, 'text1', 'This is an input value for the example text input.'),
                                       DataSourceParameterValue(cls.user_eureqa, 'datasource', 'T1',
                                                                [VariableParameterValue(cls.user_eureqa, 'target_var', 'Weight')]),
                                       TextParameterValue(cls.user_eureqa, 'text2', 'This\nis\nan\ninput\nvalue\nfor\nthe\nmultiline\nexample\ntext\ninput.', text_multiline=True)])

        cls.root_template = cls.root_eureqa.create_analysis_template(
            "T1",
            "Test description 1",
            cls.values,
            None)

        for t in cls.user_eureqa.get_all_analysis_templates():
            if t._id == cls.root_template._id:
                cls.user_template = t

        if not hasattr(cls, 'user_template'):
            raise Exception("can not find template using user credentials  with id %s" %(t._id))
        # show me the diffs!
        cls.maxDiff = 10000


    def test_execution(self):
        # create as the root
        self._make_module(self.root_template, 'the_module', create_analysis_normal)

        # execute as the user (not root)
        execution = self.user_template.execute(self.values)
        self._wait_for_not_running(execution)

        self.assertEqual(execution.state, 'DONE')

        expected_messages = [u'Starting running analysis template.',
                             u'Creating search.',
                             u'Done!']

        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))


        analyses = self.user_eureqa.get_analyses()
        self.assertIn('Test analysis template execution 1', [x.name for x in analyses])
        self.assertIn('Test analysis', [x.description for x in analyses])

    def test_with_quitting_execution(self):
        self._make_module(template = self.root_template,
                          main_module_name='the_quitting_module',
                          create_analysis_function = create_quitting_analysis)

        # the execution quits half way through (in remote VM)
        execution = self.user_template.execute(self.values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'ERRORED')
        expected_messages = [u'Starting quitting analysis.',
                             u'Execution aborted unexpectedly: \nNutonian Analysis Template Launcher\n']

        actual_messages = [ x.replace('\r\n','\n') for x in self._get_progress_update_messages(execution)]

        self.assertEqual(expected_messages, actual_messages)


    def test_with_success_quitting_execution(self):
        self._make_module(template = self.root_template,
                          main_module_name='the_successful_quitting_module',
                          create_analysis_function = create_success_quitting_analysis)

        # the execution quits half way through (in remote VM)
        execution = self.user_template.execute(self.values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'ERRORED')
        expected_messages = [u'Starting success quitting analysis.',
                             u'The execution process quit without any error (erased by agent Smith).']

        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))



    def test_with_infinite_loop_execution(self):
        self._make_module(template = self.root_template,
                          main_module_name='the_looping_module',
                          create_analysis_function = create_infinite_loop_analysis)
        execution = self.user_template._execute(self.values)
        count = 0
        while not execution._is_running():
            self._log( 'waiting for execution to be validating... ')
            for progress_update in execution.progress_updates:
                self._log('  ' + progress_update.message + " " + str(execution))
            time.sleep(1)
            self.assertTrue(count < 15, 'Did not start validating after %i seconds' % count)
            count += 1


        self.assertTrue(execution._is_waiting_on_confirmation())
        execution._report_validation_results_confirmed()

        self.assertEqual(execution.state, 'RUNNING')
        self._log('Waiting for 10 seconds for at least one update message after in RUNNING...')
        count = 0
        while len(execution.progress_updates) == 0:
            time.sleep(1)
            self.assertTrue(count < 10, 'Did not start executing after %i seconds' % count)
            count += 1
        self._log('aborting...')
        execution._abort()
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'ABORTED')

        expected_messages = [u'Starting infinite loop analysis.']

        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))


    def test_execution_with_normal_validation(self):
        ## Test that we can execute with non-error validation
        self._make_module(template = self.root_template,
                          main_module_name='the_second_module',
                          create_analysis_function = create_analysis_normal,
                          validate_parameters_function = normal_validate_parameters)

        execution = self.user_template._execute(self.values)
        self._wait_for_not_running(execution)

        self.assertEqual(execution.state, 'WAITING_FOR_CONFIRMATION')
        self.assertTrue(execution._is_waiting_on_confirmation())

        expected_validations = [u'INFO:This is informational',
                                u'WARNING:This is a warning']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        execution._report_validation_results_confirmed()
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'DONE')

        expected_messages = [u'Beginning validation',
                             u'Done with validation',
                             u'Starting running analysis template.',
                             u'Creating search.',
                             u'Done!']

        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))
        # validations should be the same still
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

    def test_execution_with_normal_canceled_validation(self):
        ## Test that we can cancel a template execution if we don't like validation
        self._make_module(template = self.root_template,
                          main_module_name='the_third_module',
                          create_analysis_function = create_analysis_normal,
                          validate_parameters_function = normal_validate_parameters)

        execution = self.user_template._execute(self.values)
        self._wait_for_not_running(execution)

        self.assertEqual(execution.state, 'WAITING_FOR_CONFIRMATION')
        self.assertTrue(execution._is_waiting_on_confirmation())

        # model user canceling after validation
        execution._abort()
        self._wait_for_not_running(execution)

        self.assertEqual(execution.state, 'ABORTED')
        expected_messages = [u'Beginning validation',
                             u'Done with validation']

        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))


    def test_execution_with_quitting_validation(self):
        ## Test that things are ok if we quit during validation
        self._make_module(template = self.root_template,
                          main_module_name='the_second_quitter',
                          create_analysis_function = create_analysis_normal,
                          validate_parameters_function = quitting_validate_parameters)

        execution = self.user_template._execute(self.values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'ERRORED')

        expected_validations = [u'INFO:This is informational']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        expected_messages = [u'Beginning quitting validation',
                             u'Validation aborted unexpectedly: \nNutonian Analysis Template Launcher\n']

        actual_messages = [ x.replace('\r\n','\n') for x in self._get_progress_update_messages(execution)]

        self.assertEqual(expected_messages, actual_messages)


    def test_execution_with_success_quitting_validation(self):
        ## Test that things are ok if we quit during validation
        self._make_module(template = self.root_template,
                          main_module_name='the_second_success_quitter',
                          create_analysis_function = create_analysis_normal,
                          validate_parameters_function = quitting_success_validate_parameters)

        execution = self.user_template._execute(self.values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'ERRORED')

        expected_validations = [u'INFO:This is success information']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        expected_messages = [u'Beginning quitting success validation',
                             u'The execution process quit without any error (erased by agent Smith).']

        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))



    def test_execution_with_infinite_loop_validation(self):
        ## Test that things are ok if we quit during validation
        self._make_module(template = self.root_template,
                          main_module_name='the_infinite_loop_validation',
                          create_analysis_function = create_analysis_normal,
                          validate_parameters_function = infinite_loop_validate_parameters)

        execution = self.user_template._execute(self.values)
        while not execution._is_running():
            self._log( 'waiting for execution to be validating... ')
            for progress_update in execution.progress_updates:
                self._log('  ' + progress_update.message)

        # will be validating forever
        self.assertEqual(execution.state, 'VALIDATING')
        self._log('waiting for one more second after in VALIDATING...')
        time.sleep(1)
        self._log('aborting...')
        execution._abort()
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'ABORTED')

        expected_validations = [u'INFO:We are about to do bad things...']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        expected_messages = [u'Beginning infinite loop validation']

        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))


    def test_execution_with_error_validation(self):
        ## Test that we can execute with non-error validation
        self._make_module(template = self.root_template,
                          main_module_name='the_second_module',
                          create_analysis_function = create_analysis_normal,
                          validate_parameters_function = error_validate_parameters)

        execution = self.user_template._execute(self.values)
        self._wait_for_not_running(execution)

        self.assertEqual(execution.state, 'WAITING_FOR_CONFIRMATION')
        self.assertTrue(execution._is_waiting_on_confirmation())
        expected_validations = [u'INFO:This is informational',
                                u'ERROR:This is an error']

        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        # We can not proceed at this point
        execution._abort()
        self._wait_for_not_running(execution)

        self.assertEqual(execution.state, 'ABORTED')

        expected_messages = [u'Beginning validation 2',
                             u'Done with validation 2']

        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))

        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))



    def test_example_validation_template(self):
        """ Test running example validation templates to make sure it continues to function """

        module_path = os.path.join("..", "analysis_templates", "other_examples",
                                   "parameter_validation")
        self.root_template.set_module('parameter_validation', module_path)

        parameters_values = ParametersValues(
            [TextParameterValue(self._eureqa, 'abc','32')]
        )

        execution = self.user_template._execute(parameters_values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'WAITING_FOR_CONFIRMATION')

        expected_validations = [u'INFO:This is an informational message',
                                u'ERROR:The value was not 42']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        expected_messages = [u'Beginning parameter validation']
        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))

        execution._abort()

    def test_example_validation_template_42(self):
        """ Test running example validation templates to make sure it continues to function (in positive path)  """

        module_path = os.path.join("..", "analysis_templates", "other_examples",
                                   "parameter_validation")
        self.root_template.set_module('parameter_validation', module_path)


        parameters_values = ParametersValues(
            [TextParameterValue(self._eureqa, 'abc','42')] # note different value here
        )

        execution = self.user_template.execute(parameters_values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'DONE')

        expected_validations = [u'INFO:This is an informational message',
                                u'WARNING:This is an example warning message']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        expected_messages = [u'Beginning parameter validation',
                             u'Starting running analysis template.',
                             u'Done!']
        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))

    def test_evaluate_model_template(self):
        """ Test running evaluate model template to make sure it continues to function"""

        module_path = os.path.join("..", "..", "built_in_ai_apps")
        self.root_template.set_module('evaluate_model_module', module_path)

        parameters_values = ParametersValues(
            [TopLevelModelParameterValue(self._eureqa, 'model_param','Weight = Calories', None, None, None),
             DataSourceParameterValue(self._eureqa, 'data_source_param','T1', None)]
        )

        execution = self.user_template.execute(parameters_values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'DONE',
                         msg="\n\n".join(str(x) for x in execution.progress_updates))

        expected_validations = [u'INFO:Target: numeric | Target data available. Model performance will be evaluated',
                                u'INFO:Expression parsed successfully',
                                u'WARNING:Data contains missing values; some output values may be missing.']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        expected_messages = [u'Beginning parameter validation',
                             u'Starting running analysis template.',
                             u'Creating search for custom solution',
                             u'Creating custom solution',
                             u'Creating model summary card',
                             u'Creating model evaluator card',
                             u'Done!']
        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))

        # validate settings on created search
        for c in execution.get_analysis().get_cards():
            from  eureqa.analysis_cards import ModelEvaluatorCard
            found_eval_card = False
            if isinstance(getattr(c.component, "content", None), ModelEvaluatorCard):
                found_eval_card = True
                solution_info = c.solution_infos[0]
                # note the search used by model evaluator is hidden, so get it directly by id
                search = solution_info.solution.search

                self.assertEqual(13, len(search.variable_options))
                for v in search.variable_options:
                    var = search.variable_options[v]
                    self.assertEqual('keep_nans', var.missing_value_policy)

        self.assertEqual(True, found_eval_card)

        execution._abort()

    def test_evaluate_model_template_classification(self):
        """ Test running evaluate model template on classification data to make sure it continues to function"""

        module_path = os.path.join("..", "..", "built_in_ai_apps")
        self.root_template.set_module('evaluate_model_module', module_path)

        parameters_values = ParametersValues(
            [TopLevelModelParameterValue(self._eureqa, 'model_param','y = x1 + x2', None, None, None),
             DataSourceParameterValue(self._eureqa, 'data_source_param','T2', None)]
        )

        execution = self.user_template.execute(parameters_values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'DONE',
                         msg="\n\n".join(str(x) for x in execution.progress_updates))

        expected_validations = [u'INFO:Target: binary | Target data available. Model performance will be evaluated',
                                u'INFO:Expression parsed successfully']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        expected_messages = [u'Beginning parameter validation',
                             u'Starting running analysis template.',
                             u'Creating search for custom solution',
                             u'Creating custom solution',
                             u'Creating model summary card',
                             u'Creating model evaluator card',
                             u'Done!']
        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))

        execution._abort()

    def test_evaluate_model_template_missing_target(self):
        """ Test running evaluate model template to make sure it continues to function with a target variable not in the data"""

        module_path = os.path.join("..", "..", "built_in_ai_apps")
        self.root_template.set_module('evaluate_model_module', module_path)

        parameters_values = ParametersValues(
            [TopLevelModelParameterValue(self._eureqa, 'model_param','abc = Calories', None, None, None),
             DataSourceParameterValue(self._eureqa, 'data_source_param','T1', None)]
        )

        execution = self.user_template.execute(parameters_values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'DONE',
                         msg="\n\n".join(str(x) for x in execution.progress_updates))

        expected_validations = [u'WARNING:Target data not available. Model performance will not be evaluated',
                                u'INFO:Expression parsed successfully',
                                u'WARNING:Data contains missing values; some output values may be missing.']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        expected_messages = [u'Beginning parameter validation',
                             u'Starting running analysis template.',
                             u'Creating target variable for custom solution',
                             u'Creating search for custom solution',
                             u'Creating custom solution',
                             u'Creating model summary card',
                             u'Creating model evaluator card',
                             u'Done!']
        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))

        execution._abort()

    def test_evaluate_model_template_missing_target_from_dataset(self):
        """ Test running evaluate model template to make sure it continues to function with a target variable not in the data"""

        module_path = os.path.join("..", "..", "built_in_ai_apps")
        self.root_template.set_module('evaluate_model_module', module_path)

        ds = self.root_eureqa.get_data_source("T1")
        search = ds.create_search(
            self.root_eureqa.search_templates.numeric("test search",
                                                      "Weight",
                                                      ["Date", "Calories"],
                                                      datasource=ds))
        soln = search.create_solution("Weight = 100")

        ds2_data = textwrap.dedent("""\
        Date,Calories
        7/3/2014,222
        7/4/2014,222
        7/5/2014,222
        7/6/2014,221
        7/7/2014,221
        7/8/2014,221
        7/9/2014,221
        7/10/2014,220
        7/11/2014,220
        """)
        self.root_eureqa.create_data_source("T1_partial", StringIO(ds2_data))

        parameters_values = ParametersValues(
             # Model based on T1
            [TopLevelModelParameterValue(self._eureqa, 'model_param', "Weight = 100",
                                         ds._data_source_id, search._id, soln._id),
             # Targeting T2, which has totally different variables
             DataSourceParameterValue(self._eureqa, 'data_source_param','T1_partial', None)]
        )

        execution = self.user_template.execute(parameters_values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'DONE',
                         msg="\n\n".join(str(x) for x in execution.progress_updates))

        expected_validations = [u'WARNING:Target data not available. Model performance will not be evaluated']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        expected_messages = [u'Beginning parameter validation',
                             u'Starting running analysis template.',
                             u"Couldn't apply solution to new data set; skipping:  Target variable 'Weight' doesn't exist in the specified datasource",
                             u'Creating target variable for custom solution',
                             u'Creating search for custom solution',
                             u'Creating custom solution',
                             u'Creating model summary card',
                             u'Creating model evaluator card',
                             u'Done!']
        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))

        execution._abort()

    def test_evaluate_model_template_no_target_error(self):
        """ Test running example evaluate model template to make sure it errors with only the right hand side of an expression"""

        module_path = os.path.join("..", "..", "built_in_ai_apps")
        self.root_template.set_module('evaluate_model_module', module_path)

        parameters_values = ParametersValues(
            [TopLevelModelParameterValue(self._eureqa, 'model_param','Calories', None, None, None),
             DataSourceParameterValue(self._eureqa, 'data_source_param','T1', None)]
        )

        execution = self.user_template._execute(parameters_values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'WAITING_FOR_CONFIRMATION')

        expected_validations = [u'ERROR:Expression must be of the form \'target_variable = expression\'',
                                u'INFO:Expression parsed successfully',
                                u'WARNING:Data contains missing values; some output values may be missing.']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        expected_messages = [u'Beginning parameter validation']
        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))

        execution._abort()

    def test_evaluate_model_template_data_source_error(self):
        """ Test running example evaluate model template to make sure it errors if the data_source doesn't exist """

        module_path = os.path.join("..", "..", "built_in_ai_apps")
        self.root_template.set_module('evaluate_model_module', module_path)

        parameters_values = ParametersValues(
            [TopLevelModelParameterValue(self._eureqa, 'model_param','Calories', None, None, None),
             DataSourceParameterValue(self._eureqa, 'data_source_param','datasource', None)]
        )

        execution = self.user_template._execute(parameters_values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'WAITING_FOR_CONFIRMATION')

        expected_validations = [u'ERROR:The selected data source does not exist']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        expected_messages = [u'Beginning parameter validation']
        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))

        execution._abort()

    def test_evaluate_model_template_expression_error(self):
        """ Test running evaluate model template to make sure it errors if the expression can't be parsed"""

        module_path = os.path.join("..", "..", "built_in_ai_apps")
        self.root_template.set_module('evaluate_model_module', module_path)

        parameters_values = ParametersValues(
            [TopLevelModelParameterValue(self._eureqa, 'model_param','Calories = 1 + NonExistantVariable', None, None, None),
             DataSourceParameterValue(self._eureqa, 'data_source_param','T1', None)]
        )

        execution = self.user_template._execute(parameters_values)
        self._wait_for_not_running(execution)
        self.assertEqual(execution.state, 'WAITING_FOR_CONFIRMATION')

        expected_validations = [u'INFO:Target: numeric | Target data available. Model performance will be evaluated',
                                u'ERROR:Unable to parse expression, check that all variables are in the data file',
                                u'WARNING:Data contains missing values; some output values may be missing.']
        self.assertEqual(expected_validations, self._get_validation_results_as_strings(execution))

        expected_messages = [u'Beginning parameter validation']
        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))

        execution._abort()

    def test_executions_list(self):
        ## Test that we can get the execution from the list of executions.
        self._make_module(template = self.root_template,
                          main_module_name='the_second_module',
                          create_analysis_function = create_analysis_normal,
                          validate_parameters_function = normal_validate_parameters)

        execution = self.user_template.execute(self.values)
        self._wait_for_not_running(execution)

        self.assertEqual(execution.state, 'DONE')
        expected_messages = [u'Beginning validation',
                             u'Done with validation',
                             u'Starting running analysis template.',
                             u'Creating search.',
                             u'Done!']
        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))

        # Now get execution from the list
        execution = self.user_template.get_executions()[-1]
        self.assertEqual(execution.state, 'DONE')
        self.assertEqual(expected_messages, self._get_progress_update_messages(execution))

    def test_add_data_file_to_server_on_start_tmpl(self):
        template = self._eureqa.create_analysis_template(
            "Test_data_file_template",
            "Test description",
            Parameters([DataFileParameter("1", "Gimme a Data File", "Because I want one.", ['txt','png','sc2', 'csv'])]),
            None)

        def create_analysis_fn(eureqa, template_execution):
            nutrition_header = """Date,Weight,Calories,"Fat (g)","Protein (g)","Carbohydrates (g)","Exercise Calories",Steps,"Over/Under Cal Budget","Calories by Breakfast (%)","Calories by Lunch (%)","Calories by Dinner (%)","Calories by Snacks (%)"\n"""
            if template_execution.parameters["1"].file.startswith(nutrition_header):
                template_execution.update_progress("Found nutrition_header")
            else:
                template_execution.update_progress("Did not find nutrition_header")

        self._make_module(template, 'the_module', create_analysis_fn)

        execution = template.execute(
            ParametersValues([
                DataFileParameterValue(self._eureqa, "1", "./Nutrition.csv")
                ]))

        execution._wait_until_finished()

        self.assertEqual(execution.state, "DONE", msg="\n\n".join(str(x) for x in execution.progress_updates))

        self.assertIn("Found nutrition_header", [x.message for x in execution.progress_updates])

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.

from eureqa import Eureqa, search_settings, DataSource
from eureqa.utils.utils import remove_files_in_directory

import unittest

from etestutils_nosearch import EureqaTestBaseNoSearch

class TestVariable(unittest.TestCase, EureqaTestBaseNoSearch):
    @classmethod
    def setUpClass(cls):
        remove_files_in_directory('temp') # clear any old test files before each test

    @classmethod
    def tearDownClass(cls):
        remove_files_in_directory('temp') # clear any old test files created by tests

    def test_url_encoded_id(self):
        data_source = self._eureqa.create_data_source('Test', 'Nutrition.csv')
        data_source.create_variable('0', '(native-country_>50K)')
        # these variable and datasource names result in a variable_id 'VGVzdC1fLShuYXRpdmUtY291bnRyeV8+NTBLKQ=='
        # which will cause problems if it isn't url encoded because of the '+'
        variable = data_source.get_variable_details('(native-country_>50K)')
        self.assertIsNotNone(variable)
        self.assertEqual('(native-country_>50K)', variable.name)

    def test_to_string(self):
        data_source = self._eureqa.create_data_source('data_source_3', 'Nutrition.csv')
        variable = data_source.get_variable('Weight')
        variable.__str__()


import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa, search_settings

import unittest

from etestutils import EureqaTestBase

class TestModelCard(unittest.TestCase, EureqaTestBase):
    def test_create(self):
        card = self._analysis.create_model_card(self._solution, 'Test model card 1',
                                           'Test model card description 1', False)
        self.assertEqual(card.title, 'Test model card 1')
        self.assertEqual(card.description, 'Test model card description 1')
        self.assertEqual(card.collapse, False)

    def test_update(self):
        card = self._analysis.create_model_card(self._solution, 'Test model card 2',
                                           'Test model card description 2', False)
        card.title = 'Test model card 2.5'
        card.description = 'Test model card description 2.5'
        card.collapse = True
        self.assertEqual(card.title, 'Test model card 2.5')
        self.assertEqual(card.description, 'Test model card description 2.5')
        self.assertEqual(card.collapse, True)

    def test_get(self):
        self._analysis.create_model_card(self._solution, 'Test model card 3',
                                           'Test model card description 3')
        cards = self._analysis.get_cards()
        cards = [x for x in cards if getattr(x, 'title', None) == 'Test model card 3']
        self.assertEqual(len(cards), 1)
        self.assertEqual(cards[0].title, 'Test model card 3')
        self.assertEqual(cards[0].description, 'Test model card description 3')

    def test_delete(self):
        card = self._analysis.create_model_card(self._solution, 'Test model card 4',
                                           'Test model card description 4')
        card.delete()
        cards = self._analysis.get_cards()
        cards = [x for x in cards if getattr(x, 'title', None) == 'Test model card 4']
        self.assertEqual(len(cards), 0)

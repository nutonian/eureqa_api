import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import math_block, MathBlock, Eureqa

import unittest
import json


class TestMathBlock(unittest.TestCase):
    def test_enable_disable(self):
        mb = MathBlock(9000, 'Constant', 1, None)

        # MathBlocks should be disabled by default.
        self.assertFalse(mb.enabled)

        mb.enable(complexity=9000)
        self.assertTrue(mb.enabled)
        self.assertEqual(mb.complexity, 9000)

        # Default complexity should be 1, same as in the constructor above.
        mb.enable()
        self.assertTrue(mb.enabled)
        self.assertEqual(mb.complexity, 1)

        # Create a MathBlock with no initial/default complexity
        mb = MathBlock(9000, 'Integer Constant', None, None)
        with self.assertRaises(AssertionError):
            mb.enable()  ## Can't enable at default complexity when we don't have a default
                
        mb.enable(complexity=1)
        self.assertEqual(mb.complexity, 1)
        mb.complexity = 2
        self.assertEqual(mb.complexity, 2)
        mb._default_complexity = 1
        mb.enable()
        self.assertEqual(mb.complexity, 1)
    
    def test_construction_from_json_with_notation(self):
        body = json.loads('{"complexity": 1, "op_name": "Constant", "op_notation": "c_", "op_id": 9000}')
        op = math_block._from_json(body)

        self.assertEqual(op.name, 'Constant')
        self.assertEqual(op.complexity, 1)
        self.assertEqual(op._notation, 'c_')

    def test_construction_from_json_without_notation(self):
        body = json.loads('{"complexity": 1, "op_name": "Constant", "op_id": 9000}')
        op = math_block._from_json(body)

        self.assertEqual(op.name, 'Constant')
        self.assertEqual(op.complexity, 1)

    def test_to_json_without_notation(self):
        mb = MathBlock(9000, 'Constant', 1, None)
        body = mb._to_json()
        text = json.dumps(body, sort_keys=True)
        self.assertEqual(text, '{"complexity": 1, "enabled": false, "op_id": 9000, "op_name": "Constant"}')

    def test_to_string(self):
        mb = MathBlock(9000, 'Constant', 1, None)
        mb.__str__()

    def test_equals(self):
        one = MathBlock(9000, 'One', 1, None)
        two = MathBlock(9000, 'One', 1, None)
        self.assertEqual(one, two)

    def test_not_equals_names(self):
        one = MathBlock(9000, 'One', 1, None)
        two = MathBlock(9000, 'Two', 1, None)
        self.assertNotEqual(one, two)

    def test_not_equals_complexity(self):
        one = MathBlock(9000, 'One', 1, None)
        two = MathBlock(9000, 'One', 2, None)
        self.assertNotEqual(one, two)

    def test_not_equals_none(self):
        one = MathBlock(9000, 'One', 1, None)
        self.assertFalse(one == None)

    def test_not_equals_other_type(self):
        one = MathBlock(9000, 'One', 1, None)
        self.assertFalse(one == 'junk')

    def test_equal_hash(self):
        one = MathBlock(9000, 'One', 1, None)
        two = MathBlock(9000, 'One', 1, None)
        self.assertEqual(one.__hash__(), two.__hash__())

    def test_not_equal_hash_names(self):
        one = MathBlock(9000, 'One', 1, None)
        two = MathBlock(9000, 'Two', 1, None)
        self.assertNotEqual(one.__hash__(), two.__hash__())

    def test_not_equal_hash_complexity(self):
        one = MathBlock(9000, 'One', 1, None)
        two = MathBlock(9000, 'One', 2, None)
        self.assertNotEqual(one.__hash__(), two.__hash__())

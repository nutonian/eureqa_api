import sys
import os
import unittest
import mock

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa
from etestutils_nosearch import EureqaTestBaseNoSearch

class TestApiKeysAndAccess(unittest.TestCase, EureqaTestBaseNoSearch):
    def test_keys(self):
        ## Generate a key; make sure we can log in and make API calls using that key
        key = self._eureqa._generate_api_key("Test Key 1")
        conn = eureqa.Eureqa(url=self._eureqa_url, user_name='root', key=key['key'])
        conn.get_analyses()

        self.assertIn("Test Key 1", [x['name'] for x in self._eureqa._list_api_keys()])

        ## Revoke the key; make sure it no longer works
        self._eureqa._revoke_api_key_by_id(key['id'])
        with self.assertRaises(Exception):
            eureqa.Eureqa(url=self._eureqa_url, user_name='root', key=key['key'])

        ## Now do it again and revoke by name
        key = self._eureqa._generate_api_key("Test Key 2")
        conn = eureqa.Eureqa(url=self._eureqa_url, user_name='root', key=key['key'])
        conn.get_analyses()

        self.assertIn("Test Key 2", [x['name'] for x in self._eureqa._list_api_keys()])

        ## Revoke non-existent names/ids and make sure the key still exists and we can still get in
        with self.assertRaises(Exception):
            eureqa._revoke_api_key_by_id(20000)
        with self.assertRaises(Exception):
            eureqa._revoke_api_key_by_name('Test Key 2000')
        self.assertIn("Test Key 2", [x['name'] for x in self._eureqa._list_api_keys()])
        conn = eureqa.Eureqa(url=self._eureqa_url, user_name='root', key=key['key'])
        conn.get_analyses()

        ## Revoke the key; make sure it no longer works
        self._eureqa._revoke_api_key_by_name(key['name'])
        with self.assertRaises(Exception):
            eureqa.Eureqa(url=self._eureqa_url, user_name='root', key=key['key'])

    def test_api_access(self):
        ## Create an org and user
        ## (Copied with modifications from test_eureqa.TestEureqa.test_two_factor_login_interactive)
        conn = eureqa.Eureqa(url=self._eureqa_url, user_name='root', password='Changeme1', organization='root')
        # have to create a new organization that requires two factor auth
        response = conn._session.execute('/api/v2/organizations', 'POST', args={'name': 'org4'})
        response['require_two_factor_auth'] = True
        conn._session.execute('/api/v2/organizations/' + response['id'], 'POST', args=response)
        # make a signup key and a new user
        key = conn._session.execute('/api/v2/' + response['id'] + '/auth/signup_key', 'POST', args={'organization':response['id'],'total_uses':1,'roles':['admin']})
        # because of the way the two factor login works the first post will fail then you post again with the one time password
        with self.assertRaises(Exception):
            conn._session.execute('/api/v2/auth/signup', 'POST', args={'firstname':'pink','lastname':'mike','username':'pinkmike4','useremail':'pinkmike4@nutonian.com','phone_number':'1234','password':'Changeme1','signup_key':key['signup_key']})

        conn._session.execute('/api/v2/auth/signup', 'POST', args={'firstname':'pink','lastname':'mike','username':'pinkmike4','useremail':'pinkmike4@nutonian.com','phone_number':'1234','password':'Changeme1','signup_key':key['signup_key'], 'one_time_password':'one-time_password'})

        org = "org4"

        ## Verify that user can't use the API initially
        with mock.patch('__builtin__.raw_input', return_value='one-time_password'):
            with self.assertRaises(Exception):
                eureqa.Eureqa(url=self._eureqa_url, user_name='pinkmike4', password='Changeme1', organization=org, interactive_mode=True).get_analyses()

        ## Grant access; verify that user can log in with both password and API key
        self._eureqa._grant_user_api_access("pinkmike4", org)
        with mock.patch('__builtin__.raw_input', return_value='one-time_password'):
            conn = eureqa.Eureqa(url=self._eureqa_url, user_name='pinkmike4', password='Changeme1', organization=org, interactive_mode=True)

        key = conn._generate_api_key("Test Key")['key']
        eureqa.Eureqa(url=self._eureqa_url, user_name='pinkmike4', key=key, organization=org).get_analyses()

        ## Revoke access; make sure it no longer works
        self._eureqa._revoke_user_api_access("pinkmike4", org)

        with mock.patch('__builtin__.raw_input', return_value='one-time_password'):
            with self.assertRaises(Exception):
                eureqa.Eureqa(url=self._eureqa_url, user_name='pinkmike4', password='Changeme1', organization=org, interactive_mode=True).get_analyses()

        with self.assertRaises(Exception):
            eureqa.Eureqa(url=self._eureqa_url, user_name='pinkmike4', key=key, organization=org).get_analyses()

        with self.assertRaises(Exception):
            conn.get_analyses()

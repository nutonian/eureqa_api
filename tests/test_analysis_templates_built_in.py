""" Test built in analysis templates """
import sys
import os
import unittest

# Need this to be able to reference eureqa package.
sys.path.insert(1, os.path.abspath('..'))

from etestutils_nosearch import EureqaTestBaseNoSearch
from eureqa import install_analysis_template

class TestAnalysisTemplatesBuiltIn(unittest.TestCase, EureqaTestBaseNoSearch):

    def _remove_template_by_name(self, template_name):
        """ removes the named template, if it exists """
        t = None
        try:
            t = self._eureqa._get_analysis_template_by_name(template_name)
        except:
            pass

        if t:
            t.delete()


    """ Test that the built in analysis templates are working as expected """
    def test_evaluate_model(self):
        """ Test the evaluate model built in matches the expected values """
        eval_model_template = self._eureqa._get_analysis_template_by_name('Model Evaluator')

        # Clean up from previous run
        self._remove_template_by_name('Copy of Model Evaluator')

        # install the evaluator from source code and ensure that it has the same settings
        eval_model_source_path = os.path.join("..", "..", "built_in_ai_apps", "evaluate_model_module")
        args = [u"install_analysis_template.py",
                u'-u',
                u'root',
                u'-p',
                u'Changeme1',
                u'--name',
                u'Copy of Model Evaluator',
                u'--no-save',
                u'--url',
                self._eureqa_url,
                eval_model_source_path]
        install_analysis_template.main(args)

        eval_model_copy_template = self._eureqa._get_analysis_template_by_name('Copy of Model Evaluator')
        self.assertEqual(eval_model_template.description, eval_model_copy_template.description)

        # expect the parameters are the same (if this diffs you
        # probably need to update the parameter definitions in
        # Built_in_template.cpp to match what is in the evaluate_model
        # source.
        self.assertEqual(eval_model_template.parameters._to_json(),
                         eval_model_copy_template.parameters._to_json())

        self._remove_template_by_name('Copy of Model Evaluator')


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAnalysisTemplatesBuiltIn)
    unittest.TextTestRunner(verbosity=2).run(suite)

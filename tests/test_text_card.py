import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa
from eureqa.utils.utils import remove_files_in_directory
from eureqa.analysis_cards.text_card import TextCard
from eureqa.session import Http404Exception

from filecmp import cmp as filecmp
import unittest
import re

from etestutils_nosearch import EureqaTestBaseNoSearch

class TestTextCard(unittest.TestCase, EureqaTestBaseNoSearch):
    @classmethod
    def setUpClass(cls):
        remove_files_in_directory('temp') # clear any old test files before each test

        # Clean up after previous test iterations
        for x in cls._analysis.get_cards():
            if getattr(x, 'text', None) in {'Test text 1', 'Test text 2', 'Test text 3', 'Test text 4'}:
                x.delete()

    @classmethod
    def tearDownClass(cls):
        remove_files_in_directory('temp') # clear any old test files created by tests

    def test_create(self):
        card = self._analysis.create_text_card("Test text 1", collapse=False)
        self.assertEqual(card.text, 'Test text 1')
        self.assertEqual(card.collapse, False)

    def test_update(self):
        card = self._analysis.create_text_card("Test text 2", collapse=False)
        card.text = 'Test text 2.5'
        card.collapse = True
        self.assertEqual(card.text, 'Test text 2.5')
        self.assertEqual(card.collapse, True)

    def test_get(self):
        self._analysis.create_text_card("Test text 3")
        cards = self._analysis.get_cards()
        cards = [x for x in cards if getattr(x, 'text', None) == 'Test text 3']
        self.assertEqual(len(cards), 1)
        self.assertEqual(cards[0].text, 'Test text 3')

    def test_delete(self):
        card = self._analysis.create_text_card("Test text 4")
        card.delete()
        cards = self._analysis.get_cards()
        cards = [x for x in cards if getattr(x, 'text', None) == 'Test text 4']
        self.assertEqual(len(cards), 0)


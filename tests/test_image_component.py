# Copyright (c) 2017, Nutonian Inc
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the Nutonian Inc nor the
#     names of its contributors may be used to endorse or promote products
#     derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL NUTONIAN INC BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa

import unittest

from etestutils_nosearch import EureqaTestBaseNoSearch
from eureqa.analysis.components import Image, _Component
from cStringIO import StringIO
import requests

class TestDownloadFileComponent(unittest.TestCase, EureqaTestBaseNoSearch):

    FILE_1 = "Test image 1.gif"
    FILE_2 = "Test image 2.png"
    FILE_3 = "Test image 3.jpg"
    FILE_4 = "Test image 4.jpeg"
    FILE_5 = "Test image 5.svg"
    FILE_UNSUPPORTED = "simple_data.csv"
    FILE_NOT_FOUND = "nonexistent image.jpg"

    def test_component(self):
        comp = Image(self.FILE_1)

        # Make sure constructor set expected fields
        self.assertEqual(comp._image_path, self.FILE_1)
        self.assertEqual(comp.image_file_id, None)
        self.assertEqual(comp.image_url, None)
        self.assertEqual(comp.width, None)
        self.assertEqual(comp.height, None)
        self.assertEqual(comp.file, None)  # File not yet uploaded

        self._analysis.create_card(comp)

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(comp._image_path, self.FILE_1)
        self.assertTrue(len(comp.image_file_id) > 0)
        self.assertEqual(comp.image_url, '/api/root/analysis/%s/files/%s' % (self._analysis.analysis_id, comp.image_file_id))
        self.assertEqual(comp.width, None)
        self.assertEqual(comp.height, None)
        self.assertNotEqual(comp.file, None)

        # Capture the image ID
        image_file_id = comp.image_file_id

        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # Make sure we can fetch all fields from scratch
        self.assertFalse(hasattr(comp, '_image_path'))
        self.assertEqual(image_file_id, comp.image_file_id)
        self.assertEqual(comp.image_url, '/api/root/analysis/%s/files/%s' % (self._analysis.analysis_id, comp.image_file_id))
        self.assertEqual(comp.width, None)
        self.assertEqual(comp.height, None)
        self.assertNotEqual(comp.file, None)

        # Test with analysis passed to constructor
        comp = Image(self.FILE_1, _analysis=self._analysis)

        # Make sure constructor set all fields
        self.assertEqual(comp._image_path, self.FILE_1)
        self.assertTrue(len(comp.image_url) > 0)
        self.assertTrue(len(comp.image_file_id) > 0)  # We forced the file to be re-uploaded -- new ID
        self.assertEqual(comp.width, None)
        self.assertEqual(comp.height, None)
        self.assertNotEqual(comp.file, None)

        # test the download filename is set correctly
        download_resp = requests.get(
                self._analysis._eureqa._session.url + comp._image_url,
                cookies=self._analysis._eureqa._session._session.cookies)
        self.assertEqual('attachment; filename="' + self.FILE_1 + '"', download_resp.headers['content-disposition'])

    def test_image_types(self):
        comp = Image(self.FILE_1)
        comp = Image(self.FILE_2)
        comp = Image(self.FILE_3)
        comp = Image(self.FILE_4)
        # Use a different helper method to construct the image, just for fun
        comp = self._analysis.upload_image(self.FILE_5)

        try:
            comp = Image(self.FILE_UNSUPPORTED)
            self.fail()
        except Exception as e:
            self.assertEqual(str(e), 'Image file "simple_data.csv" is not supported. Valid types are .svg, .jpg, .jpeg, .png, .gif')


    def test_image_not_found(self):
        try:
            comp = Image(self.FILE_NOT_FOUND)
            self.fail()
        except Exception as e:
            self.assertEqual(str(e), 'Image file located at "' + self.FILE_NOT_FOUND + '" does not exist')

# Copyright (c) 2017, Nutonian Inc
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the Nutonian Inc nor the
#     names of its contributors may be used to endorse or promote products
#     derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL NUTONIAN INC BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa

import unittest
import pandas
import json

from etestutils_nosearch import EureqaTestBaseNoSearch
from eureqa.analysis.components import TitledLayout, TextBlock, TableBuilder, _Component

class TestTitledLayoutComponent(unittest.TestCase, EureqaTestBaseNoSearch):

    def test_component(self):
        child_block = TextBlock("Hello World!")
        child_block_2 = TextBlock("hi")

        comp = TitledLayout("Title", "Description", child_block)

        # Make sure constructor set all fields
        self.assertEqual(comp.title, "Title")
        self.assertEqual(comp.description, "Description")
        self.assertEqual(comp.content, child_block)

        self._analysis.create_card(comp)

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(comp.title, "Title")
        self.assertEqual(comp.description, "Description")
        self.assertEqual(comp.content, child_block)

        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # Make sure we can fetch all fields from scratch
        self.assertEqual(comp.title, "Title")
        self.assertEqual(comp.description, "Description")
        self.assertEqual(comp.content, child_block)

        comp.title = "Title 2"
        comp.description = "Description 2"
        comp.create_card(child_block_2)

        # Make sure all fields were updated
        self.assertEqual(comp.title, "Title 2")
        self.assertEqual(comp.description, "Description 2")
        self.assertEqual(comp.content, child_block_2)

        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        self.assertEqual(comp.title, "Title 2")
        self.assertEqual(comp.description, "Description 2")
        self.assertEqual(comp.content, child_block_2)

        # Test with analysis passed to constructor
        comp = TitledLayout("Title", "Description", child_block, self._analysis)

        # Make sure constructor set all fields
        self.assertEqual(comp.title, "Title")
        self.assertEqual(comp.description, "Description")
        self.assertEqual(comp.content, child_block)

        # Make sure child_block survives round-trip to the server
        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        self.assertEqual(comp.content, child_block)


    def test_add_component_to_table(self):
        text_block = TextBlock("This text is inside the modal")
        container = TitledLayout("Title", "Description")
        container.create_card(text_block)

        df = pandas.DataFrame({'a': [0], 'b': ['this text will be deleted']})
        table = TableBuilder(df, title='title')
        table['b'].rendered_values = [container]
        self._analysis.create_card(table)

        self.assertTrue(hasattr(container, "_component_id"))
        self.assertTrue(hasattr(text_block, "_component_id"))
        self.assertIn(container._component_id, str(table._get_table_data(self._analysis)))
        self.assertIn(text_block._component_id, str(table._get_table_data(self._analysis)))

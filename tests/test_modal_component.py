# Copyright (c) 2017, Nutonian Inc
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the Nutonian Inc nor the
#     names of its contributors may be used to endorse or promote products
#     derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL NUTONIAN INC BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa

import unittest

from etestutils_nosearch import EureqaTestBaseNoSearch
from eureqa.analysis.components import Modal, ModalLink, TextBlock, TableBuilder, _Component
from cStringIO import StringIO
import requests
import pandas
import json

class TestModalComponent(unittest.TestCase, EureqaTestBaseNoSearch):

    def test_component(self):
        # first try constructor with the minimum required arguments
        t = TextBlock('text in the modal')
        comp = Modal(component=t) # min required args

        # Make sure constructor set all fields
        self.assertEqual(comp.content_component_id, None)
        self.assertEqual(comp.title, '')
        self.assertEqual(comp.size, 'small')
        self.assertEqual(comp.icon_file_url, None)
        self.assertEqual(comp.icon_file_id, None)

        self._analysis.create_card(comp)

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(comp.content_component_id, t._component_id)
        self.assertEqual(comp.title, '')
        self.assertEqual(comp.size, 'small')
        self.assertEqual(comp.icon_file_url, None)
        self.assertEqual(comp.icon_file_id, None)

        # now try filling in more of the constructor arguments
        t = TextBlock('text in the modal')
        comp = Modal(title="This is the modal title", size="large", component=t, icon_file_path='Test image 2.png')

        # Make sure constructor set all fields
        self.assertEqual(comp.content_component_id, None)
        self.assertEqual(comp.title, 'This is the modal title')
        self.assertEqual(comp.size, 'large')
        self.assertEqual(comp.icon_file_url, None)
        self.assertEqual(comp.icon_file_id, None)

        self._analysis.create_card(comp)

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(comp.content_component_id, t._component_id)
        self.assertEqual(comp.title, 'This is the modal title')
        self.assertEqual(comp.size, 'large')
        self.assertTrue(len(comp.icon_file_id) > 0)
        self.assertEqual(comp.icon_file_url, '/api/%s/analysis/%s/files/%s' % ('root', self._analysis.analysis_id, comp._icon_file_id))

        t = _Component._get(_component_id=t._component_id, _analysis=self._analysis)
        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # Make sure we can fetch all fields from scratch
        self.assertEqual(comp.content_component_id, t._component_id)
        self.assertEqual(comp.title, 'This is the modal title')
        self.assertEqual(comp.size, 'large')
        self.assertTrue(len(comp.icon_file_id) > 0)
        self.assertEqual(comp.icon_file_url, '/api/%s/analysis/%s/files/%s' % ('root', self._analysis.analysis_id, comp._icon_file_id))

        # Test with analysis passed to constructor
        t = TextBlock('text in the modal', _analysis=self._analysis)
        comp = Modal(title="This is the modal title", size="large", component=t, icon_file_path='Test image 2.png',
                     _analysis=self._analysis)

        # Make sure constructor set all fields
        self.assertEqual(comp.content_component_id, t._component_id)
        self.assertEqual(comp.title, 'This is the modal title')
        self.assertEqual(comp.size, 'large')
        self.assertTrue(len(comp.icon_file_id) > 0)
        self.assertEqual(comp.icon_file_url, '/api/%s/analysis/%s/files/%s' % ('root', self._analysis.analysis_id, comp._icon_file_id))

        # test the icon information is set correctly
        download_resp = requests.get(
                self._analysis._eureqa._session.url + comp._icon_file_url,
                cookies=self._analysis._eureqa._session._session.cookies)
        self.assertEqual('attachment; filename="Test image 2.png"', download_resp.headers['content-disposition'])

    def test_component_link_manual(self):
        t = TextBlock('text in the modal')
        modal = Modal(title="This is the modal title", size="large", component=t, icon_file_path='Test image 2.png')

        comp = ModalLink(modal=modal, link_text='My Modal Link')

        # Make sure constructor set all fields
        self.assertFalse(hasattr(modal, '_component_id'))
        self.assertEqual(comp.modal_component_id, None)
        self.assertEqual(comp.link_text, 'My Modal Link')

        self._analysis.create_card(comp)

        # Make sure all fields are still set after round-trip from server
        self.assertTrue(hasattr(modal, '_component_id'))
        self.assertEqual(comp.modal_component_id, modal._component_id)
        self.assertEqual(comp.link_text, 'My Modal Link')

        t = _Component._get(_component_id=t._component_id, _analysis=self._analysis)
        modal = _Component._get(_component_id=modal._component_id, _analysis=self._analysis)
        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # Make sure we can fetch all fields from scratch
        self.assertTrue(hasattr(modal, '_component_id'))
        self.assertEqual(comp.modal_component_id, modal._component_id)
        self.assertEqual(comp.link_text, 'My Modal Link')

        # Test with analysis passed to construcor
        t = TextBlock('text in the modal', _analysis=self._analysis)
        modal = Modal(title="This is the modal title", size="large", component=t, icon_file_path='Test image 2.png',
                     _analysis=self._analysis)
        comp = ModalLink(modal=modal, link_text='My Modal Link')
        self.assertTrue(hasattr(modal, '_component_id'))
        self.assertEqual(comp.modal_component_id, modal._component_id)
        self.assertEqual(comp.link_text, 'My Modal Link')

    def test_component_link_create(self):
        t = TextBlock('text in the modal')
        modal = Modal(title="This is the modal title", size="large", component=t, icon_file_path='Test image 2.png')
        comp = modal.link(link_text='My Modal Link')

        self.assertFalse(hasattr(modal, '_component_id'))
        self.assertEqual(comp.modal_component_id, None)
        self.assertEqual(comp.link_text, 'My Modal Link')

        self._analysis.create_card(comp)

        self.assertTrue(hasattr(modal, '_component_id'))
        self.assertEqual(comp.modal_component_id, modal._component_id)
        self.assertEqual(comp.link_text, 'My Modal Link')

        t = _Component._get(_component_id=t._component_id, _analysis=self._analysis)
        modal = _Component._get(_component_id=modal._component_id, _analysis=self._analysis)
        comp = modal.link(link_text='My Modal Link')

        self.assertTrue(hasattr(modal, '_component_id'))
        self.assertEqual(comp.modal_component_id, modal._component_id)
        self.assertEqual(comp.link_text, 'My Modal Link')

        t = TextBlock('text in the modal', _analysis=self._analysis)
        modal = Modal(title="This is the modal title", size="large", component=t, icon_file_path='Test image 2.png',
                     _analysis=self._analysis)
        comp = modal.link(link_text='My Modal Link')
        self.assertTrue(hasattr(modal, '_component_id'))
        self.assertEqual(comp.modal_component_id, modal._component_id)
        self.assertEqual(comp.link_text, 'My Modal Link')


    def test_add_component_to_table(self):
        text_block = TextBlock("This text is inside the modal")
        container = Modal(title="This is the modal title", size="large", component=text_block, icon_file_path='Test image 2.png')
        modal_link = container.link()

        df = pandas.DataFrame({'a': [0,1], 'b': ['this text will be deleted','this text will also be deleted']})
        table = TableBuilder(df, title='title')
        table['b'].rendered_values = [container, modal_link]
        self._analysis.create_card(table)

        self.assertTrue(hasattr(modal_link, "_component_id"))
        self.assertTrue(hasattr(container, "_component_id"))
        self.assertTrue(hasattr(text_block, "_component_id"))
        self.assertIn(container._component_id, str(table._get_table_data(self._analysis)))
        self.assertIn(text_block._component_id, str(table._get_table_data(self._analysis)))
        self.assertIn(modal_link._component_id, str(table._get_table_data(self._analysis)))

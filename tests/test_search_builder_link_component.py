import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.

import eureqa
import unittest
from eureqa.analysis.components import *

from eureqa.search_templates import *

from etestutils_nosearch import EureqaTestBaseNoSearch

class TestSearchBuilderLinkComponent(unittest.TestCase, EureqaTestBaseNoSearch):

    @classmethod
    def setUpClass(cls):
        cls._data_source_2 = cls._eureqa.create_data_source('SimpleDataSource', 'simple_data.csv')

    def test_component(self):
        analysis = self._analysis
        sbl = SearchBuilderLink(datasource=self._data_source,
                                search_template=SearchTemplates.Numeric,
                                target_variable='x',
                                input_variables=['inc_x', 'dbl_x'])
        self.assertEqual(sbl._link_text, 'Search Builder')
        self.assertEqual(sbl._search_template_detail._datasource_id, self._data_source._data_source_id)
        self.assertEqual(sbl._search_template_detail._search_template_id, SearchTemplates.Numeric)
        self.assertFalse(hasattr(sbl._search_template_detail, '_min_delay'))
        self.assertFalse(hasattr(sbl._search_template_detail, '_max_delay'))
        self.assertEqual(sbl._search_template_detail._target_variable, 'x')
        self.assertEqual(sbl._search_template_detail._input_variables, ['inc_x', 'dbl_x'])
        card = analysis.create_html_card("Search builder link is: {0}".format(analysis.html_ref(sbl)))

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(sbl._link_text, 'Search Builder')
        self.assertEqual(sbl._search_template_detail._datasource_id, self._data_source._data_source_id)
        self.assertEqual(sbl._search_template_detail._search_template_id, SearchTemplates.Numeric)
        self.assertFalse(hasattr(sbl._search_template_detail, '_min_delay'))
        self.assertFalse(hasattr(sbl._search_template_detail, '_max_delay'))
        self.assertEqual(sbl._search_template_detail._target_variable, 'x')
        self.assertEqual(sbl._search_template_detail._input_variables, ['inc_x', 'dbl_x'])

        # Make sure we can fetch all fields from scratch
        sbl = analysis._get_component_by_id(sbl._component_id)
        self.assertEqual(sbl._link_text, 'Search Builder')
        self.assertEqual(sbl._search_template_detail._datasource_id, self._data_source._data_source_id)
        self.assertEqual(sbl._search_template_detail._search_template_id, SearchTemplates.Numeric)
        self.assertFalse(hasattr(sbl._search_template_detail, '_min_delay'))
        self.assertFalse(hasattr(sbl._search_template_detail, '_max_delay'))
        self.assertEqual(sbl._search_template_detail._target_variable, 'x')
        self.assertEqual(sbl._search_template_detail._input_variables, ['inc_x', 'dbl_x'])
        self.assertTrue(hasattr(sbl, '_component_id'))

        # ensure we made the html that we expected
        content = card.component.content # is a html content
        self.assertEqual(content.html, u'Search builder link is: <nutonian-component id="{0}" />'.format(sbl._component_id))

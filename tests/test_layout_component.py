import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.

import eureqa
import unittest
from eureqa.analysis.components import *
import pandas
import json

from etestutils_nosearch import EureqaTestBaseNoSearch

class TestLayoutComponent(unittest.TestCase, EureqaTestBaseNoSearch):

    # unit tests for _GridItem
    def test_grid_item_ctor(self):
        analysis = self._analysis
        bar = MagnitudeBar(value=0.34)
        gi = Layout._GridItem(component=bar, analysis=analysis);

        self.assertEqual(hasattr(bar, '_component_id'), False)
        self.assertEqual("1", gi.grid_unit);
        self.assertEqual(False, gi.dark_background);
        self.assertEqual(analysis, gi._analysis)

    def test_grid_item_associate(self):
        analysis = self._analysis
        bar = MagnitudeBar(value=0.34)
        gi = Layout._GridItem(component=bar, analysis=analysis, grid_unit="1/2", dark_background=True);

        self.assertEqual(hasattr(bar, '_component_id'), False)
        self.assertEqual("1/2", gi.grid_unit);
        self.assertEqual(True, gi.dark_background);
        self.assertEqual(analysis, gi._analysis)

        bar._associate(analysis)
        gi._register(analysis)
        self.assertNotEqual(bar._component_id, None)

    def test_grid_item_roundtrip(self):
        analysis = self._analysis
        bar = MagnitudeBar(value=0.34)
        gi = Layout._GridItem(component=bar, analysis=analysis, grid_unit="1/2", dark_background=True);

        bar._associate(analysis)
        gi._register(analysis)
        self.assertEqual("1/2", gi.grid_unit);
        self.assertEqual(True, gi.dark_background);
        self.assertEqual(analysis, gi._analysis)
        self.assertEqual(gi.component, bar);

        # test conversion from json (that the component field gets populated appropriately)
        gi2 = Layout._GridItem(analysis=analysis)
        gi2._from_json(gi._to_json())
        self.assertEqual("1/2", gi2.grid_unit);
        self.assertEqual(True, gi.dark_background);
        self.assertEqual(analysis, gi2._analysis)
        self.assertEqual(gi.component._component_id, gi2.component._component_id)
        self.assertEqual(bar._component_id, gi2.component._component_id)


    # unit test for Row
    def test_row_construction(self):
        analysis = self._analysis
        row = Layout._Row(analysis=analysis)
        self.assertEqual(row.grid_items, [])

        bar = MagnitudeBar(value=0.34)
        self.assertEqual(len(row.grid_items), 0)
        row.create_card(bar, grid_unit="1/3", dark_background=True)
        self.assertEqual(len(row.grid_items), 1)
        self.assertEqual(row.grid_items[0].grid_unit, "1/3")
        self.assertEqual(row.grid_items[0].dark_background, True);
        self.assertEqual(row.grid_items[0].component, bar)

        bar2 = MagnitudeBar(value=0.50)
        row.create_card(bar, grid_unit="1/3")
        self.assertEqual(len(row.grid_items), 2)


    def test_row_associate(self):
        analysis = self._analysis
        row = Layout._Row(analysis=analysis)

        bar = MagnitudeBar(value=0.34)
        row.create_card(bar, grid_unit="1/3")

        bar2 = MagnitudeBar(value=0.50)
        row.create_card(bar2, grid_unit="1/2")

        # ensure it survives a call to register
        bar._associate(analysis)
        bar2._associate(analysis)
        row._register(analysis)

        self.assertEqual(len(row.grid_items), 2)
        self.assertEqual(row.grid_items[0].grid_unit, "1/3")
        self.assertEqual(row.grid_items[0].component._component_id, bar._component_id)
        self.assertEqual(row.grid_items[1].grid_unit, "1/2")
        self.assertEqual(row.grid_items[1].component._component_id, bar2._component_id)


    def test_row_roundtrip(self):
        analysis = self._analysis
        row = Layout._Row(analysis=analysis)

        bar = MagnitudeBar(value=0.34)
        row.create_card(bar, grid_unit="1/3", dark_background=True)

        bar2 = MagnitudeBar(value=0.50)
        row.create_card(bar2, grid_unit="1/2")
        bar._associate(analysis)
        bar2._associate(analysis)
        row._register(analysis)

        row2 = Layout._Row(analysis=analysis);
        row2._from_json(row._to_json())

        self.assertEqual(len(row2.grid_items), 2)
        self.assertEqual(row2.grid_items[0].grid_unit, "1/3")
        self.assertEqual(row2.grid_items[0].dark_background, True)
        self.assertEqual(row2.grid_items[0].component._component_id, bar._component_id)
        self.assertEqual(row2.grid_items[1].grid_unit, "1/2")
        self.assertEqual(row2.grid_items[1].dark_background, False)
        self.assertEqual(row2.grid_items[1].component._component_id, bar2._component_id)

    def test_layout_ctor(self):
        layout = Layout()
        self.assertEqual(0, len(layout.rows))
        self.assertEqual(False, layout.borders)

        layout = Layout(borders=True)
        self.assertEqual(0, len(layout.rows))
        self.assertEqual(True, layout.borders)

    def test_delete(self):
        analysis = self._analysis
        layout = Layout()
        h1 = HtmlBlock(html="<h1>Hybrid Performance</h1>")
        h2 = HtmlBlock("Custom Graph Goes Here")
        layout.add_component(h1)
        layout.add_row()
        layout.add_component(h2, "1/2")

        self._analysis.create_card(layout)
        self.assertNotEqual(h1._component_id, None)
        self.assertNotEqual(h2._component_id, None)

        # fetch components from the server to ensure no exception is raised
        analysis._get_component_by_id(layout._component_id)
        analysis._get_component_by_id(h1._component_id)
        analysis._get_component_by_id(h2._component_id)

        layout.delete()


        # ensure the component and all subcomponents are gone
        with self.assertRaises(Exception) as context:
            analysis._get_component_by_id(layout._component_id)

        with self.assertRaises(Exception) as context:
            print analysis._get_component_by_id(h1._component_id)

        with self.assertRaises(Exception) as context:
            analysis._get_component_by_id(h2._component_id)


    def test_component(self):
        analysis = self._analysis
        layout = Layout(borders=True)
        h1 = HtmlBlock(html="<h1>Hybrid Performance</h1>")
        h2 = HtmlBlock("Custom Graph Goes Here")
        h3 = HtmlBlock("Variable Panel Goes Here")
        layout.add_component(h1)
        layout.add_row()
        layout.add_component(h2, "1/2")
        layout.add_component(h3, "1/4", True)

        self.assertEqual(2, len(layout.rows))
        self.assertEqual(layout._current_row, layout.rows[1])
        self._analysis.create_card(layout)

        self.assertNotEqual(h1._component_id, None)
        self.assertNotEqual(h2._component_id, None)
        self.assertNotEqual(h3._component_id, None)

        self.assertNotEqual(h1._component_id, h2._component_id)
        self.assertNotEqual(h1._component_id, h3._component_id)

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(2, len(layout.rows))
        self.assertEqual(True, layout.borders)
        self.assertEqual(layout._current_row, layout.rows[1])
        self.assertEqual(1, len(layout.rows[0].grid_items))
        self.assertEqual(layout.rows[0].grid_items[0].component._component_id, h1._component_id)
        self.assertEqual("1", layout.rows[0].grid_items[0].grid_unit)
        self.assertEqual(2, len(layout.rows[1].grid_items))
        self.assertEqual(layout.rows[1].grid_items[0].component._component_id, h2._component_id)
        self.assertEqual("1/2", layout.rows[1].grid_items[0].grid_unit)
        self.assertEqual(False, layout.rows[1].grid_items[0].dark_background)
        self.assertEqual(layout.rows[1].grid_items[1].component._component_id, h3._component_id)
        self.assertEqual("1/4", layout.rows[1].grid_items[1].grid_unit)
        self.assertEqual(True,  layout.rows[1].grid_items[1].dark_background)

        # Make sure we can fetch all fields from scratch
        layout = analysis._get_component_by_id(layout._component_id)
        self.assertEqual(2, len(layout.rows))
        self.assertEqual(True, layout.borders)
        self.assertEqual(layout._current_row, layout.rows[1])
        self.assertEqual(1, len(layout.rows[0].grid_items))
        self.assertEqual(layout.rows[0].grid_items[0].component._component_id, h1._component_id)
        self.assertEqual("1", layout.rows[0].grid_items[0].grid_unit)
        self.assertEqual(2, len(layout.rows[1].grid_items))
        self.assertEqual(layout.rows[1].grid_items[0].component._component_id, h2._component_id)
        self.assertEqual("1/2", layout.rows[1].grid_items[0].grid_unit)
        self.assertEqual(False, layout.rows[1].grid_items[0].dark_background)
        self.assertEqual(layout.rows[1].grid_items[1].component._component_id, h3._component_id)
        self.assertEqual("1/4", layout.rows[1].grid_items[1].grid_unit)
        self.assertEqual(True,  layout.rows[1].grid_items[1].dark_background)


        # Note there is no way to update Row or GridItems in a
        # supported way and thus they are not tested


    def test_add_component_to_table(self):
        text_block = TextBlock("This text is inside the modal")
        container = Layout()
        container.add_component(text_block)

        df = pandas.DataFrame({'a': [0], 'b': ['this text will be deleted']})
        table = TableBuilder(df, title='title')
        table['b'].rendered_values = [container]
        self._analysis.create_card(table)

        self.assertTrue(hasattr(container, "_component_id"))
        self.assertTrue(hasattr(text_block, "_component_id"))
        self.assertIn(container._component_id, str(table._get_table_data(self._analysis)))
        self.assertIn(text_block._component_id, str(table._get_table_data(self._analysis)))

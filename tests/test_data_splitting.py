import sys
import os
sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import data_splitting, DataSplitting

import copy
import json
import unittest

class TestDataSplitting(unittest.TestCase):
    def test_data_splitting_const_pattern(self):
        """Sanity check that data splitting constants are visible and behave as expected."""

        splitting = data_splitting._in_order_sample
        self.assertEqual(splitting._type, 'extrapolate')
        self.assertIsNone(splitting.shuffle)
        self.assertIsNone(splitting.training_data_percentage)
        self.assertIsNone(splitting.validation_data_percentage)

    def test_construction_from_json_non_custom(self):
        body = json.loads('{"data_splitting": "extrapolate"}')
        splitting = data_splitting._from_json(body)

        self.assertEqual(splitting._type, 'extrapolate')
        self.assertIsNone(splitting.shuffle)
        self.assertIsNone(splitting.training_data_percentage)
        self.assertIsNone(splitting.validation_data_percentage)

    def test_construction_from_json_custom(self):
        body = json.loads('{"data_splitting": "custom", '
                          '"shuffle_rows": true, '
                          '"training_data_percent": 10, '
                          '"validation_data_percent": 20}')
        splitting = data_splitting._from_json(body)

        self.assertEqual(splitting._type, 'custom')
        self.assertEqual(splitting.shuffle, True)
        self.assertEqual(splitting.training_data_percentage, 10)
        self.assertEqual(splitting.validation_data_percentage, 20)


    def test_to_json_non_custom(self):
        body = {"junk_field": "junk_value"}
        data_splitting._in_order_sample._to_json(body)
        text = json.dumps(body, sort_keys=True)
        self.assertEqual(text, '{"data_splitting": "extrapolate", '
                               '"junk_field": "junk_value"}')

    def test_to_json_custom(self):
        body = {"junk_field": "junk_value"}
        DataSplitting(shuffle=True, training_data_percentage=10, validation_data_percentage=20,
                      training_selection_expression='X', validation_selection_expression='Y',
                      training_selection_expression_type='VARIABLE',
                      validation_selection_expression_type='VARIABLE')._to_json(body)
        text = json.dumps(body, sort_keys=True)
        self.assertEqual(text, '{"data_splitting": "custom", '
                               '"junk_field": "junk_value", '
                               '"shuffle_rows": true, '
                               '"training_data_percent": 10, '
                               '"training_selection_expression": "X", '
                               '"training_selection_expression_type": "VARIABLE", '
                               '"validation_data_percent": 20, '
                               '"validation_selection_expression": "Y", '
                               '"validation_selection_expression_type": "VARIABLE"}')

    def test_equal(self):
        one = DataSplitting(shuffle=True, training_data_percentage=10, validation_data_percentage=20)
        two = DataSplitting(shuffle=True, training_data_percentage=10, validation_data_percentage=20)
        self.assertEqual(one, two)

    def test_not_equal(self):
        template = DataSplitting(shuffle=True, training_data_percentage=10, validation_data_percentage=20)

        changed = copy.deepcopy(template)
        changed.shuffle = False
        self.assertNotEqual(changed, template)

        changed = copy.deepcopy(template)
        changed.training_data_percentage = 30
        self.assertNotEqual(changed, template)

        changed = copy.deepcopy(template)
        changed.validation_data_percentage = 30
        self.assertNotEqual(changed, template)

        changed = copy.deepcopy(template)
        changed._type = 'junk'
        self.assertNotEqual(changed, template)
        
    def test_to_string(self):
        data_splitting = DataSplitting(shuffle=True, training_data_percentage=10, validation_data_percentage=20,
                                       training_selection_expression='X', validation_selection_expression='Y',
                                       training_selection_expression_type='VARIABLE',
                                       validation_selection_expression_type='VARIABLE')
        text = data_splitting.__str__()
        self.assertEqual(text, '{"shuffle_rows": true, '
                               '"validation_selection_expression": "Y", '
                               '"validation_selection_expression_type": "VARIABLE", '
                               '"training_selection_expression_type": "VARIABLE", '
                               '"training_data_percent": 10, '
                               '"training_selection_expression": "X", '
                               '"validation_data_percent": 20, '
                               '"data_splitting": "custom"}')

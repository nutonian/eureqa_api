import sys
import os
from types import MethodType

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa.analysis.components import *
from eureqa.analysis.cards import Card
from eureqa.html.button import Button

from etestutils_nosearch import EureqaTestBaseNoSearch

import unittest
import pandas as pd

# This suite contains extended use cases and user stories as discussed with PM/etc.
# They represent a typical user workflow.
# They should represent defined behavior on APIs that exist.
# So the biggest requirement is that the code below parse and execute without crashing.
# "Correct" behavior is more precisely defined in other unit tests.
class TestAnalysisUseCases(unittest.TestCase, EureqaTestBaseNoSearch):
    def test_item_component(self):
        analysis = self._eureqa.create_analysis("Test Analysis")
        other_analysis = self._eureqa.create_analysis("Other Test Analysis")

        # Construct some Components
        # TODO: Constructor should take DataSource, not DataSource ID
        vl = VariableLink(variable_name=self._variables[0], datasource=self._data_source)

        # Component fields are represented by Python Properties with the same name
        assert vl.variable_name == self._variables[0]
        assert vl.datasource == self._data_source

        # Create an item; add it to an Analysis
        analysis.create_card(vl)
        # Note that it is NOT currently legal to add the item to the analysis before
        # adding the default component to the item.

        # Automatically add vl to the parent analysis
        assert vl in analysis.get_components()

        # Change immediately reflected on 'item1'
        vl.variable_name = "Score"

        analysis.create_card(vl)

    def test_html_card_refs(self):
        # Text Card
        ht = HtmlBlock()
        vl = VariableLink(variable_name=self._variables[0], datasource=self._data_source)

        # "analysis.html_ref()" generates an HTML tag for its argument.
        # If necessary, it tells the server that it is the analysis for its argument.
        ht.html = "<div><b>%s</b></div>" % self._analysis.html_ref(vl)

        Card().component = ht

    def test_interactive_components(self):
        self._analysis.create_card(TextBlock("test"))

        # TODO: NOT YET IMPLEMENTED
        return

        # Buttons
        for v in self._data_source.get_variables():
            b = Button("Variable: " + v)

            # Buttons will be a special type of Component
            assert isinstance(b, _Component)

            b.add_replace_action(target=item, new_item=mb.clone())

            # No more "button.to_html()"
            button_display = HtmlBlock()
            button_display.html = "<blink>%s</blink>" % self._analysis.ref(b)

            # Add button to the text card above
            ht.html += self._analysis.html_ref(button_display)

    def test_make_becks_page(self):

        # The following code generates a Layout that looks like:
        #
        # ------------------------------------------------------
        # | "Hybrid Performance"                               |
        # ------------------------------------------------------
        # |                         |                          |
        # | <CustomGraph>           | <Top 10 Variables>       |
        # |                         |                          |
        # ------------------------------------------------------
        # |                                                    |
        # | <Table of links and info about all variables>      |
        # |                                                    |
        # |                                                    |
        # |                                                    |
        # ------------------------------------------------------

        custom_graph = HtmlBlock(html="test 1") # CustomGraph()
        variables_panel = HtmlBlock(html="test 2")

        # Set up the Table
        df = pd.DataFrame({"variable": self._variables})

        # Invent some arbitrary column values
        df["statuses"] = [["Excellent", "Good", "Ok", "Bad"][(i * 4)/len(self._variables)]
                          for i in xrange(len(self._variables)) ]
        df["progress_bar"] = [1. - (float(i) / len(self._variables))
                              for i in xrange(len(self._variables))]

        table = TableBuilder(df, "test table")
        table["variable"].rendered_values = [VariableLink(datasource=self._data_source, variable_name=var)
                                             for var in self._variables]
        table["progress_bar"].rendered_values = [MagnitudeBar(value=0.5)
                                                 for x in df["progress_bar"]]

        # Configure visual Layout
        layout = Layout()
        layout.add_component(HtmlBlock(html="<h1>Hybrid Performance</h1>"))
        layout.add_row()
        layout.add_component(custom_graph, "1/2")
        layout.add_component(variables_panel, "1/2")
        layout.add_row()
        layout.add_component(table)
        self._analysis.create_card(layout)

        # Set up the Custom Graph
        # TODO: Determine the Custom Graph API so that we can use it here
        # For now, just add an HTML block as a placeholder
        custom_graph.html = "TODO: Custom Graph Here"

        # Set up the "Top 10 Variables" panel
        variable_links = [VariableLink(datasource=self._data_source, variable_name=var)
                          for var in self._variables]

        variables_panel.html = "<table>"
        for variable_link in sorted(variable_links)[:10]:
            variables_panel.html += "<tr><td>%s</td></tr>" % self._analysis.html_ref(variable_link)
        variables_panel.html += "</table>"



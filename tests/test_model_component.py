# Copyright (c) 2017, Nutonian Inc
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the Nutonian Inc nor the
#     names of its contributors may be used to endorse or promote products
#     derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL NUTONIAN INC BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa

import unittest

from etestutils_twosearch import EureqaTestBaseTwoSearch, _create_search
from eureqa.analysis.components import Model, _Component

class TestModelComponent(unittest.TestCase, EureqaTestBaseTwoSearch):
    @classmethod
    def setUpClass(cls):
        assert len(cls._variables) >= 4
        cls._x_var = cls._variables[0]
        cls._y_var = cls._variables[1]
        cls._x_var_2 = cls._variables[2]
        cls._y_var_2 = cls._variables[3]

    def test_component(self):
        comp = Model(self._solution)


        # Make sure constructor set all fields
        self.assertEqual(comp.solution._datasource_id, self._solution._datasource_id)
        self.assertEqual(comp.solution._search_id, self._solution._search_id)
        self.assertEqual(comp.solution._id, self._solution._id)

        self._analysis.create_card(comp)

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(comp.solution._datasource_id, self._data_source._data_source_id)
        self.assertEqual(comp.solution._search_id, self._search._id)
        self.assertEqual(comp.solution._id, self._solution._id)

        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # Make sure we can fetch all fields from scratch
        self.assertEqual(comp.solution._datasource_id, self._data_source._data_source_id)
        self.assertEqual(comp.solution._search_id, self._search._id)
        self.assertEqual(comp.solution._id, self._solution._id)

        comp.solution = self._solution_2

        # Make sure all fields were updated
        self.assertEqual(comp.solution._datasource_id, self._data_source_2._data_source_id)
        self.assertEqual(comp.solution._search_id, self._search_2._id)
        self.assertEqual(comp.solution._id, self._solution_2._id)

        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # And that they survive a round trip
        self.assertEqual(comp.solution._datasource_id, self._data_source_2._data_source_id)
        self.assertEqual(comp.solution._search_id, self._search_2._id)
        self.assertEqual(comp.solution._id, self._solution_2._id)

        # Test with analysis passed to constructor
        comp = Model(self._solution,
                     _analysis=self._analysis)

        # Make sure constructor set all fields
        self.assertEqual(comp.solution._datasource_id, self._data_source._data_source_id)
        self.assertEqual(comp.solution._search_id, self._search._id)
        self.assertEqual(comp.solution._id, self._solution._id)
        self.assertTrue(hasattr(comp, "_component_id"))

    def test_component_backward_compatible_constructor(self):
        comp = Model(self._data_source, self._search, self._solution)
        self.assertEqual(comp.solution._datasource_id, self._data_source._datasource_id)
        self.assertEqual(comp.solution._search_id, self._search._id)
        self.assertEqual(comp.solution._id, self._solution._id)

        comp = Model(datasource=self._data_source, search=self._search, solution=self._solution)
        self.assertEqual(comp.solution._datasource_id, self._data_source._datasource_id)
        self.assertEqual(comp.solution._search_id, self._search._id)
        self.assertEqual(comp.solution._id, self._solution._id)

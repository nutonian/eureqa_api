# Copyright (c) 2017, Nutonian Inc
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the Nutonian Inc nor the
#     names of its contributors may be used to endorse or promote products
#     derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL NUTONIAN INC BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa

import unittest

from etestutils_nosearch import EureqaTestBaseNoSearch
from eureqa.analysis.components import DropdownLayout, TextBlock, HtmlBlock, TableBuilder, _Component
from copy import deepcopy
import pandas
import json

class TestDropdownLayoutComponent(unittest.TestCase, EureqaTestBaseNoSearch):

    def tabs_as_json(self, tabs):
        tabs_copy = deepcopy(tabs)
        for tab in tabs_copy:
            tab._associate(self._analysis)
        return [x._to_json() for x in tabs_copy]

    def test_component(self):
        text_tab = TextBlock("Test")
        html_tab = HtmlBlock("Test")

        comp = DropdownLayout(label="Layout", padding="5pt")
        comp.add_component("Text Tab", text_tab)
        comp.add_component("HTML Tab", html_tab)

        # Make sure constructor set all fields
        self.assertEqual(comp.label, "Layout")
        self.assertEqual(comp.padding, "5pt")
        self.assertEqual(len(comp._queued_components), 2)
        self.assertEqual(comp._queued_components[0].title, "Text Tab")
        self.assertEqual(comp._queued_components[0].component, text_tab)
        self.assertEqual(comp._queued_components[1].title, "HTML Tab")
        self.assertEqual(comp._queued_components[1].component, html_tab)

        # Make sure we have the two children that we expect to have
        self.assertEqual(set(comp._walk_children()),
                         {text_tab, html_tab})

        self._analysis.create_card(comp)

        # Make sure we still have the same two children
        self.assertEqual(set(comp._walk_children()),
                         {text_tab, html_tab})

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(len(comp._components), 2)
        self.assertEqual(len(comp._queued_components), 0)
        self.assertEqual(comp.components[0], text_tab)
        self.assertEqual(comp._components[0]['value'], "Text Tab")
        self.assertEqual(comp.components[1], html_tab)
        self.assertEqual(comp._components[1]['value'], "HTML Tab")

        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # Make sure we can fetch all fields from scratch
        self.assertEqual(len(comp.components), 2)
        self.assertEqual(len(comp._queued_components), 0)
        self.assertEqual(comp.components[0], text_tab)
        self.assertEqual(comp._components[0]['value'], "Text Tab")
        self.assertEqual(comp.components[1], html_tab)
        self.assertEqual(comp._components[1]['value'], "HTML Tab")

        # Test with analysis passed to constructor
        comp = DropdownLayout("Layout", "5pt", _analysis=self._analysis)
        comp.add_component("Text Tab", text_tab)
        comp.add_component("HTML Tab", html_tab)

        # Make sure constructor set all fields
        self.assertEqual(len(comp.components), 2)
        self.assertEqual(len(comp._queued_components), 0)
        self.assertEqual(comp.components[0], text_tab)
        self.assertEqual(comp._components[0]['value'], "Text Tab")
        self.assertEqual(comp.components[1], html_tab)
        self.assertEqual(comp._components[1]['value'], "HTML Tab")

    def test_add_component_to_table(self):
        text_block = TextBlock("This text is inside the modal")
        container = DropdownLayout("Layout", "5pt")
        container.add_component("Thing 1", text_block)

        df = pandas.DataFrame({'a': [0], 'b': ['this text will be deleted']})
        table = TableBuilder(df, title='title')
        table['b'].rendered_values = [container]
        self._analysis.create_card(table)

        self.assertTrue(hasattr(container, "_component_id"))
        self.assertTrue(hasattr(text_block, "_component_id"))
        self.assertIn(container._component_id, str(table._get_table_data(self._analysis)))
        self.assertIn(text_block._component_id, str(table._get_table_data(self._analysis)))

    def test_new_component_argument(self):
        text_tab = TextBlock("Test")
        html_tab = HtmlBlock("Test")

        layout = DropdownLayout(label="Layout", padding="5pt")

        with self.assertRaises(Exception):
            layout.add_component("Text Tab")
        with self.assertRaises(Exception):
            layout.add_component("Text Tab", None, None)

        layout.add_component("Text Tab",  text_tab)
        layout.add_component("Text Tab2", content=text_tab)
        layout.add_component("Text Tab3", component=text_tab)
        layout.add_component("Text Tab4", text_tab, None)
        layout.add_component("HTML Tab",  html_tab)
        layout.add_component("HTML Tab2", content=html_tab)
        layout.add_component("HTML Tab3", component=html_tab)
        layout.add_component("HTML Tab4", None, html_tab)

        self.assertEqual(len(layout._queued_components), 8)

        with self.assertRaises(Exception):
            layout.add_component("Text And HTML", text_tab, html_tab)
        with self.assertRaises(Exception):
            layout.add_component("Text And HTML2", content=text_tab, component=html_tab)
        with self.assertRaises(Exception):
            layout.add_component("Text Twice", text_tab, text_tab)
        with self.assertRaises(Exception):
            layout.add_component("Text Twice2", content=text_tab, component=text_tab)


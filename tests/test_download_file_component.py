# Copyright (c) 2017, Nutonian Inc
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the Nutonian Inc nor the
#     names of its contributors may be used to endorse or promote products
#     derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL NUTONIAN INC BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa

import unittest

from etestutils_nosearch import EureqaTestBaseNoSearch
from eureqa.analysis.components import DownloadFile, _Component
from cStringIO import StringIO
import requests

class TestDownloadFileComponent(unittest.TestCase, EureqaTestBaseNoSearch):

    def test_component(self):
        FILE_DATA =   "*** TEST FILE CONTENT ***"
        FILE_DATA_2 = "*** TEST FILE CONTENT 2 ***"

        comp = DownloadFile(FILE_DATA, "File to Download", "file.txt")

        # Make sure constructor set all fields
        self.assertEqual(comp.link_text, "File to Download")
        self.assertEqual(comp.file, None)  # File not yet uploaded

        self._analysis.create_card(comp)

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(comp.link_text, "File to Download")
        self.assertEqual(comp.file.get().read(), FILE_DATA)

        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # Make sure we can fetch all fields from scratch
        self.assertEqual(comp.link_text, "File to Download")
        self.assertEqual(comp.file.get().read(), FILE_DATA)

        comp.link_text = "Link"
        comp.file.update(StringIO(FILE_DATA_2))

        # Make sure all fields were updated
        self.assertEqual(comp.link_text, "Link")
        self.assertEqual(comp.file.get().read(), FILE_DATA_2)

        comp = _Component._get(_component_id=comp._component_id, _analysis=self._analysis)

        # And that they survive a round trip
        self.assertEqual(comp.link_text, "Link")
        self.assertEqual(comp.file.get().read(), FILE_DATA_2)

        # Test with analysis passed to constructor
        comp = DownloadFile(FILE_DATA, "File to Download", "file.txt", _analysis=self._analysis)

        # Make sure constructor set all fields
        self.assertEqual(comp.link_text, "File to Download")
        self.assertEqual(comp.file.get().read(), FILE_DATA)

        # test the download filename is set correctly
        download_resp = requests.get(
                self._analysis._eureqa._session.url + comp._file_url,
                cookies=self._analysis._eureqa._session._session.cookies)
        self.assertEqual('attachment; filename="file.txt"', download_resp.headers['content-disposition'])

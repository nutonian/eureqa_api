import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa
from eureqa.analysis_templates import *

from eureqa import install_analysis_template, run_analysis_template

from etestutils import capture, EureqaTestBase, extendedTest

import unittest
import tempfile
import shutil
import json
import zipfile
import subprocess
import re
import io

from StringIO import StringIO

class TestAnalysisTemplateCLI(unittest.TestCase, EureqaTestBase):
    """
    Tests for packaging and running analysis templates vial the installer
    """
    @classmethod
    def setUpClass(cls):
        ## Make a copy of our example module to play around with
        cls._tempdir = tempfile.mkdtemp()
        shutil.copytree(os.path.abspath(
            os.path.join("..", "analysis_templates", "example_module")),
            os.path.join(cls._tempdir, "example_module"))
        cls._scratch_module = os.path.join(cls._tempdir, "example_module")
        cls._scratch_eureqa_config = os.path.join(cls._scratch_module, "eureqa_config.json")
        cls._orig_eureqa_config = os.path.join("..", "analysis_templates", "example_module", "eureqa_config.json")

        ## Some of the checks here can produce relatively large diffs that we still want to see
        cls.maxDiff = 2048

    @classmethod
    def tearDownClass(cls):
        ## Clean up our scratch work
        shutil.rmtree(cls._tempdir)
        try:
            cls._eureqa._revoke_api_key_by_name('Test Run Module Key')
        except:
            ## there seem to be test configurations where the key wasn't created
            pass

    def setUp(self):
        if os.path.isfile(self._scratch_eureqa_config):
            os.remove(self._scratch_eureqa_config)



    def normalizeOutput(self, script_stdout):
        """
        Apply standard normalization to the specified string (path, and trailing spaces)

        """
        ## Use some regex to strip out content that varies in an uninteresting way
        normalized_script_stdout = script_stdout
        # remove trailing spaces
        normalized_script_stdout = re.sub(r'"module_dir": ".*"', '"module_dir": "<some path>"', normalized_script_stdout)
        normalized_script_stdout = re.sub(r'"api_key": ".*"', '"api_key": "<api key>"', normalized_script_stdout)
        normalized_script_stdout = re.sub(r'"id": "[0-9]+"', r'"id": "<NNN>"', normalized_script_stdout)
        normalized_script_stdout = re.sub(r'\s+$', '', normalized_script_stdout, flags=re.MULTILINE)

        return normalized_script_stdout

    @extendedTest
    def test_run_analysis_template(self):

        params = ParametersValues(
            [TextParameterValue(self._eureqa, 'example_text_param','This is the text input value.'),
             DataSourceParameterValue(self._eureqa, '123', 'SimpleDataSource',
                {VariableParameterValue(self._eureqa, '345', 'value 345')})],
        )
        params_str = json.dumps(params._to_json())
        args = [u"run_analysis_template.py", u'-u', u'root', u'-p', u'Changeme1',
                '--parameter-values', params_str, self._scratch_module, '--url', self._eureqa_url]

        ## Make sure the script's stdout indicates that it correctly
        ## parsed all parameters and ran the example.
        with capture() as out:
            run_analysis_template.main(args)

        expected_stdout = """\
Installing module 'example_module':
{
    "api_key": "<api key>",
    "dependency_dir": [],
    "module_dir": "<some path>",
    "name": "Example Template",
    "organization": "root",
    "parameter_values": [
        {
            "id": "example_text_param",
            "type": "text",
            "value": "This is the text input value."
        },
        {
            "id": "<NNN>",
            "parameters": [
                {
                    "id": "<NNN>",
                    "type": "variable",
                    "value": "value 345"
                }
            ],
            "type": "datasource",
            "value": "SimpleDataSource"
        }
    ],
    "password": "********",
    "quiet": false,
    "save": true,
    "save_credentials": false,
    "url": "%s",
    "username": "root",
    "verify_ssl_certificate": "true"
}
Starting running analysis template.
Creating search.
Starting search
Found 2 models. Stopping search.
Creating cards.
Done!""" % self._eureqa_url

        expected_stderr = ""

        script_stdout = out[0]
        script_stderr = out[1]

        normalized_script_stdout = self.normalizeOutput(str(script_stdout))

        self.assertEqual(expected_stdout.split('\n'), normalized_script_stdout.split('\n'))
        self.assertEqual(expected_stderr.split('\n'), script_stderr.split('\n'))

        ## Check the config file that the script wrote
        expected_eureqa_conf = """\
{
    "api_key": "<api key>",
    "dependency_dir": [],
    "name": "Example Template",
    "organization": "root",
    "parameter_values": [
        {
            "id": "example_text_param",
            "type": "text",
            "value": "This is the text input value."
        },
        {
            "id": "<NNN>",
            "parameters": [
                {
                    "id": "<NNN>",
                    "type": "variable",
                    "value": "value 345"
                }
            ],
            "type": "datasource",
            "value": "SimpleDataSource"
        }
    ],
    "url": "%s",
    "username": "root"
}""" % self._eureqa_url

        with open(self._scratch_eureqa_config) as f:
            ## Python unittest pretty-prints the diff for a list of strings
            ## much more nicely than for a single giant multi-line string
            actual_eureqa_conf = self.normalizeOutput(f.read())
            self.assertEqual(expected_eureqa_conf.split('\n'), actual_eureqa_conf.split('\n'))


    def test_install_analysis_template(self):
        args = [u"install_analysis_template.py", u'-u', u'root', u'-p', u'Changeme1',
                self._scratch_module, '--icon', 'Test image 3.jpg', '--url', self._eureqa_url]

        ## Make sure the script's stdout indicates that it correctly
        ## parsed all parameters, and that it (claimed to) upload the script.
        with capture() as out:
            install_analysis_template.main(args)

        expected_stdout = """\
Installing module 'example_module':
{
    "api_key": "<api key>",
    "dependency_dir": [],
    "description": null,
    "icon": "Test image 3.jpg",
    "id": null,
    "module_dir": "<some path>",
    "name": "Example Template",
    "organization": "root",
    "password": "********",
    "quiet": false,
    "save": true,
    "save_credentials": false,
    "url": "%s",
    "username": "root",
    "verify_ssl_certificate": "true"
}
Successfully uploaded to analysis template 'Example Template' (<NNN>)""" % self._eureqa_url

        expected_stderr = ""

        script_stdout = out[0]
        script_stderr = out[1]

        normalized_script_stdout = self.normalizeOutput(str(script_stdout))
        normalized_script_stdout = re.sub(r"Successfully uploaded to analysis template '(.*)' \([0-9]+\)",
                                          r"Successfully uploaded to analysis template '\1' (<NNN>)",
                                          normalized_script_stdout)

        self.assertEqual(expected_stdout.split('\n'), normalized_script_stdout.split('\n'))
        self.assertEqual(expected_stderr.split('\n'), script_stderr.split('\n'))

        ## Check the config file
        expected_eureqa_conf = """\
{
    "api_key": "<api key>",
    "dependency_dir": [],
    "description": null,
    "icon": "Test image 3.jpg",
    "id": "<NNN>",
    "name": "Example Template",
    "organization": "root",
    "url": "%s",
    "username": "root"
}""" % self._eureqa_url

        with open(self._scratch_eureqa_config) as f:
            ## Python unittest pretty-prints the diff for a list of strings
            ## much more nicely than for a single giant multi-line string
            normalized_eureqa_conf = self.normalizeOutput(f.read())

            self.assertEqual(normalized_eureqa_conf.split('\n'),
                             expected_eureqa_conf.split('\n'))

    def test_install_update_check(self):
        args = [u"install_analysis_template.py", u'-u', u'root', u'-p', u'Changeme1',
                '-q', self._scratch_module, '--url', self._eureqa_url]

        ## Install a template
        with capture() as out:
            install_analysis_template.main(args)

        ## See if we remembered the username and parameters
        ## (Run without specifying them; the rest of this test function
        ## depends on their having been set)
        args = [u"install_analysis_template.py", u'-p', u'Changeme1',
                '-q', '--no-save', self._scratch_module, '--url', self._eureqa_url]
        with capture() as out:
            install_analysis_template.main(args)

        ## Verify that, with '-q', our only output is the ID.
        ## Also, grab the ID for future use.
        id_ = int(out[0])

        # Should be identical to the default example parameters
        template_params = Parameters(
            [TextParameter('example_text_param','Input Text'),
             TopLevelModelParameter('example_model_param','Select a Model', None),
             DataSourceParameter('example_datasource_param', 'Select a DataSource',
                                 {VariableParameter('variable_param_1', 'Variable')})])

        template = self._eureqa._get_analysis_template(id_)
        self.assertEqual(template.name, "Example Template")
        self.assertEqual(template.description, '')
        self.assertEqual(template.parameters._to_json(), template_params._to_json())

        ## Try modifying the template
        with open(os.path.join(self._scratch_module, "__init__.py"), "a") as f:
            f.write("    # This is a new comment.")

        ## See if we remembered the ID (and other params)
        args = [u"install_analysis_template.py", u'-p', u'Changeme1',
                '-q', '--no-save', self._scratch_module, '--url', self._eureqa_url]
        with capture() as out:
            install_analysis_template.main(args)

        ## Is the update for real?
        tempdir = tempfile.mkdtemp()
        try:
            output_filename = os.path.join(tempdir, "output.zip")
            main_module_name = template.get_module(output_filename)
            self.assertEqual('example_module', main_module_name)
            with zipfile.ZipFile(output_filename, 'r') as zf:
                ## Ignore .pyc files; they may or may not be generated
                self.assertEqual(set(x for x in zf.namelist() if not x.endswith(".pyc")),
                                 set([
                                     'manifest/eureqa_main_module_name.txt',
                                     'example_module/__init__.py',
                                     'example_module/README-STRUCTURE.txt',
                                     'example_module/sample.csv'
                                 ]))

                with zf.open("example_module/__init__.py") as f:
                    self.assertIn("# This is a new comment.", f.read())
        finally:
            shutil.rmtree(tempdir)

        ## Try modifying modifiable fields
        ## TODO: Modify '--parameters'; functionally blocks on Assembla #4874
        args = [u"install_analysis_template.py", u'-p', u'Changeme1',
                u'-n', u'Made-Up Template', u'-d', u'Made-Up Description',
                '-q', '--no-save', self._scratch_module,
                '--url', self._eureqa_url]
        with capture() as out:
            install_analysis_template.main(args)

        template = self._eureqa._get_analysis_template(id_)
        self.assertEqual(template.name, "Made-Up Template")
        self.assertEqual(template.description, "Made-Up Description")

    def test_invalid_name(self):
        args = [u"install_analysis_template.py", u'-u', u'root', u'-p', u'Changeme1',
                '-q', '9.not-a-valid-package.9', '--url', self._eureqa_url]
        ## Should complain because '9notavalidpackage9' is not a valid package name
        with self.assertRaises(RuntimeError):
            install_analysis_template.main(args)

    def test_invalid_id(self):
        args = [u"install_analysis_template.py", u'-u', u'root', u'-p', u'Changeme1',
                '-q', '--id', '99999999', self._scratch_module, '--url', self._eureqa_url]

        with capture() as out:
            install_analysis_template.main(args)
        id_ = int(out[0])

        ## Should not have make a template 9999999, but should create a new one
        self.assertNotEqual(id_, 99999999)
        template = self._eureqa._get_analysis_template(id_)

    def test_invalid_id_warning(self):
        args = [u"install_analysis_template.py", u'-u', u'root', u'-p', u'Changeme1',
                '--id', '99999999', self._scratch_module, '--url', self._eureqa_url]

        with capture() as out:
            install_analysis_template.main(args)

        self.assertTrue("ID '99999999' was provided but no analysis template with such ID exists." in out[0])
        self.assertTrue('Therefore ID will be ignored a new analysis template will be created.' in out[0])

    def test_missing_argument(self):
        args = [u"install_analysis_template.py", u'-p', u'Changeme1',
                '-q', self._scratch_module, '--url', self._eureqa_url]
        ## Should complain because no username was specified, and
        ## '-q' says we're not allowed to prompt for one
        with self.assertRaises(RuntimeError):
            install_analysis_template.main(args)

    @extendedTest
    def test_eureqa_config(self):
        with open(self._orig_eureqa_config, "rb") as f:
            json_example = json.load(f)

        json_example["username"] = "root"
        json_example["parameter_values"] = ParametersValues(
            [TextParameterValue(None, 'example_text_param','Text entered by the user'),
             DataSourceParameterValue(None, 'example_datasource_param', 'value of datasource',
                {VariableParameterValue(None, 'variable_param_1', 'value of variable')})])._to_json()['parameters']

        with open(self._scratch_eureqa_config, "wb") as f:
            json.dump(json_example, f)

        ## Run 'install' first because it has the broader set of arguments,
        ## so is more likely to clobber something that 'run' needs
        args = [u"install_analysis_template.py", '-p', "Changeme1", self._scratch_module, '--url', self._eureqa_url]
        with capture() as out:
            install_analysis_template.main(args)

        args = [u"run_analysis_template.py", '-p', "Changeme1", self._scratch_module, '--url', self._eureqa_url]
        with capture() as out:
            run_analysis_template.main(args)

    @extendedTest
    def test_run_module(self):
        """ Explicitly test the "run `__init__.py` mode of operation """
        key = self._eureqa._generate_api_key('Test Run Module Key')['key']

        shutil.copy(self._orig_eureqa_config, self._scratch_eureqa_config)
        proc = subprocess.Popen(["python", os.path.join(self._scratch_module, "__init__.py"), '-k', key], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate()

        self.assertEqual([''], stderr.split("\n"))

        self.assertEqual(["Installing module 'example_module':",
  '{',
  '    "api_key": "<api key>",',
  '    "dependency_dir": [],',
  '    "module_dir": "<some path>",',
  '    "name": "Example Template",',
  '    "organization": "root",',
  '    "parameter_values": [',
  '        {',
  '            "id": "example_text_param",',
  '            "type": "text",',
  '            "value": "Text entered by the user"',
  '        },',
  '        {',
  '            "id": "example_model_param",',
  '            "type": "top_level_model",',
  '            "value": "a + 2"',
  '        },',
  '        {',
  '            "id": "example_datasource_param",',
  '            "parameters": [',
  '                {',
  '                    "id": "variable_param_1",',
  '                    "type": "variable",',
  '                    "value": "value of variable"',
  '                }',
  '            ],',
  '            "type": "datasource",',
  '            "value": "value of datasource"',
  '        }',
  '    ],',
  '    "password": "********",',
  '    "quiet": false,',
  '    "save": true,',
  '    "save_credentials": false,',
  '    "url": "%s",' % self._eureqa_url,
  '    "username": "root",',
  '    "verify_ssl_certificate": "true"',
  '}',
  'Starting running analysis template.',
  'Creating search.',
  'Starting search',
  'Found 2 models. Stopping search.',
  'Creating cards.',
  'Done!'], self.normalizeOutput(stdout).split("\n"))

        with open(self._orig_eureqa_config, "rb") as orig_f:
            with open(self._scratch_eureqa_config, "rb") as scratch_f:
                orig_f_data = orig_f.read().strip()
                scratch_f_data = scratch_f.read().strip()
                self.assertEqual(self.normalizeOutput(orig_f_data).split("\n"), self.normalizeOutput(scratch_f_data).split("\n"))

    @extendedTest
    def test_credential_store(self):
        params = ParametersValues(
            [TextParameterValue(self._eureqa, 'example_text_param','This is the text input value.'),
             DataSourceParameterValue(self._eureqa, '123', 'SimpleDataSource',
                {VariableParameterValue(self._eureqa, '345', 'value 345')})],
        )
        params_str = json.dumps(params._to_json())
        args = [u"run_analysis_template.py", u'-u', u'root', u'-p', u'Changeme1',
                '-q', '--save-credentials', "--parameter-values", params_str, self._scratch_module, '--url', self._eureqa_url]
        # Once to save credentials
        run_analysis_template.main(args)

        args = [u"install_analysis_template.py", u'-u', u'root',
                '-q', self._scratch_module, '--url', self._eureqa_url]
        ## Should not complain -- credentials should be in credential store
        install_analysis_template.main(args)

    def test_empty_parameters_and_dependencies(self):
        tempdir = tempfile.mkdtemp()

        try:
            # Create some dependency modules, to be bundled with the main module
            depsmodule_path = os.path.join(tempdir, "depsmodule")
            os.mkdir(depsmodule_path)
            with open(os.path.join(depsmodule_path, "__init__.py"), "wb") as f:
                f.write("")

            depsmodule2_path = os.path.join(tempdir, "depsmodule2")
            os.mkdir(depsmodule2_path)
            with open(os.path.join(depsmodule2_path, "__init__.py"), "wb") as f:
                f.write("")

            testmodule_path = os.path.join(tempdir, "testmodule")
            os.mkdir(testmodule_path)

            ## Create an __init__.py to make it a module
            with open(os.path.join(testmodule_path, "__init__.py"), "wb") as f:
                f.write("""
import depsmodule
import depsmodule2
def analysis_template_function_body(eureqa, template_execution):
    print('DEF')
def template_parameters():
    from eureqa.analysis_templates.parameters import Parameters
    return Parameters([])
""")

            args = [u"install_analysis_template.py", u'-u', u'root', u'-p', u'Changeme1',
                    '-q', '--no-save', testmodule_path, depsmodule_path, depsmodule2_path, '--url', self._eureqa_url]
            with capture() as out:
                install_analysis_template.main(args)

            id_ = int(out[0])
            template = self._eureqa._get_analysis_template(id_);

            ## Verify that, if we pass in the empty Parameters list,
            ## the server's JSON endpoint feeds us back the empty list.
            self.assertEqual(template._body['parameters'], [])

        finally:
            shutil.rmtree(tempdir)

    def test_non_utf8_output(self):
        tempdir = tempfile.mkdtemp()

        try:
            testmodule_path = os.path.join(tempdir, "testmodule")
            os.mkdir(testmodule_path)

            ## Create an __init__.py to make it a module
            with open(os.path.join(testmodule_path, "__init__.py"), "wb") as f:
                f.write("""
import sys
def analysis_template_function_body(eureqa, template_execution):
    for i in xrange(256):
        sys.stdout.write(chr(i))
        sys.stderr.write(chr(i))
    sys.stdout.write('\\n')
    sys.stderr.write('\\n')
""")

            args = [u"install_analysis_template.py", u'-u', u'root', u'-p', u'Changeme1',
                    '-q', '--no-save', testmodule_path, '--url', self._eureqa_url]
            with capture() as out:
                install_analysis_template.main(args)

            id_ = int(out[0])
            template = self._eureqa._get_analysis_template(id_);
            template.execute(ParametersValues([]))

            # This should not throw.
            template = self._eureqa._get_analysis_template(id_);

        finally:
            shutil.rmtree(tempdir)

    def test_default_options(self):
        args = [u"install_analysis_template.py", u'-u', u'root', u'-p', u'Changeme1',
                '-d', 'Test Description',
                '-q', self._scratch_module, '--url', self._eureqa_url]

        ## Install a template
        with capture() as out:
            install_analysis_template.main(args)

        ## 'description' should be written to eureqa_config.json,
        ## so should be inferred
        args = [u"install_analysis_template.py", u'-p', u'Changeme1',
                '-q', '--no-save', self._scratch_module, '--url', self._eureqa_url]
        with capture() as out:
            install_analysis_template.main(args)

        id_ = int(out[0])

        ## Make sure we used the description from the config file
        template = self._eureqa._get_analysis_template(id_)
        self.assertEqual(template.description, "Test Description")

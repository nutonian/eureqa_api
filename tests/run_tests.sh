#!/bin/bash

# Start with a clean docstore
(cd "$(dirname "$0")"/../../skynet/ && node clear_docstore.js) 

# Install dependencies if needed
pip install requests
pip install mock
pip install numpy
pip install pandas

# By default, bash only marks "cmd1 | cmd2 | cmd3" as failing if cmd3 fails.
# This tells bash to fail if any of the commands fail.
# Important below because the actual test is in the middle of a pipe-chain.
set -o pipefail

# Run tests
#-v parameter makes it to include test names into the output
#-b parameter makes it to buffer standard output from all tests unless they fail.
cd "$(dirname "$0")" && python -u -m unittest discover -v -b 2>&1 | perl -pne 'print scalar(localtime()), " ";'

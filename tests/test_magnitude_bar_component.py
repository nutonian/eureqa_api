import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.

import eureqa
import unittest
from eureqa.analysis.components import *

from etestutils_nosearch import EureqaTestBaseNoSearch

class TestMagnitudeBar(unittest.TestCase, EureqaTestBaseNoSearch):

    def test_component(self):
        analysis = self._analysis

        green_bar = MagnitudeBar(value=0.34)
        pink_bar = MagnitudeBar(value=-0.22, color='#ff00ff')
        analysis.create_card(green_bar)
        analysis.create_card(pink_bar)

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(green_bar.value, 0.34)
        self.assertEqual(green_bar.color, None)
        self.assertEqual(pink_bar.value, -0.22)
        self.assertEqual(pink_bar.color, '#ff00ff')

        # Make sure we can fetch all fields from scratch
        pink_bar = analysis._get_component_by_id(pink_bar._component_id)
        self.assertEqual(pink_bar.value, -0.22)
        self.assertEqual(pink_bar.color, '#ff00ff')

        # test updating fields via setters and ensure they match
        pink_bar.value = 0.33;
        pink_bar.color = '#ffffff'
        self.assertEqual(pink_bar.value, 0.33)
        self.assertEqual(pink_bar.color, '#ffffff')

        # refetch and they should still match
        pink_bar = analysis._get_component_by_id(pink_bar._component_id)
        self.assertEqual(pink_bar.value, 0.33)
        self.assertEqual(pink_bar.color, '#ffffff')

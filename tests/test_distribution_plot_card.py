import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa

import unittest

from etestutils import EureqaTestBase

class TestDistributionPlotCard(unittest.TestCase, EureqaTestBase):
    def test_create(self):
        card = self._analysis.create_distribution_plot_card(self._data_source, self._variables[1],
                                                                           'Test variable card 1', collapse=True)
        self.assertEqual(card.title, 'Test variable card 1')
        self.assertEqual(card.collapse, True)

    def test_update(self):
        card = self._analysis.create_distribution_plot_card(self._data_source, self._variables[1],
                                                                           'Test variable card 2', collapse=True)
        card.title = 'Test variable card 2.5'
        card.collapse = False
        self.assertEqual(card.title, 'Test variable card 2.5')
        self.assertEqual(card.collapse, False)

    def test_get(self):
        self._analysis.create_distribution_plot_card(self._data_source, self._variables[1],
                                                                    'Test variable card 3')
        cards = self._analysis.get_cards()
        cards = [x for x in cards if getattr(x, "title", "") == 'Test variable card 3']
        self.assertEqual(len(cards), 1)
        self.assertEqual(cards[0].title, 'Test variable card 3')

    def test_delete(self):
        card = self._analysis.create_distribution_plot_card(self._data_source, self._variables[1],
                                                                           'Test variable card 4')
        card.delete()
        cards = self._analysis.get_cards()
        cards = [x for x in cards if getattr(x, "title", "") == 'Test variable card 4']
        self.assertEqual(len(cards), 0)

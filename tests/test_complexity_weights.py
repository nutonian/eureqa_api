import sys
import os
sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import complexity_weights
from eureqa import ComplexityWeights

import copy
import json
import unittest


class TestComplexityWeights(unittest.TestCase):
    def test_from_json_(self):
        body = json.loads('{"nest_depth_weight": 1.0, '
                          '"num_boolean_variable_weight": 2.0, '
                          '"num_continuous_variable_weight": 3.0}')
        weights = complexity_weights._from_json(body)

        self.assertEqual(weights.nest_depth, 1.0)
        self.assertEqual(weights.boolean_variable, 2.0)
        self.assertEqual(weights.continuous_variable, 3.0)

    def test_to_json(self):
        body = ComplexityWeights(nest_depth=1.0, boolean_variable=2.0, continuous_variable=3.0)._to_json()
        text = json.dumps(body, sort_keys=True)
        self.assertEqual(text, '{"nest_depth_weight": 1.0, '
                               '"num_boolean_variable_weight": 2.0, '
                               '"num_continuous_variable_weight": 3.0}')

    def test_equal(self):
        one = ComplexityWeights(nest_depth=1.0, boolean_variable=2.0, continuous_variable=3.0)
        two = ComplexityWeights(nest_depth=1.0, boolean_variable=2.0, continuous_variable=3.0)
        self.assertEqual(one, two)

    def test_not_equal(self):
        template = ComplexityWeights(nest_depth=1.0, boolean_variable=2.0, continuous_variable=3.0)

        changed = copy.deepcopy(template)
        changed.nest_depth = 4.0
        self.assertNotEqual(changed, template)

        changed = copy.deepcopy(template)
        changed.boolean_variable = 4.0
        self.assertNotEqual(changed, template)

        changed = copy.deepcopy(template)
        changed.continuous_variable = 4.0
        self.assertNotEqual(changed, template)

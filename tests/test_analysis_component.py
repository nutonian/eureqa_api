
import sys
import os
from types import MethodType

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa.analysis.components import *

from etestutils_nosearch import EureqaTestBaseNoSearch

import unittest

class TestAnalysisComponent(unittest.TestCase, EureqaTestBaseNoSearch):
    def test_walk(self):
        layout = Layout()

        ddl = DropdownLayout()

        text = TextBlock("Text")
        ddl.add_component("Text Component", text)

        brp = ByRowPlot()
        ddl.add_component("By Row Plot Component", brp)

        html = HtmlBlock("HTML")
        modal = Modal(component=html)

        dp = DistributionPlot()
        ml = ModalLink(modal=dp)

        layout.add_component(ddl)
        layout.add_row()
        layout.add_component(modal, "1/2")
        layout.add_component(ml, "1/2")

        tl = TitledLayout(content=layout)

        expected_order = [tl, layout, ddl, text, brp, modal, html, ml, dp]

        # Make sure that all expected components are present
        self.assertEqual(
            set(tl._walk()),
            set(expected_order)
        )

        # Make sure components are returned in the correct order
        self.assertEqual(
            list(tl._walk()),
            expected_order
        )

    def test_save(self):
        layout = Layout()

        ddl = DropdownLayout()

        text = TextBlock("Text")
        ddl.add_component("Text Component", text)

        brp = TextBlock("Not a By-Row Plot")
        ddl.add_component("By Row Plot Component", brp)

        html = HtmlBlock("HTML")
        modal = Modal(component=html)

        dp = TextBlock("Not a Distribution Plot")
        ml = ModalLink(modal=dp)

        layout.add_component(ddl)
        layout.add_row()
        layout.add_component(modal, "1/2")
        layout.add_component(ml, "1/2")

        tl = TitledLayout(content=layout)

        # After batch-associating, '_associate()' should not make additional HTTP requests
        layout._associate(self._analysis)

        # Break our ability to make HTTP requests, then try '_associate()'
        tmp_session = self._analysis._eureqa._session
        self._analysis._eureqa._session = None
        layout._associate(self._analysis)
        self._analysis._eureqa._session = tmp_session

    def test_basic_save_restore(self):
        # Pick a type arbitrarily
        p = TextBlock(_analysis=self._analysis, text="Test Text")
        self.assertEqual(p._analysis, self._analysis)
        self.assertIsNotNone(p._component_id)

    def test_clone(self):
        c = TextBlock(_analysis=self._analysis, text="Test Text")
        c2 = c.clone()
        self.assertFalse(hasattr(c2, "_analysis_id"))
        self.assertFalse(hasattr(c2, "_analysis"))
        self.assertFalse(hasattr(c2, "_component_id"))
        self.assertEqual(c2.text, "Test Text")
        self.assertEqual(c2._component_type, "ANALYSIS_TEXT_BLOCK")

    def test_registrar(self):
        # Pick a type arbitrarily
        e = ModelEvaluator()
        e_json = e._to_json()
        c = _Component._construct_from_json(e_json)
        self.assertIsInstance(c, ModelEvaluator)

    def test_dropdown_layout(self):
        # create a container that will contain tabs
        layout = DropdownLayout(label="DropDown", padding=10)

        # Add two options
        layout.add_component(title="Option 1", content = HtmlBlock("Dropdown option 1"))
        layout.add_component(title="Option 2", content = HtmlBlock("Dropdown option 2"))

        # put the container in the actual analysis
        self._analysis.create_card(layout)

    def test_validate_known_components(self):
        # When adding a new card, add it to this list as well.
        # Test makes sure that Component's metaclass is correctly registering new Components
        self.assertEqual({
            'ANALYSIS_HTML_BLOCK',
            'ANALYSIS_TEXT_BLOCK',
            'BINNED_MEAN_PLOT',
            'BOX_PLOT',
            'CENTIPEDE_CARD',
            'CUSTOM_GRAPH',
            'DOUBLE_HISTOGRAM_PLOT',
            'DOWNLOAD_FILE',
            'DROPDOWN_LAYOUT',
            'EVALUATE_MODEL',
            'GENERIC_COMPONENT',
            'IMAGE',
            'INTERACTIVE_EXPLAINER',
            'GENERIC_LAYOUT',
            'MAGNITUDE_BAR',
            'MODAL',
            'MODAL_LINK',
            'MODEL_SUMMARY',
            'PROJECTION_CARD',
            'SCATTER_PLOT',
            'TABBED_LAYOUT',
            'TABLE',
            'TITLED_LAYOUT',
            'VARIABLE_DISTRIBUTION_PLOT',
            'VARIABLE_LINE_GRAPH',
            'VARIABLE_LINE_GRAPH',
            'VARIABLE_LINK',
            'SEARCH_LINK',
            'SEARCH_BUILDER_LINK',
            'VARIABLE_LINK',
            'TOOLTIP',
            'TwoVariablePlot helper mixin (not a complete type)',
            'VARIABLE_HISTOGRAM',
            'CLASS_FREQUENCY',
            'TERMS_IN_TIME'
        }, set(_Component._registered_types.keys()))

        # Make sure we don't have any classes registered twice
        # (implies that we probably overwrote a registration)
        self.assertEqual(len(_Component._registered_types),
                         len(set(_Component._registered_types.items())))


    def test_css_class(self):
        component = TextBlock(_analysis=self._analysis, text="Test Text")
        component._css_class = "foo bar baz"
        component_json = component._to_json()

        self.assertEqual(component_json["class_names"], "foo bar baz")

    def test_css_style(self):
        component = TextBlock(_analysis=self._analysis, text="Test Text")
        component._css_style = "background: purple;"
        component_json = component._to_json()

        self.assertEqual(component_json["style_attribute"], "background: purple;")

# Try constructing all Component types.  Make sure they are basically functional.
def addPerTypeAnalysisComponentTests():
    for name, cls in _Component._registered_types.iteritems():
        def test_fn(self):
            c = cls()
        setattr(TestAnalysisComponent, "test_basic_%s" % name, test_fn)
addPerTypeAnalysisComponentTests()

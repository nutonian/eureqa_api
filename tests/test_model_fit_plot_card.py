import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
import eureqa

import unittest

from etestutils import EureqaTestBase

class TestModelFitPlotCard(unittest.TestCase, EureqaTestBase):
    @classmethod
    def setUpClass(cls):
        assert len(cls._variables) >= 4
        cls._x_var = cls._variables[0]
        cls._y_var = cls._variables[1]

        ## Clean up after other test runs if needed
        for x in cls._analysis.get_cards():
            if getattr(x, 'title', None) == 'Test Updated Card':
                x.delete()

    def test_create_model_fit_plot_card(self):
        card = self._analysis.create_model_fit_plot_card(self._solution,
                                                         title="Test Card",
                                                         description="Test Card Description",
                                                         collapse=False)

        self.assertEqual(card.title, "Test Card")
        self.assertEqual(card.description, "Test Card Description")
        self.assertEqual(card.collapse, False)

    def test_create_model_fit_all_cards(self):
        card_ctors = [
            self._analysis.create_model_fit_by_row_plot_card,
            self._analysis.create_model_fit_separation_plot_card
            ]

        cards = [x(self._solution,
                   "Test Plot for %s" % repr(x.im_func.func_name),
                   "Test Card Description",
                   False)
                 for x in card_ctors]

        for card, fn in zip(cards, card_ctors):
            self.assertEqual(card.title, "Test Plot for %s" % repr(fn.im_func.func_name))
            self.assertEqual(card.description, "Test Card Description")
            self.assertEqual(card.collapse, False)
            
    def test_all_templates_have_model_fit_plots(self):
        templates = self._eureqa._list_search_templates()
        for template in templates:
            self._analysis._get_card_type_from_search_type(template)
            self._analysis._get_card_typename_from_search_type(template)
            
    def test_create_model_fit_all_search_types(self):
        variables = self._data_source.get_variables()
        target = variables[0]
        inputs = variables[1:]
        
        generic_settings = self._eureqa.search_templates.numeric('Test_2', target_variable=target,
                                                                 input_variables=inputs)
        generic_search = self._data_source.create_search(generic_settings)
        generic_solution = generic_search.create_solution("0")
        generic_card = self._analysis.create_model_fit_plot_card(generic_solution,
                                                                 target,
                                                                 "Test Card",
                                                                 "Test Card Description")
        self.assertEqual("PROJECTION_CARD", generic_card.component.content._component_type)
                                                                  
        time_series_settings = self._eureqa.search_templates.time_series('Test_2', target_variable=target,
                                                                         input_variables=inputs,
                                                                         max_delays_per_variable=1)
        time_series_search = self._data_source.create_search(time_series_settings)
        time_series_solution = time_series_search.create_solution("0")
        time_series_card = self._analysis.create_model_fit_plot_card(time_series_solution,
                                                                     target,
                                                                     "Test Card",
                                                                     "Test Card Description")
        self.assertEqual("PROJECTION_CARD", time_series_card.component.content._component_type)
                                                                  
        classification_settings = self._eureqa.search_templates.classification('Test_2', target_variable=target,
                                                                 input_variables=inputs)
        classification_search = self._data_source.create_search(classification_settings)
        classification_solution = classification_search.create_solution("0")
        classification_card = self._analysis.create_model_fit_plot_card(classification_solution,
                                                                        target,
                                                                        "Test Card",
                                                                        "Test Card Description")
        self.assertEqual("CENTIPEDE_CARD", classification_card.component.content._component_type)

        time_series_classification_settings = self._eureqa.search_templates.time_series_classification('Test_2', target_variable=target,
                                                                                                       input_variables=inputs,
                                                                                                       max_delays_per_variable=1)
        time_series_classification_search = self._data_source.create_search(time_series_classification_settings)
        time_series_classification_solution = time_series_classification_search.create_solution("0")
        time_series_classification_card = self._analysis.create_model_fit_plot_card(time_series_classification_solution,
                                                                                    target,
                                                                                    "Test Card",
                                                                                    "Test Card Description")
        self.assertEqual("CENTIPEDE_CARD", time_series_classification_card.component.content._component_type)

    def test_update_get(self):
        card = self._analysis.create_model_fit_plot_card(self._solution,
                                                         "Test Card",
                                                         "Test Card Description",
                                                         self._x_var)

        card.title = "Test Updated Card"
        card.description = "Test Updated Description"
        card.collapse = True

        self.assertEqual(card.title, "Test Updated Card")
        self.assertEqual(card.description, "Test Updated Description")
        self.assertEqual(card.collapse, True)

        fetched_cards = [x for x in self._analysis.get_cards() if getattr(x, 'title', None) == 'Test Updated Card']
        self.assertEqual(len(fetched_cards), 1)
        fetched_card = fetched_cards[0]

        self.assertEqual(card.title, fetched_card.title)
        self.assertEqual(card.description, fetched_card.description)
        self.assertEqual(card.collapse, fetched_card.collapse)

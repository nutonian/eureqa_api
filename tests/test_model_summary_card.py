import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.
from eureqa import Eureqa, search_settings

import unittest

from etestutils import EureqaTestBase

class TestModelSummaryCard(unittest.TestCase, EureqaTestBase):
    def test_create(self):
        card = self._analysis.create_model_summary_card(self._solution, False)
        self.assertIsNotNone(card._item_id)
        self.assertEqual(card.collapse, False)

    def test_get(self):
        card = self._analysis.create_model_summary_card(self._solution)
        cards = self._analysis.get_cards()
        cards = [x for x in cards if x._item_id == card._item_id]
        self.assertEqual(len(cards), 1)
        self.assertEqual(cards[0]._item_id, card._item_id)

    def test_delete(self):
        card = self._analysis.create_model_card(self._solution)
        card.delete()
        cards = self._analysis.get_cards()
        cards = [x for x in cards if x._item_id == card._item_id]
        self.assertEqual(len(cards), 0)

import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.

import eureqa
import unittest
from eureqa.analysis.components import *

from etestutils_nosearch import EureqaTestBaseNoSearch

class TestToolTipComponent(unittest.TestCase, EureqaTestBaseNoSearch):

    def test_component(self):
        analysis = self._analysis
        tt = Tooltip(html="This", tooltip="<strong>Hello</strong>, is it me you're looking for?")

        card = analysis.create_html_card("This is a component with a tooltip: {0}".format(analysis.html_ref(tt)))

        # Make sure all fields are still set after round-trip from server
        self.assertEqual(tt.html, "This")
        self.assertEqual(tt.tooltip, "<strong>Hello</strong>, is it me you're looking for?")

        # Make sure we can fetch all fields from scratch
        tt = analysis._get_component_by_id(tt._component_id)
        self.assertEqual(tt.html, "This")
        self.assertEqual(tt.tooltip, "<strong>Hello</strong>, is it me you're looking for?")

        # test updating fields via setters and ensure they match
        tt.html = "Second"
        tt.tooltip = "New Tooltip"

        self.assertEqual(tt.html, "Second")
        self.assertEqual(tt.tooltip, "New Tooltip")

        # refetch
        tt = analysis._get_component_by_id(tt._component_id)
        self.assertEqual(tt.html, "Second")
        self.assertEqual(tt.tooltip, "New Tooltip")

import io
import sys
import os

sys.path.insert(1, os.path.abspath('..'))  # Need this to be able to reference eureqa package.

from eureqa import Eureqa, search_settings, DataSource
from eureqa.utils.utils import remove_files_in_directory

import time
import unittest
from cStringIO import StringIO

from etestutils import EureqaTestBase

class TestDataSource(unittest.TestCase, EureqaTestBase):
    @classmethod
    def setUpClass(cls):
        remove_files_in_directory('temp') # clear any old test files before each test

    @classmethod
    def tearDownClass(cls):
        remove_files_in_directory('temp') # clear any old test files created by tests

    def test_create_data_source_from_csv(self):
        data_source = self._eureqa.create_data_source('data_source_1', 'Nutrition.csv')
        self.assertEqual(data_source.name, 'data_source_1')

    def test_create_data_source_from_csv_hidden(self):
        data_source = self._eureqa.create_data_source('data_source_1_hidden', 'Nutrition.csv', _hidden = True)

        self.assertEqual(data_source.name, 'data_source_1_hidden')
        self.assertEqual(data_source._hidden, True)

    def test_create_the_same_data_source_name(self):
        first_data_source = self._eureqa.create_data_source('data_source_2', 'Nutrition.csv')
        second_data_source = self._eureqa.create_data_source('data_source_2', 'Nutrition.csv')
        self.assertEqual(first_data_source.name, second_data_source.name)
        self.assertNotEqual(first_data_source._data_source_id, second_data_source._data_source_id)

    def test_create_the_same_data_source_name_with_get(self):
        first_data_source = self._eureqa.create_data_source('data_source_3', 'Nutrition.csv')
        self.assertIsNotNone(self._eureqa.get_data_source_by_id(first_data_source._data_source_id))
        self.assertIsNotNone(self._eureqa.get_data_source('data_source_3'))
        second_data_source = self._eureqa.create_data_source('data_source_3', 'Nutrition.csv')
        self.assertIsNotNone(self._eureqa.get_data_source_by_id(first_data_source._data_source_id))
        self.assertIsNotNone(self._eureqa.get_data_source_by_id(second_data_source._data_source_id))
        with self.assertRaises(Exception):
            self._eureqa.get_data_source('data_source_3')

    def test_create_with_metadata(self):
        data_source_no_metadata = self._eureqa.create_data_source('create_data_source_no_metadata', 'Nutrition.csv')
        self.assertEqual(data_source_no_metadata.series_id_column_name, None)
        self.assertEqual(data_source_no_metadata.series_order_column_name, None)
        self.assertEqual(data_source_no_metadata.series_order_variable_name, None)

        data_source_blank_metadata = self._eureqa.create_data_source('create_data_source_no_metadata', 'Nutrition.csv', series_id_column_name = "", series_order_column_name = "")
        self.assertEqual(data_source_blank_metadata.series_id_column_name, None)
        self.assertEqual(data_source_blank_metadata.series_order_column_name, None)
        self.assertEqual(data_source_blank_metadata.series_order_variable_name, None)

        data_source_series_id_only = self._eureqa.create_data_source('create_data_source_series_id_only_test', 'Nutrition.csv', series_id_column_name = 'Calories')
        self.assertEqual(data_source_series_id_only.series_id_column_name, 'Calories')
        self.assertEqual(data_source_series_id_only.series_order_column_name, None)
        self.assertEqual(data_source_series_id_only.series_order_variable_name, None)

        data_source_timeseries_default = self._eureqa.create_data_source('create_data_source_timeseries_default_test', 'Nutrition.csv', series_order_column_name = DataSource.EXISTING_ROW_ORDER)
        self.assertEqual(data_source_timeseries_default.series_id_column_name, None)
        self.assertEqual(data_source_timeseries_default.series_order_column_name, DataSource.EXISTING_ROW_ORDER)
        self.assertEqual(data_source_timeseries_default.series_order_variable_name, DataSource.EXISTING_ROW_ORDER)

        data_source_timeseries_order_col = self._eureqa.create_data_source('create_data_source_timeseries_order_col_test', 'Nutrition.csv', series_order_column_name = 'Date')
        self.assertEqual(data_source_timeseries_order_col.series_id_column_name, None)
        self.assertEqual(data_source_timeseries_order_col.series_order_column_name, 'Date')
        self.assertEqual(data_source_timeseries_order_col.series_order_variable_name, 'Date')

        data_source_both = self._eureqa.create_data_source('create_data_source_both_test', 'Nutrition.csv', series_order_column_name = 'Date', series_id_column_name = 'Calories')
        self.assertEqual(data_source_both.series_order_column_name, 'Date')
        self.assertEqual(data_source_both.series_order_variable_name, 'Date')
        self.assertEqual(data_source_both.series_id_column_name, 'Calories')

        with self.assertRaises(Exception):
            data_source_bad_id_col = self._eureqa.create_data_source('create_data_source_bad_id_col_test', 'Nutrition.csv', series_id_column_name = 'not_a_col')
        with self.assertRaises(Exception):
            data_source_bad_order_col = self._eureqa.create_data_source('create_data_source_bad_order_col_test', 'Nutrition.csv', series_order_column_name = 'not_a_col')

    def create_test_datasource(self, contents):
        """Creates a test dataset from contents.
        ::param contents str the raw csv data
"""
        return self._eureqa.create_data_source(name='data', file_or_path=io.BytesIO(contents));

    def test_datasource_with_unicode_varnames(self):
        data_source = self.create_test_datasource("""\
x,(\xef\xbb\xbfVar),z
1,2,3
4,5,6
7,8,9
""")
        vars = data_source.get_variables()
        self.assertEqual(vars[0], "x")
        self.assertEqual(vars[1], "(\xef\xbb\xbfVar)".decode('utf-8'))
        self.assertEqual(vars[2], "z")

    def test_get_series_id_values(self):
        data_source = self.create_test_datasource("""\
id,A
X,1
X,2
X,3
X,4
X,5

Y,1
Y,2
Y,3
Y,4
Y,5
""")
        self.assertEqual(['Series 1', 'Series 2'], data_source.get_series_id_values())

        data_source.series_id_column_name = 'id'
        self.assertEqual(['X', 'Y'], data_source.get_series_id_values())

    def test_get_series_id_variable(self):
        file_content = """id,A
X,1
X,2
X,3
X,4
X,5

Y,1
Y,2
Y,3
Y,4
Y,5
"""
        data_source = self._eureqa.create_data_source(
                "test", StringIO(file_content), series_id_column_name = 'id')
        self.assertEqual(['A'], data_source.get_variables())

        data_source = self._eureqa.create_data_source(
                "test", StringIO(file_content), series_id_column_name = 'id', _create_series_id_variable = True)
        self.assertEqual(['id_X', 'id_Y', 'A'], data_source.get_variables())

    def test_create_non_existing_file(self):
        try:
            self._eureqa.create_data_source('data_source_4', 'jumbo.csv')
            self.fail('Should throw exception for non-existing file.')
        except:
            pass
        with self.assertRaisesRegexp(Exception, "Unknown datasource_name: 'data_source_4'"):
            self._eureqa.get_data_source('data_source_4')

    def test_get(self):
        with self.assertRaisesRegexp(Exception, "Unknown datasource_name: 'data_source_5'"):
            self._eureqa.get_data_source('data_source_5')
        data_source = self._eureqa.create_data_source('data_source_5', 'Nutrition.csv')
        self.assertIsNotNone(self._eureqa.get_data_source(data_source._data_source_id))

    def test_delete(self):
        data_source = self._eureqa.create_data_source('data_source_6', 'Nutrition.csv')
        data_source.delete()
        with self.assertRaisesRegexp(Exception, "Unknown datasource_name: 'data_source_6'"):
            self._eureqa.get_data_source(data_source._data_source_id)
        with self.assertRaises(Exception):
            data_source.delete()

    def test_properties(self):
        time_before_upload = time.time()

        data_source = self._eureqa.create_data_source('data_source_7', 'Nutrition.csv')

        self.assertEqual(data_source.name, 'data_source_7')
        self.assertEqual(data_source.number_variables, 13)
        self.assertEqual(data_source.number_rows, 132)
        self.assertEqual(data_source.number_series, 1)
        self.assertEqual(data_source._data_file_name, 'Nutrition.csv')
        # There is a discrepincy between file sizes in windows and unix and the point of the test isn't
        # to validate what the csv parser is doing so just check that the file size is greater than 10000 bytes
        self.assertGreater(data_source._data_file_size, 10000)
        self.assertEqual(data_source._data_file_uploaded_user, 'root')
        # Checking that the upload time is within 5 minutes from the test start
        # seems like a good enough idea to test this field.
        #
        # Need to multiply the Python time by 1000 because it is in seconds
        # but our time is in milliseconds
        #
        # Skip if running against a remote server because our clocks may not be in sync.
        if not 'EUREQAURL' in os.environ:
            self.assertGreater(data_source._data_file_uploaded_date, time_before_upload * 1000)
            self.assertLess(data_source._data_file_uploaded_date, (time_before_upload + 5 * 60) * 1000)

    def test_properties_multi_series(self):
        data_source = self._eureqa.create_data_source('multi_series_data_source', 'simple_multi_series_data.csv')
        self.assertEqual(data_source.name, 'multi_series_data_source')
        self.assertEqual(data_source.number_variables, 4)
        self.assertEqual(data_source.number_rows, 8)
        self.assertEqual(data_source.number_series, 2)


    def test_download_data_file(self):
        data_source = self._eureqa.create_data_source('data_source_7', 'Nutrition.csv')
        data_source.download_data_file('temp/Nutrition.csv')

        l1 = l2 = ' '
        with open("Nutrition.csv", 'rb') as f1, open("temp/Nutrition.csv", 'rb') as f2:
            while l1 != '' and l2 != '':
                l1 = f1.readline()
                l2 = f2.readline()
                if l1 != l2:
                    self.fail('Files are not equivalent: ' + repr(l1) + ', ' + repr(l2))

    def test_searches(self):
        data_source = self._eureqa.create_data_source('data_source_9', 'Nutrition.csv')
        variables = data_source.get_variables()
        settings = self._eureqa.search_templates.numeric('Test_1', target_variable=variables[0],
                                                                 input_variables=variables[1:])
        search = data_source.create_search(settings)
        self.assertEqual(len(data_source.get_searches()), 1)
        self.assertEqual(data_source.get_searches()[0]._id, search._id)

    def test_evaluate_expression(self):
        data_source = self._eureqa.create_data_source('data_source_10', 'Nutrition.csv')
        variables = data_source.get_variables()
        expressions = variables[:]
        ## append a variable a second time to ensure we handle duplicate expressions
        expressions.append(variables[0])
        values = data_source.evaluate_expression(expressions)
        num_lines = open('Nutrition.csv').read().count('\n')
        self.assertEqual(sorted(values.keys()), sorted(variables))
        for var in variables:
            self.assertEqual(len(values[var]), num_lines - 1)

        ## Check null-value policy
        ## Should be a sequence of nulls in the middle of the 'Weight' column
        for i in xrange(67):
            self.assertNotEqual(values['Weight'][i], None)
        for i in xrange(67, 74):
            self.assertEqual(values['Weight'][i], None)
        for i in xrange(74, len(values['Weight'])):
            self.assertNotEqual(values['Weight'][i], None)

    def test_evaluate_expression_multi_series(self):
        data_source = self._eureqa.create_data_source('multi_series_data_source', 'simple_multi_series_data.csv')
        variables = data_source.get_variables()
        expressions = variables[:]
        ## append a variable a second time to ensure we handle duplicate expressions
        expressions.append(variables[0])
        values = data_source.evaluate_expression(expressions)
        self.assertEqual(sorted(values.keys()), sorted(variables))
        for var in variables:
            self.assertEqual(len(values[var]), data_source.number_rows)


    def test_to_string(self):
        data_source = self._eureqa.create_data_source('data_source_11', 'Nutrition.csv')
        data_source.__str__()

    def test_create_variable_functions(self):
        data_source = self._eureqa.create_data_source('data_source_12', 'Nutrition.csv')
        original_num_cols = data_source.number_variables
        original_variables = data_source.get_variables()

        custom_var_expr = '0'
        seasonal_var_expr = '-0.080255*sin(0.897598*Date) + 0.147658*cos(0.897598*Date) + ' +\
                            '215.959638'


        # test create_variable (create custom variable)
        data_source.create_variable(custom_var_expr, 'new_var')
        self.assertEqual(data_source.number_variables, original_num_cols + 1)
        custom_var_name = 'new_var'
        # check the results of get_variables
        self.assertItemsEqual(data_source.get_variables('original'), original_variables)
        self.assertItemsEqual(data_source.get_variables('custom'), [custom_var_name])
        self.assertItemsEqual(data_source.get_variables('all'), original_variables + [custom_var_name])
        self.assertItemsEqual(data_source.get_variables('all'), data_source.get_variables())

        # test create_seasonality_variable
        data_source.series_order_column_name = 'Date'
        seasonal_var_name = data_source.create_seasonality_variable(seasonal_target_variable = 'Weight', seasonal_period = 'weekly')
        # check the results of get_variables
        self.assertItemsEqual(data_source.get_variables('original'), original_variables)
        self.assertItemsEqual(data_source.get_variables('custom'), [custom_var_name])
        self.assertItemsEqual(data_source.get_variables('seasonal'), [seasonal_var_name])
        self.assertItemsEqual(data_source.get_variables('all'), original_variables + [custom_var_name, seasonal_var_name])
        self.assertItemsEqual(data_source.get_variables('all'), data_source.get_variables())

        # check the results of get_variable_details
        original_var_detail = data_source.get_variable_details('Weight')
        self.assertEqual('original', original_var_detail.derivation_type)
        self.assertEqual(None, original_var_detail.expression)

        custom_var_detail = data_source.get_variable_details(custom_var_name)
        self.assertEqual('custom', custom_var_detail.derivation_type)
        self.assertEqual(custom_var_expr, custom_var_detail.expression)

        seasonal_var_detail = data_source.get_variable_details(seasonal_var_name)
        self.assertEqual('seasonal', seasonal_var_detail.derivation_type)
        self.assertEqual(seasonal_var_expr, seasonal_var_detail.expression)
        self.assertEqual('Weight', seasonal_var_detail.seasonal_target_variable)
        self.assertEqual('weekly', seasonal_var_detail.seasonal_period)

        # test create_variable_from_template
        data_source2 = self._eureqa.create_data_source('data_source_12_copy', 'Nutrition.csv')
        data_source2.series_order_column_name = 'Date'

        with self.assertRaisesRegexp(Exception, "Variable 'Weight' is a variable from the original spreadsheet of dataset"):
            data_source2.create_variable_from_template(original_var_detail)

        data_source2.create_variable_from_template(custom_var_detail)
        custom_var_detail2 = data_source2.get_variable_details(custom_var_name)
        self.assertEqual('custom', custom_var_detail2.derivation_type)
        self.assertEqual(custom_var_expr, custom_var_detail2.expression)

        # the created variable is also a seasonal variable
        data_source2.create_variable_from_template(seasonal_var_detail)
        seasonal_var_detail2 = data_source2.get_variable_details(seasonal_var_name)

        self.assertEqual('seasonal', seasonal_var_detail2.derivation_type)
        self.assertEqual(seasonal_var_expr, seasonal_var_detail2.expression)

    def test_get_variables(self):
        data_source = self._eureqa.create_data_source('data_source_13', 'Nutrition.csv')
        self.assertEqual(13, len(data_source.get_variables()))
        variable = data_source.get_variable('Weight')
        self.assertEqual('Weight', variable.name)
        self.assertEqual(None, variable.expression)
        self.assertEqual(203.4, variable.min_value)
        self.assertEqual(222.0, variable.max_value)
        self.assertAlmostEqual(215.9584, variable.mean_value)
        self.assertEqual(4.910970315528267, variable.standard_deviation)
        self.assertEqual(7, variable.missing_values)
        self.assertEqual(60, variable.distinct_values)
        self.assertEqual(0, variable.num_zeroes)
        self.assertEqual(132, variable.total_rows)
        self.assertEqual('numeric', variable.datatype)
        self.assertEqual(0, variable.num_ones)
        data_source.create_variable('Weight + 2', 'new_var')
        new_variable = data_source.get_variable('new_var')
        self.assertEqual('Weight + 2', new_variable.expression)

    def test_all(self):
        for data_source in self._eureqa.get_all_data_sources():
            if data_source.name != "SimpleDataSource":
                data_source.delete()
        expected_data_source = self._eureqa.create_data_source('data_source_14', 'Nutrition.csv')
        data_sources = self._eureqa.get_all_data_sources()
        self.assertIn(expected_data_source._data_source_id, [x._data_source_id for x in data_sources])
        next_expected_datasource = self._eureqa.create_data_source('data_source_15', 'Nutrition.csv')
        data_sources = self._eureqa.get_all_data_sources()
        self.assertIn(next_expected_datasource._data_source_id, [x._data_source_id for x in data_sources])

    def test_null_current_dataset(self):
        # Should not throw
        DataSource(self._eureqa, {
            'datasource_id': 'something',
            'datasource_name': 'Something',
            'current_dataset': None  ## <- This is what we're testing
            })

    def test_rename_data_source(self):
        old_name = 'data_source_old_name'
        new_name = 'data_source_new_name'

        for data_source in self._eureqa.get_all_data_sources():
            if data_source.name == old_name or data_source.name == new_name:
                data_source.delete()

        source = self._eureqa.create_data_source(old_name, 'Nutrition.csv')
        original_id = source._data_source_id

        source.name = new_name

        self.assertNotEqual(source.name, old_name)
        self.assertEqual(source.name, new_name)
        self.assertEqual(source._data_source_id, original_id)

        #Check the changes reached the server
        with self.assertRaises(Exception):
            updated_source = self._eureqa.get_data_source(old_name)
        updated_source = self._eureqa.get_data_source(new_name)
        self.assertEqual(updated_source.name, new_name)
        self.assertEqual(updated_source._data_source_id, original_id)

        #Test that name can't be blank
        with self.assertRaises(Exception):
            source.name = None
        with self.assertRaises(Exception):
            source.name = ''

    def test_change_metadata(self):
        data_source = self._eureqa.create_data_source('data_source_change_metadata_test', 'Nutrition.csv')

        data_source.series_id_column_name = 'Calories'
        resp = self._eureqa.get_data_source_by_id(data_source._data_source_id)
        self.assertEqual(resp.series_id_column_name, 'Calories')
        self.assertEqual(resp.series_order_column_name, None)

        data_source.series_order_column_name = 'Date'
        resp = self._eureqa.get_data_source_by_id(data_source._data_source_id)
        self.assertEqual(resp.series_id_column_name, 'Calories')
        self.assertEqual(resp.series_order_column_name, 'Date')

        data_source.series_order_column_name = DataSource.EXISTING_ROW_ORDER
        resp = self._eureqa.get_data_source_by_id(data_source._data_source_id)
        self.assertEqual(resp.series_id_column_name, 'Calories')
        self.assertEqual(resp.series_order_column_name, DataSource.EXISTING_ROW_ORDER)

        data_source.series_id_column_name = ''
        data_source.series_order_column_name = ''
        resp = self._eureqa.get_data_source_by_id(data_source._data_source_id)
        self.assertEqual(resp.series_id_column_name, None)
        self.assertEqual(resp.series_order_column_name, None)

    def test_duplicate(self):
        new_datasource = self._data_source._duplicate('new name')
        self.assertEqual(new_datasource.name, 'new name')
        self.assertNotEqual(self._data_source._datasource_id, new_datasource._datasource_id)
        self.assertFalse(new_datasource._hidden)

        new_datasource = self._data_source._duplicate('new name 2', True)
        self.assertEqual(new_datasource.name, 'new name 2')
        self.assertNotEqual(self._data_source._datasource_id, new_datasource._datasource_id)
        self.assertTrue(new_datasource._hidden)

    def test_urlencode(self):
        # The following lines should not throw.
        # This test verifies that we correctly URL-encode
        # the '#' in the generated datasource ID.
        # If we don't, then the server will strip out '#hash.csv' from the
        # generated URL, and it won't be able to find a datasource with ID 'test'.
        self._eureqa.create_data_source("test#hash.csv", StringIO("a\n1\n"))
        self._eureqa.get_data_source_by_id("test#hash.csv")

    def test_url_encode_variable_id(self):
        # This tests depends on the database not containing data source with a particular ID
        # So it can create one.
        for datasource in self._eureqa.get_all_data_sources():
            if datasource._datasource_id != 'data_source_99': continue
            datasource.delete()
            break
        data_source = self._eureqa.create_data_source('data_source_99', 'Nutrition.csv')
        data_source.create_variable(4, '(work class_?)')
        # This call generates variable ID which contains '/' symbol.
        # Which should be properly URL encoded.
        # Otherwise the call fails.
        variable = data_source.get_variable_details('(work class_?)')
        import base64
        self.assertTrue('/' in base64.b64encode('%s-_-%s' % (data_source._datasource_id, '(work class_?)')))


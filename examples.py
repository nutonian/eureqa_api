from eureqa import *


def search_all_variables():
    # Creates a numeric search for each variable to find the best model that describes
    # its dependency from other variables.

    eureqa = Eureqa(url='https://rds.nutonian.com', user_name='bob@nutonian.com', password='password')
    data_source = eureqa.create_data_source('Sample_data_1', 'sample.csv')
    variables = set(data_source.get_variables())
    searches = []
    for variable in variables:
        settings = eureqa.search_templates.numeric('Test_5', variable, variables - {variable})
        search = data_source.create_search(settings)
        search.submit(10)
        searches.append(search)
    for search in searches:
        search.wait_until_done()
        best_solution = search.get_best_solution()
        print 'Best model for the %s variable is %s' % (best_solution.target, best_solution.model)


def create_custom_search():
    # Creates a search with a number of custom parameters.

    eureqa = Eureqa(url='https://rds.nutonian.com', user_name='bob@nutonian.com', password='password')
    data_source = eureqa.create_data_source('Sample_data_2', 'sample.csv')

    variables = set(data_source.get_variables())
    settings = eureqa.search_templates.numeric('Weight_model', 'Weight', variables - {'Weight'})
    settings.error_metric = error_metric.log_loss_error()
    settings.data_splitting.training_data_percentage = 80
    settings.data_splitting.validation_data_percentage = 20
    settings.math_blocks.append(math_block.and_op())

    search = data_source.create_search(settings)
    search.submit(10)
    search.wait_until_done()

    print 'Best model for Weight is: ' + search.get_best_solution().model

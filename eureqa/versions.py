# This value has to equal to the version of the REST API.
# It should be incremented every time REST API is changed in the way that
# can break this API binding.
# For example, if an end-point used by this API binding is changed.
# Or if a new endpoint is added and used by a new version of this API binding.
API_VERSION = 14

# This value has to equal the server version.
# It should be changed every release.
SERVER_VERSION = '1.76.0'

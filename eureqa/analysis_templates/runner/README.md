Contains code to invoke anaylsis templates, both from the Eureqa application
(via eeb) and via a command line launcher (for development)

For more information run: `python -m eureqa.analysis_templates.runner --help`

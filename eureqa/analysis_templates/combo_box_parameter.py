#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL NUTONIAN INC BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from parameter import Parameter

import json

class ComboBoxParameter(Parameter):
    """Combo box parameter description for analysis template

    :param str id: The id of the parameter that will be passed together with its value to an analysis script.
    :param str label: The parameter label that will be shown in UI.
    :param list[str] items: The items to populate the combo box with.

    :var str id: The id of the parameter that will be passed together with its value to an analysis script.
    :var str ComboBoxParameter.label: The parameter label that will be shown in UI.
    :var list[str] items: The items to populate the combo box with.
    """

    def __init__(self, id, label, items):
        """Initializes a new instance of the ~ComboBoxParameter class
        """
        Parameter.__init__(self, id, label, "combo_box")
        self.items = items

    def _to_json(self):
        body = Parameter._to_json(self)
        if self.items is not None and len(self.items) > 0:
            body['items'] = [x for x in self.items]
        return body

    def __str__(self):
        return json.dumps(self._to_json(), indent=4)

    @staticmethod
    def _from_json(body):
        ret = ComboBoxParameter(None, None, None)
        Parameter.from_json(ret, body)
        if ret._type != "combo_box": raise Exception("Invalid type '%s' specified for combo_box parameter" % ret._type)
        ret.variables = []
        if 'items' in body:
            ret.items = [x for x in body['items']]
        return ret

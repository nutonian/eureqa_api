The _JsonREST util class is very helpful for abstracting standard calls to the server.
It can be a little complicated to transition a class to use the utils, but once its done,
interacting with the class becomes very easy.

Here is a general guide to transition a class to use _JsonREST:


1. Add the following 3 methods:

def _directory_endpoint(self):
def _object_endpoint(self):
def _fields(self):

_directory_endpoint() should return a string of the URL where all of these objects can
be found. It will be used for server-side object creation and get_all() methods.
ex: DataSource's _directory_endpoint returns '/fxp/datasources'

_object_endpoint() should return a string of the URL where each individual object is held.
It will be used for fetching, updating, and deleting server-side objects.
ex: DataSource's _object_endpoint returns ('/fxp/datasources/%s' % self._datasource_id)

_fields() is a little bit complicated.
It should return a list of strings. This list should contain the names of all variables
needed for communication with the server. This includes both the variables needed to send
requests to the server and the variables that need to get populated by responses from the
server. The strings must exactly match the keys in the JSON being passed back and forth.
Do not include leading underscores in the variable names.
The strings in _fields() determine which variables the _JsonREST framework will work with.
If a variable is not included in _fields(), it will not be passed to or receive values
from the server. Furthermore, it will not be included in any _to_json() or _from_json()
calls.
The _JsonREST framework treats all variables listed as private variables. So, if you list
a variable 'sample_var', the actual variable that _JsonREST will create and act upon is
self._sample_var. This is the variable that must be changed for _JsonREST to send
'sample_var' to the server with the change, and the variable that will be set when
it receives a value whose key is 'sample_var'. If the variable needs to be public
or accessed by another name, create a property for it.

ex: DataSource's _fields returns
["datasource_id", "datasource_name", "row_count", "column_count", "file_name",
"file_size", "file_uploaded_user", "file_uploaded_date", "file_objstore_uri", "hidden"]

(there are other values returned from the server but they are not being used. If they
were, they would also be included)



2. Refactor the constructor:

This step may vary quite a bit from class to class depending on the current setup
of the constructor and the need to maintain backwards compatibility. In general:

A) You need to take in a Eureqa connection. If the constructor can take one directly,
great! If not, you'll need to get one from one of the other objects passed into 
the constructor. Either way, at some point in the constructor, call the super.__init__()
and pass in the Eureqa instance. 

B) The common signature of the constructors is to take a required eureqa connection
followed by all of the variables from _fields() that can be sent to the server with
a default value of None. Note that this means all variables that can be sent to the
server in any HTTP call, not just the creation. Then, in the body of the constructor,
each private variable is set if its argument is not None.

C) Any of the variables that need to be present can be given default values in the
constructor.

ex: Here is DataSource's new constructor
-----------------------------------------------------------------------------------
#B) New Signature- contains all variables that will be sent to server in create, get, etc.
def __init__(self, eureqa, datasource_name=None, hidden=False, datasource_id=None):

    #A) Set up Eureqa Connection
    super(DataSource, self).__init__(eureqa)

    #B) Set private variables if they are not None
    if datasource_id is not None:
        self._datasource_id = datasource_id
    if datasource_name is not None:
        self._datasource_name = datasource_name
    if hidden is not None:
        self._hidden = hidden

    #C) Set default values
    self._column_count = 0
    self._row_count = 0
    self._file_name = ''
    self._file_size = 0
    self._file_uploaded_user = ''
    self._file_uploaded_date = ''
    self._file_objstore_uri = ''
------------------------------------------------------------------------------------



3. Replace server interactions that use this class with the _JsonREST functions.

Create server-side object with current variable values:            self._create()
Change the values on the server-side object to the current values: self._update()
Gather/Refresh values on object from server-side values:           self._get_self()
Delete the object from the server:                                 self._delete()


Popular ways to interact with the newly refactored class:

Create Methods:
Construct an object, the call _create() to send it to the server.
ex:
create_data_source(eureqa, name, _hidden = False):
    data_source = DataSource(eureqa, datasource_name=name, hidden=_hidden)
    data_source._create()
    return data_source

Get Methods:
Call _get() on the class and pass in any information needed to get the object URL
ex:
get_data_source(eureqa, data_source_id):
    return DataSource._get(eureqa, datasource_id = data_source_id)

Get All Methods:
Call _get_all() on the class and pass in any information needed to get the directory URL
ex:
get_all_data_sources(eureqa):
    DataSource._get_all(eureqa)



Notes: 
Make sure the class does not have its own _to_json() method. It will overwrite
_JsonREST's _to_json() and prevent it from working properly.

cls._get() and cls._get_all() construct an object under the hood. If the constructor
has more required arguments, they will need to be passed into these methods as well.

self._create() and self._update() will automatically update the client-side object with
the server's response. In most (but not all) cases, it is unnecessary to call 
self._get_self() afterwords.

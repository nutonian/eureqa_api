#!/bin/bash -x
#
# This script packages the various eureqa client libraries into the
# appropriate files for distribution

# http://stackoverflow.com/questions/2870992/automatic-exit-from-bash-shell-script-on-error
# Any subsequent(*) commands which fail will cause the shell script to exit immediately
set -e

# Where binaries are installed if installed as a local user on a Mac, mac builder or Linux machine (respectively)
export PATH="$PATH:$HOME/Library/Python/2.7/bin:/opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin:$HOME/.local/bin"


(cd docs && make clean && make html)
rm -rf doc
mv docs/build/html doc

rm    -rf deploy
mkdir -p  deploy
mkdir -p  deploy/python

# package
zip -x *.pyc -r deploy/python/eureqa.zip  eureqa analysis_templates doc sample.csv examples.py

# package as official pip source package
# don't do this on Mac -- we don't use it, and Apple's setuptools is too old and is annoying to upgrade
if [ "$(uname)" != "Darwin" ]; then
	python setup.py sdist
fi

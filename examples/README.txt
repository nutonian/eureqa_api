To use these notebooks, run the following commands in a Terminal or Command Prompt from the directory containing your 'eureqa_api' checkout:

pip install jupyter  ## One-time command
export PYTHONPATH=$PWD/eureqa_api/python/eureqa  ## Or install eureqa systemwide, or otherwise 
cd eureqa_api/python/examples
jupyter notebook

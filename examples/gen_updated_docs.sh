#!/bin/bash

EXAMPLES_DIR="$PWD"
OUTPUT_DIR="$PWD/../docs/source/examples"

mkdir -p "$OUTPUT_DIR"
cd "$OUTPUT_DIR"
for IPYNB in $EXAMPLES_DIR/*.ipynb; do
	NAME="$(echo $(basename "$IPYNB") | sed 's/.ipynb//')"
	RST="$NAME.rst"
	HEADER_RST="$RST.tmp"
	jupyter nbconvert "$IPYNB" --to rst
	echo "$NAME" > "$HEADER_RST"
	echo "$NAME" | sed 's/./*/g' >> "$HEADER_RST"
	echo >> "$HEADER_RST"
	cat "$RST" >> "$HEADER_RST"
	mv "$HEADER_RST" "$RST"
done

CreateCustomSearch
******************


Create a search with customized search settings. Validate the model
against a test datasource and get actual and predicted values to be used
for additional analysis.

This datasource uses experimental data from a double pendulum. Variables
represent the x positions (x1 and x2), velocities (v1 and v2), and
accelerations (a1 and a2) of the two arms at different points in time.

Create a connection to Eureqa:

.. code:: python

    from eureqa import Eureqa
    from eureqa import error_metric

    e = Eureqa(url="https://rds.nutonian.com", user_name="user@nutonian.com",
               key="JA24SZI94AKV0EJAEVPK")

Create a data source:

.. code:: python

    data_source = e.create_data_source("Double Pendulum - Training Data",
                                       "double_pendulum_train.csv")


:download:`Download double_pendulum_train.csv<./double_pendulum_train.csv>`.


Initialize search settings with the "numeric" template. Target variable is "a2", the acceleration of the second arm of the double pendulum:

.. code:: python

    variables = set(data_source.get_variables())
    target_variable = "a2"
    settings = e.search_templates.numeric("Model a2", target_variable, variables - {target_variable, 't'})

The double pendulum is a physical system. Customize the search settings
to disable irrelevant building blocks like if-then-else, and to enable
those that are relevant like the trigonometric operators:

.. code:: python

    settings.math_blocks.const.complexity = 1
    settings.math_blocks.if_op.disable()  # disable if-then-else
    settings.math_blocks.less.disable()   # disable less
    settings.math_blocks.sin.enable(3)    # enable sine and set complexity to 3
    settings.math_blocks.cos.enable(3)    # enable cosine and set complexity to 3
    settings.error_metric = error_metric.mean_square_error()

Create a search and run for 30 seconds:

.. code:: python

    search = data_source.create_search(settings)
    search.submit(30)
    search.wait_until_done()

Get the best model, view the model and the error metrics:

.. code:: python

    solution = search.get_best_solution()
    print("The best model found is:\n %s = %s" % (solution.target, solution.model))


.. parsed-literal::

    The best model found is:
     a2 = -0.0239994769615275 - a1*cos(x2 - x1) - v1^2*sin(x2 - x1) - 9.81639183106058*sin(x2)


Get the model performance:

.. code:: python

    print("The %s value for this search is %.2f" % (solution.optimized_error_metric, solution.optimized_error_metric_value))


.. parsed-literal::

    The Mean Squared Error value for this search is 0.49


Evaluate the model against a test datasource withheld from Eureqa:

.. code:: python

    test_data_source = e.create_data_source("Double Pendulum - Test Data", "double_pendulum_test.csv")
    test_metrics = e.compute_error_metrics(test_data_source, target_variable, solution.model)
    test_mse = test_metrics.mean_square_error
    print("The %s value for the test data is %.2f" % (solution.optimized_error_metric, test_mse))


.. parsed-literal::

    The Mean Squared Error value for the test data is 1.34



:download:`Download double_pendulum_test.csv<./double_pendulum_test.csv>`.

Get the actual and predicted values for a test set and load into a DataFrame for future analysis:

.. code:: python

    predicted_and_actual = e.evaluate_expression(test_data_source, ["t", 'a2', solution.model])
    import pandas as pd
    df = pd.DataFrame(predicted_and_actual)
    df




.. raw:: html

    <div>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>-0.0239994769615275 - a1*cos(x2 - x1) - v1^2*sin(x2 - x1) - 9.81639183106058*sin(x2)</th>
          <th>a2</th>
          <th>t</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>-44.613217</td>
          <td>-42.80</td>
          <td>7.14</td>
        </tr>
        <tr>
          <th>1</th>
          <td>-45.778557</td>
          <td>-45.10</td>
          <td>7.14</td>
        </tr>
        <tr>
          <th>2</th>
          <td>-47.013552</td>
          <td>-47.30</td>
          <td>7.15</td>
        </tr>
        <tr>
          <th>3</th>
          <td>-51.071663</td>
          <td>-49.50</td>
          <td>7.15</td>
        </tr>
        <tr>
          <th>4</th>
          <td>-52.363975</td>
          <td>-51.70</td>
          <td>7.15</td>
        </tr>
        <tr>
          <th>5</th>
          <td>-53.608793</td>
          <td>-53.80</td>
          <td>7.15</td>
        </tr>
        <tr>
          <th>6</th>
          <td>-54.806116</td>
          <td>-55.80</td>
          <td>7.16</td>
        </tr>
        <tr>
          <th>7</th>
          <td>-58.057057</td>
          <td>-57.80</td>
          <td>7.16</td>
        </tr>
        <tr>
          <th>8</th>
          <td>-59.106151</td>
          <td>-59.50</td>
          <td>7.16</td>
        </tr>
        <tr>
          <th>9</th>
          <td>-59.895165</td>
          <td>-60.90</td>
          <td>7.16</td>
        </tr>
        <tr>
          <th>10</th>
          <td>-60.382955</td>
          <td>-61.80</td>
          <td>7.17</td>
        </tr>
        <tr>
          <th>11</th>
          <td>-61.704556</td>
          <td>-62.20</td>
          <td>7.17</td>
        </tr>
        <tr>
          <th>12</th>
          <td>-62.667552</td>
          <td>-62.20</td>
          <td>7.17</td>
        </tr>
        <tr>
          <th>13</th>
          <td>-62.072153</td>
          <td>-61.80</td>
          <td>7.17</td>
        </tr>
        <tr>
          <th>14</th>
          <td>-61.063694</td>
          <td>-60.80</td>
          <td>7.17</td>
        </tr>
        <tr>
          <th>15</th>
          <td>-58.765531</td>
          <td>-59.20</td>
          <td>7.18</td>
        </tr>
        <tr>
          <th>16</th>
          <td>-56.596001</td>
          <td>-56.90</td>
          <td>7.18</td>
        </tr>
        <tr>
          <th>17</th>
          <td>-53.955954</td>
          <td>-53.90</td>
          <td>7.18</td>
        </tr>
        <tr>
          <th>18</th>
          <td>-50.749857</td>
          <td>-50.10</td>
          <td>7.18</td>
        </tr>
        <tr>
          <th>19</th>
          <td>-47.745222</td>
          <td>-46.50</td>
          <td>7.18</td>
        </tr>
        <tr>
          <th>20</th>
          <td>-42.061859</td>
          <td>-42.30</td>
          <td>7.18</td>
        </tr>
        <tr>
          <th>21</th>
          <td>-38.071695</td>
          <td>-37.70</td>
          <td>7.19</td>
        </tr>
        <tr>
          <th>22</th>
          <td>-33.788941</td>
          <td>-32.60</td>
          <td>7.19</td>
        </tr>
        <tr>
          <th>23</th>
          <td>-27.042389</td>
          <td>-27.40</td>
          <td>7.19</td>
        </tr>
        <tr>
          <th>24</th>
          <td>-22.409397</td>
          <td>-22.00</td>
          <td>7.19</td>
        </tr>
        <tr>
          <th>25</th>
          <td>-13.933737</td>
          <td>-16.30</td>
          <td>7.19</td>
        </tr>
        <tr>
          <th>26</th>
          <td>-8.833737</td>
          <td>-10.30</td>
          <td>7.19</td>
        </tr>
        <tr>
          <th>27</th>
          <td>-3.433737</td>
          <td>-3.92</td>
          <td>7.19</td>
        </tr>
        <tr>
          <th>28</th>
          <td>2.096263</td>
          <td>2.55</td>
          <td>7.20</td>
        </tr>
        <tr>
          <th>29</th>
          <td>7.598263</td>
          <td>9.02</td>
          <td>7.20</td>
        </tr>
        <tr>
          <th>...</th>
          <td>...</td>
          <td>...</td>
          <td>...</td>
        </tr>
        <tr>
          <th>781</th>
          <td>-7.208298</td>
          <td>-7.15</td>
          <td>9.75</td>
        </tr>
        <tr>
          <th>782</th>
          <td>-6.753226</td>
          <td>-7.02</td>
          <td>9.76</td>
        </tr>
        <tr>
          <th>783</th>
          <td>-6.712980</td>
          <td>-6.89</td>
          <td>9.77</td>
        </tr>
        <tr>
          <th>784</th>
          <td>-6.672694</td>
          <td>-6.76</td>
          <td>9.77</td>
        </tr>
        <tr>
          <th>785</th>
          <td>-6.632427</td>
          <td>-6.64</td>
          <td>9.78</td>
        </tr>
        <tr>
          <th>786</th>
          <td>-6.589876</td>
          <td>-6.53</td>
          <td>9.79</td>
        </tr>
        <tr>
          <th>787</th>
          <td>-6.200674</td>
          <td>-6.42</td>
          <td>9.80</td>
        </tr>
        <tr>
          <th>788</th>
          <td>-6.175753</td>
          <td>-6.31</td>
          <td>9.81</td>
        </tr>
        <tr>
          <th>789</th>
          <td>-6.148107</td>
          <td>-6.21</td>
          <td>9.82</td>
        </tr>
        <tr>
          <th>790</th>
          <td>-6.117373</td>
          <td>-6.11</td>
          <td>9.82</td>
        </tr>
        <tr>
          <th>791</th>
          <td>-5.761961</td>
          <td>-6.01</td>
          <td>9.83</td>
        </tr>
        <tr>
          <th>792</th>
          <td>-5.742415</td>
          <td>-5.90</td>
          <td>9.84</td>
        </tr>
        <tr>
          <th>793</th>
          <td>-5.718649</td>
          <td>-5.80</td>
          <td>9.85</td>
        </tr>
        <tr>
          <th>794</th>
          <td>-5.688919</td>
          <td>-5.69</td>
          <td>9.86</td>
        </tr>
        <tr>
          <th>795</th>
          <td>-5.652192</td>
          <td>-5.58</td>
          <td>9.87</td>
        </tr>
        <tr>
          <th>796</th>
          <td>-5.329606</td>
          <td>-5.47</td>
          <td>9.88</td>
        </tr>
        <tr>
          <th>797</th>
          <td>-5.297914</td>
          <td>-5.35</td>
          <td>9.89</td>
        </tr>
        <tr>
          <th>798</th>
          <td>-5.257292</td>
          <td>-5.22</td>
          <td>9.89</td>
        </tr>
        <tr>
          <th>799</th>
          <td>-5.208666</td>
          <td>-5.09</td>
          <td>9.90</td>
        </tr>
        <tr>
          <th>800</th>
          <td>-5.151411</td>
          <td>-4.94</td>
          <td>9.91</td>
        </tr>
        <tr>
          <th>801</th>
          <td>-4.837154</td>
          <td>-4.79</td>
          <td>9.92</td>
        </tr>
        <tr>
          <th>802</th>
          <td>-4.777049</td>
          <td>-4.63</td>
          <td>9.93</td>
        </tr>
        <tr>
          <th>803</th>
          <td>-4.705135</td>
          <td>-4.45</td>
          <td>9.94</td>
        </tr>
        <tr>
          <th>804</th>
          <td>-4.625141</td>
          <td>-4.26</td>
          <td>9.95</td>
        </tr>
        <tr>
          <th>805</th>
          <td>-3.821506</td>
          <td>-4.06</td>
          <td>9.96</td>
        </tr>
        <tr>
          <th>806</th>
          <td>-3.706753</td>
          <td>-3.84</td>
          <td>9.97</td>
        </tr>
        <tr>
          <th>807</th>
          <td>-3.382900</td>
          <td>-3.64</td>
          <td>9.98</td>
        </tr>
        <tr>
          <th>808</th>
          <td>-3.284431</td>
          <td>-3.42</td>
          <td>9.98</td>
        </tr>
        <tr>
          <th>809</th>
          <td>-3.155859</td>
          <td>-3.19</td>
          <td>9.99</td>
        </tr>
        <tr>
          <th>810</th>
          <td>-3.020318</td>
          <td>-2.95</td>
          <td>10.00</td>
        </tr>
      </tbody>
    </table>
    <p>811 rows × 3 columns</p>
    </div>

SearchAutomation
****************


Automate a set of models and summarize model results. In this example
we'll test how well we can model each variable in a datasource based on the
other variables.

Create a connection to Eureqa:

.. code:: python

    from eureqa import Eureqa

    e = Eureqa(url="https://rds.nutonian.com", user_name="user@nutonian.com",
               key="JA24SZI94AKV0EJAEVPK")

Get the list of variables in the datasource:

.. code:: python

    data_source = e.create_data_source("Sales & Marketing", "sales_marketing.csv")
    variables = set(data_source.get_variables())
    variables




.. parsed-literal::

    {u'Avg_Temp',
     u'Days_Precipitation',
     u'Days_Rain',
     u'Days_Snow',
     u'Emails_Clicked',
     u'Emails_Opened',
     u'Emails_Sent',
     u'Flyer_Spend',
     u'Inches_Rain',
     u'Inches_Snow',
     u'Max_Temp',
     u'Min_Temp',
     u'Online_Ad_Spend',
     u'Sales',
     u'TV_Ad_Spend',
     u'Web_New_Visitors',
     u'Web_Returning_Visitors',
     u'Web_Total_Visits',
     u'Web_Unique_Visitors',
     u'Week'}




:download:`Download sales_marketing.csv<./sales_marketing.csv>`.

Create a search for each variable, modeling the variable as the target with all other variables as inputs:

.. code:: python

    searches = []
    for variable in variables:
        settings = e.search_templates.numeric("Model for variable: %s" % variable,
                                              variable, variables - {variable})
        search = data_source.create_search(settings)
        search.submit(2)
        searches.append(search)
    searches




.. parsed-literal::

    [<eureqa.search.Search instance at 0x00000000046A3E48>,
     <eureqa.search.Search instance at 0x00000000046A3848>,
     <eureqa.search.Search instance at 0x000000000472BDC8>,
     <eureqa.search.Search instance at 0x00000000046A39C8>,
     <eureqa.search.Search instance at 0x00000000046A3608>,
     <eureqa.search.Search instance at 0x0000000004748E48>,
     <eureqa.search.Search instance at 0x00000000046E6A48>,
     <eureqa.search.Search instance at 0x0000000004701D48>,
     <eureqa.search.Search instance at 0x0000000004748A08>,
     <eureqa.search.Search instance at 0x0000000004748A48>,
     <eureqa.search.Search instance at 0x0000000004701F48>,
     <eureqa.search.Search instance at 0x000000000480A748>,
     <eureqa.search.Search instance at 0x00000000047EA408>,
     <eureqa.search.Search instance at 0x000000000486AB08>,
     <eureqa.search.Search instance at 0x000000000480A588>,
     <eureqa.search.Search instance at 0x0000000004889A48>,
     <eureqa.search.Search instance at 0x000000000486A548>,
     <eureqa.search.Search instance at 0x00000000048FEF88>,
     <eureqa.search.Search instance at 0x0000000004911C88>,
     <eureqa.search.Search instance at 0x00000000048FE888>]



Wait for searches to complete. Print the best solution for each search
as they complete:

.. code:: python

    for search in searches:
        search.wait_until_done()
        best_solution = search.get_best_solution()
        print 'Best model for the %s variable is %s' % (best_solution.target, best_solution.model)


.. parsed-literal::

    Best model for the Week variable is 0.318191337724807 + 0.648759972160649*step(14.9834893404746 + 224.504952816115*Inches_Rain - 0.241807519877208*Emails_Clicked*Days_Snow)
    Best model for the Web_Unique_Visitors variable is 1.04484030337699 + 1.00003420535283*Web_New_Visitors + 0.999968858451333*Web_Returning_Visitors
    Best model for the Inches_Rain variable is 0.14180331189039 + 0.389285765629462*Days_Rain + 0.0124599517651205*Max_Temp*Days_Snow*if(Days_Rain, Days_Rain, Days_Rain)*less(Days_Rain^2, 34.9648397385641)
    Best model for the Web_Returning_Visitors variable is 1.00001733340471*Web_Unique_Visitors - 0.946614979140577 - 1.00001961497412*Web_New_Visitors
    Best model for the Days_Snow variable is Days_Precipitation - Days_Rain
    Best model for the Days_Precipitation variable is Days_Rain + Days_Snow
    Best model for the Emails_Opened variable is Week + 3.58602233572101*Emails_Clicked + 1.79569685179191*Days_Rain + 0.0192648881611015*Emails_Sent - Week^4*step(Week)*min(Week, Days_Rain) - 4.69556560921945*min(4.38383023636057, if(if(7.16927771874282, Week, Week), Emails_Clicked, Week) - 3.77684153751508)
    Best model for the Sales variable is 88547999.0622628 + 553925.992067077*Avg_Temp + 57999.4727128087*Emails_Clicked + 823.355005775147*Web_New_Visitors
    Best model for the Inches_Snow variable is Days_Snow
    Best model for the Online_Ad_Spend variable is 324756.430393052 + Emails_Opened*Emails_Clicked*less(124366.219370021, 8.85600038586988*Web_New_Visitors)
    Best model for the Emails_Sent variable is 4.88394640914473*Emails_Opened + 1.36338463476562*min(Emails_Opened, 341.67130181691) - 55.7686005330632
    Best model for the TV_Ad_Spend variable is 230229.215764672 + 11226.1289950602*Inches_Snow + 5367.92713954699*Max_Temp + 23.2465785097142*Emails_Sent - 3163.07071452257*Avg_Temp - 23.2465785097142*Week*Emails_Sent
    Best model for the Web_New_Visitors variable is 0.999992737715856*Web_Unique_Visitors - 0.979362480109558 - 0.999972707572729*Web_Returning_Visitors
    Best model for the Avg_Temp variable is 36.4457896586784 + 1.52605710485033*Max_Temp + 6.71485944864352*min(sqrt(Min_Temp), 6.77081966263542 + Week) - 15.5868326032906*sqrt(Max_Temp)
    Best model for the Emails_Clicked variable is 4.70332166064393 + 0.249648462804642*Emails_Opened - 0.270944462573689*Inches_Rain
    Best model for the Flyer_Spend variable is 121967.639533629 + 1.27247056501962*Web_New_Visitors*Week^2*less(Week + Days_Snow, 1.10835841958016)
    Best model for the Min_Temp variable is if(15.2531317411839, Avg_Temp, 15.2531317411839) - 7.48217571071768 - 15.5468176381711*less(79.036550144347, Avg_Temp)
    Best model for the Days_Rain variable is Days_Precipitation - Days_Snow
    Best model for the Web_Total_Visits variable is 15.6700031160911*Emails_Opened + 7.8982810405798*Web_New_Visitors + 7.15337755614677*Web_Unique_Visitors - 83604.6282562617 - 8.09421501760967e-5*Web_Unique_Visitors*Web_New_Visitors
    Best model for the Max_Temp variable is 8.63740515122003 + max(1 + 0.961215843860963*Avg_Temp, Min_Temp)


Plot the error metric values to show which variables are easiest and
hardest to predict:

.. code:: python

    %matplotlib inline
    import matplotlib.pyplot as plt

    error_metric = searches[0].error_metric
    solutions = [search.get_best_solution() for search in searches]

    plt.plot(range(len(solutions)), [s.get_error_metric_value(error_metric) for s in solutions])
    plt.title('Solution quality')
    plt.show()



.. image:: SearchAutomation_files/SearchAutomation_10_0.png

AnalysisCustomPlot
******************


Build two custom plots and add them to an analysis.

Create a connection to Eureqa:

.. code:: python

    from eureqa import Eureqa
    from eureqa.analysis_cards import Plot

    e = Eureqa(url="https://rds.nutonian.com", user_name="user@nutonian.com",
               key="JA24SZI94AKV0EJAEVPK")

    analysis = e.create_analysis('Custom Plot Analysis')

Create a plot with three components. The X and Y positions for these
components are specified directly using Python lists.

.. code:: python

    plot = Plot()
    plot.plot([1,2,3], [4,5,6], color='blue', line_width=3, legend_label='Label 1')
    plot.plot([11,22,33], [44,55,66], style='o', legend_label='Label 2',
              color='red', circle_radius=7)
    plot.plot([10,11,12], [13,14,15], line_width=5, color='orange')
    plot_card = analysis.create_custom_plot_card(plot, 'This card contains my custom plot with data from Python.')

Create a second plot with two components. The X and Y positions for
these components come from variables within a Eureqa data source. The
"<row>" variable is the row number from the data source.

.. code:: python

    datasource = e.create_data_source('Nutrition Data', 'nutrition.csv')
    plot2 = Plot(width='500px', height='500px', x_axis_label='x axis label', y_axis_label='y axis label')
    plot2.plot('<row>', 'Calories', datasource=datasource, style='o', line_width=3,
               circle_radius=5, color='blue', legend_label='Calories')
    plot2.plot('<row>', '30*(Fat (g))', datasource=datasource, style='-', line_width=3,
               circle_radius=5, color='red', legend_label='30*(Fat (g))')
    plot_card_2 = analysis.create_custom_plot_card(plot2, 'This plot contains data from my Nutrition datasource.')


:download:`Download nutrition.csv<./nutrition.csv>`.

Log into Eureqa and navigate to the Analysis area to view the custom plots.

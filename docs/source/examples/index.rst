.. _examples-page-label:

Examples
********

.. toctree::
   :maxdepth: 1
   :glob:

   CreateSimpleSearch.rst
   CreateCustomSearch.rst
   SearchAutomation.rst
   AnalysisCustomPlot.rst

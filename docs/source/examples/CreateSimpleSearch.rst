CreateSimpleSearch
******************


Build a model to forecast sales and build an analysis in Eureqa that
displays the results.

The example datasource includes weekly metrics for Sales along with
marketing spend in various channels, website activity, and weather data.

Create a connection to Eureqa:

.. code:: python

    from eureqa import Eureqa

    e = Eureqa(url="https://rds.nutonian.com", user_name="user@nutonian.com",
               key="JA24SZI94AKV0EJAEVPK")

Create a data source:

.. code:: python

    data_source = e.create_data_source("Sales & Marketing", "sales_marketing.csv")




.. parsed-literal::

    <eureqa.data_source.DataSource instance at 0x00000000042F24C8>




:download:`Download sales_marketing.csv<./sales_marketing.csv>`.


Create a search. The goal is to forecast Sales using all other variables in the datasource. The time series template default settings will work well for this search:

.. code:: python

    variables = set(data_source.get_variables())
    target_variable = "Sales"
    settings = e.search_templates.time_series("Sales Forecast", target_variable,
                                              variables - {target_variable})
    search = data_source.create_search(settings)

Submit the search and wait until the search is done:

.. code:: python

    search.submit(30)
    search.wait_until_done()

Get the best solution. Eureqa identifies the solution that gives the
best trade-off between error and complexity:

.. code:: python

    solution = search.get_best_solution()
    print("The best model found is:\n %s = %s" % (solution.target, solution.model))


.. parsed-literal::

    The best model found is:
     Sales = 109569817.15572 + 1055.11588490586*delay(Emails_Sent, 1) + 1.0544595112534*wma(delay(Avg_Temp, 1), 2)*max(wma(delay(Web_Total_Visits, 1), 3), delay(Online_Ad_Spend, 1))


Get the model performance:

.. code:: python

    print("The %s value for this search is %.2f" % (solution.optimized_error_metric,
                                                    solution.optimized_error_metric_value))


.. parsed-literal::

    The R^2 Goodness of Fit value for this search is 0.79


Create an analysis that summarizes the results:

.. code:: python

    analysis = e.create_analysis("Sales Forecasting Model",
                                 "This model forecasts sales for next week.")
    analysis.create_model_summary_card(solution)
    analysis.create_model_fit_by_row_plot_card(solution, title="Model Fit with Sales Forecast",
                       description="View the predicted value for next week by positioning
                                               your mouse over the yellow predicted value.")
    analysis.create_model_card(solution, title="Model Details",
                     description="Use the interactive model mode to see how predicted Sales
                               changes as the model inputs change.")




.. parsed-literal::

    <eureqa.analysis_cards.model_card.ModelCard at 0x4629668>



.. code:: python

    Log into Eureqa to view and interact with the resulting analysis!

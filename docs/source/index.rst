Welcome to the Eureqa Python API!
*********************************

The Eureqa Python API provides a way to automate the entire Eureqa modeling process.  Use it to:

* Connect to a data platform
* Automate model creation
* Generate summary reports
* Integrate with other modeling tools
* Deploy production models
* and much more!

The Python API integrates seamlessly with your Eureqa SaaS or Eureqa On Prem environment -- access data, models, and analyses built via the API in the Eureqa UI and vice versa.

Please click on one of the links below or in the sidebar for more information.


.. toctree::
   :maxdepth: 2

   getting_started.rst
   examples/index.rst
   api.rst


.. _api-page-label:

====================
Eureqa API Reference
====================

.. toctree::
    :maxdepth: 2



analysis
--------

.. automodule:: eureqa.analysis
    :members:

analysis_file
^^^^^^^^^^^^^

.. automodule:: eureqa.analysis.analysis_file
    :members:

cards
^^^^^

.. automodule:: eureqa.analysis.cards
    :members:

components
^^^^^^^^^^

.. automodule:: eureqa.analysis.components
    :members:

base
""""

.. automodule:: eureqa.analysis.components.base
    :members:

binned_mean_plot
""""""""""""""""

.. automodule:: eureqa.analysis.components.binned_mean_plot
    :members:

box_plot
""""""""

.. automodule:: eureqa.analysis.components.box_plot
    :members:

by_row_plot
"""""""""""

.. automodule:: eureqa.analysis.components.by_row_plot
    :members:

custom_plot
"""""""""""

.. automodule:: eureqa.analysis.components.custom_plot
    :members:

distribution_plot
"""""""""""""""""

.. automodule:: eureqa.analysis.components.distribution_plot
    :members:

double_histogram_plot
"""""""""""""""""""""

.. automodule:: eureqa.analysis.components.double_histogram_plot
    :members:

download_file
"""""""""""""

.. automodule:: eureqa.analysis.components.download_file
    :members:

dropdown_layout
"""""""""""""""

.. automodule:: eureqa.analysis.components.dropdown_layout
    :members:

formatted_text
""""""""""""""

.. automodule:: eureqa.analysis.components.formatted_text
    :members:

html_block
""""""""""

.. automodule:: eureqa.analysis.components.html_block
    :members:

image
"""""

.. automodule:: eureqa.analysis.components.image
    :members:

layout
""""""

.. automodule:: eureqa.analysis.components.layout
    :members:

magnitude_bar
"""""""""""""

.. automodule:: eureqa.analysis.components.magnitude_bar
    :members:

modal
"""""

.. automodule:: eureqa.analysis.components.modal
    :members:

modal_link
""""""""""

.. automodule:: eureqa.analysis.components.modal_link
    :members:

model
"""""

.. automodule:: eureqa.analysis.components.model
    :members:

model_evaluator
"""""""""""""""

.. automodule:: eureqa.analysis.components.model_evaluator
    :members:

model_fit_by_row_plot
"""""""""""""""""""""

.. automodule:: eureqa.analysis.components.model_fit_by_row_plot
    :members:

model_fit_separation_plot
"""""""""""""""""""""""""

.. automodule:: eureqa.analysis.components.model_fit_separation_plot
    :members:

model_summary
"""""""""""""

.. automodule:: eureqa.analysis.components.model_summary
    :members:

model_terms_plot
""""""""""""""""

.. automodule:: eureqa.analysis.components.model_terms_plot
    :members:

most_frequent_variables_plot
""""""""""""""""""""""""""""

.. automodule:: eureqa.analysis.components.most_frequent_variables_plot
    :members:

scatter_plot
""""""""""""

.. automodule:: eureqa.analysis.components.scatter_plot
    :members:

search_builder_link
"""""""""""""""""""

.. automodule:: eureqa.analysis.components.search_builder_link
    :members:

search_link
"""""""""""

.. automodule:: eureqa.analysis.components.search_link
    :members:

tabbed_layout
"""""""""""""

.. automodule:: eureqa.analysis.components.tabbed_layout
    :members:

table
"""""

.. automodule:: eureqa.analysis.components.table
    :members:

table_builder
"""""""""""""

.. automodule:: eureqa.analysis.components.table_builder
    :members:

table_column
""""""""""""

.. automodule:: eureqa.analysis.components.table_column
    :members:

text_block
""""""""""

.. automodule:: eureqa.analysis.components.text_block
    :members:

threshold_selection_plot
""""""""""""""""""""""""

.. automodule:: eureqa.analysis.components.threshold_selection_plot
    :members:

titled_layout
"""""""""""""

.. automodule:: eureqa.analysis.components.titled_layout
    :members:

tooltip
"""""""

.. automodule:: eureqa.analysis.components.tooltip
    :members:

variable_link
"""""""""""""

.. automodule:: eureqa.analysis.components.variable_link
    :members:

analysis_templates
------------------

.. automodule:: eureqa.analysis_templates
    :members:

analysis_template
^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.analysis_template
    :members:

combo_box_parameter
^^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.combo_box_parameter
    :members:

combo_box_parameter_value
^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.combo_box_parameter_value
    :members:

data_file_parameter
^^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.data_file_parameter
    :members:

data_file_parameter_value
^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.data_file_parameter_value
    :members:

data_source_parameter
^^^^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.data_source_parameter
    :members:

data_source_parameter_value
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.data_source_parameter_value
    :members:

execution
^^^^^^^^^

.. automodule:: eureqa.analysis_templates.execution
    :members:

numeric_parameter
^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.numeric_parameter
    :members:

numeric_parameter_value
^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.numeric_parameter_value
    :members:

parameter
^^^^^^^^^

.. automodule:: eureqa.analysis_templates.parameter
    :members:

parameters
^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.parameters
    :members:

parameters_values
^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.parameters_values
    :members:

parameter_validation_result
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.parameter_validation_result
    :members:

parameter_value
^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.parameter_value
    :members:

progress_update
^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.progress_update
    :members:

runner
^^^^^^

.. automodule:: eureqa.analysis_templates.runner
    :members:

analysis_template_runner
""""""""""""""""""""""""

.. automodule:: eureqa.analysis_templates.runner.analysis_template_runner
    :members:

client
""""""

.. automodule:: eureqa.analysis_templates.runner.client
    :members:

text_parameter
^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.text_parameter
    :members:

text_parameter_value
^^^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.text_parameter_value
    :members:

top_level_model_parameter
^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.top_level_model_parameter
    :members:

top_level_model_parameter_value
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.top_level_model_parameter_value
    :members:

variable_parameter
^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.variable_parameter
    :members:

variable_parameter_value
^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: eureqa.analysis_templates.variable_parameter_value
    :members:

apply_solution_result
---------------------

.. automodule:: eureqa.apply_solution_result
    :members:

data_source
-----------

.. automodule:: eureqa.data_source
    :members:

data_splitting
--------------

.. automodule:: eureqa.data_splitting
    :members:

error_metric
------------

.. automodule:: eureqa.error_metric
    :members:

eureqa
------

.. automodule:: eureqa.eureqa
    :members:

html
----

.. automodule:: eureqa.html
    :members:

button
^^^^^^

.. automodule:: eureqa.html.button
    :members:

math_block
----------

.. automodule:: eureqa.math_block
    :members:

math_block_set
--------------

.. automodule:: eureqa.math_block_set
    :members:

model_evaluation
----------------

.. automodule:: eureqa.model_evaluation
    :members:

search
------

.. automodule:: eureqa.search
    :members:

search_settings
---------------

.. automodule:: eureqa.search_settings
    :members:

search_templates
----------------

.. automodule:: eureqa.search_templates
    :members:

solution
--------

.. automodule:: eureqa.solution
    :members:

variable_details
----------------

.. automodule:: eureqa.variable_details
    :members:

variable_options
----------------

.. automodule:: eureqa.variable_options
    :members:

variable_options_dict
---------------------

.. automodule:: eureqa.variable_options_dict
    :members:

versions
--------

.. automodule:: eureqa.versions
    :members:


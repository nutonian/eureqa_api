===============
Getting Started
===============

It is very easy to become productive with the Nutonian Python API - just ensure that you have the prerequisite Python version and Eureqa libraries installed, obtain an access key, and then connect.  Details are below.

Installation
============

**Prerequisites**

*Verify Python Installation*

The Eureqa API requires Python 2.x version 2.7.11 or later. It is not tested with Python 3.x. Before getting started, ensure you have the correct version of python installed.

To verify your Python version in your terminal::

   python --version

If Python is not installed, you can download it here:
https://www.python.org.downloads

*Install or Verify Pip*

The Eureqa Python API is installed using *pip*.  If you don’t have pip, follow the installation instructions here:
https://pip.pypa.io/en/stable/installing/

**Eureqa API Installation**

*Installing the Eureqa API*

To install the Eureqa API, use your terminal to run::

   pip install eureqa[==version]

You can find the correct version by opening Eureqa and going to Settings > Eureqa API

*Upgrading or Downgrading the Eureqa API*

To upgrade or downgrade the API, install again with the version specified, e.g.::

   pip install eureqa[==version]

**Note:** pip does have "-upgrade" flag which will upgrade to the latest version.   We recommend *not* using this flag and instead using the command above to install the specific version that you need.

*Uninstalling the Eureqa API*

To uninstall the Eureqa API::

   pip uninstall eureqa

Connecting to Eureqa
====================

To get started with the API, connect to Eureqa as follows:

*Eureqa SaaS*::

   from eureqa import Eureqa

   e = Eureqa(url='Eureqa URL', user_name='user name', key='access key')

Where:

* 'Eureqa URL' is the base URL used to connect to Eureqa (e.g. “https://rds.nutonian.com”)
* 'user name' is your Eureqa user name (e.g. ”bob@nutonian.com”)
* 'access key' is your Eureqa API access key (see below)

*Eureqa OnPrem*::

   from eureqa import Eureqa

   e = Eureqa(url='Eureqa URL', user_name='user name', key='access key', verify_ssl_certificate=False)

Where:

* 'Eureqa URL' is the base URL used to connect to your local installation of Eureqa (e.g. “https://eureqa.company.com:10002”)
* 'user name' is your Eureqa user name (e.g. ”bob@nutonian.com”)
* 'access key' is your Eureqa API access key (see below)
* 'verify_ssl_certificates=False' is needed if your local Eureqa installation hasn't been configured with SSL certificates


**Access Keys**

The Eureqa Python API uses key-based authentication.  To generate a license key for your user account, open Eureqa and navigate to Settings > Python API and select the option to “Get access key”

.. image:: _static/get_access_key.png

Edit the Key name / description if desired and then select Generate Key.  Copy and paste the access key into your code -- the key will only be shown once!

.. image:: _static/access_key.png

You may have up to 2 active keys a time.  You can delete an existing key and generate a new key at any time.




Next Steps
==========

Now that you have the Eureqa API installed and have successfully connected to Eureqa, get started uploading data, building models, generating reports and more.

Check out the :ref:`examples-page-label` for sample code and the :ref:`api-page-label` for details on specific classes and functions.

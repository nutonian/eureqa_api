#!/usr/bin/env python

import sys, os
if len(sys.argv) <= 1:
    print >>sys.stderr, "Usage: python gen_index.py <path-to-scan>"
    sys.exit(0)

TARGET_PATH = sys.argv[1]
sys.path.insert(0, os.path.abspath(TARGET_PATH))
os.chdir(TARGET_PATH)

import pkgutil
import inspect
import importlib

from collections import defaultdict
from pprint import pprint

# If import process generates output,
# send it to stderr so it doesn't wind up in our output file
orig_stdout = sys.stdout
sys.stdout = sys.stderr

# modules and patterns to exclude from documentation completely
# this is used with a 'startswith()' so be aware when adding
# to make sure it is specific enough
exclusion_list = ['_',
                  'eureqa._',       # picks up __main__ and eureqa._
                  'eureqa.utils',   # hide all utils for now
                  'eureqa.session',
                  'eureqa.passwords',
                  'eureqa.api_version',
                  'eureqa.organization',
                  'eureqa.analysis_cards',
                  'eureqa.complexity_weights',
                  'eureqa.run_analysis_template',
                  'eureqa.install_analysis_template',
                  'eureqa.missing_value_policies']

# If class X is defined in file A, and file B does "from A import X",
# then any other file can do either "from A import X" or "from B import X"
# to get at X.
# (Unless B defines the magic global "__all__" to exclude X.)
# This dictionary is constructed to be a mapping of, in essence,
# "{<class X>: {str('A.X'), str('B.X')}}"
# for all classes that are discovered by recursively introspecting on
# the module at TARGET_PATH, including all sub-modules.
PKG_NAMES=set()

def update_packages(pkgpath):
    for pkg, name, ispkg in pkgutil.walk_packages([pkgpath], pkgpath+'.'):
        if name.endswith(".__main__"):
            # Skip executable modules; importing them may try to run them instead
            continue
        try:
            module = importlib.import_module(name)
        except:
            # TODO:  There's a weird issue right now importing
            # the analysis template runner lib.
            # It tries to use the wrong import path.  I don't know why.
            # Oh well.  We don't really want to document that anyway.
            print >>sys.stderr, "Can't import module '%s'" % name
        else:
            PKG_NAMES.add(name)
            update_packages(name)

update_packages("eureqa")

sys.stdout = orig_stdout

## OUTPUT
page_header = """\
.. _api-page-label:

====================
Eureqa API Reference
====================

.. toctree::
    :maxdepth: 2


"""

print page_header

HEADER_DEPTH = ['=', '-', '^', '"', '"', '"', '"']

for module in sorted(PKG_NAMES, key=str.upper):
    if any([module.startswith(ex) for ex in exclusion_list]):
        continue  ## Don't document internal or otherwise hidden modules

    last_name = module.split('.')[-1]
    print last_name
    print HEADER_DEPTH[module.count('.')] * len(last_name)
    print
    print ".. automodule:: %s" % module
    print "    :members:"
    print

REM Batchfile to create client packages for windows
@echo on

REM Make sure environment is set up
SET PATH=%PATH%;C:\Python27;C:\Python27\Scripts

REM Make sure our build dependencies are installed
pip install sphinx
pip install request
pip install sphinx_rtd_theme

mkdir deploy
mkdir deploy\python

cd docs
cmd /c make.bat html

cd ..
rmdir /Q /S doc
move /Y docs/build/html doc

REM Package the actual python files....

wbin\zip.exe -x *.pyc -r deploy\python\eureqa.zip eureqa analysis_templates doc sample.csv examples.py

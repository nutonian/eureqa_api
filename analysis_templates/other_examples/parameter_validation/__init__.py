import sys, os

from eureqa import *
from eureqa.analysis_templates import *
from eureqa.utils.modulefiles import open_module_file

def validate_parameters(eureqa, template_execution):
    template_execution.update_progress('Beginning parameter validation')
    template_execution.report_validation_result(type='INFO', message='This is an informational message')

    the_value = template_execution.parameters['abc'].value
    if the_value != '42':
        template_execution.report_validation_result(type='ERROR', message='The value was not 42')
    else:
        template_execution.report_validation_result(type='WARNING', message='This is an example warning message')


## This function is required for the analysis template.
## Modify it to create any analyses that you need.
def create_analysis(eureqa, template_execution):
    template_execution.update_progress('Starting running analysis template.')
    analysis = template_execution.get_analysis()
    analysis.name = 'Parameter Validation Result'
    template_execution.update_progress('Done!')


## This section is not required for an analysis template,
## but can be a convenience for developers.
## It causes the module to be runnable; and when run, to
## behave as if you had invoked run_analysis_template.py to
## run the template.
if __name__ == "__main__":
    from run_analysis_template import main

    ## By default, take any arguments from the command line.
    args = list(sys.argv)
    ## Alternatively, if you want to construct the command line from scratch:
    #args = ["run_analysis_template.py"]

    ## Construct a Parameters object representing the template's parameters.
    ## These can be difficult to construct at the command line;
    ## constructing them in Python can be easier.
    import json
    parameters = ParametersValues(
        [TextParameterValue('abc','32.')]
    )
    args += ["--parameters", json.dumps(parameters._to_json())]

    ## The one required argument to run_analysis_template.py
    ## is the path to ourselves.
    ## Python knows that, so add it automatically.
    args += [os.path.abspath(os.path.curdir)]

    main(args)

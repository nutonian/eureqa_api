An analysis template is defined as a series of python modules which is
invoked by the Eureqa server.

One module is designated as the "main" module. When an analysis
template is run, Eureqa attempts to invoke well known entrypoints in
the main module at various parts of the template execution.

create_analysis(eureqa, template_execution):
    # This method is responsible for implementing the logic of the analysis template
    # given a set of supplied parameter values
    pass

validate_parameters(eureqa, template_execution):
    # If this method is defined in the main module, it can provide validation_results for each
    # of the supplied parameter values
    pass

The minimal structure of a main module is as follows:

    module_dir
       |- __init__.py (define create_analysis, etc. here)
       |- (other source files, if any)
       |- (data files, if any)
       \- (sub-directories, if any)

__init__.py contains:
    def create_analysis(eureqa, template_execution):
        (...)

To create an analysis template, simply implement the
"create_analysis()" function within the __init__.py file of the module_dir.

"install_analysis_template.py" gathers all files located under "module_dir",
including (recursively) subdirectories and their contents (but excluding the
'eureqa_config.json' configuration file for security reasons), combines
them into a .zip file, and uploads this file to the Eureqa server.  The
server imports your code directly from the .zip file.  See the included
example for how to read data files out of the .zip file.

In Python terms, the .zip file is treated as a compressed Python module.
See Python's documentation regarding modules in order to construct
more-sophisticated packages.
